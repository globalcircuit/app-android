package com.gc.coregestionclientes;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gc.coregestionclientes.objetos.AutocompletarCampo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Vector;

public class ActividadListaCampo extends ListActivity {
	
	// Objetos
	private AutocompletarCampo autocompletarCampo;
	private HashMap<String, String> datosAdicionales;
	
	// Elementos Gráficos
	private TextView titulo_lista_campo = null;
	private EditText campo_filtro = null;
	private Button botonAsignar = null;
	
	// Variables auxiliares
	private CustomAdapterItems adapter = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.actividad_lista_campo);
		
		// Obtiene los datos del intent
		Bundle extras = getIntent().getExtras();
		autocompletarCampo = extras.getParcelable("autocompletarCampo");
		datosAdicionales = (HashMap<String, String>) extras.getSerializable("datosAdicionales");
				
		// Inicializa elementos gráficos
		//titulo_lista_campo = (TextView) findViewById(R.id.titulo_lista_campo);
		//titulo_lista_campo.setText(autocompletarCampo.getNombreCampo());
		setTitle(autocompletarCampo.getNombreCampo());
		campo_filtro = (EditText) findViewById(R.id.campo_filtro);
		campo_filtro.addTextChangedListener(filterTextWatcher);
		botonAsignar = (Button)findViewById(R.id.botonAsignar);
		botonAsignar.setOnClickListener(clickBoton);
		
		// Inicializa la lista de items
		inicializarLista();
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		// Extrae el objeto seleccionado y finaliza la actividad retornando el Objeto
		@SuppressWarnings("unchecked")
		HashMap<String, String> item = (HashMap<String, String>) this.getListAdapter().getItem(position);
		Intent resultIntent = new Intent();
		resultIntent.putExtra("idCampo", autocompletarCampo.getIdCampo());
		// TODO:
		String nombreCampo = autocompletarCampo.obtenerNombreCampoFiltro(ActividadListaCampo.this);
		resultIntent.putExtra("valor", item.get(nombreCampo == null ? autocompletarCampo.getNombreCampo() : nombreCampo));
		setResult(RESULT_OK, resultIntent);
    	finish();		
	}
	
	// Inicializa la lista de elementos
	private void inicializarLista() {
		Vector<LinkedHashMap<String, String>> items = new Vector<LinkedHashMap<String, String>>();
		
		// Consulta los objetos
		items = autocompletarCampo.consultarItems(this, null, datosAdicionales);
			
		if (items != null && items.size() > 0) {
			// Inicializa el adapter con los items
	        adapter = new CustomAdapterItems(this, items);
	        setListAdapter(adapter);
		}
	}

	// ?? TODO
	public OnClickListener clickBoton = new OnClickListener() {
		@Override
		public void onClick(View v) {
			String valor = campo_filtro.getText().toString();
			Intent resultIntent = new Intent();
			resultIntent.putExtra("idCampo", autocompletarCampo.getIdCampo());
			resultIntent.putExtra("valor", valor);
			setResult(RESULT_OK, resultIntent);
	    	finish();
		}
	};
	
	// TextWatcher para filtrar los items
	private TextWatcher filterTextWatcher = new TextWatcher() {
		@Override
		public void afterTextChanged(Editable s) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		// Permite filtrar el TextView a medida que va cambiando
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if(adapter != null){
				adapter.getFilter().filter(s);
			}
		}
	};
	
	// Clase customizada para la visualizacion de los items
	private class CustomAdapterItems extends BaseAdapter implements Filterable {
		private Activity activity;
		private Vector<LinkedHashMap<String, String>> list;
		private LinkedHashMap<String, String> item;
		private LayoutInflater inflater = null;
		protected Vector<LinkedHashMap<String, String>> original;

		// Constructor
		public CustomAdapterItems(Activity activity, Vector<LinkedHashMap<String, String>> list) {
			this.activity = activity;
			this.list = list;
			this.inflater = (LayoutInflater) this.activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		// Retorna el número de items en el vector
		public int getCount() {
			return (list != null ? list.size() : 0);
		}

		// Permite obtener el objeto en la posición
		public Object getItem(int position) {
			item = list.get(position);
			return item;
		}

		// Permite obtener el estilo de layout previamente definido y coloca los
		// valores de la fila que serán mostrados
		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;
			if (convertView == null)
				vi = inflater.inflate(R.layout.row_consulta_elementos, parent, false);

			// Obtiene los view donde será colocada la información
			LinearLayout row_rsmen_cntndor_infrmcion = (LinearLayout) vi.findViewById(R.id.row_rsmen_cntndor_infrmcion);
			row_rsmen_cntndor_infrmcion.removeAllViews();

			// Obtiene los datos en la posición dada
			LinkedHashMap<String, String> item = list.get(position);

			// Recorre los campos del item
			// Recorre el HashMap
			Iterator<?> it = item.entrySet().iterator();
			int indice = 0;
			while (it.hasNext()) {
				@SuppressWarnings("rawtypes")
				LinkedHashMap.Entry atributo = (LinkedHashMap.Entry)it.next();
				
				// Extrae los datos
				String nombreCampo = (String) atributo.getKey();	
				String valor = (String) atributo.getValue();

				// Setea los valroes
				TextView textView = new TextView(ActividadListaCampo.this);
				textView.setText(nombreCampo + ": " + valor);
				
				// Si es el primer TextView lo coloca en BOLD
				if (indice==0) {
					textView.setTextAppearance(ActividadListaCampo.this, R.style.titulo_etiqueta_claro);
				} else {
					textView.setTextAppearance(ActividadListaCampo.this, R.style.valor_etiqueta_claro);
				}
				
				// Adiciona a la fila
				row_rsmen_cntndor_infrmcion.addView(textView);

				// Incrementa contador
				indice++;
			}
			return vi;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public Filter getFilter() {
			return new Filter() {

				@Override
				protected FilterResults performFiltering(CharSequence constraint) {
					final FilterResults oReturn = new FilterResults();
					final Vector<LinkedHashMap<String, String>> results = new Vector<LinkedHashMap<String, String>>();
					if (original == null){
						original = list;
					}
					
					// Recorre la lista original y dependiendo del filtro aplica el criterio
					if (original != null && original.size() > 0) {
						for (LinkedHashMap<String, String> item : original) {
							String atributo = null;
							
							// Dependiendo del formulario extrae el campo sobre el cual se realiza la búsqueda
							String nombreCampo = autocompletarCampo.obtenerNombreCampoFiltro(ActividadListaCampo.this);
							atributo = item.get(nombreCampo == null ? autocompletarCampo.getNombreCampo() : nombreCampo)
									.toLowerCase(Locale.getDefault());							

							// Verifica que el atributo sea válido
							if (atributo != null && atributo.contains(
									constraint.toString().toLowerCase(Locale.getDefault()))) {
								results.add(item);
							}
						}
					}
				
					oReturn.values = results;
					return oReturn;
				}

				@SuppressWarnings("unchecked")
				@Override
				protected void publishResults(CharSequence constraint,
						FilterResults results) {
					list = (Vector<LinkedHashMap<String, String>>) results.values;
					notifyDataSetChanged();
				}
			};
		}
	}
}