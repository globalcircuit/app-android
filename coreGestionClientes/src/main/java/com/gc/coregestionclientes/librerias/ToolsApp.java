package com.gc.coregestionclientes.librerias;

import java.io.File;
import java.io.FileWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Environment;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ToolsApp {

	public ToolsApp() {}
	
	/** Normaliza un String SQL*/
	public String normalizarStringSQLite(String string){
		if(string == null){
			return "";
		} else {
			String sql_nrmlzdo = string.replace("'", "''");
			return sql_nrmlzdo;
		}
	}
	
	/*
    public int nivelBateria () 
    { 
        try 
        { 
           IntentFilter batIntentFilter = 
              new IntentFilter(Intent.ACTION_BATTERY_CHANGED); 
            Intent battery = 
               this.registerReceiver(null, batIntentFilter); 
            int nivelBateria = battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1); 
            return nivelBateria; 
        } 
        catch (Exception e) 
        {            
           Toast.makeText(getApplicationContext(), 
                    "Error al obtener estado de la batería", 
                    Toast.LENGTH_SHORT).show(); 
           return 0; 
        }        
    } */
	
	/** Retorna la fecha actual en formato AAAA-MM-DD HH:MM */
	public String getDateTimeNOW(){
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
		String fcha_hra_now = sdf.format(new Date());
		return fcha_hra_now;
	}
	
	/** Obtiene un String de la fecha actual AAAA-MM-DD */
	public String getDateNOW(){
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
		String fcha_hra_now = sdf.format(new Date());
		return fcha_hra_now;
	}
	
	/** Obtiene un long de la fecha actual AAAA-MM-DD */
	public long getUnixDateNOW(boolean incluirHora, int offsetDias, int offsetHoras, int offsetMinutos){
		// Crea un objeto calendar para obtener los milisegundos de la fecha actual (sin tiempo)
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        
        // Halla los valores de las fecha en milisegundos
        Calendar calendar;
        if(incluirHora) {
        	calendar = new GregorianCalendar(year, month, day, (hour + offsetHoras), (minute + offsetMinutos));
        } else {
    		calendar = new GregorianCalendar(year, month, day);
        }
		long fcha_ms = calendar.getTimeInMillis() + (86400000 * offsetDias);
        
		// Retorno
		return fcha_ms;
	}
	
	/** Obtiene un long de tiempo actual HH:MM:SS */
	public long getUnixTimeNOW(int offsetHoras, int offsetMinutos){
		// Crea un objeto calendar para obtener los milisegundos de la Hora actual (Sólo tiempo)
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Calcula los milisegundos
        long milisegundos = ((hour + offsetHoras) * 60 + (minute + offsetMinutos)) * 60000;

        // Retorno
		return milisegundos;
	}
	
	/** Convierte un String a un objeto Date dependiendo*/
	/*public Date stringToDate(String date_string){
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
		Date fcha = null;
		try {
			fcha = sdf.parse(date_string);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return fcha;
	}*/
	
	/** Convierte un String de fecha hora a milisegundos*/
	public long stringToUnixDate(String date_string, boolean incluir_hra){
		SimpleDateFormat sdf = null;
		if(incluir_hra){
			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
		} else {
			sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
		}
	    long unixDate = 0;
		try {
			Date fcha = sdf.parse(date_string);
			unixDate = fcha.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return unixDate;
	}
	
	/** Convierte un objeto Date a String dependiendo del modo */
	/*public String dateToString(Date date, int mdo){
		SimpleDateFormat sdf = null;
		switch (mdo) {
			case 1:
			    sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
				break;
			case 2:
			    sdf = new SimpleDateFormat("EEEE dd 'de' MMMM 'de' yyyy", Locale.getDefault());
				break;
			case 3:
			    sdf = new SimpleDateFormat("dd '-' MMM '-' yyyy", Locale.getDefault());
				break;
		}	
	    String fcha = sdf.format(date);
		return fcha;
	}*/
	
	/** Convierte un long en milisegundos a String dependiendo del modo */
	public String unixDateToString(long mlscnds, int mdo, boolean mstrar_hra){
		SimpleDateFormat sdf = null;
		
		// Verifica si se quiere mostrar la hora
		if(mstrar_hra){
			// Se quiere mostrar la hora,
			// Verifica que tenga algún valor (>0)
			mstrar_hra = (obtenerHoraMinutosSegundos(mlscnds) > 0 ? true : false);
		}
		
		// Dependiendo del modo, selecciona el formato de la fecha.
		switch (mdo) {
			case 1:
				if(mstrar_hra){
				    sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
				} else {
				    sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
				}
				break;
			case 2:
				if(mstrar_hra){
				    sdf = new SimpleDateFormat("EEE dd 'de' MMM 'a las' HH:mm", Locale.getDefault());
				} else {
				    sdf = new SimpleDateFormat("EEE dd 'de' MMM", Locale.getDefault());
				}
				break;
			case 3:
				if(mstrar_hra){
				    sdf = new SimpleDateFormat("dd '-' MMM '-' yyyy 'a las' HH:mm", Locale.getDefault());
				} else {
				    sdf = new SimpleDateFormat("dd '-' MMM '-' yyyy", Locale.getDefault());
				}
				break;
			case 4:
				sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
				break;
		}	
	    String string = sdf.format(new Date(mlscnds));
		return string;
	}
	
	// Función que extrae hora, minutos y segundos en milisegundos
	public long obtenerHoraMinutosSegundos(long milisegundos){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milisegundos);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		long referencia = calendar.getTimeInMillis();
		return (milisegundos - referencia); 
	}
	
	/** ?? */
	/*public long hourMinutesToMiliseconds(int hra, int mntos){
		long mlsgndos;
		mlsgndos = ((hra*60*60) + (mntos*60)) * 1000;
		return mlsgndos;
	}*/
	
	/** ?? */
	public String unixTimeToStringHourMinutes(long mlsgndos){
		long sgndos = (mlsgndos / 1000);
		String hra = (sgndos / 3600) < 10 ? 
				"0" + String.valueOf((sgndos / 3600)) : String.valueOf((sgndos / 3600));
		String mntos = ((sgndos % 3600) / 60) < 10 ? 
				"0" + String.valueOf((sgndos % 3600) / 60) : String.valueOf((sgndos % 3600) / 60);
		return (hra + ":" + mntos);
	}

	public int unixTimeToHours(long milisegundos) {
		int hours = (int) ((milisegundos / 1000) / 3600);
		return hours;
	}

	public int unixTimeToMinutes(long milisegundos) {
		int minutes = (int) (((milisegundos / 1000) % 3600) / 60);
		return minutes;
	}
	
	/** Retorna un boolean como 0 o 1 */
	public int booleanToBit(boolean varBoolean){
		int bit = 0;
		if(varBoolean){
			bit = 1;
		}
		return bit;
	}
	
	/** Retorna un bit como boolean */
	public boolean bitToBoolean(int bit){
		boolean varBoolean = false;
		if(bit == 1){
			varBoolean = true;
		}
		return varBoolean;
	}
	
	/** Muestra un mensaje de larga duración */
	public void showLongMessage(String message, Context activityContext){
    	Toast.makeText(activityContext, message, Toast.LENGTH_LONG).show();
    }
	
	/** Muestra un mensaje de corta duración */
	public void showShortMessage(String message, Context activityContext){
    	Toast.makeText(activityContext, message, Toast.LENGTH_SHORT).show();
    }

	/** Verifica si el dispositivo está conectado a internet */
	public boolean isConnected(Context context) {
	    ConnectivityManager connectivityManager
	    		= (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    if (activeNetworkInfo != null){
	    	return activeNetworkInfo.isConnected();
	    }
	    return false;
	}
	
	/** Permite mostrar un cuadro de alerta con la posibilidad de tomar una decision */
	public void alertDialogYESNO(Context context, 
		String message, String afrmcion, String ngcion, DialogInterface.OnClickListener dialogClickListener
	){
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setCancelable(false);
		builder.setMessage(message);
		builder.setPositiveButton((afrmcion == null ? "SI" : afrmcion), dialogClickListener);
		builder.setNegativeButton((ngcion == null ? "NO" : ngcion), dialogClickListener).show();
	}
	
	/** Permite mostrar un cuadro de alerta con un view personalizado*/
	public AlertDialog alertDialogView(Context context, CharSequence titulo, View view){
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setCancelable(false);
		AlertDialog dialog = builder.create();
		if (titulo != null) {
			dialog.setTitle(titulo);
		}
	    dialog.setView(view, 0, 0, 0, 0);
	    dialog.show();
	    return dialog;
	}
	
	/** Permite mostrar un cuadro de alerta */
	public void alertDialogYES(
			Context context, String title, String message,  DialogInterface.OnClickListener dialogClickListener){
		// Crea el text view
		final TextView tv_message = new TextView(context);
		final SpannableString spannableString = new SpannableString(message);
		Linkify.addLinks(spannableString, Linkify.WEB_URLS);
		tv_message.setText(spannableString);
		tv_message.setMovementMethod(LinkMovementMethod.getInstance());
		tv_message.setTextSize(18);
		  
		// Crea el dialog	
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setCancelable(false);
		builder.setTitle((title == null ? "Importante!" : title));
		builder.setPositiveButton("Aceptar", dialogClickListener);
		AlertDialog dialog = builder.create();
		dialog.setView(tv_message, 10, 0, 10, 10);
		dialog.show();
	}
	
	/** Verifica si los servicios de posicionamiento están activados */
	public boolean verificarServiciosPosicionamiento(final Context context){
		boolean srvcios_actvos = true;
		
		// Inicializa variables
		LocationManager lm = null;
		boolean gps_enabled = false, network_enabled = false;
		
		// Inicializa el Location Manager
		if(lm == null) {
			lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		}
		
		// Verifica proveedor GPS
		try{
			gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
		}catch(Exception ex){}
		
		// Verifica proveedor de Red
		try{
			network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		}catch(Exception ex){}
		
		// Verifica si no está habilitado el posicionamiento por GPS y RED
		if(!gps_enabled && !network_enabled){
			srvcios_actvos = false;
			AlertDialog.Builder dialog = new AlertDialog.Builder(context);
			dialog.setMessage(
					"Es necesario acceder a los servicios de posicionamiento por GPS o Red. " +
					"Activa los servicios de ubicación."
			);
			dialog.setPositiveButton("Activar", new DialogInterface.OnClickListener() {
			        @Override
			        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
			            // TODO Auto-generated method stub
		        		Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		        		context.startActivity(myIntent);
		        		//get gps
			        }
			});
			
			dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
			    @Override
			    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
			    }
			});
			dialog.show();
		}
		return srvcios_actvos;
	}
	
	// Metodo que genera el archivo log
	public static void generaLog(String log){
		try{
			
		   String directorio = Environment.getExternalStorageDirectory() + 
				   File.separator + "GC" + File.separator + "databases" + File.separator+"log.txt" ;
			File TextFile = new File(directorio); 
			
			// El valor booleano en el costructor de filewriter que indica si o no para anexar los datos escritos
			FileWriter salidaTexto = new FileWriter(TextFile, true);
			
			// Escribe en el archivo log
			salidaTexto.write(log+"\n");
			salidaTexto.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
