package com.gc.coregestionclientes.librerias;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

public class MultiSelectionSpinner extends Spinner implements OnMultiChoiceClickListener {
	String[] _items = null;
	boolean[] arraySelection = null;
	ArrayAdapter<String> arraySimpleAdapter;

	// Constructores
	public MultiSelectionSpinner(Context context) {
		super(context);
		arraySimpleAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item);
		super.setAdapter(arraySimpleAdapter);
	}

	public MultiSelectionSpinner(Context context, AttributeSet attrs) {
		super(context, attrs);
		arraySimpleAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item);
		super.setAdapter(arraySimpleAdapter);
	}

	// Onclick
	public void onClick(DialogInterface dialog, int which, boolean isChecked) {
		if (arraySelection != null && which < arraySelection.length) {
			arraySelection[which] = isChecked;

			arraySimpleAdapter.clear();
			arraySimpleAdapter.add(buildSelectedItemString());
		} else {
			throw new IllegalArgumentException("Argument 'which' is out of bounds.");
		}
	}

	@Override
	public boolean performClick() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
		builder.setMultiChoiceItems(_items, arraySelection, this);
		builder.show();
		return true;
	}

	@Override
	public void setAdapter(SpinnerAdapter adapter) {
		throw new RuntimeException("setAdapter is not supported by MultiSelectSpinner.");
	}

	public void setItems(String[] items) {
		_items = items;
		arraySelection = new boolean[_items.length];
		arraySimpleAdapter.clear();
		arraySimpleAdapter.add(_items[0]);
		Arrays.fill(arraySelection, false);
	}

	public void setItems(List<String> items) {
		_items = items.toArray(new String[items.size()]);
		arraySelection = new boolean[_items.length];
		arraySimpleAdapter.clear();
		arraySimpleAdapter.add(_items[0]);
		Arrays.fill(arraySelection, false);
	}

	public void setSelection(String[] selection) {
		for (String cell : selection) {
			for (int j = 0; j < _items.length; ++j) {
				if (_items[j].equals(cell)) {
					arraySelection[j] = true;
				}
			}
		}
		arraySimpleAdapter.clear();
		arraySimpleAdapter.add(buildSelectedItemString());
	}

	public void setSelection(List<String> selection) {
		for (int i = 0; i < arraySelection.length; i++) {
			arraySelection[i] = false;
		}
		for (String sel : selection) {
			for (int j = 0; j < _items.length; ++j) {
				if (_items[j].equals(sel)) {
					arraySelection[j] = true;
				}
			}
		}
		arraySimpleAdapter.clear();
		arraySimpleAdapter.add(buildSelectedItemString());
	}

	public void setSelection(int index) {
		for (int i = 0; i < arraySelection.length; i++) {
			arraySelection[i] = false;
		}
		if (index >= 0 && index < arraySelection.length) {
			arraySelection[index] = true;
		} else {
			throw new IllegalArgumentException("Index " + index
					+ " is out of bounds.");
		}
		arraySimpleAdapter.clear();
		arraySimpleAdapter.add(buildSelectedItemString());
	}

	public void setSelection(int[] selectedIndices) {
		for (int i = 0; i < arraySelection.length; i++) {
			arraySelection[i] = false;
		}
		for (int index : selectedIndices) {
			if (index >= 0 && index < arraySelection.length) {
				arraySelection[index] = true;
			} else {
				throw new IllegalArgumentException("Index " + index
						+ " is out of bounds.");
			}
		}
		arraySimpleAdapter.clear();
		arraySimpleAdapter.add(buildSelectedItemString());
	}

	public List<String> getSelectedStrings() {
		List<String> selection = new LinkedList<String>();
		for (int i = 0; i < _items.length; ++i) {
			if (arraySelection[i]) {
				selection.add(_items[i]);
			}
		}
		return selection;
	}

	public List<Integer> getSelectedIndices() {
		List<Integer> selection = new LinkedList<Integer>();
		for (int i = 0; i < _items.length; ++i) {
			if (arraySelection[i]) {
				selection.add(i);
			}
		}
		return selection;
	}

	private String buildSelectedItemString() {
		StringBuilder sb = new StringBuilder();
		boolean foundOne = false;

		for (int i = 0; i < _items.length; ++i) {
			if (arraySelection[i]) {
				if (foundOne) {
					sb.append(", ");
				}
				foundOne = true;

				sb.append(_items[i]);
			}
		}
		return sb.toString();
	}

	public String getSelectedItemsAsString() {
		StringBuilder sb = new StringBuilder();
		boolean foundOne = false;

		for (int i = 0; i < _items.length; ++i) {
			if (arraySelection[i]) {
				if (foundOne) {
					sb.append(", ");
				}
				foundOne = true;
				sb.append(_items[i]);
			}
		}
		return sb.toString();
	}
}
