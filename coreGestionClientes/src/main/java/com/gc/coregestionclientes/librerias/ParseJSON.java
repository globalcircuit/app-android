package com.gc.coregestionclientes.librerias;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.CharBuffer;
import java.util.LinkedHashMap;
import java.util.Vector;

public class ParseJSON {
	private BufferedReader stream = null;
	private byte[] cadenaJSON = null;
	private String ultimoError = null;
	
	public enum Estado {
	    SALTANDO_CARACTERES_ESCAPE, LEYENDO_NOMBRE, LEYENDO_SEPARADOR, LEYENDO_VALOR, LEYENDO_SIGUE_FIN
	}
	
	public enum TipoValor {
		INDEFINIDO, OBJETO, ARREGLO, NUMERO, CADENA
	}
	
	public ParseJSON(byte[] cadenaJSON) {
		this.cadenaJSON = cadenaJSON;
	}
	
	private Object objetoValor(Object valor, TipoValor tipoValor) {
		// Según el tipo de valor se crea el objeto valor (para números y cadenas)
		if(tipoValor == TipoValor.NUMERO) {
			((CharBuffer)valor).flip();
			return(Double.valueOf(((CharBuffer)valor).toString()));
		} else if(tipoValor == TipoValor.CADENA) {
			((CharBuffer)valor).flip();
			return(((CharBuffer)valor).toString());
		} else {
			// Error, no debe llegar a este punto...
			System.out.println("objetoValor, error: Tipo de valor: " + tipoValor + " no esperado");
			return(null);
		}
	}
	
	private Vector<Object> crearArreglo() {
		int caracter;
		Vector<Object> arreglo = new Vector<Object>();
		
		// Inicialización
		CharBuffer valor = CharBuffer.allocate(256);
		Object objetoJSON = null;
		Object arregloJSON = null;
		Estado estado = Estado.SALTANDO_CARACTERES_ESCAPE, siguienteEstado = Estado.LEYENDO_VALOR;
		TipoValor tipoValor = TipoValor.INDEFINIDO;
		boolean primerCaracter = true;
		
		// Ciclo de leer caracteres
		do {
			try {
				caracter = stream.read();
				if(caracter == -1) break;
			} catch (IOException e) {
				// Error...
				System.out.println("crearObjeto, error: " + e.toString());
				break;
			}
			
			// Saltar caracteres de escapa, si se debe
			if(estado == Estado.SALTANDO_CARACTERES_ESCAPE) {
				if(caracter == 0x0A || caracter == 0x0D || caracter == 0x20)
					// Omitir...
					continue;
				else
					// Cambiar de estado
					estado = siguienteEstado;
			}
			
			// Acción a seguir según el estado
			switch (estado) {
			case LEYENDO_VALOR:
				if(primerCaracter) {
					// Definir el tipo del valor: dato primitivo, arreglo u objeto
					primerCaracter = false;
					if(caracter == 0x007B) {
						// Objeto: {
						tipoValor = TipoValor.OBJETO;
						objetoJSON = crearObjeto();
						siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
						estado = Estado.SALTANDO_CARACTERES_ESCAPE;
					} else if(caracter == 0x005B) {
						// Arreglo: [
						tipoValor = TipoValor.ARREGLO;
						arregloJSON = crearArreglo();
						siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
						estado = Estado.SALTANDO_CARACTERES_ESCAPE;
					} else if(caracter == 0x0022) {
						// Cadena: "
						tipoValor = TipoValor.CADENA;
					} else {
						// Numero
						tipoValor = TipoValor.NUMERO;
						((CharBuffer)valor).put((char)caracter);
						siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
						estado = Estado.SALTANDO_CARACTERES_ESCAPE;
					}
				} else {
					// Adicionar caracter a la cadena hasta que lleguen las comillas "
					if(caracter == 0x0022) {
						// Comillas ("), fin de la cadena
						siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
						estado = Estado.SALTANDO_CARACTERES_ESCAPE;
					} else {
						// Contenido...
						((CharBuffer)valor).put((char)caracter);
					}
				}
				break;
				
			case LEYENDO_SIGUE_FIN:
				if((caracter == 0x002C) || (caracter == 0x005D)) {
					// Guardar el elemento
					if(tipoValor == TipoValor.OBJETO)
						arreglo.add(objetoJSON);
					else if(tipoValor == TipoValor.ARREGLO)
						arreglo.add(arregloJSON);
					else
						arreglo.add(objetoValor(valor, tipoValor));
					
					// Definir siguiente acción
					if(caracter == 0x002C) {
						// Coma (,) leer el siguiente elemento
						primerCaracter = true;
						valor.clear();
						siguienteEstado = Estado.LEYENDO_VALOR;
						estado = Estado.SALTANDO_CARACTERES_ESCAPE;
					} else {
						// Fin de arreglo: ], retornar arreglo
						return(arreglo); 
					}
				} else {
					// Contenido de un campo numérico
					if(tipoValor == TipoValor.NUMERO) {
						// Adicionar caracter al número
						((CharBuffer)valor).put((char)caracter);
					} else {
						// Error...
						System.out.println("Cadena JSON malformada: se esperaba , o ]");
					}
				}
			}
		} while(true);
		
		return(arreglo);
	}
	
	private LinkedHashMap<String, Object> crearObjeto() {
		int caracter;
		LinkedHashMap<String, Object> objeto = new LinkedHashMap<String, Object>();
		
		// Inicialización
		CharBuffer nombre = CharBuffer.allocate(256);
		CharBuffer valor = CharBuffer.allocate(256);
		Object objetoJSON = null;
		Object arregloJSON = null;
		Estado estado = Estado.SALTANDO_CARACTERES_ESCAPE, siguienteEstado = Estado.LEYENDO_NOMBRE;
		TipoValor tipoValor = TipoValor.INDEFINIDO;
		boolean primerCaracter = true;
		
		// Ciclo de leer caracteres
		do {
			try {
				caracter = stream.read();
				if(caracter == -1) break;
			} catch (IOException e) {
				// Error...
				System.out.println("crearObjeto, error: " + e.toString());
				break;
			}
			
			// Saltar caracteres de escape, si se debe
			if(estado == Estado.SALTANDO_CARACTERES_ESCAPE) {
				if(caracter == 0x0A || caracter == 0x0D || caracter == 0x20)
					// Omitir...
					continue;
				else
					// Cambiar de estado
					estado = siguienteEstado;
			}
			
			// Acción a seguir según el estado
			switch (estado) {
			case LEYENDO_NOMBRE:
				if(caracter == 0x0022) {
					// Comillas (")...
					if(nombre.position() == 0) {
						// Inicio del nombre...
					} else {
						// Fin del nombre
						siguienteEstado = Estado.LEYENDO_SEPARADOR;
						estado = Estado.SALTANDO_CARACTERES_ESCAPE;
					}
				} else {
					// Contenido...
					nombre.put((char)caracter);
				}
				break;
				
			case LEYENDO_SEPARADOR:
				if(caracter == 0x003A) {
					// Dos puntos (:)...
					valor.clear();
					primerCaracter = true;
					siguienteEstado = Estado.LEYENDO_VALOR;
					estado = Estado.SALTANDO_CARACTERES_ESCAPE;
				} else {
					// Error...
					System.out.println("Cadena JSON malformada, falta separador de nombre y valor (:)");
				}
				break;
				
			case LEYENDO_VALOR:
				if(primerCaracter) {
					// Definir el tipo del valor: dato primitivo, arreglo u objeto
					primerCaracter = false;
					if(caracter == 0x007B) {
						// Objeto: {
						tipoValor = TipoValor.OBJETO;
						objetoJSON = crearObjeto();
						siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
						estado = Estado.SALTANDO_CARACTERES_ESCAPE;
					} else if(caracter == 0x005B) {
						// Arreglo: [
						tipoValor = TipoValor.ARREGLO;
						arregloJSON = crearArreglo();
						siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
						estado = Estado.SALTANDO_CARACTERES_ESCAPE;
					} else if(caracter == 0x0022) {
						// Cadena: "
						tipoValor = TipoValor.CADENA;
					} else {
						// Numero
						tipoValor = TipoValor.NUMERO;
						((CharBuffer)valor).put((char)caracter);
						siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
						estado = Estado.SALTANDO_CARACTERES_ESCAPE;
					}
				} else {
					// Adicionar caracter a la cadena hasta que lleguen las comillas "
					if(caracter == 0x0022) {
						// Comillas ("), fin de la cadena
						siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
						estado = Estado.SALTANDO_CARACTERES_ESCAPE;
					} else {
						// Contenido...
						((CharBuffer)valor).put((char)caracter);
					}
				}
				break;
				
			case LEYENDO_SIGUE_FIN:
				if((caracter == 0x002C) || (caracter == 0x007D)) {
					// Guardar la pareja nombre - valor
					nombre.flip();
					if(tipoValor == TipoValor.OBJETO)
						objeto.put(nombre.toString(), objetoJSON);
					else if(tipoValor == TipoValor.ARREGLO)
						objeto.put(nombre.toString(), arregloJSON);
					else
						objeto.put(nombre.toString(), objetoValor(valor, tipoValor));
					
					// Definir siguiente acción
					if(caracter == 0x002C) {
						// Coma (,) leer la siguiente pareja nombre - valor
						nombre.clear();
						siguienteEstado = Estado.LEYENDO_NOMBRE;
						estado = Estado.SALTANDO_CARACTERES_ESCAPE;
					} else {
						// Fin de objeto: }, retornar objeto
						return(objeto); 
					}
				} else {
					// Contenido de un campo numérico
					if(tipoValor == TipoValor.NUMERO) {
						// Adicionar caracter al número
						((CharBuffer)valor).put((char)caracter);
					} else {
						// Error...
						System.out.println("Cadena JSON malformada: se esperaba , o }");
					}
				}
			}
		} while(true);
		
		return(objeto);
	}
	
	public String getError() {
		return(ultimoError);
	}

	public Object datosJSON() {
		Object datos = null;
		int caracter;
		
		try {
			// Abrir stream de datos
			stream = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(cadenaJSON)));
			
			// Leer primer caracter, puede ser el Byte Order Mark (BOM)
			do {
				caracter = stream.read();
			} while(caracter == 0xFEFF || caracter == 0xFFFE);		// Big endian o Little endian
			
			// Si hay caracteres de escape saltarlos
			while(caracter == 0x0A || caracter == 0x0D || caracter == 0x20) {
				// Omitir...
				caracter = stream.read();
			}
			
			// Objeto o arreglo?
			if(caracter == 0x007B) {
				// Inicio de objeto ({), leerlo
				datos = crearObjeto();
			} else if(caracter == 0x005B) {
				// Inicio de Arreglo: ([), leerlo
				datos = crearArreglo();
			} else {
				// Error...
				ultimoError = "Cadena JSON malformada: el objeto no inicia con { o [";
			}
			
			// Cerrar stream
			stream.close();
		} catch (UnsupportedEncodingException e) {
			ultimoError = e.getMessage();
		} catch (IOException e) {
			ultimoError = e.getMessage();
		} catch (Exception e) {
			ultimoError = e.getMessage();
		}
		
		return(datos);
	}
}