package com.gc.coregestionclientes.librerias;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Hashtable;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.coregestionclientes.Configuracion;

import android.util.Log;

public class SOAP11Client {
    private Hashtable<String, String> _response = null;
    private Vector<?> _prmtros_srvcio = null;
    private String _url;
    private String _urn;
    private String _nmbre_srvcio;
    private int _connectTimeout = 0; //segundos
    private int _readTimeout = 0; //segundos
    
    // Constructor
    public SOAP11Client(Vector<?> prmtros_srvcio, int connectTimeout, int readTimeout) {
    	_prmtros_srvcio = prmtros_srvcio;
        _url = (String)_prmtros_srvcio.elementAt(0);
        _urn = (String)_prmtros_srvcio.elementAt(1);
        _nmbre_srvcio = (String)_prmtros_srvcio.elementAt(2);
        _connectTimeout = connectTimeout;
        _readTimeout = readTimeout;
    }

    // Parse response from web service
    private Hashtable<String, String> ParseResponse(String rspsta, String nmbre_srvcio) {
    	Hashtable<String, String> key_values = new Hashtable<String, String>();
    	
    	try {
    		// Decodifica la respuesta y la guarda en un stream
    		InputStream inputStream = new ByteArrayInputStream(rspsta.getBytes("ISO-8859-1"));
    		
    		// Crear objeto que permite interpretar un XML
	    	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder builder = factory.newDocumentBuilder();
	        Document document = builder.parse(inputStream);
	        
	        // Normalizar el documento (XML)
	        document.getDocumentElement().normalize();
	        
	        // Obtener la sección de respuesta
	        NodeList list = document.getElementsByTagName(nmbre_srvcio + "Response");
        	NodeList children = list.item(0).getChildNodes();
        	
        	// Recorrerla y obtener los nodos y sus valores
        	for(int j = 0; j < children.getLength(); ++j) {
        		Node n = children.item(j);
        		String key = new String(n.getNodeName());
        		String value = new String();
        		if(n.getNodeType() == Node.ELEMENT_NODE) {
        			NodeList a = n.getChildNodes();
        			int nmro = a.getLength();
        			if(nmro == 0) {
        				value = "";
        			} else if(nmro == 1) {
        				Node f = a.item(0);
	        			if(f.getNodeType() == Node.TEXT_NODE) value = a.item(0).getNodeValue();
        			}
        		}
        		key_values.put(key, value);
        	}
    	} catch (Exception e) {
    		// Error leyendo la respuesta
    		Log.e(Configuracion.tagLog, "ParseResp=" + e.toString());
    	}
    	
    	return(key_values);
    }
    
    // Construct the body of SOAP message
    private String ServiceBody() {
    	String body = null;
    	
    	// Set the body of SOAP message based on the service solicited
    	body = "<" + _nmbre_srvcio + " xmlns=\"" + _urn + "\">";
		for (int i =3; i < _prmtros_srvcio.size(); i++) {
			body = body +
				"<" + _prmtros_srvcio.elementAt(i) + " xmlns=\"\">" +
					_prmtros_srvcio.elementAt(i + 1) +
				"</" + _prmtros_srvcio.elementAt(i++) + ">";
		}
		body = body + "</" + _nmbre_srvcio + ">";
    	
    	return body;
    }

    public Hashtable<String, String> CallWebService() {
    	try {
        	// Set request body
        	String SoapXML =
        		"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" + 
        		"<SOAP-ENV:Envelope " +
        			"xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
        			"xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" " +
        			"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
        			"xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" " +
        			"xmlns:tns=\"" + _urn + "\">" +
        			"<SOAP-ENV:Body>" + ServiceBody() + "</SOAP-ENV:Body>"+
        		"</SOAP-ENV:Envelope>";

        	// Open connection
        	URL connectURL = new URL(_url);
        	HttpURLConnection httpConn = (HttpURLConnection) connectURL.openConnection();
            
        	// Start the send request
            httpConn.setRequestProperty("Content-Type", "text/xml; charset=ISO-8859-1");
            httpConn.setRequestProperty("SOAPAction", "\"" + _urn + "#" + _nmbre_srvcio + "\"");
            httpConn.setRequestProperty("Content-Length", String.valueOf(SoapXML.getBytes().length));
            httpConn.setRequestMethod("POST");
            httpConn.setConnectTimeout(_connectTimeout * 1000);
            httpConn.setReadTimeout(_readTimeout * 1000);

            // Send request string to server
            httpConn.setDoOutput(true);
            OutputStream _outStream = httpConn.getOutputStream();
            _outStream.write(SoapXML.getBytes());
            
            // Get the response code
            int rc = httpConn.getResponseCode();
            if (rc != HttpURLConnection.HTTP_OK) {
                throw new IOException("HTTP response code: " + rc);
            }

            // Obtain DataInputStream for receiving server response
            DataInputStream _inputStream = new DataInputStream(httpConn.getInputStream());
            
            // Retrieve the response from server
            int ch;
            StringBuffer tmpral = new StringBuffer();
            while ((ch = _inputStream.read()) != -1) tmpral.append((char) ch);
            
            // Extract response from soap body section
            _response = ParseResponse(tmpral.toString(), _nmbre_srvcio);
            _response.put("ConsumioServicio", "exito");
        	_response.put("MensajeSOAP", "OK");
        } catch (Exception e) {
        	// Error...
        	_response = new Hashtable<String, String>();
        	_response.put("ConsumioServicio", "error");
        	_response.put("MensajeSOAP", e.toString());
        }

        // Return response
    	return(_response);
    }
}