package com.gc.coregestionclientes.librerias;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Clase para el manejo de Objetos JSON genericos "tipo tablas"
// TODO: Estructurar mejor esta clase
// Adicionaar validaciones
public class JSONManager {
	
	JSONObject jsonObject;
	JSONArray tblas;
	
	// Constructor
	public JSONManager(JSONObject jsonObject) throws JSONException {
		this.jsonObject = jsonObject;
		if(this.jsonObject.has("Tablas")){
			tblas = jsonObject.getJSONArray("Tablas");
		}
	}
	
	// Obtiene el numero de Tablas
	public int getNumeroTablas() {
		if(tblas != null) {
			return tblas.length();
		}else{
			return -1;
		}
	}
	
	// Obtiene el numero de Registros dependiendo de la tabla
	public int getNumeroRegistros(int indce_tbla) throws JSONException{
		JSONObject tbla = (JSONObject) tblas.get(indce_tbla);
		if(tbla.has("Registros")){
			JSONArray rgstros = tbla.getJSONArray("Registros");
			return rgstros.length();
		}else{
			return -1;
		}
	}
	
	// Retorna los campos de un registro (Array Llaves)
	public JSONArray getNombresCampos(int indce_tbla, int indce_rgstro) throws JSONException{
		if(tblas != null) {
			JSONObject tbla = (JSONObject) tblas.get(indce_tbla);
			JSONArray rgstros = tbla.getJSONArray("Registros");
			JSONObject rgstro = (JSONObject) rgstros.get(indce_rgstro);
			return rgstro.names();
		}else{
			return null;
		}
	}
	
	// Retorna el valor dado el registro y campo (Key)
	public String getValorByKey(int indce_tbla, int indce_rgstro, String key) throws JSONException{
		if(tblas != null) {
			JSONObject tbla = (JSONObject) tblas.get(indce_tbla);
			JSONArray rgstros = tbla.getJSONArray("Registros");
			JSONObject rgstro = (JSONObject) rgstros.get(indce_rgstro);
			return rgstro.getString(key);
		}else{
			return null;
		}
	}

	// Retorna una lista de registros (JSONArray) teniendo en cuenta el indice de la tabla
	public JSONArray getRegistros(int indce_tbla) throws JSONException{
		
		// Inicializa la lista
		//List<JSONObject> lsta_rgstros = new ArrayList<JSONObject>();
		
		// Extrae la tabla
		JSONObject tbla = (JSONObject) tblas.get(indce_tbla);
		JSONArray rgstros = tbla.getJSONArray("Registros");
		/*int nmro_rgstros = rgstros.length();
		
		// Recorre los registros
		for (int i = 0; i < nmro_rgstros; i++) {
			JSONObject rgstro = (JSONObject) rgstros.get(i);
			lsta_rgstros.add(rgstro);
		}*/

		// Retorna la Lista
		return rgstros;
	}
	
}
