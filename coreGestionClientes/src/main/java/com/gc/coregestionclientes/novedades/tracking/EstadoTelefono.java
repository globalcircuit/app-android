package com.gc.coregestionclientes.novedades.tracking;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class EstadoTelefono extends PhoneStateListener {
    private final Context contexto;
    private int potencia;
    private int estadoDatos;
    private int cellId = -1;
    private int lac = -1;
    private int baseStationId;

    private Lock mutexLeerEstado = null;

    public EstadoTelefono(Context contexto) {
        this.contexto = contexto;
        this.mutexLeerEstado = new ReentrantLock();
    }

    @SuppressLint("NewApi")
	private boolean estaTelefonoModoAvion() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(
                    contexto.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(
                    contexto.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }
    }

    @SuppressLint("NewApi")
	private boolean estaTiempoModoAutomatico() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(
                    contexto.getContentResolver(), Settings.System.AUTO_TIME, 0) != 0;
        } else {
            return Settings.Global.getInt(
                    contexto.getContentResolver(), Settings.Global.AUTO_TIME, 0) != 0;
        }
    }

    @SuppressLint("NewApi")
	private void cambiarTiempoModoAutomatico() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Settings.System.putInt(
                    contexto.getContentResolver(), Settings.System.AUTO_TIME, 1);
        } else {
            Settings.Global.putInt(
                    contexto.getContentResolver(), Settings.Global.AUTO_TIME, 1);
        }
    }

    private int nivelBateria() {
        // Leer estado de batería (no hay necesidad de registrar el intento, por eso se pasa null)
        IntentFilter intento = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent estadoBateria = contexto.registerReceiver(null, intento);

        // Leer nivel y escala
        if (estadoBateria != null) {
            int nivel = estadoBateria.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int escala = estadoBateria.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            if (nivel != -1 && escala != -1) {
                // Ok
                return Math.round((float) 100 * nivel / (float) escala);
            }
        }

        // Líos, retornar cero (0)...
        return 0;
    }

    @Override
    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        super.onSignalStrengthsChanged(signalStrength);

        try {
            mutexLeerEstado.lock();

            // Cálculo señal según: 3GPP TS 27.007 V6.3.0 (2003-06)
            potencia = signalStrength.getGsmSignalStrength();
            if (potencia <= 30) potencia = -113 + (2 * potencia);
            else if (potencia == 31) potencia = -51;
            else potencia = -115;
        } finally {
            mutexLeerEstado.unlock();
        }
    }

    @Override
    public void onDataConnectionStateChanged(int state) {
        try {
            mutexLeerEstado.lock();

            // Verificar si hay cambio de estado en datos
            if ((estadoDatos == -1 || estadoDatos != state) && state != -1) {
                // Actualizar estado del dispositivo y generar trama
                estadoDatos = state;
            }
        } finally {
            mutexLeerEstado.unlock();
        }
    }

    @Override
    public void onCellLocationChanged(CellLocation location) {
        super.onCellLocationChanged(location);

        try {
            mutexLeerEstado.lock();

            // Verificar el tipo de red
            if (location instanceof GsmCellLocation) {
                cellId = ((GsmCellLocation) location).getCid();
                lac = ((GsmCellLocation) location).getLac();
            } else if (location instanceof CdmaCellLocation) {
                baseStationId = ((CdmaCellLocation) location).getBaseStationId();

                Log.i(EstadoTelefono.class.getName(), "CDMA base station Id = " + baseStationId);
            }
        } finally {
            mutexLeerEstado.unlock();
        }
    }

    public int obtenerEstado(boolean estaHabilitadoNETWORK, boolean estaHabilitadoGPS) {
        int estado;

        try {
            mutexLeerEstado.lock();

            estado = nivelBateria() << 5 |
                    (estaHabilitadoNETWORK ? 1 : 0) << 4 |
                    (estaHabilitadoGPS ? 1 : 0) << 3 |
                    (estaTiempoModoAutomatico() ? 1 : 0) << 2 |
                    estadoDatos;
            if (!estaTiempoModoAutomatico()) cambiarTiempoModoAutomatico();

            return estado;
        } finally {
            mutexLeerEstado.unlock();
        }
    }

    public int potenciaAntena() {
        return potencia;
    }

    public int locationAreaCode() {
        return lac;
    }

    public int cellIdentifier() {
        return cellId;
    }

    public boolean hayConexionDatos(Context contexto) {
        // Obtener instancia de un administrador de conexiones (mobile or wifi)
        ConnectivityManager connManager =
                (ConnectivityManager) contexto.getSystemService(Context.CONNECTIVITY_SERVICE);

        // Evaluar si hay conexión de datos nG
        NetworkInfo infoMobile = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (infoMobile != null && infoMobile.isConnected()) return true;

        // No hay conexión de datos nG, verificar WiFi
        NetworkInfo infoWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return infoWifi != null && infoWifi.isConnected();
    }
}
