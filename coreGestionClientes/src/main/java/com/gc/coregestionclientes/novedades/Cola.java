package com.gc.coregestionclientes.novedades;

import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Cola {
    private Vector<Object> cola = null;				// The queue
    private Lock mutex = null;						// Mutual exclusion lock for access to fetch and deposit methods

    // Constructor
    public Cola() {
        // Inicializar el buffer circular
        cola = new Vector<Object>();
        mutex = new ReentrantLock();
    }

    // Adiciona un comando a la cola
    public void deposit(Object comando, boolean alInicio) {
        // Bloquear
        mutex.lock();

        try {
            if(alInicio)
                // Adicionar al inicio
                cola.add(0, comando);
            else
                // Adicionar al final
                cola.add(comando);
        } catch(Exception e) {
            // ...
        } finally {
            // Desbloquear
            mutex.unlock();
        }
    }

    public void deposit(Object comando) {
        deposit(comando, false);
    }

    // Retorna el primer elemento en la cola
    public Object fetch() {
        Object cabeza = null;

        // Bloquear
        mutex.lock();

        try {
            // Leer la cabeza de la cola
            cabeza = cola.firstElement();
            cola.remove(0);
        } catch(Exception e) {
            // ...
        } finally {
            // Desbloquear
            mutex.unlock();
        }

        return(cabeza);
    }

    public int numeroElementos() {
        int n;

        // Bloquear
        mutex.lock();
        n = cola.size();

        // Desbloquear
        mutex.unlock();

        return(n);
    }
}
