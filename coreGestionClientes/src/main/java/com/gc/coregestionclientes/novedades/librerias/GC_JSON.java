package com.gc.coregestionclientes.novedades.librerias;

import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;

import com.gc.coregestionclientes.novedades.DynamicByteBuffer;

public class GC_JSON{
    private static Object parseNumero(StringBuilder numero) {
        char c;
        int i = 0;
        int longitud = numero.length();
        boolean puntoDecimal = false;
        boolean exponente = false;

        // Es negativo (-) ?
        c = numero.charAt(0);
        if (c == 0x002D) {
            if (longitud == 1) return null;
            i++;
        }

        // Inicia con punto (.) ?
        c = numero.charAt(i);
        if (c == 0x002E) {
            puntoDecimal = true;
            if (i == (longitud - 1)) return null;
            i++;
        }

        // Recorrer mientras los caracteres sean dígitos
        boolean hayDigitos = false;
        do {
            c = numero.charAt(i++);
            if (c < 0x0030 || c > 0x0039) break;
            hayDigitos = true;
        } while (i < longitud);

        // En este punto:
        if (!hayDigitos) return null;
        if (c >= 0x0030 && c <= 0x0039) {
            // No hay más datos. Es un múmero entero o real
            if (puntoDecimal) return Double.valueOf(numero.toString());
            else return Long.valueOf(numero.toString());
        }
        if (puntoDecimal && c == 0x002E) {
            // Llegó otro símbolo de punto
            return null;
        }
        if (c == 0x002E) {
            // Llegó símbolo de punto
            if (i == longitud) {
                // No hay más datos, es un entero
                return Long.valueOf(numero.substring(0, longitud - 1));
            }
        } else if (c == 0x0045 || c == 0x0065) {
            // Llegó simbolo de exponente
            exponente = true;
            if (i == longitud) return null;
        } else {
            // No llegó un caracter válido
            return null;
        }

        // Último caracter es exponente o punto ?
        if (!exponente) {
            // El último caracter leído es un punto
            // Recorrer mientras los caracteres sean dígitos
            do {
                c = numero.charAt(i++);
                if (c < 0x0030 || c > 0x0039) break;
            } while (i < longitud);

            // En este punto:
            if (c >= 0x0030 && c <= 0x0039) {
                // No hay más datos, es un número real
                return Double.valueOf(numero.toString());
            }
            if (c != 0x0045 && c != 0x0065) {
                // No llegó símbolo de exponente
                return null;
            }
            if (i == longitud) return null;
        }

        // El último caracter leído es el símbolo de exponente
        // Leer el valor del exponente. Es negativo (-) ?
        c = numero.charAt(i);
        if (c == 0x002D) {
            if (i == (longitud - 1)) return null;
            i++;
        }

        // Recorrer mientras los caracteres sean dígitos
        do {
            c = numero.charAt(i++);
            if (c < 0x0030 || c > 0x0039) break;
        } while (i < longitud);

        // En este punto:
        if (c < 0x0030 || c > 0x0039) {
            // Llegó un caracter inválido
            return null;
        }

        // La cadena representa un número real
        return Double.valueOf(numero.toString());
    }

    private static Object objetoValor(StringBuilder valor, TipoValor tipoValor) {
        // Nulo ?
        if (valor == null) return null;

        // Vacío ?
        if (valor.length() == 0) return null;

        // Según el tipo de valor se crea el objeto valor
        String texto = valor.toString();
        if (tipoValor == TipoValor.STRING) {
            return ("\"" + texto + "\"");
        } else if (tipoValor == TipoValor.NUMBER) {
            return parseNumero(valor);
        } else if (tipoValor == TipoValor.BOOLEAN) {
            if (texto.equals("false") || texto.equals("true"))
                return (texto);
            else
                return (null);
        } else if (tipoValor == TipoValor.NULL) {
            if (texto.equals("null"))
                return (texto);
            else
                return (null);
        } else {
            // Error, no debe llegar a este punto...
            Log.e(GC_JSON.class.getName(),
                    "objetoValor, error: Tipo de valor: " + tipoValor + " no esperado");
            return (null);
        }
    }

    private static VectorGC<Object> crearArreglo(
            boolean primerNivel, BufferedReader stream) {
            
        int caracterAnterior, caracter = -1;
        VectorGC<Object> arreglo = new VectorGC<Object>();

        // Inicialización
        StringBuilder valor = new StringBuilder();
        Object objetoJSON = null;
        Object arregloJSON = null;
        Estado estado = Estado.SALTANDO_CARACTERES_ESCAPE;
        Estado siguienteEstado = Estado.LEYENDO_VALOR;
        TipoValor tipoValor = TipoValor.NONE;
        boolean primerCaracter = true;
        int cuentaInicio = 0;
        char caracterInicio = 0, caracterFin = 0;

        try {
            // Ciclo de leer caracteres
            do {
                caracterAnterior = caracter;
                caracter = stream.read();
                if (caracter == -1) break;

                // Saltar caracteres de escape
                if (estado == Estado.SALTANDO_CARACTERES_ESCAPE) {
                    if (caracter == 0x0A || caracter == 0x0D || caracter == 0x20) {
                        // Omitir...
                        continue;
                    } else {
                        // Cambiar de estado
                        estado = siguienteEstado;
                    }
                }

                // Acción a seguir según el estado
                switch (estado) {
                    case LEYENDO_VALOR:
                        if (primerCaracter) {
                            // Definir el tipo del valor: dato primitivo, arreglo u objeto
                            primerCaracter = false;
                            if (caracter == 0x005D) {
                                // Fin de arreglo (]), arreglo vacío o error
                                if (arreglo.size() == 0) return arreglo;
                                else return null;
                            } else if (caracter == 0x007B) {
                                // Objeto: {
                                tipoValor = TipoValor.OBJECT;
                                if (primerNivel) {
                                    valor.append((char) 0x007B);
                                    cuentaInicio = 1;
                                    caracterInicio = 0x007B;
                                    caracterFin = 0x007D;
                                } else {
                                    objetoJSON = crearObjeto(primerNivel, stream);
                                    if (objetoJSON == null) return null;
                                    siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
                                    estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                                }
                            } else if (caracter == 0x005B) {
                                // Arreglo: [
                                tipoValor = TipoValor.ARRAY;
                                if (primerNivel) {
                                    valor.append((char) 0x005B);
                                    cuentaInicio = 1;
                                    caracterInicio = 0x005B;
                                    caracterFin = 0x005D;
                                } else {
                                    arregloJSON = crearArreglo(primerNivel, stream);
                                    if (arregloJSON == null) return null;
                                    siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
                                    estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                                }
                            } else if (caracter == 0x0022) {
                                // Cadena: "
                                tipoValor = TipoValor.STRING;
                            } else if (caracter == 0x0074 || caracter == 0x0066) {
                                // Boolean: t(rue) / f(alse)
                                tipoValor = TipoValor.BOOLEAN;
                                valor.append((char)caracter);
                                siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
                                estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                            } else if (caracter == 0x006E) {
                                // Nulo: n
                                tipoValor = TipoValor.NULL;
                                valor.append((char)caracter);
                                siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
                                estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                            } else {
                                // Número
                                tipoValor = TipoValor.NUMBER;
                                valor.append((char) caracter);
                                siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
                                estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                            }
                        } else {
                            // En este punto, si solo se decodifica el primer nivel, se está
                            // leyendo una sección del JSON (sin decodificar) o un String
                            if (cuentaInicio > 0) {
                                // Arreglo u objeto JSON (sin decodificar)
                                valor.append((char) caracter);
                                if (caracter == caracterInicio) cuentaInicio++;
                                if (caracter == caracterFin) cuentaInicio--;
                                if (cuentaInicio == 0) {
                                    // Llegó caracter final
                                    if (tipoValor == TipoValor.ARRAY)
                                        arregloJSON = valor.toString().getBytes("UTF-8");
                                    else
                                        objetoJSON = valor.toString().getBytes("UTF-8");
                                    siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
                                    estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                                }
                            } else if (caracter == 0x0022) {
                                if (caracterAnterior == 0x005C) {
                                    // Comillas (\") escapadas, cambiar el \ por "
                                    valor.replace(valor.length() - 1, valor.length(), "\"");
                                } else {
                                    // String. Llegó caracter final (")
                                    siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
                                    estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                                }
                            } else {
                                // String. Adicionar caracter
                                valor.append((char) caracter);
                            }
                        }
                        break;

                    case LEYENDO_SIGUE_FIN:
                        // Es una coma (,) o un corchete cerrado (])
                        if ((caracter == 0x002C) || (caracter == 0x005D)) {
                            // Guardar el elemento
                            if (tipoValor == TipoValor.OBJECT)
                                arreglo.add(objetoJSON);
                            else if (tipoValor == TipoValor.ARRAY)
                                arreglo.add(arregloJSON);
                            else
                            	if(valor != null){
                            		arreglo.add(objetoValor(valor, tipoValor));
                            	}
                            // Definir siguiente acción
                            if (caracter == 0x002C) {
                                // Coma (,) leer el siguiente elemento
                                valor.setLength(0);
                                primerCaracter = true;
                                siguienteEstado = Estado.LEYENDO_VALOR;
                                estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                            } else {
                                // Fin de arreglo: ], retornar arreglo
                                return (arreglo);
                            }
                        } else {
                            // Contenido de un campo numérico, true, false, null
                            valor.append((char) caracter);
                        }
                }
            } while (true);
        } catch (Exception e) {
            // Error...
            Log.e(GC_JSON.class.getName(), Log.getStackTraceString(e));
            return null;
        }

        return (arreglo);
    }

    private static HashMapGC<String, Object> crearObjeto(
            boolean primerNivel, BufferedReader stream) {

        int caracterAnterior, caracter = -1;
        HashMapGC<String, Object> objeto = new HashMapGC<String, Object>();

        // Inicialización
        StringBuilder nombre = new StringBuilder();
        StringBuilder valor = new StringBuilder();
        Object objetoJSON = null;
        Object arregloJSON = null;
        Estado estado = Estado.SALTANDO_CARACTERES_ESCAPE;
        Estado siguienteEstado = Estado.LEYENDO_NOMBRE;
        TipoValor tipoValor = TipoValor.NONE;
        boolean primerCaracter = true;
        int cuentaInicio = 0;
        char caracterInicio = 0, caracterFin = 0;

        try {
            // Ciclo de leer caracteres
            do {
                caracterAnterior = caracter;
                caracter = stream.read();
                if (caracter == -1) break;

                // Saltar caracteres de escape, si se debe
                if (estado == Estado.SALTANDO_CARACTERES_ESCAPE) {
                    if (caracter == 0x0A || caracter == 0x0D || caracter == 0x20) {
                        // Omitir...
                        continue;
                    } else {
                        // Cambiar de estado
                        estado = siguienteEstado;
                    }
                }

                // Acción a seguir según el estado
                switch (estado) {
                    case LEYENDO_NOMBRE:
                        if (caracter == 0x007D) {
                            // Fin de objeto: }, objeto vacío o error
                            if (objeto.size() == 0) return objeto;
                            else return null;
                        } else if (caracter == 0x0022) {
                            if (caracterAnterior == 0x005C) {
                                // Comillas (\") escapadas, cambiar el \ por "
                                nombre.replace(nombre.length() - 1, nombre.length(), "\"");
                            } else {
                                // Comillas ("), inicio o fin ?
                                if (nombre.length() > 0) {
                                    // Fin del nombre
                                    siguienteEstado = Estado.LEYENDO_SEPARADOR;
                                    estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                                }
                            }
                        } else {
                            // Contenido...
                            nombre.append((char) caracter);
                        }
                        break;

                    case LEYENDO_SEPARADOR:
                        if (caracter == 0x003A) {
                            // Dos puntos (:)...
                            valor.setLength(0);
                            primerCaracter = true;
                            siguienteEstado = Estado.LEYENDO_VALOR;
                            estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                        } else {
                            // Error...
                            Log.e(GC_JSON.class.getName(),
                                    "Error cadena JSON, falta separador de nombre y valor (:)");
                            return null;
                        }
                        break;

                    case LEYENDO_VALOR:
                        if (primerCaracter) {
                            // Definir el tipo del valor: dato primitivo, arreglo u objeto
                            primerCaracter = false;
                            if (caracter == 0x007B) {
                                // Objeto: {
                                tipoValor = TipoValor.OBJECT;
                                if (primerNivel) {
                                    valor.append((char) 0x007B);
                                    cuentaInicio = 1;
                                    caracterInicio = 0x007B;
                                    caracterFin = 0x007D;
                                } else {
                                    objetoJSON = crearObjeto(primerNivel, stream);
                                    if (objetoJSON == null) return null;
                                    siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
                                    estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                                }
                            } else if (caracter == 0x005B) {
                                // Arreglo: [
                                tipoValor = TipoValor.ARRAY;
                                if (primerNivel) {
                                    valor.append((char) 0x005B);
                                    cuentaInicio = 1;
                                    caracterInicio = 0x005B;
                                    caracterFin = 0x005D;
                                } else {
                                    arregloJSON = crearArreglo(primerNivel, stream);
                                    if (arregloJSON == null) return null;
                                    siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
                                    estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                                }
                            } else if (caracter == 0x0022) {
                                // Cadena: "
                                tipoValor = TipoValor.STRING;
                            } else if (caracter == 0x0074 || caracter == 0x0066) {
                                // Boolean: t(rue) / f(alse)
                                tipoValor = TipoValor.BOOLEAN;
                                valor.append((char) caracter);
                                siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
                                estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                            } else if (caracter == 0x006E) {
                                // Nulo: n
                                tipoValor = TipoValor.NULL;
                                valor.append((char) caracter);
                                siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
                                estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                            } else {
                                // Número
                                tipoValor = TipoValor.NUMBER;
                                valor.append((char) caracter);
                                siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
                                estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                            }
                        } else {
                            // En este punto se está leyendo un String o, si solo se decodifica
                            // el primer nivel, una sección del JSON (sin decodificar)
                            if (cuentaInicio > 0) {
                                // Arreglo u objeto JSON (sin decodificar)
                                valor.append((char) caracter);
                                if (caracter == caracterInicio) cuentaInicio++;
                                if (caracter == caracterFin) cuentaInicio--;
                                if (cuentaInicio == 0) {
                                    // Llegó caracter final
                                    if (tipoValor == TipoValor.ARRAY)
                                        arregloJSON = valor.toString().getBytes("UTF-8");
                                    else
                                        objetoJSON = valor.toString().getBytes("UTF-8");
                                    siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
                                    estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                                }
                            } else if (caracter == 0x0022) {
                                if (caracterAnterior == 0x005C) {
                                    // Comillas (\") escapadas, cambiar el \ por "
                                    valor.replace(valor.length() - 1, valor.length(), "\"");
                                } else {
                                    // String. Llegó caracter final (")
                                    siguienteEstado = Estado.LEYENDO_SIGUE_FIN;
                                    estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                                }
                            } else {
                                // String, adicionar caracter
                                valor.append((char) caracter);
                            }
                        }
                        break;

                    case LEYENDO_SIGUE_FIN:
                        // Es una coma (,) o una llave cerrada (})
                        if ((caracter == 0x002C) || (caracter == 0x007D)) {
                            // Guardar la pareja nombre - valor
                            if (tipoValor == TipoValor.OBJECT)
                                objeto.put(nombre.toString(), objetoJSON);
                            else if (tipoValor == TipoValor.ARRAY)
                                objeto.put(nombre.toString(), arregloJSON);
                            else
                            	if(valor != null){
                            		objeto.put(nombre.toString(), objetoValor(valor, tipoValor));
                            	}
                            // Definir siguiente acción
                            if (caracter == 0x002C) {
                                // Coma (,) leer la siguiente pareja nombre - valor
                                nombre.setLength(0);
                                siguienteEstado = Estado.LEYENDO_NOMBRE;
                                estado = Estado.SALTANDO_CARACTERES_ESCAPE;
                            } else {
                                // Fin de objeto: }, retornar objeto
                                return (objeto);
                            }
                        } else {
                            // Contenido de un campo numérico, true, false, null
                            valor.append((char) caracter);
                        }
                }
            } while (true);
        } catch (Exception e) {
            // Error...
            Log.e(GC_JSON.class.getName(), Log.getStackTraceString(e));
            return null;
        }

        return (objeto);
    }

    public static  Object parseJSON(BufferedReader stream, boolean primerNivel) throws Exception {
        Object datos;
        int caracter;
        
        // Leer primer caracter, puede ser el Byte Order Mark (BOM)
        // Big endian o Little endian
        do {
        	// Si los datos vienen comprimidos, el read() se ejecuta internamente 2 veces (hay dos stream).
            caracter = stream.read();
        } while (caracter == 0xFEFF || caracter == 0xFFFE);

        // Si hay caracteres de escape saltarlos
        while (caracter == 0x0A || caracter == 0x0D || caracter == 0x20) {
            // Omitir...
            caracter = stream.read();
        }

        // Objeto o arreglo ?
        if (caracter == 0x007B) {
            // Inicio de objeto ({), leerlo
            datos = crearObjeto(primerNivel, stream);
        } else if (caracter == 0x005B) {
            // Inicio de Arreglo: ([), leerlo
            datos = crearArreglo(primerNivel, stream);
        } else {
            // Error...
            Log.e(GC_JSON.class.getName(),
                    "Cadena JSON malformada: el objeto no inicia con { o [");
            throw new Exception("Cadena JSON malformada");
        }

        // Cerrar stream y retornar
        stream.close();
        return (datos);
    }

    public static Object parseJSON(
            DynamicByteBuffer buffer, boolean datosComprimidos, boolean primerNivel) throws Exception {
    	// NOTA: Si los datos vienen comprimidos, sólo serán parseados si se logra descomprimir
        // Abrir stream de datos
        BufferedReader stream;
        if (datosComprimidos) {
            GZIPInputStream gzis = new GZIPInputStream(buffer);
            stream = new BufferedReader(new InputStreamReader(gzis, "UTF-8"));
        } else {
            stream = new BufferedReader(new InputStreamReader(buffer, "UTF-8"));
        }

        // Parsear buffer
        return parseJSON(stream, primerNivel);
    }

    public static Object parseJSON(
            byte[] buffer, boolean datosComprimidos, boolean primerNivel) throws Exception {
    	// NOTA: Si los datos vienen comprimidos, sólo serán parseados si se logra descomprimir
        // Abrir stream de datos
        BufferedReader stream;
        if (datosComprimidos) {
            GZIPInputStream gzis = new GZIPInputStream(new ByteArrayInputStream(buffer));
            stream = new BufferedReader(new InputStreamReader(gzis, "UTF-8"));
        } else {
            stream = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(buffer), "UTF-8"));
        }

        // Parsear buffer
        return parseJSON(stream, primerNivel);
    }

    private enum Estado {
        SALTANDO_CARACTERES_ESCAPE,
        LEYENDO_NOMBRE,
        LEYENDO_SEPARADOR,
        LEYENDO_VALOR,
        LEYENDO_SIGUE_FIN
    }

    private enum TipoValor {
        NONE, OBJECT, ARRAY, STRING, NUMBER, BOOLEAN, NULL
    }
}
