package com.gc.coregestionclientes.novedades;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DynamicByteBuffer extends InputStream {
    private int bytesBuffer = 0;
    private int longitudComando = 0;
    private boolean comandoComprimido = false;

    final Lock mutex = new ReentrantLock();
    final Condition notEmpty = mutex.newCondition();

    public boolean getBytes(SocketChannel cliente) {
        boolean conectado = false;

        mutex.lock();
        try {
            // Leer número de bytes
        	int numeroBytes;
            ByteBuffer bufferLectura = getBuffer();
            while ((numeroBytes = cliente.read(bufferLectura)) > 0) {
                bytesBuffer += numeroBytes;
                if (!bufferLectura.hasRemaining()) {
                    // Se llenó el buffer, actualizarlo
                    bufferLectura = getBuffer();
                }
            }
            
            // Se desbloquea señal de "No vacío" si hay bytes, al igual que si la conexión se cayó
            // para no dejar bloqueado un thread.
            if (bytesBuffer > 0 || numeroBytes == -1) notEmpty.signal();
            
            // Informa al thread si la conexión sigue activa o no.
            if (numeroBytes == -1){
            	conectado = false;
            } else {
            	conectado = true;
            }
        } catch (Exception e) {
            Log.e(DynamicByteBuffer.class.getName(), Log.getStackTraceString(e));
        } finally {
            mutex.unlock();
        }

        return conectado;
    }

    class Fragmento {
        ByteBuffer fragmento = ByteBuffer.allocate(16 * 1024);
        Fragmento siguiente = null;
        boolean modoEscritura = true;
    }
    
    Fragmento primerFragmento, ultimoFragmento;

    public DynamicByteBuffer() {
        // Crear primer y único fragmento
        this.primerFragmento = this.ultimoFragmento = new Fragmento();

        // Cerrar la cola circular
        this.primerFragmento.siguiente = this.primerFragmento;
    }

    public DynamicByteBuffer(int capacidad) {
        Fragmento temporal, fragmento;

        // Validar parámetro
        if (capacidad <= 0) capacidad = 1;

        // Crear el primer fragmento
        this.primerFragmento = this.ultimoFragmento = new Fragmento();

        // Crear y conectar el resto de fragmentos solicitados
        temporal = this.primerFragmento;
        for (int i = 1; i < capacidad; i++) {
            fragmento = new Fragmento();
            temporal.siguiente = fragmento;
            temporal = fragmento;
        }

        // Cerrar la cola circular
        temporal.siguiente = this.primerFragmento;
    }
    
    // Abortar lectura por timeout de recepción
    public void abortarLectura() {
        // Desbloquear thread que espera datos
        try { 
        	mutex.lock();
            notEmpty.signal();
        } finally {
            mutex.unlock();
        }        
    }
    
    // Inicializar variables después de abortar por timeout de recepción
    public void inicializarBuffer() {
        // Clarear variables
        bytesBuffer = 0;
        longitudComando = 0;
        comandoComprimido = false;
        
        // Clarear el buffer dinámico  ???
        primerFragmento.fragmento.clear();
        primerFragmento.modoEscritura = true;
    }

    private int get() {
        // Cambiar primer fragmento a modo lectura
        if (primerFragmento.modoEscritura) {
            primerFragmento.modoEscritura = false;
            primerFragmento.fragmento.flip();
        }

        // Ya no hay más datos en este fragmento ?
        if (!primerFragmento.fragmento.hasRemaining()) {
            // Reiniciar el fragmento actual
            primerFragmento.fragmento.clear();
            primerFragmento.modoEscritura = true;

            // Cambiar al siguiente fragmento y ponerlo en modo lectura
            primerFragmento = primerFragmento.siguiente;
            primerFragmento.modoEscritura = false;
            primerFragmento.fragmento.flip();
        }

        return primerFragmento.fragmento.get() & 0xFF;
    }

    private long getNumber(int numeroBytes) {
        long respuesta = 0;

        for (int i = 0, bits = 8 * numeroBytes; i < numeroBytes; i++) {
            respuesta |= ((get() & 0xFF) << (bits -= 8));
        }

        return(respuesta);
    }

    private int leerLongitudComando() {
        int longitud;

        // Leer longitud del comando
        comandoComprimido = ((get() & 0x02) == 2);
        longitud = get();
        bytesBuffer -= 2;
        if (longitud == 126) {
            // Longitud en los dos (2) siguientes bytes
            longitud = (int) getNumber(2);
            bytesBuffer -= 2;
        } else if (longitud == 127) {
            // Longitud en los ocho (8) siguientes bytes
            longitud = (int) getNumber(8);
            bytesBuffer -= 8;
        }

        return longitud;
    }

    public boolean hayDatos() {
        if (bytesBuffer > 0) {
        	// Obtiene el número de bytes que deben ser leídos.
            longitudComando = leerLongitudComando();
            return true;
        }
        return false;
    }

    public boolean estaComprimido() {
        return comandoComprimido;
    }

    @Override
    public int read() throws IOException {
        int respuesta = -1;

        mutex.lock();
        try {
            if (longitudComando > 0) {
                try {
                	// Si se han leído todos los bytes del BUFFER y 
                	// todavía existen bytes pendientes (longitudComando > 0), bloquea.
                    if (bytesBuffer == 0) {
                    	notEmpty.await();
                    }
                    
                    // Vuelve a verificar si hay bytes pues la conexión pudo haberse caido
                    if (bytesBuffer > 0) {
                        bytesBuffer--;
                        longitudComando--;
                        respuesta = get();
					} else {
                        // La conexión se cae o se aborta por timeout
				        // Al retornar -1 finaliza el stream
                        respuesta = -1;
                        bytesBuffer = -1;
                    }
                } catch (Exception e) {
                    throw new IOException(e.toString());
                }
            }
        } finally {
            mutex.unlock();
        }

        // Retorno...
        return respuesta;
    }

    private ByteBuffer getBuffer() {
        // Último fragmento en modo escritura?
        if (ultimoFragmento.modoEscritura) {
            // Está lleno?
            if (!ultimoFragmento.fragmento.hasRemaining()) {
                // Tomar el siguiente o crear uno nuevo
                if (ultimoFragmento.siguiente == primerFragmento) {
                    // La cola está llena, crear un fragmento nuevo
                    Fragmento fragmento = new Fragmento();
                    ultimoFragmento.siguiente = fragmento;
                    fragmento.siguiente = primerFragmento;
                    ultimoFragmento = fragmento;
                } else {
                    // Siguiente
                    ultimoFragmento = ultimoFragmento.siguiente;
                }
            }
        } else {
            // Modo lectura. Hay datos por leer?
            if (ultimoFragmento.fragmento.hasRemaining()) {
                // Tomar el siguiente o crear uno nuevo
                if (ultimoFragmento.siguiente == primerFragmento) {
                    // La cola está llena, crear un fragmento nuevo
                    Fragmento fragmento = new Fragmento();
                    ultimoFragmento.siguiente = fragmento;
                    fragmento.siguiente = primerFragmento;
                    ultimoFragmento = fragmento;
                } else {
                    // Siguiente
                    ultimoFragmento = ultimoFragmento.siguiente;
                }
            } else {
                // Reiniciar fragmento
                ultimoFragmento.fragmento.clear();
                ultimoFragmento.modoEscritura = true;
            }
        }

        return ultimoFragmento.fragmento;
    }

    private int bytesDisponibles() {
        // Modo escritura o modo lectura?
        if (primerFragmento.modoEscritura) {
            // Cambiar a modo lectura
            primerFragmento.modoEscritura = false;
            primerFragmento.fragmento.flip();
        }

        // Hay datos disponibles en el fragmento actual?
        if (primerFragmento.fragmento.hasRemaining()) {
            return(primerFragmento.fragmento.remaining());
        } else {
            // Reiniciar el fragmento actual
            primerFragmento.fragmento.clear();
            primerFragmento.modoEscritura = true;

            // Aún hay fragmentos?
            if (primerFragmento != ultimoFragmento) {
                // Cambiar al siguiente fragmento
                primerFragmento = primerFragmento.siguiente;

                // Cambiar a modo lectura
                primerFragmento.modoEscritura = false;
                primerFragmento.fragmento.flip();

                return(primerFragmento.fragmento.remaining());
            } else {
                // No hay más datos...
                return(0);
            }
        }
    }

    public int get(byte[] destino, int inicio, int numeroBytes) throws Exception {
        // Salir si los parámetos no son válidos
        boolean parametrosValidos = destino != null
                && inicio >= 0 && inicio < destino.length
                && numeroBytes <= (destino.length - inicio);
        if (!parametrosValidos)
            throw new Exception("Parámetro(s) inválido(s)...");

        // Salir si no hay datos
        int n = bytesDisponibles();
        if (n == 0)
            throw new Exception("No hay datos...");

        // Ciclo mientras el fragmento actual no llena el arreglo
        int bytesLeidos = 0;
        while (n > 0 && n < numeroBytes) {
            primerFragmento.fragmento.get(destino, inicio, n);
            numeroBytes -= n;
            inicio += n;
            bytesLeidos += n;
            n = bytesDisponibles();
        }

        // Completar los bytes solicitados
        if (n > 0) {
            primerFragmento.fragmento.get(destino, inicio, numeroBytes);
            bytesLeidos += numeroBytes;
            return(bytesLeidos);
        } else {
            // Falla, no hay más bytes...
            throw new Exception("Datos insuficientes...");
        }
    }

    public int get(byte[] destino) throws Exception {
        // Salir si los parámetos no son válidos
        if (destino == null)
            throw new Exception("Parámetro(s) inválido(s)...");

        return(get(destino, 0, destino.length));
    }
}
