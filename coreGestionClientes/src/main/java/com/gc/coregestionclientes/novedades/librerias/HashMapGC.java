package com.gc.coregestionclientes.novedades.librerias;

import java.util.HashMap;


public class HashMapGC<K, V> extends HashMap<K, V> {
    public HashMapGC() {
        super();
    }

    public int getInteger(Object key, int valorDefecto) {
        Object temporal;

        if (key != null && (temporal = this.get(key)) != null
                && temporal instanceof Integer)
            return (Integer)temporal;
        return valorDefecto;
    }

    public long getLong(Object key, long valorDefecto) {
        Object temporal;

        if (key != null && (temporal = this.get(key)) != null
                && temporal instanceof Long)
            return (Long)temporal;
        return valorDefecto;
    }

    public float getFloat(Object key, float valorDefecto) {
        Object temporal;

        if (key != null && (temporal = this.get(key)) != null
                && temporal instanceof Float)
            return (Float)temporal;
        return valorDefecto;
    }

    public double getDouble(Object key, double valorDefecto) {
        Object temporal;

        if (key != null && (temporal = this.get(key)) != null
                && temporal instanceof Double)
            return (Double)temporal;
        return valorDefecto;
    }

    public String getString(Object key) {
        Object temporal;

        if (key != null && (temporal = this.get(key)) != null
                && temporal instanceof String
                && ((String) temporal).startsWith("\"")) {

            if (((String) temporal).length() == 2)
                return "";
            else
                return ((String) temporal).substring(1, ((String) temporal).length() - 1);
        }
        return null;
    }

    public Boolean getBoolean(Object key) {
        Object temporal;

        if (key != null && (temporal = this.get(key)) != null
                && temporal instanceof String) {

            if (temporal.equals("true")) return true;
            else if (temporal.equals("false")) return false;
            else return null;
        }
        return null;
    }

    public VectorGC getArray(Object key) {
        Object temporal;

        if (key != null && (temporal = this.get(key)) != null
                && temporal instanceof VectorGC)
            return (VectorGC)temporal;
        return null;
    }

    public HashMapGC getObject(Object key) {
        Object temporal;

        if (key != null && (temporal = this.get(key)) != null
                && temporal instanceof HashMapGC)
            return (HashMapGC)temporal;
        return null;
    }

    public byte[] getRawJSON(Object key) {
        Object temporal;

        if (key != null && (temporal = this.get(key)) != null
                && temporal instanceof byte[])
            return (byte[])temporal;
        return null;
    }
}
