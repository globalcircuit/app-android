package com.gc.coregestionclientes.novedades.librerias;

import java.util.Vector;


public class VectorGC<Object> extends Vector<Object> {
    public VectorGC() {
        super();
    }

    public int getInteger(int lugar, int valorDefecto) {
        Object temporal;

        if ((lugar >= 0 && lugar < this.size())
                && (temporal = this.elementAt(lugar)) != null
                && temporal instanceof Integer)
            return (Integer)temporal;
        return valorDefecto;
    }

    public long getLong(int lugar, long valorDefecto) {
        Object temporal;

        if ((lugar >= 0 && lugar < this.size())
                && (temporal = this.elementAt(lugar)) != null
                && temporal instanceof Long)
            return (Long)temporal;
        return valorDefecto;
    }

    public float getFloat(int lugar, float valorDefecto) {
        Object temporal;

        if ((lugar >= 0 && lugar < this.size())
                && (temporal = this.elementAt(lugar)) != null
                && temporal instanceof Float)
            return (Float)temporal;
        return valorDefecto;
    }

    public double getDouble(int lugar, double valorDefecto) {
        Object temporal;

        if ((lugar >= 0 && lugar < this.size())
                && (temporal = this.elementAt(lugar)) != null
                && temporal instanceof Double)
            return (Double)temporal;
        return valorDefecto;
    }

    public String getString(int lugar) {
        Object temporal;

        if ((lugar >= 0 && lugar < this.size())
                && (temporal = this.elementAt(lugar)) != null
                && temporal instanceof String
                && ((String) temporal).startsWith("\"")) {

            if (((String) temporal).length() == 2)
                return "";
            else
                return ((String) temporal).substring(1, ((String) temporal).length() - 1);
        }
        return null;
    }

    public Boolean getBoolean(int lugar) {
        Object temporal;

        if ((lugar >= 0 && lugar < this.size())
                && (temporal = this.elementAt(lugar)) != null
                && temporal instanceof String) {

            if (temporal.equals("true")) return true;
            else if (temporal.equals("false")) return false;
            else return null;
        }
        return null;
    }

    public VectorGC getArray(int lugar) {
        Object temporal;

        if ((lugar >= 0 && lugar < this.size())
                && (temporal = this.elementAt(lugar)) != null
                && temporal instanceof VectorGC)
            return (VectorGC)temporal;
        return null;
    }

    public HashMapGC getObject(int lugar) {
        Object temporal;

        if ((lugar >= 0 && lugar < this.size())
                && (temporal = this.elementAt(lugar)) != null
                && temporal instanceof HashMapGC)
            return (HashMapGC)temporal;
        return null;
    }

    public byte[] getRawJSON(int lugar) {
        Object temporal;

        if ((lugar >= 0 && lugar < this.size())
                && (temporal = this.elementAt(lugar)) != null
                && temporal instanceof byte[])
            return (byte[])temporal;
        return null;
    }
}
