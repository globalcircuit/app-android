package com.gc.coregestionclientes.novedades;

import java.util.HashMap;

import com.gc.coregestionclientes.Configuracion;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import android.util.SparseArray;

public class BaseDatosTemporal extends SQLiteOpenHelper {
	private static BaseDatosTemporal bdSeguimiento = null;

	// Atributos
	private static String nombreBaseDatos = "seguimiento.db";

	// Constructor
	private BaseDatosTemporal(Context contexto) {
		super(contexto, nombreBaseDatos, null, 1);
	}

	// Singleton
	public static BaseDatosTemporal getInstance(Context contexto) {
		if (bdSeguimiento == null) {
			bdSeguimiento = new BaseDatosTemporal(contexto.getApplicationContext());
		}
		return(bdSeguimiento);
	}

	@Override
	public synchronized void close() {
		super.close();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// Este se llama cuando se crea la base de datos (primera vez)
		db.execSQL("CREATE TABLE IF NOT EXISTS tramas (id INTEGER PRIMARY KEY, trama TEXT);");
		db.execSQL("CREATE TABLE IF NOT EXISTS fotosAdjuntos ("
				+ "id INTEGER PRIMARY KEY, "
				+ "idOrdenServicio INTEGER, "
				+ "idVisita INTEGER, "
				+ "datosImagen TEXT);");
		db.execSQL("CREATE TABLE IF NOT EXISTS parametros (minutosAcumulados INTEGER, timestamp INTEGER);");
		db.execSQL("INSERT INTO parametros VALUES (0,0);");
		db.execSQL("CREATE TABLE IF NOT EXISTS firma ("
				+ "id INTEGER PRIMARY KEY, "
				+ "nombre TEXT, "
				+ "ruta TEXT);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	// Método que permite guardar los datos de una imagen.
	public void guardarDatosImagen(int id, int idOrdenServicio, int idVisita, String datosImagen) {
		Log.i(Configuracion.tagLog, "Guardando datosImagen: " + id);
		SQLiteStatement sqliteStatement = null;
		try {
			// Crear registro en la base de datos
			String sql = "INSERT INTO fotosAdjuntos VALUES(?,?,?,?);";
			sqliteStatement = this.getWritableDatabase().compileStatement(sql);
			sqliteStatement.clearBindings();
			sqliteStatement.bindLong(1, id);
			sqliteStatement.bindLong(2, idOrdenServicio);
			sqliteStatement.bindLong(3, idVisita);
			sqliteStatement.bindString(4, datosImagen);
			sqliteStatement.executeInsert();
		} catch(Exception e) {
			Log.e(Configuracion.tagLog, "guardarDatosImagen, error: " + e.toString());
		} finally {
			sqliteStatement.close();
		}
	}

	public void guardarDatosFirma(int id, String nombre, String ruta) {
		Log.i(Configuracion.tagLog, "Guardando datosFirma: " + id);
		SQLiteStatement sqliteStatement = null;
		try {
			// Crear registro en la base de datos
			String sql = "INSERT INTO firma VALUES(?,?,?);";
			sqliteStatement = this.getWritableDatabase().compileStatement(sql);
			sqliteStatement.clearBindings();
			sqliteStatement.bindLong(1, id);
			sqliteStatement.bindString(2, nombre);
			sqliteStatement.bindString(3, ruta);
			sqliteStatement.executeInsert();
			Log.e(Configuracion.tagLog, "Guardarda Firma");

		} catch(Exception e) {
			Log.e(Configuracion.tagLog, "guardarfirma, error: " + e.toString());
		} finally {
			sqliteStatement.close();
		}
	}

	// Método que consulta los datos de imagen pendiente por enviar
	public HashMap<String, String> consultarImagenPendiente() {
		HashMap<String, String> datos = null;
		Cursor cursor = null;
		try {
			// Ejecutar query
			cursor = this.getWritableDatabase().rawQuery(
					"SELECT id, idOrdenServicio, idVisita, datosImagen FROM fotosAdjuntos ORDER BY id LIMIT 1", null);
			if (cursor.moveToFirst()) {
				datos = new HashMap<String, String>();

				// Extrae la trama
				int id = cursor.getInt(0);
				int idOrdenServicio = cursor.getInt(1);
				int idVisita = cursor.getInt(2);
				String datosImagen = cursor.getString(3);

				// Adiciona los datos al objeto que será retornado
				datos.put("id", String.valueOf(id));
				datos.put("idOrdenServicio", String.valueOf(idOrdenServicio));
				datos.put("idVisita", String.valueOf(idVisita));
				datos.put("datosImagen", datosImagen);
			}
		} catch(Exception e) {
			Log.e(Configuracion.tagLog, "consultarImagenPendiente, error: " + e.toString());
		} finally {
			if (cursor != null && !cursor.isClosed()) cursor.close();
		}

		// Retorno...
		return datos;
	}

	// Método que consulta los datos de firma pendiente por enviar
	public HashMap<String, String> consultarFirma() {
		HashMap<String, String> datos = null;
		Cursor cursor = null;

		Log.d("sebas", "Entré a consultar una firma");
		try {
			// Ejecutar query
			cursor = this.getWritableDatabase().rawQuery(
					"SELECT id, nombre, ruta FROM firma ORDER BY id", null);
			if (cursor.moveToFirst()) {
				// Se recorren los registros encontrados
				int numeroFirmas = cursor.getCount();
				Log.d("sebas", "Existen: " + numeroFirmas +" firmas");
			}
		} catch(Exception e) {
			Log.e(Configuracion.tagLog, "consultarFirmaPendiente, error: " + e.toString());
		} finally {
			if (cursor != null && !cursor.isClosed()) cursor.close();
		}

		try {
			// Ejecutar query
			cursor = this.getWritableDatabase().rawQuery(
					"SELECT id, nombre, ruta FROM firma ORDER BY id LIMIT 1", null);
			if (cursor.moveToFirst()) {
				datos = new HashMap<String, String>();

				// Extrae la trama
				int id = cursor.getInt(0);
				String nombre = cursor.getString(1);
				String ruta = cursor.getString(2);

				// Adiciona los datos al objeto que será retornado
				datos.put("id", String.valueOf(id));
				datos.put("nombre", nombre);
				datos.put("ruta", ruta);
			}
		} catch(Exception e) {
			Log.e(Configuracion.tagLog, "consultarFirmaPendiente, error: " + e.toString());
		} finally {
			if (cursor != null && !cursor.isClosed()) cursor.close();
		}
		Log.d("sebas", "Salí de a consultar la firma");
		// Retorno...
		return datos;
	}

	// Método que permite eliminar los datos de una imagen.
	public void eliminarFotoAdjunto(int id) {
		Log.i(Configuracion.tagLog, "Eliminando fotoAdjunto: " + id);
		try {
			this.getWritableDatabase().execSQL("DELETE FROM fotosAdjuntos WHERE id = " + id);
		} catch(Exception e) {
			Log.e(Configuracion.tagLog, "eliminarFotoAdjunto, error: " + e.toString());
		}

		// TODO:
		int numeroFotos = consultarNumeroFotos();
		Log.i(Configuracion.tagLog, "Se elimino y hay FotoAdjunto ??" +
				(numeroFotos != 0 ? numeroFotos : "nada"));
	}

	// Método que permite eliminar los datos de una imagen.
	public void eliminarFirma(int id) {
		Log.i(Configuracion.tagLog, "Eliminando firmaAdjunto: " + id);
		Cursor cursor = null;
		Log.d("sebas", "Entré a eliminar la firma: " + id);
		try {
			// Ejecutar query
			cursor = this.getWritableDatabase().rawQuery(
					"SELECT id, nombre, ruta FROM firma ORDER BY id", null);
			if (cursor.moveToFirst()) {
				// Se recorren los registros encontrados
				int numeroFirmas = cursor.getCount();
				Log.d("sebas", "Existen: " + numeroFirmas +" firmas");
			}
		} catch(Exception e) {
			Log.e(Configuracion.tagLog, "consultarFirmaPendiente, error: " + e.toString());
		} finally {
			if (cursor != null && !cursor.isClosed()) cursor.close();
		}

		try {
			this.getWritableDatabase().execSQL("DELETE FROM firma WHERE id = " + id);
		} catch(Exception e) {
			Log.e(Configuracion.tagLog, "eliminarFirma, error: " + e.toString());
		}
	}

	// Método que consulta el número de fotos adjuntos pendientes
	public int consultarNumeroFotos() {
		int numeroFotos = 0;
		Cursor cursor = null;
		try {
			// Ejecutar query
			cursor = this.getWritableDatabase().rawQuery("SELECT count(*) FROM fotosAdjuntos", null);
			if (cursor.moveToFirst()) {
				// Extrae el total de registros
				numeroFotos = cursor.getInt(0);
			}
		} catch(Exception e) {
			Log.e(Configuracion.tagLog, "consultarNumeroFotos, error: " + e.toString());
		} finally {
			if (cursor != null && !cursor.isClosed()) cursor.close();
		}

		// Retorno...
		return numeroFotos;
	}

	// Método que permite guardar una trama.
	public void guardarTrama(int id, String trama) {
		Log.i(Configuracion.tagLog, "Guardando trama: " + id);
		SQLiteStatement sqliteStatement = null;
		try {
			// Crear registro para la trama en la base de datos
			String sql = "INSERT INTO tramas VALUES(?,?);";
			sqliteStatement = this.getWritableDatabase().compileStatement(sql);
			sqliteStatement.clearBindings();
			sqliteStatement.bindLong(1, id);
			sqliteStatement.bindString(2, trama);
			sqliteStatement.executeInsert();
		} catch(Exception e) {
			Log.e(Configuracion.tagLog, "guardarTrama, error: " + e.toString());
		} finally {
			sqliteStatement.close();
		}
	}

	// Método que consulta todas las tramas en la base de datos.
	public SparseArray<String> consultarTramas() {
		SparseArray<String> tramas = null;
		Cursor cursor = null;
		try {
			// Ejecutar query
			cursor = this.getWritableDatabase().rawQuery("SELECT id, trama FROM tramas ORDER BY id", null);
			if (cursor.moveToFirst()) {
				// Se recorren los registros encontrados
				tramas = new SparseArray<String>();
				do {
					// Extrae la trama y la adiciona al arreglo
					int id = cursor.getInt(0);
					String trama = cursor.getString(1);
					tramas.append(id, trama);
				} while(cursor.moveToNext());
			}
		} catch(Exception e) {
			Log.e(Configuracion.tagLog, "consultarTramas, error: " + e.toString());
		} finally {
			if (cursor != null && !cursor.isClosed()) cursor.close();
		}

		// Retorno...
		return tramas;
	}

	// Método que consulta todas las tramas en la base de datos.
	public HashMap<String, String> consultarTramasPendientes() {
		HashMap<String, String> datos = null;
		StringBuilder arregloTramas = new StringBuilder();
		Cursor cursor = null;
		try {
			// Ejecutar query
			cursor = this.getWritableDatabase().rawQuery("SELECT id, trama FROM tramas ORDER BY id", null);
			if (cursor.moveToFirst()) {
				// Se recorren los registros encontrados
				int numeroTramas = cursor.getCount();
				int id = -1;
				datos = new HashMap<String, String>();
				do {
					// Extrae la trama
					id = cursor.getInt(0);
					String trama = cursor.getString(1);

					// Adiciona la trama al arreglo
					if (arregloTramas.length() > 0) arregloTramas.append(", ");
					arregloTramas.append(trama);
				} while(cursor.moveToNext());

				// Adiciona los datos al objeto que será retornado
				datos.put("numeroTramas", String.valueOf(numeroTramas));
				datos.put("ultimoId", String.valueOf(id));
				arregloTramas.insert(0, "[").append("]");
				datos.put("tramas", arregloTramas.toString());
			}
		} catch(Exception e) {
			Log.e(Configuracion.tagLog, "consultarTramasPendientes, error: " + e.toString());
		} finally {
			if (cursor != null && !cursor.isClosed()) cursor.close();
		}

		// Retorno...
		return datos;
	}

	// Método que permite eliminar una trama.
	public void eliminarTramas(int id) {
		Log.i(Configuracion.tagLog, "Eliminando trama: " + id);
		try {
			this.getWritableDatabase().execSQL("DELETE FROM tramas WHERE id <= " + id);
		} catch(Exception e) {
			Log.e(Configuracion.tagLog, "eliminarTrama, error: " + e.toString());
		}

		// TODO: 
		Log.i(Configuracion.tagLog, "Se elimino y hay tramas ??" +
				(consultarTramas() != null ? consultarTramas().size() : "nada"));
	}

	public int consultarMinutosAcumulados() {
		int minutosAcumulados = 0;
		Cursor cursor = null;
		try {
			// Ejecutar query
			cursor = this.getWritableDatabase().rawQuery("SELECT minutosAcumulados FROM parametros", null);
			if (cursor.moveToFirst()) {
				minutosAcumulados = cursor.getInt(0);
			}
		} catch(Exception e) {
			Log.e(Configuracion.tagLog, "consultarMinutosAcumulados, error: " + e.toString());
		} finally {
			if (cursor != null && !cursor.isClosed()) cursor.close();
		}

		// Retorno...
		return minutosAcumulados;
	}

	public long consultarTimeStamp() {
		long timestamp = 0;
		Cursor cursor = null;
		try {
			// Ejecutar query
			cursor = this.getWritableDatabase().rawQuery("SELECT timestamp FROM parametros", null);
			if (cursor.moveToFirst()) {
				timestamp = cursor.getLong(0);
			}
		} catch(Exception e) {
			Log.e(Configuracion.tagLog, "consultarTimeStamp, error: " + e.toString());
		} finally {
			if (cursor != null && !cursor.isClosed()) cursor.close();
		}

		// Retorno...
		return timestamp;
	}

	public void actualizarParametros(int minutosAcumulados) {
		try {
			this.getWritableDatabase().execSQL(
					"UPDATE parametros SET " +
							"minutosAcumulados = " + minutosAcumulados + ", " +
							"timestamp = " + System.currentTimeMillis() + ";");
		} catch(Exception e) {
			Log.e(Configuracion.tagLog, "actualizarParametros, error: " + e.toString());
		}
	}
}