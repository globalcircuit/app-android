package com.gc.coregestionclientes;

import java.io.File;
import java.util.Vector;

import org.json.JSONException;

import android.content.Context;
import android.os.Environment;
import android.util.SparseArray;

import com.gc.coregestionclientes.accesodatos.CustomSQLiteRoutines;
import com.gc.coregestionclientes.accesodatos.SQLiteRoutines;
import com.gc.coregestionclientes.librerias.JSONManager;
import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.objetos.Cliente;
import com.gc.coregestionclientes.objetos.Dispositivo;
import com.gc.coregestionclientes.objetos.Usuario;

public class Configuracion {
	// Atributos
    private Context contexto;
    private String tipoAplicacion;
    private String nombreBasePrincipal;
    private String nombreBaseSecundaria;
	public static String directorioDB = Environment.getExternalStorageDirectory() + 
			File.separator + "GC" + File.separator + "databases" + File.separator;
    
	// BANDERA IMPORTANTE
    public static boolean utilizarWEBSOCKET = true;
    
	// Parametros Estaticos
	public final static String tagLog = "CORE_GESTION_CLIENTES";
	public final static String GESTION_VISITAS_APP = "GESTION_VISITAS_APP";
	public final static String ORDENES_SERVICIO_APP = "ORDENES_SERVICIO_APP";
	public final static String ENCUESTAS_APP = "ENCUESTAS_APP";
   
    // Tools
    private ToolsApp toolsApp = new ToolsApp();
    
    // Objeto Dispositivo
    private Dispositivo dispositivo;
    
	// Objeto de Sesión de Usuario
	private boolean usrio_rgstrdo = false;
	private Usuario usuario;
	
	// Objeto de Sesion de Cliente
	private boolean prsncial = false;
	private Cliente clienteActivo;
    
    // Proveedor de Memoria
	private SparseArray<Vector<?>> memoria = new SparseArray<Vector<?>>();
    
	// Objetos de acceso a SQlite
	private SQLiteRoutines sqliteRoutines;
	
	// Objetos de acceso a SQlite ADICIONAL (Dependiendo del cliente)
	private CustomSQLiteRoutines customSqliteRoutines;
	
	// Variables auxiliares
    public boolean rcprcion_cntxto_actvdades;
	
    public Configuracion(Context contexto, String tipoAplicacion, Dispositivo dispositivo, 
    		SQLiteRoutines sqliteRoutines, CustomSQLiteRoutines customSqliteRoutines,
    		String nombreBasePrincipal, String nombreBaseSecundaria) {
    	this.contexto = contexto;
    	this.tipoAplicacion = tipoAplicacion;
    	this.dispositivo = dispositivo;
    	this.sqliteRoutines = sqliteRoutines;
    	this.customSqliteRoutines = customSqliteRoutines;
    	this.nombreBasePrincipal = nombreBasePrincipal;
    	this.nombreBaseSecundaria = nombreBaseSecundaria;
    }
    
	/**
	 * @return the tipoAplicacion
	 */
	public String getTipoAplicacion() {
		return tipoAplicacion;
	}

	/**
	 * @param tipoAplicacion the tipoAplicacion to set
	 */
	public void setTipoAplicacion(String tipoAplicacion) {
		this.tipoAplicacion = tipoAplicacion;
	}
	
	/**
	 * @return the nombreBasePrincipal
	 */
	public String getNombreBasePrincipal() {
		return nombreBasePrincipal;
	}

	/**
	 * @return the nombreBaseSecundaria
	 */
	public String getNombreBaseAdicional() {
		return nombreBaseSecundaria;
	}

	/**
	 * @param nombreBasePrincipal the nombreBasePrincipal to set
	 */
	public void setNombreBasePrincipal(String nombreBasePrincipal) {
		this.nombreBasePrincipal = nombreBasePrincipal;
	}

	/**
	 * @param nombreBaseSecundaria the nombreBaseSecundaria to set
	 */
	public void setNombreBaseSecundaria(String nombreBaseSecundaria) {
		this.nombreBaseSecundaria = nombreBaseSecundaria;
	}

	/**
	 * @return the dispositivo
	 */
	public Dispositivo getDispositivo() {
		return dispositivo;
	}

	
	public Context getContexto() {
		return contexto;
	}

	public void setContexto(Context contexto) {
		this.contexto = contexto;
	}

	/**
	 * @return the usrio_rgstrdo
	 */
	public boolean isUsuarioRegistrado() {
		return usrio_rgstrdo;
	}

	/**
	 * @param usrio_rgstrdo the usrio_rgstrdo to set
	 */
	public void setUsuarioRegistrado(boolean usrio_rgstrdo) {
		this.usrio_rgstrdo = usrio_rgstrdo;
	}
	
	/**
	 * @return the ssion_usrio
	 */
	public Usuario getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the ssion_usrio to set
	 */
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	/**
	 * @return the prsncial
	 */
	public boolean isPresencial() {
		return prsncial;
	}

	/**
	 * @param prsncial the prsncial to set
	 */
	public void setPresencial(boolean prsncial) {
		this.prsncial = prsncial;
	}

	/**
	 * @return the clnte_actvo
	 */
	public Cliente getClienteActivo() {
		return clienteActivo;
	}

	/**
	 * @param clnte_actvo the clnte_actvo to set
	 */
	public void setClienteActivo(Cliente clnte_actvo) {
		this.clienteActivo = clnte_actvo;
	}
	
	/**
	 * @return the prvdor_mmria
	 */
	public Vector<?> getMemoria(int punteroMemoria) {
		return memoria.get(punteroMemoria);
	}

	/**
	 * @param prvdor_mmria the prvdor_mmria to set
	 */
	public void setMemoria(int punteroMemoria, Vector<?> prvdor_mmria) {
		this.memoria.append(punteroMemoria, prvdor_mmria);
	}

	// Permite limpiar el objeto que provee de memoria
	public void limpiarMemoria() {
		this.memoria.clear();
	}

	/**
	 * @return the sqliteRoutines
	 */
	public SQLiteRoutines getSqliteRoutines() {
        // http://developer.android.com/resources/articles/avoiding-memory-leaks.html)
        if (sqliteRoutines == null) {
        	sqliteRoutines = new SQLiteRoutines(contexto, nombreBasePrincipal);
        }
        
        return(sqliteRoutines);
    }

	/**
	 * @param sqliteRoutines the sqliteRoutines to set
	 */
	public void setSqliteRoutines(SQLiteRoutines sqliteRoutines) {
		this.sqliteRoutines = sqliteRoutines;
	}
	
	/**
	 * @return the rcprcion_cntxto
	 */
	public boolean isRecuperacionContexto() {
		return rcprcion_cntxto_actvdades;
	}

	/**
	 * @param rcprcion_cntxto the rcprcion_cntxto to set
	 */
	public void setRecuperacionContexto(boolean rcprcion_cntxto) {
		this.rcprcion_cntxto_actvdades = rcprcion_cntxto;
	}

	/*******************************************/
	// Métodos para el registro de Usuario
	/*******************************************/
	
	// Guarda los datos de Usuario en Sqlite
	public void guardarDatosUsuario(){
		this.sqliteRoutines.routine_guardarDatosUsuario(this.usrio_rgstrdo, this.usuario);
	}
	
	// Actualiza los datos de usuario en Sqlite
	public void actualizarDatosUsuario(int mdo_oprcion){
		switch (mdo_oprcion) {
			// Actualiza los datos de usuario en base de datos
			case 1:
				this.sqliteRoutines.routine_actualizarDatosUsuario(this.usrio_rgstrdo, this.usuario);
				break;
			// Desregistra el usuario
			case 2:
				this.sqliteRoutines.routine_desregistrarUsuario();
				break;
		}
	}
	
	// Realiza la asignación de usuario desde la Web
	public String asignarUsuarioWeb(JSONManager jsonManager, Context context){
		String mnsje_alrta = null;
		try {
			// Extrae los parametros del JSON				
			int id_clnte_global = Integer.parseInt(jsonManager.getValorByKey(0, 0, "IdClienteGlobal"));
			String nmbres_clnte_global = jsonManager.getValorByKey(0, 0, "NombresClienteGlobal");
			String sdnmo = jsonManager.getValorByKey(0, 0, "Seudonimo");
			boolean bd_adcnal = toolsApp.bitToBoolean(Integer.parseInt(jsonManager.getValorByKey(0, 0, "BDAdicional")));
			int id_usrio = Integer.parseInt(jsonManager.getValorByKey(0, 0, "IdUsuario"));
			String nmbres_usrio = jsonManager.getValorByKey(0, 0, "NombresUsuario");
			String cdgo_usrio = jsonManager.getValorByKey(0, 0, "CodigoUsuario");
			boolean sncrnzar_BD = toolsApp.bitToBoolean(Integer.parseInt(jsonManager.getValorByKey(0, 0, "SincronizarBD")));
			boolean sncrnzar_BD_adcnal = toolsApp.bitToBoolean(Integer.parseInt(jsonManager.getValorByKey(0, 0, "SincronizarBDAdicional")));
			String mnsje = jsonManager.getValorByKey(0, 0, "Mensaje");
			int intervaloSeguimiento = Integer.parseInt(jsonManager.getValorByKey(0, 0, "IntervaloSeguimiento"));
			String user = jsonManager.getValorByKey(0, 0, "Login");
			String pass = jsonManager.getValorByKey(0, 0, "Pass");
			boolean bandera_login = toolsApp.bitToBoolean(Integer.parseInt(jsonManager.getValorByKey(0, 0, "BanderaLogin")));
			
			// Verifica existe mensaje y lo muestra
			if(!mnsje.equals("")){
				mnsje_alrta = mnsje;
			}
	
			// Setea los datos de login en el objeto de configuracion
			Usuario usuario = new Usuario(id_clnte_global, nmbres_clnte_global, 
					sdnmo, bd_adcnal, id_usrio, nmbres_usrio, cdgo_usrio, intervaloSeguimiento, user, pass, bandera_login);
			
			usuario.setSincronizarBD(sncrnzar_BD);
			usuario.setSincronizarBDAdicional(sncrnzar_BD_adcnal);
			this.usrio_rgstrdo = true;
			this.usuario = usuario;
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return mnsje_alrta;
	}
	
	// Realiza la asignación de usuario desde Sqlite
	public void asignarUsuarioLocal(){
		Vector<Object> dtos = this.sqliteRoutines.routine_extraerDatosUsuario();
		if(!dtos.isEmpty()){
			this.usrio_rgstrdo = (Boolean) dtos.elementAt(0);
			this.usuario = (Usuario) dtos.elementAt(1);
		}
	}

	/*******************************************/
	// Métodos relacionadas con los manejadores
	// de la BD personalizada.
	/*******************************************/

	/**
	 * @return the customSqliteRoutines
	 */
	public CustomSQLiteRoutines getCustomSqliteRoutines() {
        // http://developer.android.com/resources/articles/avoiding-memory-leaks.html)
        if (customSqliteRoutines == null) {
        	customSqliteRoutines = new CustomSQLiteRoutines(contexto, nombreBaseSecundaria);
        }
        
        return(customSqliteRoutines);
    }	
}
