package com.gc.coregestionclientes.interfaces;

public interface OnTaskCompleted{
    void onTaskCompleted(String accion, Object objto, String... prmtros_callback);
}
