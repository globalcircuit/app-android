package com.gc.coregestionclientes.accesodatos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.gc.coregestionclientes.Configuracion;
import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.objetos.Campo;
import com.gc.coregestionclientes.objetos.Cliente;
import com.gc.coregestionclientes.objetos.Formulario;
import com.gc.coregestionclientes.objetos.Talonario;
import com.gc.coregestionclientes.objetos.Usuario;


public class SQLiteRoutines extends SQLiteManager {
	protected ToolsApp toolsApp = new ToolsApp();
	protected static final int VERSION_BASE_DATOS = 1;
	
	// Constructor
	public SQLiteRoutines(Context contexto, String nombreBasePrincipal) {
		super(contexto, Configuracion.directorioDB, nombreBasePrincipal, VERSION_BASE_DATOS);
	}

	/*************************************************/
	// RUTINAS PARA EL MANEJO DE SESION DE USUARIO
	/*************************************************/
	
	// Guarda los datos de usuario
	public void routine_guardarDatosUsuario(boolean usrio_rgstrdo, Usuario usuario) {
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);

		// Crea y ejecuta el query
		String sql = 
			"REPLACE INTO ssion_dspstvo " + 
			"	(fcha_hra_actlzcion, usrio_rgstrdo, id_clnte_global, nmbres_clnte_global, sdnmo, bd_adcnal, id_usrio,"
			+ " nmbres_usrio, cdgo_usrio, user, pass, bandera_login) " +
			"VALUES (" +
				"'" + toolsApp.getDateTimeNOW() + "', " + 
				toolsApp.booleanToBit(usrio_rgstrdo) + ", " + 
				usuario.getIdClienteGlobal() + ", " + 
				"'" + usuario.getNombresClienteGlobal() + "', " + 
				"'" + usuario.getSeudonimo() + "', " + 
				toolsApp.booleanToBit(usuario.isBDAdicional()) + ", " + 
				usuario.getId() + ", " + 
				"'" + usuario.getNombres() + "'," +
				"'" + usuario.getCodigo() + "'," +
				"'" + usuario.getLogin() + "'," +
				"'" + usuario.getPass() + "', " +
				toolsApp.booleanToBit(usuario.getBanderaLogin())  +
			") ";

		ejecutarSQL(sql);
		//sqliteManager.cerrarDB();		
	}

	// Actualiza los datos de usuario
	public void routine_actualizarDatosUsuario(boolean usrio_rgstrdo, Usuario usuario) {
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Crea y ejecuta el query
		String sql = 
			"UPDATE ssion_dspstvo " + 
			"SET " +
			"	fcha_hra_actlzcion = '" + toolsApp.getDateTimeNOW() + "', " +
			"	usrio_rgstrdo = " + toolsApp.booleanToBit(usrio_rgstrdo) + ", " +
			"	id_clnte_global = " + usuario.getIdClienteGlobal() + ", " +
			"	nmbres_clnte_global = '" + usuario.getNombresClienteGlobal() + "', " +
			"	sdnmo = '" + usuario.getSeudonimo() + "', " +
			"	bd_adcnal = '" + toolsApp.booleanToBit(usuario.isBDAdicional()) + "', " +
			"	id_usrio = " + usuario.getId() + ", " +
			"	nmbres_usrio = '" + usuario.getNombres() + "', " +
			"	cdgo_usrio = '" + usuario.getCodigo() + "', " +
			"	intervaloSeguimiento = " + usuario.getIntervaloSeguimiento() + ", " +
			"	user = '" + usuario.getLogin() + "', " +
			"	pass = '" + usuario.getPass() + "', " +
			"	bandera_login = " +  toolsApp.booleanToBit(usuario.getBanderaLogin());
		ejecutarSQL(sql);
		//sqliteManager.cerrarDB();			
	}
	
	// Desregistra usuario
	public void routine_desregistrarUsuario() {
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		String sql = "UPDATE ssion_dspstvo SET usrio_rgstrdo = 0";
		ejecutarSQL(sql);
		//sqliteManager.cerrarDB();	
	}

	// Consulta os datos del usuario
	public Vector<Object> routine_extraerDatosUsuario() {
		Vector<Object> dtos = new Vector<Object>();
		Usuario usuario = null;
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Crea y ejecuta el query
		String sql = 
			"SELECT " +
				"usrio_rgstrdo, id_clnte_global, nmbres_clnte_global, sdnmo, " +
				"bd_adcnal, id_usrio, nmbres_usrio, cdgo_usrio, intervaloSeguimiento, user, pass, bandera_login " +
			"FROM ssion_dspstvo";
		Cursor cursor = ejecutarConsultaSQL(sql, null);
		
		// Evalua si hay datos y los extrae
		if (cursor != null){
			if(cursor.moveToFirst()) {			
				boolean usrio_rgtrdo 		= toolsApp.bitToBoolean(cursor.getInt(0));
				int id_clnte_global 		= cursor.getInt(1);
				String nmbres_clnte_global 	= cursor.getString(2);
				String sdnmo 				= cursor.getString(3);
				boolean bd_adcnal			= toolsApp.bitToBoolean(cursor.getInt(4));
				int id_usrio 				= cursor.getInt(5);
				String nmbres_usrio 		= cursor.getString(6);
				String cdgo_usrio			= cursor.getString(7);
				int intervaloSeguimiento 	=  cursor.getInt(8);
				String login	 = cursor.getString(9);
				String pass	= cursor.getString(10);
				boolean bandera_login =  toolsApp.bitToBoolean(cursor.getInt(11));
				
				usuario = new Usuario(id_clnte_global, nmbres_clnte_global, sdnmo, 
						bd_adcnal, id_usrio, nmbres_usrio, cdgo_usrio, intervaloSeguimiento, login, pass, bandera_login);
				
				// Adiciona los datos que serán retornados
				dtos.add(usrio_rgtrdo);
				dtos.add(usuario);
			}
			cursor.close();
		}
		//sqliteManager.cerrarDB();
		
		// Retorno de resultado
		return dtos;
	}
	
	/*************************************************/
	// RUTINAS PARA EL MANEJO DE CLIENTES
	/*************************************************/

	// Permite crear un cliente local
	public void routine_crearCliente(Cliente clnte) {
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Crea y ejecuta el query
		String sql_insert = 
			"INSERT INTO clntes " +
			"	(id, nmbres, lttud, lngtud, sncrnzdo) " +
			"VALUES " +
			"(" +
				clnte.getId() + ", '" +
				clnte.getNombres() + "', " + 
				clnte.getLatitud() + ", " + 
				clnte.getLongitud() + ", 0" +
			")";
		ejecutarSQL(sql_insert);
		//sqliteManager.cerrarDB();			
	}
	
	// Consulta los clientes que se encuentran pendientes por ser sincronizados
	// mdo_clnslta = 1: Consulta todos los clientes pendientes
	// mdo_clnslta = 2: Consulta el cliente especificiado
	public List<Cliente> routine_consultarClientesNoSincronizados(int mdo_clnslta, Cliente clnte_cnslta){
		List<Cliente> clntes = null;
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Dependiendo del modo de consulta, adiciona la porcion de sql
		String adcion_sql = null;
		switch (mdo_clnslta) {
			case 1:
				adcion_sql = "";
				break;
			case 2:
				adcion_sql = "AND id = " + clnte_cnslta.getId();
				break;
		}
		
		// Crea y ejecuta el query
		String sql_clnte = 
			"SELECT " +
					"id, nmbres, actvo " +
			"FROM clntes " +
			"WHERE " +
				"sncrnzdo = 0 " + adcion_sql + " " +
			"ORDER BY id";
		Cursor cursor_clnte = ejecutarConsultaSQL(sql_clnte, null);
		
		// Evalua si hay datos y los extrae
		if (cursor_clnte != null){
			int nmro_clntes = cursor_clnte.getCount();
			if(cursor_clnte.moveToFirst()) {	
				clntes = new ArrayList<Cliente>(nmro_clntes);
				do {
					// Extrae los datos del cliente
					int id_clnte = cursor_clnte.getInt(0);
					String nmbres = cursor_clnte.getString(1);
					boolean actvo = toolsApp.bitToBoolean(cursor_clnte.getInt(2));
					
					// Crea un objeto cliente
					Cliente clnte = new Cliente(id_clnte, null, nmbres, -1, -1, actvo, null);
				
					// Adiciona el cliente a la lista
					clntes.add(clnte);
				}while(cursor_clnte.moveToNext());
			}
			cursor_clnte.close();
		}
		//sqliteManager.cerrarDB();
		
		// Retorno de resultado
		return clntes;
	}
	
	// Desactiva un cliente
	public void routine_desactivarCliente(int id_clnte) {
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Crea y ejecuta el query
		String sql_update = 
			"UPDATE clntes " +
			"SET " +
			"	actvo = 0, " +
			"	sncrnzdo = 0 " +
			"WHERE" +
			"	id = " + id_clnte;
		ejecutarSQL(sql_update);
		//sqliteManager.cerrarDB();			
	}
	
	// Actualiza la bandera de cliente sincronizado
	public void routine_actualizarBanderaSincronizacionCliente(int id_clnte) {
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Crea y ejecuta el query
		String sql_update = 
			"UPDATE clntes " +
			"SET " +
			"	sncrnzdo = 1 " +
			"WHERE" +
			"	id = " + id_clnte;
		ejecutarSQL(sql_update);
		//sqliteManager.cerrarDB();			
	}

	/*************************************************/
	// RUTINAS PARA EL MANEJO DE FORMULARIOS
	/*************************************************/
	
	// Consulta los formularios TRANSACCIONALES Y SUBFORMULARIOS para las visitas 
	public List<Formulario> routine_consultarFormulariosLocales(String tpo_bsqda_frmlrio) {
		List<Formulario> frmlrios = null; 
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);

		// Crea y ejecuta el query dependiendo del tipo de formulario
		String sql_frmlrios = null;
		if(tpo_bsqda_frmlrio == null){
			// Carga todos los formularios.
			sql_frmlrios = "SELECT id, dscrpcion, tpo, multiple FROM frmlrios";	
		} else {
			// Carga los formularios segun el tipo dado.
			sql_frmlrios = "SELECT id, dscrpcion, tpo, multiple FROM frmlrios WHERE tpo IN ('" + tpo_bsqda_frmlrio + "')";			
		}

		Cursor cursor_frmlrios = ejecutarConsultaSQL(sql_frmlrios, null);
		
		// Evalua si hay datos y los extrae
		if (cursor_frmlrios != null){
			int nmro_frmlrios = cursor_frmlrios.getCount(); 
			if(cursor_frmlrios.moveToFirst()) {
				frmlrios = new ArrayList<Formulario>(nmro_frmlrios);
				do {
					// Extrae los parametros del formulario
					int id_frmlrio = cursor_frmlrios.getInt(0);
					String nmbre_frmlrio = cursor_frmlrios.getString(1);
					String tpo_frmlrio = cursor_frmlrios.getString(2);
					boolean multiple = toolsApp.bitToBoolean(cursor_frmlrios.getInt(3));

					//Consulta talas del formulario.
					List<String[]> tablasAsociadas = consultarTablasAsociadas(id_frmlrio);
					HashMap<Integer,List<String[]>> hashCamposAsociados = consultarCamposAsociados(tablasAsociadas);

					// Crea y ejecuta el query
					String sql_cmpos = 
							"SELECT " +
							"	id, " +
							"	nmbre, " +
							"	mdo_edcion, " +
							"	tpo_dto, " +
							"	tbla_ascda, " +
							"	cmpo_ascdo, " +
							"	dtos_adcnles, " +
							"	oblgtrio, " +
							"	vsble, " +
							"	infrmtvo, " +
							"	ttlza, " +
							"	rsmen, " +
							"	editable " +
							"FROM " +
							"	cmpos_frmlrio " +
							"WHERE " +
							"	id_frmlrio = " + id_frmlrio + " " +
							"ORDER BY orden";
					Cursor cursor_cmpos = ejecutarConsultaSQL(sql_cmpos, null);
					Vector<Campo> cmpos = null;
					
					// Evalua si hay datos y los extrae
					if (cursor_cmpos != null){
						int nmro_cmpos = cursor_cmpos.getCount(); 
						if(cursor_cmpos.moveToFirst()) {
							cmpos = new Vector<Campo>(nmro_cmpos);
							do{
								// Extrae los parametros del campo
								int id_cmpo = cursor_cmpos.getInt(0);
								String nmbre_cmpo = cursor_cmpos.getString(1);
								String mdo_edcion = cursor_cmpos.getString(2);
								String tpo_dto = cursor_cmpos.getString(3);
								//String tbla_ascda = cursor_cmpos.getString(4);
								//String cmpo_ascdo = cursor_cmpos.getString(5);
								String dtos_adcnles = cursor_cmpos.getString(6);
								boolean oblgtrio = toolsApp.bitToBoolean(cursor_cmpos.getInt(7));
								boolean vsble = toolsApp.bitToBoolean(cursor_cmpos.getInt(8));
								boolean infrmtvo = toolsApp.bitToBoolean(cursor_cmpos.getInt(9));
								boolean ttlza = toolsApp.bitToBoolean(cursor_cmpos.getInt(10));
								boolean rsmen = toolsApp.bitToBoolean(cursor_cmpos.getInt(11));
								List<String[]> camposAsociados = (hashCamposAsociados != null ? 
										hashCamposAsociados.get(id_cmpo) : null);
								boolean editable = toolsApp.bitToBoolean(cursor_cmpos.getInt(12));

								// Crea el nuevo objeto Campo
								cmpos.add(
									new Campo(id_cmpo, nmbre_cmpo, mdo_edcion, tpo_dto, 
										dtos_adcnles, 
										oblgtrio, vsble, infrmtvo, ttlza, rsmen, camposAsociados, editable
									)
								);
							}while(cursor_cmpos.moveToNext());
						}
						cursor_cmpos.close();
					}
					
					// Crea el formulario
					frmlrios.add(new Formulario(id_frmlrio, nmbre_frmlrio, null, cmpos, tpo_frmlrio, multiple, tablasAsociadas));
				} while(cursor_frmlrios.moveToNext());
			}
			cursor_frmlrios.close();
		}
		//sqliteManager.cerrarDB();
		
		// Retorno de resultado
		return frmlrios;
	}

	// Método que permite consultar un formulario específico
	public Formulario routine_consultarFormularioLocal(String dscrpcion_frmlrio, String tpo_frmlrio, boolean cerrarDB, int idFormulario) {
		Formulario frmlrio = null;
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);

		// Crea y ejecuta el query
		String sql_frmlrios = 
			"SELECT id, dscrpcion, multiple FROM frmlrios " +
			"WHERE ";
		
		if(dscrpcion_frmlrio != null && !dscrpcion_frmlrio.equals("")) {
			sql_frmlrios += "	dscrpcion = '" + dscrpcion_frmlrio + "' " +
			"	AND tpo = '" + tpo_frmlrio + "'";
		} else if(idFormulario != -1){
			sql_frmlrios += "	id = '" + idFormulario + "' ";
		} else {
			return null;
		}
		Cursor cursor_frmlrios = ejecutarConsultaSQL(sql_frmlrios, null);
		
		// Evalua si hay datos y los extrae
		if (cursor_frmlrios != null){
			if(cursor_frmlrios.moveToFirst()) {
				// Extrae los parametros del formulario
				int id_frmlrio = cursor_frmlrios.getInt(0);
				String nmbre_frmlrio = cursor_frmlrios.getString(1);
				boolean multiple = toolsApp.bitToBoolean(cursor_frmlrios.getInt(2));
				
				// Consulta Tablas asociadas y campos asociados del formulario				
				List<String[]> tablasAsociadas = consultarTablasAsociadas(id_frmlrio);
				HashMap<Integer,List<String[]>> hashCamposAsociados = consultarCamposAsociados(tablasAsociadas);
				
				// Crea y ejecuta el query
				String sql_cmpos = 
						"SELECT " +
							"id, " +
							"nmbre, " +
							"mdo_edcion, " +
							"tpo_dto, " +
							"tbla_ascda, " +
							"cmpo_ascdo, " +
							"dtos_adcnles, " +
							"oblgtrio, " +
							"vsble, " +
							"infrmtvo, " +
							"ttlza, " +
							"rsmen, " +
							"editable " +
						"FROM " +
						"	cmpos_frmlrio " +
						"WHERE " +
						"	id_frmlrio = " + id_frmlrio + " " +
						"ORDER BY orden";
				Cursor cursor_cmpos = ejecutarConsultaSQL(sql_cmpos, null);
				Vector<Campo> cmpos = null;
				
				// Evalua si hay datos y los extrae
				if (cursor_cmpos != null){
					int nmro_cmpos = cursor_cmpos.getCount(); 
					if(cursor_cmpos.moveToFirst()) {
						cmpos = new Vector<Campo>(nmro_cmpos);
						do{
							// Extrae los parametros del campo						
							int id_cmpo = cursor_cmpos.getInt(0);
							String nmbre_cmpo = cursor_cmpos.getString(1);
							String mdo_edcion = cursor_cmpos.getString(2);
							String tpo_dto = cursor_cmpos.getString(3);
							//String tbla_ascda = cursor_cmpos.getString(4);
							//String cmpo_ascdo = cursor_cmpos.getString(5);
							String dtos_adcnles = cursor_cmpos.getString(6);
							boolean oblgtrio = toolsApp.bitToBoolean(cursor_cmpos.getInt(7));
							boolean vsble = toolsApp.bitToBoolean(cursor_cmpos.getInt(8));
							boolean infrmtvo = toolsApp.bitToBoolean(cursor_cmpos.getInt(9));
							boolean ttlza = toolsApp.bitToBoolean(cursor_cmpos.getInt(10));
							boolean rsmen = toolsApp.bitToBoolean(cursor_cmpos.getInt(11));
							List<String[]> camposAsociados = (hashCamposAsociados != null ? 
									hashCamposAsociados.get(id_cmpo) : null);
							boolean editable = toolsApp.bitToBoolean(cursor_cmpos.getInt(12));

							// Crea el nuevo objeto Campo
							Campo cmpo = new Campo(id_cmpo, nmbre_cmpo, mdo_edcion, tpo_dto,  
									dtos_adcnles, oblgtrio, vsble, infrmtvo, ttlza, rsmen, camposAsociados, editable
							);
							
							// Adiciona el campo a la lista
							cmpos.add(cmpo);

						}while(cursor_cmpos.moveToNext());
					}
					cursor_cmpos.close();
				}
				
				// Crea el formulario
				frmlrio = new Formulario(id_frmlrio, nmbre_frmlrio, null, cmpos, tpo_frmlrio, multiple, tablasAsociadas);
			}
			cursor_frmlrios.close();
		}
		
		// Verifica si debe cerrar la base de datos.
		if (cerrarDB){
			//sqliteManager.cerrarDB();
		}
		
		// Retorno de resultado
		return frmlrio;
	}
	
	// Método que permite, consultar las tablas asociadas a un formulario
	public List<String[]> consultarTablasAsociadas(int idFormulario){
		List<String[]> listaTablasAsociadas = null;
		try {
			String sql = "SELECT id, nombreTabla FROM tablasFormulario WHERE idFormulario = " + idFormulario;
			Cursor cursor = ejecutarConsultaSQL(sql, null);
			if(cursor != null){
				if(cursor.moveToFirst()){
					// Recorre los registros y construye la lista de tablas asociadas
					listaTablasAsociadas = new ArrayList<String[]>();
					do {
						String[] arrayString = {cursor.getString(0), cursor.getString(1)};
						listaTablasAsociadas.add(arrayString);
					} while(cursor.moveToNext());
				}
				cursor.close();
			}
			
		} catch(Exception e) {
			Log.e(Configuracion.tagLog, e.getMessage());
		}
		
		// Retorno..
		return listaTablasAsociadas;
	}
 	
	// Método que permite consultar los campos asociados a un campo de formulario
	public HashMap<Integer, List<String[]>> consultarCamposAsociados(List<String[]> tablasAsociadas){
		HashMap<Integer, List<String[]>> hashCamposAsociados = null;	
		
		// Verificar si tablas asociadas tiene información
		if (tablasAsociadas != null && tablasAsociadas.size() > 0) {
			// Si hay tablas asociadas
			try {
				String sql = null;
				
				// Recorre tablas asociadas para armar HashMap de campos asociados
				for(int i=0;i<tablasAsociadas.size();i++){
					sql = 
						"SELECT idCampoFormulario, nombreCampoTabla " +
						"FROM tablasCamposFormulario WHERE idTablaFormulario = " + tablasAsociadas.get(i)[0];
					Cursor cursor = ejecutarConsultaSQL(sql, null);
					if(cursor != null){
						if(cursor.moveToFirst()){
							hashCamposAsociados = new HashMap<Integer, List<String[]>>();
							do {
								int idCampo = cursor.getInt(0);
								String nombreCampo = cursor.getString(1);
								List<String[]> listaCamposAsociados = hashCamposAsociados.get(idCampo);
								
								// Valida si no exite aún la lista de campo en el hash
								if(listaCamposAsociados == null) {
									listaCamposAsociados = new ArrayList<String[]>();
									String[] campoAsociado = {tablasAsociadas.get(i)[1],nombreCampo};
									listaCamposAsociados.add(campoAsociado);
								} else {
									String[] campoAsociado = {tablasAsociadas.get(i)[1],nombreCampo};
									listaCamposAsociados.add(campoAsociado);
								}
								hashCamposAsociados.put(cursor.getInt(0), listaCamposAsociados);
							} while(cursor.moveToNext());
						}
						cursor.close();
					}
				}
			} catch(Exception e) {
				Log.i(Configuracion.tagLog, e.getMessage());
			}
		}
		
		// Retorno... 
		return hashCamposAsociados;
	}
	
	/*************************************************/
	// RUTINAS PARA EL MANEJO DE TALONARIOS
	/*************************************************/
	
	// Permite consultar un talonario
	public Talonario routine_cargarTalonario(String nmbre_tbla,	String nmbre_cmpo, String estdo) {
		Talonario tlnrio = null;
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Crea y ejecuta el query
		String sql_enUso = 
			"SELECT " +
			"	incio, fin, cnsctvo_utlzdo " +
			"FROM tlnrios " +
			"WHERE" +
			"	nmbre_tbla = '" + nmbre_tbla + "' AND " +
			"	nmbre_cmpo = '" + nmbre_cmpo + "' AND " +
			"	estdo = '" + estdo + "'";
		Cursor cursor = ejecutarConsultaSQL(sql_enUso, null);
		
		// Evalua si hay datos y los extrae
		if (cursor != null){
			if(cursor.moveToFirst()) {	
				// Existe un talonario en uso
				int incio = cursor.getInt(0);
				int fin = cursor.getInt(1);
				int cnsctvo_utlzdo = cursor.getInt(2);
				tlnrio = new Talonario(nmbre_tbla, nmbre_cmpo, estdo, incio, fin, cnsctvo_utlzdo);
			}
			cursor.close();
		}
		//sqliteManager.cerrarDB();
		
		// Retorno de resultado
		return tlnrio;
	}

	// Permite actualizar el consecutivo y el estado de un talonario
	public void routine_actualizarTalonario(Talonario tlnrio, String nvo_estdo) {
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Crea y ejecuta el query
		String sql_update = 
			"UPDATE tlnrios " +
			"SET " +
			"	estdo = '" + nvo_estdo + "', " +
			"	cnsctvo_utlzdo = " + tlnrio.getConsecutivoUtilizado() + " "+
			"WHERE" +
			"	nmbre_tbla = '" + tlnrio.getNombreTabla() + "' AND " +
			"	nmbre_cmpo = '" + tlnrio.getNombreCampo() + "' AND " +
			"	estdo = '" + tlnrio.getEstado() + "'";
		ejecutarSQL(sql_update);
		//sqliteManager.cerrarDB();		
	}
	
	// Permite actualizar el consecutivo y el estado de un talonario
	public void routine_eliminarTalonario(Talonario tlnrio) {
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Crea y ejecuta el query
		String sql_delete = 
				"DELETE FROM tlnrios " +
				"WHERE" +
				"	nmbre_tbla = '" + tlnrio.getNombreTabla() + "' AND " +
				"	nmbre_cmpo = '" + tlnrio.getNombreCampo() + "' AND " +
				"	estdo = 'EN USO'";
		ejecutarSQL(sql_delete);
		//sqliteManager.cerrarDB();		
	}

	// Permite guardar un nuevo talonario
	public void routine_guardarNuevoTalonario(String nmbre_tbla,
			String nmbre_cmpo, int incio, int fin, String estdo
	) {
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Crea y ejecuta el query
		String sql_delete = 
				"INSERT INTO tlnrios " +
				"	(nmbre_tbla, nmbre_cmpo, estdo, incio, fin, cnsctvo_utlzdo) " +
				"VALUES " +
				"	('" + nmbre_tbla + "', '" + nmbre_cmpo + "', '" + estdo + "', " + incio + ", " + fin + ", 0)";
		ejecutarSQL(sql_delete);
		//sqliteManager.cerrarDB();	
	}
	
	/****************************************************/
	// RUTINAS PARA EL MANEJO DE HORARIOS DE SEGUIMIENTO
	/****************************************************/	
	public int horaValida() {
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		Cursor cursor = null;
		int rspsta = -1;

		try {
			// Hallar día de la semana y los minutos corridos del día
			Calendar fecha = Calendar.getInstance();
			int dia = fecha.get(Calendar.DAY_OF_WEEK);
			dia = (dia == 1 ? 7 : dia - 1);
			int minutos = fecha.get(Calendar.HOUR_OF_DAY) * 60 + fecha.get(Calendar.MINUTE);

			// Crear query
			String sql =
					"SELECT r.inicio, r.fin FROM horarios h, rangosHoras r " +
					"WHERE h.dia = " + dia + " AND h.idRango = r.id AND " + minutos + " BETWEEN r.inicio AND r.fin";

			// Realizar la consulta
			cursor = ejecutarConsultaSQL(sql, null);
			if(cursor.moveToFirst()) {
				// Está dentro de un rango de actividad
				rspsta = cursor.getInt(1);
			}
		} catch(Exception e) {
			// Error...
			Log.e(Configuracion.tagLog, "horaValida, error: " + e.toString());
		} finally {
			// Cerrar objetos
			if (cursor != null && !cursor.isClosed()) cursor.close();
		}
		//sqliteManager.cerrarDB();	

		// Retorno
		return(rspsta);
	}

	public int leerIntervaloSeguimiento() {
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		Cursor cursor = null;
		int intervaloSeguimiento = -1;
		
		// Crea y ejecuta el query
		try {
			String sql = "SELECT intervaloSeguimiento FROM ssion_dspstvo";
			cursor = ejecutarConsultaSQL(sql, null);
			if(cursor.moveToFirst()) {
				intervaloSeguimiento = cursor.getInt(0);
			}
		} finally {
			if (cursor != null && !cursor.isClosed()) cursor.close();
		}
		//sqliteManager.cerrarDB();
		
		// Retorno 15 por defecto si no hay uno válido. 
		return  (intervaloSeguimiento > 0 ? intervaloSeguimiento : 15);
	}

}