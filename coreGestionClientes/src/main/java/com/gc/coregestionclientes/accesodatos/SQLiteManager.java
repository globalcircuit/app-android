package com.gc.coregestionclientes.accesodatos;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.StringReader;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Base64;
import android.util.Base64InputStream;
import android.util.Log;

import com.gc.coregestionclientes.Configuracion;

public class SQLiteManager extends SQLiteOpenHelper {
	private boolean ocupada = false;
	protected Context contexto;
	private String directorio;
	private String nombreBaseDatos;
	
	// Constructor
	public SQLiteManager(
			Context contexto, String directorio, String nombreBaseDatos, int versionBaseDatos) {
		super(contexto.getApplicationContext(), directorio + nombreBaseDatos, null, versionBaseDatos);
		this.contexto = contexto;
		this.directorio = directorio;
		this.nombreBaseDatos = nombreBaseDatos;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
	
	// Verificar Existencia de archio .db en SD
	public synchronized boolean verificarArchivoDB(){
		boolean bndra_existe_DB = false;
    	File archvo = new File(directorio + nombreBaseDatos);
    	if(archvo.exists()){
    		bndra_existe_DB = true;
    	} 
    	return bndra_existe_DB;
	}
	
	// Eliminar Base de Datos
	public synchronized boolean eliminarArchivoDB(){
		this.close();
		File file_db = new File(directorio + nombreBaseDatos);
		return (file_db.delete());
	}

	// Renombrar Base de Datos
	public synchronized boolean renombrarBaseDatos(){
		this.close();
		//optengo el epoc para renombrar
		Long tsLong = System.currentTimeMillis()/1000;
		String ts = tsLong.toString();
		// busco el archivo en esa dieccion
		File currentFile = new File(directorio + nombreBaseDatos);
		boolean bandera = false;
		if(currentFile.exists()) {

			File newFile = new File(directorio + nombreBaseDatos + ts);


			if (rename(currentFile, newFile)) {
				bandera = true;
				//Success
				Log.e(Configuracion.tagLog, "Guarde la base de datos con otro nombre ");
			} else {
				bandera = false;

				//Fail
				Log.e(Configuracion.tagLog, "Errror al cambiar archivo de la bd ");
			}

		}
		return (bandera);
	}


	private boolean rename(File from, File to) {
		return from.getParentFile().exists() && from.exists() && from.renameTo(to);
	}

	// Ejecutar Query sin resultados
	public synchronized boolean ejecutarSQL(String sql){
		try {
			this.getWritableDatabase().execSQL(sql);
			return true;
		} catch (Exception e) {
			Log.e(Configuracion.tagLog, "Error en ejecutarSQL: " + e.toString() + " SQL: " + sql);
			return false;
		}
	}
	
	// Ejecutar Query con Resultados
	public synchronized Cursor ejecutarConsultaSQL(String sql, String[] argumentos){
		Cursor cursor = null;
		try {
			cursor = this.getReadableDatabase().rawQuery(sql, argumentos);
		} catch (Exception e) {
			Log.e(Configuracion.tagLog, "Error en ejecutarConsultaSQL: " + e.toString() + " SQL: " + sql);
		}
		return cursor;
	}
	
	// Setea la bandera "ocupada"
	public synchronized void baseOcupada(boolean ocupada) {
		this.ocupada = ocupada;
	}
	
	// Obtiene la bandera "ocupada"
	public synchronized boolean estaBaseOcupada() {
		return this.ocupada;
	}
	
	// Actualizar archivo de base de datos
    public synchronized void actualizarArchivo(String base64) throws Exception {
        Base64InputStream base64InputStream = null;
        FileOutputStream fileOutputStream = null;
        
        // Cierra la base de datos       
		this.close();
		
		// Actualizar archivo
        try {
            // Abrir archivo, si existe borrarlo
            File archivo = new File(directorio + nombreBaseDatos);
            if (!archivo.exists() || archivo.delete()) {
                // Abrir stream de entrada (base64) y stream de salida (archivo)
                base64InputStream = new Base64InputStream(
                        new ByteArrayInputStream(base64.getBytes()), Base64.DEFAULT);
                fileOutputStream = new FileOutputStream(archivo, true);

                // Escribir bytes al archivo
                int numeroBytes;
                byte[] buffer = new byte[4096];
                while ((numeroBytes = base64InputStream.read(buffer)) > 0)
                    fileOutputStream.write(buffer, 0, numeroBytes);
                fileOutputStream.flush();
                Log.i(Configuracion.tagLog, "actualizarArchivo: " + nombreBaseDatos);
            } else {
                throw new Exception("Archivo inválido o no se pudo borrar " + nombreBaseDatos);
            }
        } catch (Exception e) {
            throw new Exception(e.toString());
        } finally {
            try {
                if (fileOutputStream != null) fileOutputStream.close();
                if (base64InputStream != null) base64InputStream.close();
            } catch (Exception e) {
                Log.e(Configuracion.tagLog, Log.getStackTraceString(e));
            }
        }
    }
    
    // Actualizar novedades
    public void actualizarBaseDatos(String datos) {
        String query;
        //SQLiteDatabase db = null;
        try {
            //db = this.getWritableDatabase();
            BufferedReader reader = new BufferedReader(new StringReader(datos));
            Log.i(Configuracion.tagLog, "Ejecutando queries en " + nombreBaseDatos + "...");
            //db.beginTransaction();
            while ((query = reader.readLine()) != null) {
                try {
                    //db.execSQL(query);
                	ejecutarSQL(query);
                    Log.i(Configuracion.tagLog, "Query: " + query);
                } catch (Exception e) {
                    Log.e(Configuracion.tagLog, "actualizarBaseDatos bad query: " + query);
                }
            }
            //db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e(Configuracion.tagLog, Log.getStackTraceString(e));
        } finally {
           /* if (db != null) {
                db.endTransaction();
                db.close();
            }*/
        }
    }
}
