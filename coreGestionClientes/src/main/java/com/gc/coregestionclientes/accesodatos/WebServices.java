package com.gc.coregestionclientes.accesodatos;


import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.Vector;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.gc.coregestionclientes.Configuracion;
import com.gc.coregestionclientes.librerias.SOAP11Client;
import com.gc.coregestionclientes.librerias.ToolsApp;



public class WebServices {
	ToolsApp toolsApp = new ToolsApp();
	
	/******************************************************/
	
	// Método que realiza el login y la verificación del dispositivo
	public Hashtable<String, String> realizarLogin(String id_dspstvo, String vrsion, String token,String email){
		// Creates the request
		Vector<String> prmtros = new Vector<String>();
		prmtros.addElement("http://23.253.57.17/GestionClientes/WebServices/VerificarDispositivoTokenEmail.php?wsdl");
		prmtros.addElement("urn:GlobalCircuit:GestionClientes");
		prmtros.addElement("VerificarDispositivo");
		prmtros.addElement("id_dspstvo");
		prmtros.addElement(id_dspstvo);
		prmtros.addElement("vrsion");
		prmtros.addElement(vrsion);
		prmtros.addElement("mdo_oprcion");
		prmtros.addElement("1");
		prmtros.addElement("token");
		prmtros.addElement(token);
		prmtros.addElement("email");
		prmtros.addElement(email);

		// Creates a Soap Client object
		SOAP11Client client = new SOAP11Client(prmtros, 15, 15);
		Hashtable<String, String> rspsta = client.CallWebService();
    	return rspsta;
	}
	
	// Método que analiza la respuesta del login
	public Vector<Object> verificaRealizarLogin(Hashtable<String, String> response, Context s){
		Vector<Object> dtos_rspsta = null;
		JSONObject json_object = null;
		
		// Verificar si el consumo del servicio fue exitoso
		if(response.get("ConsumioServicio").equals("exito")) {
			// Se consumió con exito, 
			// Inicializa el objto de rspsta
			dtos_rspsta = new Vector<Object>();
			dtos_rspsta.add(response.get("Resultado"));

			// Verificar la respuesta del servidor
			if(response.get("Resultado").equals("OK")) {
		    	// Extrae los datos
				String dtos_cdfcdos = response.get("DatosJSON");
				byte[] dtos_dcdfcdos = Base64.decode(dtos_cdfcdos, Base64.DEFAULT);
				try {
					String json_string = new String(dtos_dcdfcdos, "ISO-8859-1");
					json_object = new JSONObject(json_string);
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				dtos_rspsta.add(json_object);
			} else {
				// TODO: manejar el error de verificación de equipo e inhabilitar el teléfono
		    	// Error.
				//toolsApp.showLongMessage(response.get("Mensaje"), contexto);
				dtos_rspsta.add(response.get("Mensaje"));
			}
		}else{
	    	// Fallo.
			Log.i(Configuracion.tagLog, "WS - Timeout");
		}
		return dtos_rspsta;
	} 

	/******************************************************/
	
	// Método que permite consumir el WS para actualizar la bandera de sincronicación
	public Hashtable<String, String> actualizarBanderaSincronizacion(String id_dspstvo, String vrsion){
		
		// Creates the request
		Vector<String> prmtros = new Vector<String>();
		prmtros.addElement("http://23.253.57.17/GestionClientes/WebServices/VerificarDispositivo.php?wsdl");
		prmtros.addElement("urn:GlobalCircuit:GestionClientes");
		prmtros.addElement("VerificarDispositivo");
		prmtros.addElement("id_dspstvo");
		prmtros.addElement(id_dspstvo);
		prmtros.addElement("vrsion");
		prmtros.addElement(vrsion);
		prmtros.addElement("mdo_oprcion");
		prmtros.addElement("2");

		// Creates a Soap Client object
		SOAP11Client client = new SOAP11Client(prmtros, 15, 15);
		Hashtable<String, String> rspsta = client.CallWebService();
    	return rspsta;
	}
	
	// Método que permite verificar la respuesta de la actualización de la bandera
	public String verificaActualizarBanderaSincronizacion(Hashtable<String, String> response, Context contexto){	
		String rspsta = null;
		
		// Verificar si el consumo del servicio fue exitoso
		if(response.get("ConsumioServicio").equals("exito")) {
			// Verificar la respuesta del servidor
			if(response.get("Resultado").equals("OK")) {
		    	// Todo ok, No extrae datos
				rspsta = "OK";
			} else {
		    	// Error.
				toolsApp.showLongMessage("WS - Error: " + response.get("Mensaje"), contexto);
			}
		}else{
	    	// Fallo.
			Log.i(Configuracion.tagLog, "WS - Timeout");
		}
		return rspsta;
	} 
	
	/******************************************************/
	
	// Método que permite preparar la base de datos
	public Hashtable<String, String> prepararBaseDatos(
			String sdnmo, int id_clnte_global, int id_usrio, String id_dspstvo, int bd_adcnal){
		// Creates the request
		Vector<String> prmtros = new Vector<String>();
		prmtros.addElement("http://23.253.57.17/GestionClientes/WebServices/PrepararBaseDatos/" +
				"PrepararBaseDatos2014-02-21.php?wsdl");
		prmtros.addElement("urn:GlobalCircuit:GestionClientes");
		prmtros.addElement("PrepararBaseDatos");
		prmtros.addElement("sdnmo");
		prmtros.addElement(sdnmo);
		prmtros.addElement("id_clnte_global");
		prmtros.addElement(String.valueOf(id_clnte_global));
		prmtros.addElement("id_usrio");
		prmtros.addElement(String.valueOf(id_usrio));
		prmtros.addElement("id_dspstvo");
		prmtros.addElement(id_dspstvo);
		prmtros.addElement("bd_adcnal");
		prmtros.addElement(String.valueOf(bd_adcnal));

		// Creates a Soap Client object
		SOAP11Client client = new SOAP11Client(prmtros, 30, 30);
		Hashtable<String, String> rspsta = client.CallWebService();
    	return rspsta;
	}
	
	// Verifica la respuesta de la preparacion de la Base de Datos
	public JSONObject verificaPrepararBaseDatos(Hashtable<String, String> response, Context contexto){	
		JSONObject json_object = null;
		
		// Verificar si el consumo del servicio fue exitoso
		if(response.get("ConsumioServicio").equals("exito")) {
			// Verificar la respuesta del servidor
			if(response.get("Resultado").equals("OK")) {
				
		    	// Extrae los datos
				String dtos_cdfcdos = response.get("DatosJSON");
				byte[] dtos_dcdfcdos = Base64.decode(dtos_cdfcdos, Base64.DEFAULT);
				try {
					String json_string = new String(dtos_dcdfcdos, "ISO-8859-1");
					json_object = new JSONObject(json_string);
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				
			} else {
		    	// Error.
				toolsApp.showLongMessage("WS - Error: " + response.get("Mensaje"), contexto);
			}
		}else{
	    	// Fallo.
			Log.i(Configuracion.tagLog, "WS - Timeout");
		}
		return json_object;
	} 	
	
	/******************************************************/
	
	// Método que permite preparar la base de datos
	public Hashtable<String, String> prepararBaseDatosOS(
			String sdnmo, int id_clnte_global, int id_usrio, String id_dspstvo, int bd_adcnal){
		// Creates the request
		Vector<String> prmtros = new Vector<String>();
		prmtros.addElement("http://23.253.57.17/GestionClientes/WebServices/PrepararBaseDatos/" +
				"PrepararBaseDatos2016-10-14OS.php?wsdl");
		prmtros.addElement("urn:GlobalCircuit:GestionClientes");
		prmtros.addElement("PrepararBaseDatos");
		prmtros.addElement("sdnmo");
		prmtros.addElement(sdnmo);
		prmtros.addElement("id_clnte_global");
		prmtros.addElement(String.valueOf(id_clnte_global));
		prmtros.addElement("id_usrio");
		prmtros.addElement(String.valueOf(id_usrio));
		prmtros.addElement("id_dspstvo");
		prmtros.addElement(id_dspstvo);
		prmtros.addElement("bd_adcnal");
		prmtros.addElement(String.valueOf(bd_adcnal));

		// Creates a Soap Client object
		SOAP11Client client = new SOAP11Client(prmtros, 30, 30);
		Hashtable<String, String> rspsta = client.CallWebService();
    	return rspsta;
	}
	
	/**********************************************************/
	
	public Hashtable<String, String> crearActualizarClienteUsuario(
			String sdnmo, int id_clnte_global, String id_dspstvo, int id_usrio, String dtos_clnte){
		
		// Creates the request
		Vector<String> prmtros = new Vector<String>();
		prmtros.addElement(
				"http://23.253.57.17/GestionClientes/WebServices/Clientes/CrearActualizarClienteUsuario2014-03-07.php?wsdl");
		prmtros.addElement("urn:GlobalCircuit:GestionClientes");
		prmtros.addElement("CrearActualizarClienteUsuario");
		prmtros.addElement("sdnmo");
		prmtros.addElement(sdnmo);
		prmtros.addElement("id_clnte_global");
		prmtros.addElement(String.valueOf(id_clnte_global));
		prmtros.addElement("id_dspstvo");
		prmtros.addElement(id_dspstvo);
		prmtros.addElement("id_usrio");
		prmtros.addElement(String.valueOf(id_usrio));
		prmtros.addElement("dtos_clnte");
		prmtros.addElement(dtos_clnte);

		// Creates a Soap Client object
		SOAP11Client client = new SOAP11Client(prmtros, 30, 30);
		Hashtable<String, String> rspsta = client.CallWebService();
    	return rspsta;
	}
	
	// ??
	public JSONObject verificaCrearActualizarClienteUsuario(Hashtable<String, String> response, Context contexto){	
		JSONObject json_object = null;
		
		// Verificar si el consumo del servicio fue exitoso
		if(response.get("ConsumioServicio").equals("exito")) {
			// Verificar la respuesta del servidor
			if(response.get("Resultado").equals("OK")) {
				
		    	// Extrae los datos
				String dtos_cdfcdos = response.get("DatosJSON");
				byte[] dtos_dcdfcdos = Base64.decode(dtos_cdfcdos, Base64.DEFAULT);
				try {
					String json_string = new String(dtos_dcdfcdos, "ISO-8859-1");
					json_object = new JSONObject(json_string);
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				
			} else {
		    	// Error.
				toolsApp.showLongMessage("WS - Error: " + response.get("Mensaje"), contexto);
			}
		}else{
	    	// Fallo.
			Log.i(Configuracion.tagLog, "WS - Timeout");
		}
		return json_object;
	} 
	
	/*******************************************************************/
	
	// ??
	public Hashtable<String, String> solicitarNuevoTalonario(
				int id_clnte_global, String id_dspstvo, int id_usrio, String nmbre_tbla, String nmbre_cmpo
		) {
		
		// Creates the request
		Vector<String> prmtros = new Vector<String>();
		prmtros.addElement("http://23.253.57.17/GestionClientes/WebServices/SolicitarNuevoTalonario.php?wsdl");
		prmtros.addElement("urn:GlobalCircuit:GestionClientes");
		prmtros.addElement("SolicitarNuevoTalonario");
		prmtros.addElement("id_clnte_global");
		prmtros.addElement(String.valueOf(id_clnte_global));
		prmtros.addElement("id_dspstvo");
		prmtros.addElement(id_dspstvo);
		prmtros.addElement("id_usrio");
		prmtros.addElement(String.valueOf(id_usrio));
		prmtros.addElement("nmbre_tbla");
		prmtros.addElement(nmbre_tbla);
		prmtros.addElement("nmbre_cmpo");
		prmtros.addElement(nmbre_cmpo);
		
		// Creates a Soap Client object
		SOAP11Client client = new SOAP11Client(prmtros, 30, 30);
		Hashtable<String, String> rspsta = client.CallWebService();
    	return rspsta;
	} 
	
	// ??
	public JSONObject verificaSolicitarNuevoTalonario(Hashtable<String, String> response, Context contexto){	
		JSONObject json_object = null;
		
		// Verificar si el consumo del servicio fue exitoso
		if(response.get("ConsumioServicio").equals("exito")) {
			// Verificar la respuesta del servidor
			if(response.get("Resultado").equals("OK")) {
				
		    	// Extrae los datos
				String dtos_cdfcdos = response.get("DatosJSON");
				byte[] dtos_dcdfcdos = Base64.decode(dtos_cdfcdos, Base64.DEFAULT);
				try {
					String json_string = new String(dtos_dcdfcdos, "ISO-8859-1");
					json_object = new JSONObject(json_string);
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				
			} else {
		    	// Error.
				toolsApp.showLongMessage("WS - Error: " + response.get("Mensaje"), contexto);
			}
		}else{
	    	// Fallo.
			Log.i(Configuracion.tagLog, "WS - Timeout");
		}
		return json_object;
	}

}
