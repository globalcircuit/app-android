package com.gc.coregestionclientes.accesodatos;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;

import com.gc.coregestionclientes.Configuracion;
import com.gc.coregestionclientes.librerias.ToolsApp;

public class CustomSQLiteRoutines extends SQLiteManager {
	protected ToolsApp toolsApp = new ToolsApp();
	protected static final int VERSION_BASE_DATOS = 1;
	
	// Constructor
	public CustomSQLiteRoutines(Context contexto, String nombreBaseAdicional) {
		super(contexto, Configuracion.directorioDB, nombreBaseAdicional, VERSION_BASE_DATOS);
	}

	/*************************************************/
	// RUTINAS GENERICAS EN LA BD ADICIONAL
	/*************************************************/
	// Permite consultar el campo de una tabla dependiendo del patron
	public List<String> routine_consultarCampoTabla(String tbla, String cmpo, String ptron) {
		List<String> dtos = null;
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);

		// Crea y ejecuta el query
		String sql = 
				"SELECT " + 
				" " + cmpo + " " + 
				"FROM " + tbla + " " + 
				"WHERE" + 
				" " + cmpo + " LIKE '%" + ptron + "%' " + 
				"ORDER BY " + cmpo;
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			int nmro_rgstros = cursor.getCount();
			if (cursor.moveToFirst()) {
				dtos = new ArrayList<String>(nmro_rgstros);
				do {
					// Extrae los datos del cliente
					String dto = cursor.getString(0);

					// Adiciona el dato a la lista
					dtos.add(dto);
				} while (cursor.moveToNext());
			}
			cursor.close();
		}
		//sqliteManager.cerrarDB();

		// Retorno de resultado
		return dtos;
	}

}
