package com.gc.coregestionclientes.accesodatos;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.gc.coregestionclientes.interfaces.OnTaskCompleted;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class DownloadsManager extends AsyncTask<String, Integer, String>{

	// Tag para el log
	private static final String DownloadTag = "DownloadFile";
	private static final int PERMISSION_WRITE_STORAGE = 1 ;
	String file_path = "";
	// Atributos de la clase
	private Context context;
	private OnTaskCompleted listener;
	private String accion;
	private String ttlo_dscrga;
	private URL url;
	private String drccion_dstno;
	private String nmbre_archvo;
	private String carpeta = "/GC/databases/";
	private int rntntos;
	private int timeout_cnxion = 15;
	private int timeout_lctra = 15;
	private boolean dscrga_extsa = false;
	private boolean mstrar_prgrso = true;

	// Tamaño del archivo a ser descargado
	private int fle_lngth = -1;

	// Barra de progreso en la descarga de los datos
	private ProgressDialog progressDialogBar;

	// Contador para los reintentos
	//private int cntdor = 0;

	/**
	 * @param context : Contexto desde donde fue instanciado
	 * @param ttlo_dscrga : Titulo utilizado en el proceso de descarga
	 * @param url : Url del archivo que será descargado
	 * @param drccion_dstno : Ruta del archivo donde se guardara.
	 * @param nmbre_archvo : Nombre del archivo que será guardado
	 * @param rntntos : Numero de reintentos al perder conexión de lectura
	 * @param timeout_cnxion : TimeOut minimo para estalecer conexión
	 * @param timeout_lctra : TimeOut minimo para restablecer conexión
	 * @throws MalformedURLException This exception is thrown when a
	 * program attempts to create an URL from an incorrect specification.
	 */
	public DownloadsManager(
			Context context, String ttlo_dscrga, String accion, String url, String drccion_dstno, String nmbre_archvo,
			int rntntos, int timeout_cnxion, int timeout_lctra, boolean mstrar_prgrso)
	{
		this.context = context;
		this.listener = (OnTaskCompleted) context;
		this.ttlo_dscrga = ttlo_dscrga;
		this.accion = accion;
		try {
			this.url = new URL(url);
		} catch (MalformedURLException e) {
			Log.e(DownloadTag, "Url inválida");
		}
		this.drccion_dstno = drccion_dstno;
		this.nmbre_archvo = nmbre_archvo;
		this.rntntos = rntntos;
		this.timeout_cnxion = timeout_cnxion;
		this.timeout_lctra = timeout_lctra;
		this.mstrar_prgrso = mstrar_prgrso;
	}

	// Permite iniciar la descarga
	public void iniciarDescarga(int db_size) {
		// Longitud del archivo que será descargado
		fle_lngth = db_size;

		// Inicializa la barra de progreso
		if(mstrar_prgrso){
			progressDialogBar = new ProgressDialog(context);
			progressDialogBar.setMessage(ttlo_dscrga);
			progressDialogBar.setIndeterminate(false);
			progressDialogBar.setMax(100);
			progressDialogBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		}

		// Inicia la descarga
		this.execute();
	}

    @Override
    protected String doInBackground(String... sUrl) {
    	int cntdor = 0;

    	// Permite realizar un numero definido de reintentos para descargar el archivo
    	while(cntdor < rntntos) {
    		cntdor ++;

    		// Descarga el archivo
            int bytes_dscrgdos = 0;
            try {

            	// Configura la conexión teniendo en cuenta la url y los timeout, luego se conecta
                HttpURLConnection  connection = (HttpURLConnection) url.openConnection();
                connection.setConnectTimeout(timeout_cnxion*1000);
                connection.setReadTimeout(timeout_lctra*1000);
                connection.setRequestMethod("GET");
                connection.setDoInput(true);

                // Establece conexión
                connection.connect();

                // Obtiene el codigo de respuesta de la conexión
                int response = connection.getResponseCode();

                // Evalúa si la respuesta fue correcta
                if(response == 200){

    	            // Obtiene el tamaño del archivo
    	            //int fle_lngth = connection.getContentLength();

    	            // Descarga el archivo
    	            //InputStream input = new BufferedInputStream(url.openStream());
    	            int BUFFER = 1024;
    	            InputStream input = connection.getInputStream();
					//BufferedInputStream bis = new BufferedInputStream(input, BUFFER);
					ByteArrayOutputStream baf = new ByteArrayOutputStream (BUFFER);
					byte dtos[] = new byte[BUFFER];

    	            // Inicializa variables
    	            long total = 0;
    	            int count;

    	        	Log.i(DownloadTag, "Inicio de la descarga");

    	            // Lee y guarda en el buffer
    	            //while ((count = bis.read(dtos, 0, BUFFER)) > 0) {
					//We create an array of bytes
					while((count = input.read(dtos,0,dtos.length)) != -1){
						total += count;
						baf.write(dtos,0,count);

						// Publica el progreso de la descarga
						int ttal_dscrgdo = (int) (total * 100 / fle_lngth);
						this.doProgress(ttal_dscrgdo);
					}
    	        	/*while ((count = input.read(dtos)) > 0) {

                    	total += count;
                        //baf.append((byte) count);
                    	baf.append(dtos, 0, count);

                        // Publica el progreso de la descarga
    	                int ttal_dscrgdo = (int) (total * 100 / fle_lngth);
    	                this.doProgress(ttal_dscrgdo);
                    }*/

    	            // Extrae la cantidad de bytes descargados
                    bytes_dscrgdos = baf.size(); //length();
    	        	//bytes_dscrgdos = (int) total;
                    if (bytes_dscrgdos > 0 && (bytes_dscrgdos == fle_lngth)) {


        	            // Crea los directorios en caso que no existan
        	            File drctrio = new File(drccion_dstno);
						drctrio.mkdirs();

						if (drctrio.exists()) {
							//File drctrios = new File(drccion_dstno + nmbre_archvo);






							OutputStream output = new FileOutputStream(drccion_dstno + nmbre_archvo);
							output.write(baf.toByteArray());
							//output.write(dtos, 0, (int) total);
							output.flush();
							output.close();
							dscrga_extsa = true;
						}else {
							Log.i(DownloadTag, "Descarga no exitosa");
						}
        	    		// Escribe el archivo en la ruta indicada


    	            	// Setea la descarga exitosa
    	            	//dscrga_extsa = true;
    		        	Log.i(DownloadTag, "Descarga finalizada exitosamente");
                    }
                }

                // Cierra la conexión con la url
                connection.disconnect();
            } catch (Exception e) {
            	Log.e(DownloadTag, "Error al descargar el archivo: "+e.toString());
            }

    		// Si la descarga fue exitosa, detiene los reintentos
    		if (dscrga_extsa) {break;}
        }
    	cntdor = 0;
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    	if(mstrar_prgrso){
    		progressDialogBar.show();
    	}
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
    	if(mstrar_prgrso){
    		progressDialogBar.setProgress(progress[0]);
    	}
    }

    @Override
    protected void onPostExecute(String result) {
    	if(mstrar_prgrso){
    		progressDialogBar.dismiss();
    	}
    	super.onPostExecute(result);
		/*File archvo = new File(context.getExternalFilesDir(
				Environment.getExternalStorageState()), this.carpeta);


		 */

		File archvo = new File(drccion_dstno + nmbre_archvo);

    	// Informa sobre el exito o la falla de la descarga
    	if(archvo.exists()){
    		Log.i(DownloadTag, "Archivo descargado y guardado con exito");
    		Toast.makeText(context, "Datos descargados con éxito", Toast.LENGTH_SHORT).show();
    	}else {
    		Log.i(DownloadTag, "No se pudo realizar la descarga. Intente nuevamente");
    		Toast.makeText(context, "No se pudo realizar la descarga. Intente nuevamente",
    				Toast.LENGTH_LONG).show();
    		dscrga_extsa = false;
		}

		// Una vez finaliza la ejecución en background, ejecuta la tarea propia de la actividad
		if(listener != null){
			listener.onTaskCompleted(accion, dscrga_extsa);
		}
    }

    // Método que permite ejecutar la publicación del progreso
    public void doProgress(int value){
        publishProgress(value);
    }
}
