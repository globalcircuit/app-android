package com.gc.coregestionclientes.accesodatos;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Vector;

import com.gc.coregestionclientes.interfaces.OnTaskCompleted;
import com.gc.coregestionclientes.objetos.Dispositivo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;

// Clase que permite la ejecución de tareas asincronas (consumir un servicio)
public class ConsumeWebService {
	
	// Variables
	private Context context;
	private Activity activity;
	private OnTaskCompleted listener; 
	private boolean showProgress;
	private ProgressDialog progressDialog;
	private String accion;
	private String[] prmtros_callback;
	private WebServices webServices;
	public Dispositivo dispositivo;
	private Hashtable<String, TareaWSAsincrona> tareasWSActivas = new Hashtable<String, ConsumeWebService.TareaWSAsincrona>();

	// Constructor
	public ConsumeWebService(Context context) {
		super();
		this.context = context;
		this.activity = (Activity) context;
		this.listener = (OnTaskCompleted) context;
		this.webServices = new WebServices();
	}
	
	// Obtiene las tareas activas
	public Hashtable<String, TareaWSAsincrona> obtenerTareasActivas(){
		return tareasWSActivas;
	}
	
	// Iniciar el proceso en background
	public void iniciarProceso(
			String nmbre_srvcio, Hashtable<String, String> prmtros_adcnles, 
			boolean showProgress, String mnsje_prgreso, String accion, String... prmtros_callback){
		this.accion = accion;
		this.showProgress = showProgress;
		this.prmtros_callback = prmtros_callback;
		
		// Adiciona la tarea a la pila de tareas activas
		final TareaWSAsincrona tareaWSAsincrona = new TareaWSAsincrona(nmbre_srvcio, prmtros_adcnles);
		tareasWSActivas.put(nmbre_srvcio, tareaWSAsincrona);
		
		// Verifica si existe un progressdialog
		if(showProgress){
			progressDialog = ProgressDialog.show(context, "", mnsje_prgreso, true);
			progressDialog.setCancelable(true);
	        progressDialog.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					tareaWSAsincrona.cancel(true);
				}
			});
		}
		
		// Ejecuta la tarea
		tareaWSAsincrona.execute();
	}
	
	public class TareaWSAsincrona extends AsyncTask<Object, Object, Object>{
		private String nmbre_srvcio = null;
		private Hashtable<String, String> prmtros_adcnles;
		
		// Constructor
		public TareaWSAsincrona(String nmbre_srvcio, Hashtable<String, String> prmtros_adcnles) {
			this.nmbre_srvcio = nmbre_srvcio;
			this.prmtros_adcnles = prmtros_adcnles;
		}
		
		// Proceso en Background
		@Override
		protected Vector<Serializable> doInBackground(Object... prmtros) {		
			Vector<Serializable> respuesta = new Vector<Serializable>();
			Hashtable<String, String> responseMessage = null;
			
			// Dependiendo del nombre del servicio consume el servicio
			if(nmbre_srvcio.equals("verificarDispositivo")){
				String id_dspstvo = prmtros_adcnles.get("IdDispositivo");
				String vrsion = prmtros_adcnles.get("Version");
				String token = prmtros_adcnles.get("token");
				String email= prmtros_adcnles.get("email");
				responseMessage = webServices.realizarLogin(id_dspstvo, vrsion, token, email );
	
			} else if(nmbre_srvcio.equals("actualizarBanderaSincronizacion")){
				String id_dspstvo = prmtros_adcnles.get("IdDispositivo");
				responseMessage = webServices.actualizarBanderaSincronizacion(id_dspstvo, null);
	
			} else if(nmbre_srvcio.equals("prepararBaseDatos")){
				String sdnmo = prmtros_adcnles.get("Seudonimo");
				int id_clnte_global = Integer.parseInt(prmtros_adcnles.get("IdClienteGlobal"));
				int id_usrio = Integer.parseInt(prmtros_adcnles.get("IdUsuario"));
				String id_dspstvo = prmtros_adcnles.get("IdDispositivo");
				int bd_adcnal = Integer.parseInt(prmtros_adcnles.get("BDAdicional"));
				responseMessage = webServices.prepararBaseDatos(sdnmo, id_clnte_global, id_usrio, id_dspstvo, bd_adcnal);
				
			} else if(nmbre_srvcio.equals("prepararBaseDatosOS")){
				String sdnmo = prmtros_adcnles.get("Seudonimo");
				int id_clnte_global = Integer.parseInt(prmtros_adcnles.get("IdClienteGlobal"));
				int id_usrio = Integer.parseInt(prmtros_adcnles.get("IdUsuario"));
				String id_dspstvo = prmtros_adcnles.get("IdDispositivo");
				int bd_adcnal = Integer.parseInt(prmtros_adcnles.get("BDAdicional"));
				responseMessage = webServices.prepararBaseDatosOS(sdnmo, id_clnte_global, id_usrio, id_dspstvo, bd_adcnal);
				
			} else if(nmbre_srvcio.equals("crearCliente")){
				String sdnmo = prmtros_adcnles.get("Seudonimo");
				int id_clnte_global = Integer.parseInt(prmtros_adcnles.get("IdClienteGlobal"));
				String id_dspstvo = prmtros_adcnles.get("IdDispositivo");
				int id_usrio = Integer.parseInt(prmtros_adcnles.get("IdUsuario"));
				String dtos_clnte = prmtros_adcnles.get("DatosCliente");
				responseMessage = webServices.crearActualizarClienteUsuario(sdnmo, id_clnte_global, id_dspstvo, id_usrio, dtos_clnte);
				
			} else if(nmbre_srvcio.equals("solicitarNuevoTalonario")){
				int id_clnte_global = Integer.parseInt(prmtros_adcnles.get("IdClienteGlobal"));
				String id_dspstvo = prmtros_adcnles.get("IdDispositivo");
				int id_usrio = Integer.parseInt(prmtros_adcnles.get("IdUsuario"));
				String nmbre_tbla = prmtros_adcnles.get("NombreTabla");
				String nmbre_cmpo = prmtros_adcnles.get("NombreCampo");		
				responseMessage = webServices.solicitarNuevoTalonario(
						id_clnte_global, id_dspstvo, id_usrio, nmbre_tbla, nmbre_cmpo);
			}
			
			// Arma la respuesta con la respuesta del webservice y el nombre del servicio
			respuesta.add(responseMessage);
			respuesta.add((String) nmbre_srvcio);
			return respuesta;
		}
		
	
		// Luego de ejecutar la tarea en background
		@Override
		protected void onPostExecute(Object result) {
			// Extrae los parámetros del resultado
			
			@SuppressWarnings("unchecked")
			Hashtable<String, String> responseMessage = (Hashtable<String, String>) ((Vector<?>)result).get(0);
			String tpo_srvcio = (String) ((Vector<?>)result).get(1);
			
			// Si esta habilitada la opcion para mostrar el el progresDialog,
			// Lo remueve de la pantalla una vez se ha completado la tarea en background
			if(showProgress){
				progressDialog.dismiss();
			}
			
			// Inicializa el objeto con los datos del webservice
			Object object = null;
	
			// Dependiendo del servicio que fue consumido, se verifica la respuesta
			if(responseMessage!=null && !responseMessage.equals("null")){
				if(tpo_srvcio.equals("verificarDispositivo")){
					object = webServices.verificaRealizarLogin(responseMessage, context);
					
				}else if(tpo_srvcio.equals("actualizarBanderaSincronizacion")){
					object = webServices.verificaActualizarBanderaSincronizacion(responseMessage, context);
					
				}else if(tpo_srvcio.equals("prepararBaseDatos") || tpo_srvcio.equals("prepararBaseDatosOS")){
					object = webServices.verificaPrepararBaseDatos(responseMessage, context);
					
				}else if(tpo_srvcio.equals("crearCliente")){
					object = webServices.verificaCrearActualizarClienteUsuario(responseMessage, context);
					
				}else if(tpo_srvcio.equals("solicitarNuevoTalonario")){
					object = webServices.verificaSolicitarNuevoTalonario(responseMessage, context);
				}
				
				// Una vez finaliza la ejecución en background, ejecuta la tarea propia de la actividad
				if(listener != null && !activity.isFinishing()){
					listener.onTaskCompleted(accion, object, prmtros_callback);
				}
			}
			
			// Elimina la tareaWS de las tareasActivas
			tareasWSActivas.remove(nmbre_srvcio);
		}
		
		@Override
		protected void onCancelled() {
			super.onCancelled();
			if(!activity.isFinishing()){
				activity.finish();
			}
		}
	}
}
