package com.gc.coregestionclientes.fragmentos;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.widget.DatePicker;
 
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
 
	// Parametros
	private OnDatePickerEvent mListener;
	private int idCampo;
	
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	int ano, mes, dia;
    	
    	// Obtiene una instancia de calendar (Fecha actual)
        Calendar calendar = Calendar.getInstance();

        this.idCampo = getArguments().getInt("idCampo");
    	// Revisa si hay argumentos
		boolean definirFecha = getArguments().getBoolean("definirFecha");
		if (definirFecha) {
			// Extrae los argumentos
			long fecha = getArguments().getLong("fecha");
			
			// Cambia el valor del objeto
			calendar.setTimeInMillis(fecha);
		}	
		
		// Extrae los datos de la fecha
        ano = calendar.get(Calendar.YEAR);
        mes = calendar.get(Calendar.MONTH);
        dia = calendar.get(Calendar.DAY_OF_MONTH);
 
        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, ano, mes, dia);
    }
 
    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Halla los valores de las fecha en milisegundos
		Calendar calendar = new GregorianCalendar(year, month, day);
		long unixDate = calendar.getTimeInMillis();
	    this.mListener.onDatePickerEvent(unixDate,idCampo);
    }
    
    public interface OnDatePickerEvent {
        void onDatePickerEvent(long unixDate,int idCampo);
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (OnDatePickerEvent)activity;
        }
        catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " debe implementar OnDatePickerEvent");
        }
    }
}