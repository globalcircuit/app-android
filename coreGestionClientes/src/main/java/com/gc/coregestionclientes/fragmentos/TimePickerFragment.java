package com.gc.coregestionclientes.fragmentos;

import java.util.Calendar;
import java.util.Locale;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.widget.TimePicker;
 
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
 
	// Parametros
	private OnTimePickerEvent mListener;
	private int idCampo;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	int hora, minutos;
    	
    	// Revisa los argumentos
		this.idCampo = getArguments().getInt("idCampo");
		boolean definirTiempo = getArguments().getBoolean("definirTiempo");
		if (definirTiempo) {
			// Extrae los argumentos
			hora = getArguments().getInt("hora");
			minutos = getArguments().getInt("minutos");
		} else {
	        // Utiliza el tiempo actual.
	        final Calendar c = Calendar.getInstance(Locale.getDefault());
	        hora = c.get(Calendar.HOUR_OF_DAY);
	        minutos = c.get(Calendar.MINUTE);
		}
 
        // Create a new instance of DatePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hora, minutos, true);
    }

	@Override
	public void onTimeSet(TimePicker view, int hra, int mntos) {
		long unixTime = ((hra*60*60) + (mntos*60)) * 1000;
	    this.mListener.onTimePickerEvent(this.idCampo, unixTime);	
	}
	
	public interface OnTimePickerEvent {
        void onTimePickerEvent(int idCampo, long unixTime);
    }
	
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (OnTimePickerEvent)activity;
        }
        catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " debe implementar OnTimePickerEvent");
        }
    }
}