package com.gc.coregestionclientes;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Vector;
//
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.inputmethodservice.KeyboardView.OnKeyboardActionListener;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gc.coregestionclientes.objetos.AutocompletarCampo;

public class ActividadAutocompletarCampo extends FragmentActivity {

	// Objetos
	private AutocompletarCampo autocompletarCampo;
	private HashMap<String, String> datosAdicionales;
	private Vector<LinkedHashMap<String, String>> items = new Vector<LinkedHashMap<String, String>>();
	private AlertDialog dialog;
	
	// Elementos Gráficos
	private KeyboardView mKeyboardView;
	private Keyboard mKeyboard;
	private EditText target;
	private Button boton;
	private LinearLayout contenedor_sugerencias;
	
	// Variables auxiliares
	private String PREFS_NAME = "VariablesConsultaElementos";
	private boolean dialogoActivo = false;
	private int numeroItems = 0;
	
	// Modo operación
	// 1. Autocomplete utilizando detalle
	// 2. Autocomplete simple
	private int MODO_OPERACION = -1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.actividad_autocompletar_campo);
			
		// Obtiene los datos del intent
		Bundle extras = getIntent().getExtras();
		autocompletarCampo = (AutocompletarCampo) extras.getParcelable("autocompletarCampo");
		datosAdicionales = (HashMap<String, String>) extras.getSerializable("datosAdicionales");
		
		// Consulta el modo de operacion
		MODO_OPERACION = autocompletarCampo.consultarModoFuncionamiento(this);
		
		// Inicializa elementos gráficos
		mKeyboard = new Keyboard(this, R.xml.teclado_qwerty);
		mKeyboardView = (KeyboardView) findViewById(R.id.keyboard_view);
		mKeyboardView.setKeyboard(mKeyboard);
		mKeyboardView.setOnKeyboardActionListener(new BasicOnKeyboardActionListener(this));
		mKeyboardView.setVisibility(View.VISIBLE);
		boton = (Button)findViewById(R.id.button1);		
		boton.setEnabled(false);
		target = (EditText)findViewById(R.id.target);
		target.setInputType(InputType.TYPE_NULL);
		target.setCursorVisible(true);
		target.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}
			
			@Override
			public void afterTextChanged(Editable s) {
				// Realiza la consulta a la base de datos, y dependiendo de los resultados, se crea el vector.
				switch (MODO_OPERACION) {
				case 1:
					// Modo 1. Autocomplete utilizando detalle
					// Solo muestra el número de coincidencias
					numeroItems = autocompletarCampo.consultarNumeroItems(
							ActividadAutocompletarCampo.this, s.toString(), datosAdicionales);
					
					// Dependiendo de los resultados, habilita el botón
					if(numeroItems > 100){
						boton.setTextColor(Color.RED);
						boton.setEnabled(false);
					} else {
						boton.setTextColor(Color.GREEN);
						boton.setEnabled(true);
					}
					break;

				case 2:
					// Modo 2. Autocomplete simple
					// Consulta y muestra las opciones encontradas
					items = autocompletarCampo.consultarItems(ActividadAutocompletarCampo.this, s.toString(), datosAdicionales);
					
					// Verifica los items
					if (items != null) {
						numeroItems = items.size();
					} else {
						numeroItems = 0;
					}
					// Muestra las opciones
					verOpcionesSimples();
					break;
				}
				
				// Muestra el numeroItem en el botón
				boton.setText(String.valueOf(numeroItems));
			}
		});
		contenedor_sugerencias = (LinearLayout) findViewById(R.id.contenedor_sugerencias);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		SharedPreferences sharedPreferences = getSharedPreferences(PREFS_NAME, 0);
		dialogoActivo = sharedPreferences.getBoolean("dialogoActivo", false);
		
		// Si el dialogo estaba activo y no se está mostrando, lo muestra
		if(numeroItems > 0 && dialogoActivo && (dialog == null || !dialog.isShowing())){
			dialogoActivo = false;
			boton.performClick();
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if(dialog != null && dialog.isShowing()) {
			dialog.dismiss();
			dialogoActivo = true;
	    }
		
		// Almacena el estado de dialogo
		SharedPreferences sharedPreferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean("dialogoActivo", dialogoActivo);
		editor.commit(); 
	}
	
	// Listener del botón que permite mostrar la lista de elementos
	public void verLista(View view) {
		
		if(numeroItems <= 0){
			// No hay resultados.
			Toast.makeText(ActividadAutocompletarCampo.this, "No hay resultados para mostrar!", Toast.LENGTH_LONG).show();
		} else {
			// Hay resultados,
			// Consulta los objetos
			items = autocompletarCampo.consultarItems(this, target.getText().toString(), datosAdicionales);
			
			// Inicializa el adapter con los items
	        CustomAdapterItems adapter = new CustomAdapterItems(this, items);
	        
	        // Inicializa el view que será mostrado en el Alert
			View viewAlert = getLayoutInflater().inflate(R.layout.dialog_consulta_elementos, null);
			((TextView)viewAlert.findViewById(R.id.titulo_consulta_elementos))
				.setText(items.size() + " resultado(s)");
			ListView listView = (ListView)viewAlert.findViewById(R.id.lv_consulta_elementos);
	        listView.setAdapter(adapter);
	        listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// Extrae el objeto seleccionado y finaliza la actividad retornando el Objeto
					HashMap<String, String> item = items.elementAt(position);
					Intent resultIntent = new Intent();
					resultIntent.putExtra("idCampo", autocompletarCampo.getIdCampo());
					resultIntent.putExtra("valor", item);
					setResult(RESULT_OK, resultIntent);
		        	finish();					
				}
			
	        
	        });
	        
	        // Crea el alert dialog
	        AlertDialog.Builder builder = new AlertDialog.Builder(this)
		        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
		    		
		            @Override
		            public void onClick(DialogInterface dialog, int which) {}
		        })
		        .setCancelable(false);
	        dialog = builder.create();
	        dialog.setView(viewAlert, 0, 0, 0, 0);
	        dialog.show();
		}
	}
	
	// Función que permite visualizar las opciones encontradas
	private void verOpcionesSimples(){
		contenedor_sugerencias.removeAllViews();
		SpannableStringBuilder sugerencias = new SpannableStringBuilder();
			
		// Verifica si hay mas de 15 elementos.
		if(numeroItems > 10) return;
		
		// Recorre los items y adiciona las sugerencias al contenedor	
		for (int i = 0; i < numeroItems; i++) {
			LinkedHashMap<String, String> item = items.elementAt(i);
			String valor = item.get("valor");
			
			// Personaliza el valor del item encontrado
			SpannableStringBuilder ssb = new SpannableStringBuilder(valor);
			ssb.setSpan(new CustomClickableSpan(valor), 0, ssb.length(), 0);
			if (i % 2 == 0) {
				ssb.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.CornflowerBlue)), 0, ssb.length(), 0);
			} else {
				ssb.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.LightSteelBlue)), 0, ssb.length(), 0);
			}
			ssb.setSpan(new UnderlineSpan(), 0, ssb.length(), 0);
			
			// Adiciona la sugerencia a la secuencia de sugerencias
			sugerencias.append(ssb).append("  ");
			
			/*Button coincidencia = new Button(this);
			LinkedHashMap<String, String> item = items.elementAt(i);
			String valor = item.get("valor");
			coincidencia.setText(valor);
			coincidencia.setSingleLine(true);
			
			// Adiciona el listener a la sugerencia
			coincidencia.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// Extrae el valor y lo retorna al formulario
					Intent resultIntent = new Intent();
					resultIntent.putExtra("idCampo", idCampo);
					resultIntent.putExtra("valor", ((Button) v).getText().toString());
					setResult(RESULT_OK, resultIntent);
		        	finish();				
		        }
			});
			
			// Adiciona la sugerencia al contenedor
			contenedor_sugerencias.addView(coincidencia);*/
		}
		
		// Elimina el | del final
		if (sugerencias.length() > 0) {
			sugerencias.delete(sugerencias.length() - 2, sugerencias.length());
		}
		
		// Adiciona la sugerencia al contenedor
		TextView tv = new TextView(this);
		tv.setText(sugerencias);
		tv.setTextSize(24);
		tv.setMovementMethod(LinkMovementMethod.getInstance());
		contenedor_sugerencias.addView(tv);
	}
	
	// Clase personalizada que permite capturar el evento Click de las sugerencias simples
	public class CustomClickableSpan extends ClickableSpan {
	    String texto;

	    public CustomClickableSpan(String texto){
	         super();
	         this.texto = texto;
	    }

	    @Override
	    public void onClick(View widget) {
			Intent resultIntent = new Intent();
			resultIntent.putExtra("idCampo", autocompletarCampo.getIdCampo());
			resultIntent.putExtra("valor", texto);
			setResult(RESULT_OK, resultIntent);
        	finish();
	    }
	}
	
	// Clase customizada para la visualizacion de los items
	private class CustomAdapterItems extends BaseAdapter implements
			Filterable {
		private Activity activity;
		private Vector<LinkedHashMap<String, String>> list;
		private LinkedHashMap<String, String> item;
		private LayoutInflater inflater = null;

		// Constructor
		public CustomAdapterItems(Activity activity, Vector<LinkedHashMap<String, String>> list) {
			this.activity = activity;
			this.list = list;
			this.inflater = (LayoutInflater) this.activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		// Retorna el número de items en el vector
		public int getCount() {
			return list.size();
		}

		// Permite obtener el objeto en la posición
		public Object getItem(int position) {
			item = list.get(position);
			return item;
		}

		// Permite obtener el estilo de layout previamente definido y coloca los
		// valores de la fila
		// que serán mostrados
		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;
			if (convertView == null)
				vi = inflater.inflate(R.layout.row_consulta_elementos, parent, false);

			// Obtiene los view donde será colocada la información
			LinearLayout row_rsmen_cntndor_infrmcion = (LinearLayout) vi.findViewById(R.id.row_rsmen_cntndor_infrmcion);
			row_rsmen_cntndor_infrmcion.removeAllViews();

			// Obtiene los datos en la posición dada
			LinkedHashMap<String, String> item = list.get(position);

			// Recorre los campos del item
			// Recorre el HashMap
			Iterator<?> it = item.entrySet().iterator();
			int indice = 0;
			while (it.hasNext()) {
				@SuppressWarnings("rawtypes")
				LinkedHashMap.Entry atributo = (LinkedHashMap.Entry)it.next();
				
				// Extrae los datos
				String nombreCampo = (String) atributo.getKey();	
				String valor = (String) atributo.getValue();

				// Setea los valroes
				TextView textView = new TextView(ActividadAutocompletarCampo.this);
				textView.setText(nombreCampo + ": " + valor);
				
				// Si es el primer TextView lo coloca en BOLD
				if (indice==0) {
					textView.setTextAppearance(ActividadAutocompletarCampo.this, R.style.titulo_etiqueta_claro);
				} else {
					textView.setTextAppearance(ActividadAutocompletarCampo.this, R.style.valor_etiqueta_claro);
				}
				
				// Adiciona a la fila
				row_rsmen_cntndor_infrmcion.addView(textView);

				// Incrementa contador
				indice++;
			}
			return vi;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public Filter getFilter() {
			return null;
		}
	}
	
	// Clase que escucha las teclas presionadas
	private class BasicOnKeyboardActionListener implements OnKeyboardActionListener {

		private Activity mTargetActivity;

		public BasicOnKeyboardActionListener(Activity targetActivity) {
			mTargetActivity = targetActivity;
		}

		@Override
		public void swipeUp() {}

		@Override
		public void swipeRight() {}

		@Override
		public void swipeLeft() {}

		@Override
		public void swipeDown() {}

		@Override
		public void onText(CharSequence text) {}

		@Override
		public void onRelease(int primaryCode) {}

		@Override
		public void onPress(int primaryCode) {}

		@Override
		public void onKey(int primaryCode, int[] keyCodes) {
			long eventTime = System.currentTimeMillis();
			KeyEvent event = null;

			// Revisa si es la Ñ
			if(primaryCode == 942){
				// Es la Ñ
				String string = "ñ";
				event = new KeyEvent(eventTime, string, 0, 0);
			} else {
				// No es la Ñ
				event = new KeyEvent(eventTime, eventTime,
						KeyEvent.ACTION_DOWN, primaryCode, 0, 0, 0, 0,
						KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE);
			}

			// Llama al manejador de KeyEvents
			mTargetActivity.dispatchKeyEvent(event);
		}
	}
}