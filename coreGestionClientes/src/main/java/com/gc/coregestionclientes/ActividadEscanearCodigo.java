package com.gc.coregestionclientes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class ActividadEscanearCodigo extends Activity {

	// Atributos
	private int idCampo = -1;
	private boolean habilitaEscaneo = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Obtiene los datos del intent
		Bundle extras = getIntent().getExtras();
		idCampo = extras.getInt("idCampo");
	}

	@Override
	protected void onStart() {
		super.onStart();
		if (!habilitaEscaneo) {
			Intent intent = new Intent("com.google.zxing.client.android.SCAN");
			// intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
			intent.putExtra("com.google.zxing.client.android.SCAN.SCAN_MODE",
					"QR_CODE_MODE");
			startActivityForResult(intent, 0);
		}

		// Actualiza la bandera
		habilitaEscaneo = true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			data.putExtra("idCampo", idCampo);
			setResult(RESULT_OK, data);
		}

		// Finaliza la actividad
		finish();
	}
}
