package com.gc.coregestionclientes.objetos;

import com.gc.coregestionclientes.librerias.ToolsApp;

public class VisitaProgramada {
		
	public static String PENDIENTE = "PENDIENTE";
	//public static String EN_CURSO = "EN_CURSO"; // Solo se utiliza en memoria (No está en BD por ahora)
	public static String REALIZADA = "REALIZADA";
	public static String CANCELADA = "CANCELADA";
	
	// Parámetros Auxiliares
	public static String PROGRAMADO = "PROGRAMADO";
	public static String LIBRE = "LIBRE";
	
	// Parametros
	private int id;
	private int id_clnte;
	//private Cliente cliente;
	private String estdo;
	private long fcha_incio_unix;
	private long fcha_fin_unix;
	private String crgo_usrio_orgen;
	private String obsrvcion;
	private String rango;
	private String justificacionCancelacion;
	
	// Datos auxiliares
	private Formulario formularioAdicional = null;

	// Objetos Auxiliares
	private ToolsApp toolsapp = new ToolsApp();
	
	// Constructor
	public VisitaProgramada(int id_clnte, String estdo, long fcha_incio,
			long fcha_fin, String crgo_usrio_orgen, String rango) {
		super();
		this.id_clnte = id_clnte;
		// this.cliente = cliente;
		this.estdo = estdo;
		this.fcha_incio_unix = fcha_incio;
		this.fcha_fin_unix = fcha_fin;
		this.crgo_usrio_orgen = crgo_usrio_orgen;
		if(rango != null) {
			this.rango = rango;
		} else {
			this.rango = PROGRAMADO;
		}
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getIdCliente() {
		return id_clnte;
	}

	public void setIdCliente(int id_clnte) {
		this.id_clnte = id_clnte;
	}
	
	/*public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}*/

	/**
	 * @return the estdo
	 */
	public String getEstado() {
		return estdo;
	}

	/**
	 * @param estdo the estdo to set
	 */
	public void setEstado(String estdo) {
		this.estdo = estdo;
	}

	/**
	 * @return the fcha_incio
	 */
	public long getFechaInicio() {
		return fcha_incio_unix;
	}

	/**
	 * @return the fcha_fin
	 */
	public long getFechaFin() {
		return fcha_fin_unix;
	}

	/**
	 * @return the crgo_usrio_orgen
	 */
	public String getCargoUsuarioOrigen() {
		return crgo_usrio_orgen;
	}

	/**
	 * @param fcha_incio the fcha_incio to set
	 */
	public void setFechaInicio(long fcha_incio) {
		this.fcha_incio_unix = fcha_incio;
	}

	/**
	 * @param fcha_fin the fcha_fin to set
	 */
	public void setFechaFin(long fcha_fin) {
		this.fcha_fin_unix = fcha_fin;
	}

	/**
	 * @param crgo_usrio_orgen the crgo_usrio_orgen to set
	 */
	public void setCargoUsuarioOrigen(String crgo_usrio_orgen) {
		this.crgo_usrio_orgen = crgo_usrio_orgen;
	}

	/**
	 * @return the obsrvcion
	 */
	public String getObservacion() {
		return obsrvcion;
	}

	/**
	 * @param obsrvcion the obsrvcion to set
	 */
	public void setObsrvcion(String obsrvcion) {
		this.obsrvcion = obsrvcion;
	}	
		
	/**
	 * @return the rango
	 */
	public String getRango() {
		return rango;
	}

	/**
	 * @param rango the rango to set
	 */
	public void setRango(String rango) {
		this.rango = rango;
	}

	/**
	 * @return the justificacionCancelacion
	 */
	public String getJustificacionCancelacion() {
		return justificacionCancelacion;
	}

	/**
	 * @param justificacionCancelacion the justificacionCancelacion to set
	 */
	public void setJustificacionCancelacion(String justificacionCancelacion) {
		this.justificacionCancelacion = justificacionCancelacion;
	}

	/**
	 * @return the formularioAdicional
	 */
	public Formulario getFormularioAdicional() {
		return formularioAdicional;
	}

	/**
	 * @param formularioAdicional the formularioAdicional to set
	 */
	public void setFormularioAdicional(Formulario formularioAdicional) {
		this.formularioAdicional = formularioAdicional;
	}

	public String toJSONString(){
		StringBuilder dtos = new StringBuilder();
		
		dtos.append("{");
		if(crgo_usrio_orgen != null){
			dtos.append("\"Estado:\"\"" + estdo + "(" + crgo_usrio_orgen + ")\"");
		}else{
			dtos.append("\"Estado\":\"" + estdo + "\"");
		}
		dtos.append(",\"Rango Fechas\":\"" + 
			toolsapp.unixDateToString(fcha_incio_unix, 2, true) + " - " + 
			toolsapp.unixDateToString(fcha_fin_unix, 2, true) + 
		"\"");
		if(obsrvcion != null){
			dtos.append(",\"Observación\":\"" + obsrvcion + "\"");
		}
		dtos.append("}");

		return dtos.toString();
	}
}
