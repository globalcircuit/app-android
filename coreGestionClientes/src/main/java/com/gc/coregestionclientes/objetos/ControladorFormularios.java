package com.gc.coregestionclientes.objetos;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.fragment.app.DialogFragment;

import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.gc.coregestionclientes.ActividadEscanearCodigo;
import com.gc.coregestionclientes.Configuracion;
import com.gc.coregestionclientes.R;
import com.gc.coregestionclientes.accesodatos.SQLiteRoutines;
import com.gc.coregestionclientes.fragmentos.DatePickerFragment;
import com.gc.coregestionclientes.fragmentos.TimePickerFragment;
import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.objetos.graficos.MultiSelectionSpinner;
import static android.Manifest.permission.CAMERA;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static java.security.AccessController.getContext;

public class ControladorFormularios {

	// Parametros
	private List<Formulario> formularios;
	private Formulario formularioActivo;
	private ToolsApp toolsApp = new ToolsApp();


	public static final int REQUEST_AUTOCOMPLETE = 2000;
	public static final int REQUEST_SUBFORMULARIO = 3000;
	public static final int REQUEST_SCANCODE = 4000;
	public static final int REQUEST_LISTACAMPO = 5000;
	public static final int REQUEST_ADJUNTAFOTOS = 6000;

	private final int MIS_PERMISOS = 100;
	public static final int REQUEST_FOTO = 7000;
	public static final int GALERIA = 8000;


	// elementos necesarios para la creacion del directorio que contiene la imagen

	public static final String pathSD = Environment.getExternalStorageDirectory()
			.getAbsolutePath() + "/GC/";
	public static final String pathMedia =  "Media/";
	public static final String pathImagenes =  "Imagenes/";


	// Constructores
	public ControladorFormularios(Formulario frmlrio){
		this.formularioActivo = frmlrio;
	}
	public ControladorFormularios() {}
	
	// TODO:
	public void setFormularioActivo(Formulario formulario){
		this.formularioActivo = formulario;
	}
	
	/*******************************************************/
	// MÉTODOS QUE INVOLUCRAN MULTIPLES FORMULARIOS
	/*******************************************************/

	// Permite asignar los formularios de la base de SQLite
	public void cargarFormulariosLocales(SQLiteRoutines sqliteRoutines, String tpo_bsqda_frmlrio){
		this.formularios = sqliteRoutines.routine_consultarFormulariosLocales(tpo_bsqda_frmlrio);
	}
	
	// Permite visualizar el menu de formularios
	public void visualizarMenusFormularios(
			final Context context, LinearLayout cntndor, final int REQUEST_CODE, Cliente clnte,
			final AuditorFormulario auditorFormulario) {
		if (formularios != null) {
			int nmro_frmlrios = formularios.size();

			// Recorre los formularios
			for (int i = 0; i < nmro_frmlrios; i++) {
				// Extrae el formulario
				final Formulario frmlrio = formularios.get(i);
				
				// Extrae los parametros del formulario necesarios para  crear los menus
				int id = frmlrio.getId();
				String nmbre = frmlrio.getNombre();
				String tpo = frmlrio.getTipo();
				//byte[] icno = frmlrio.getIcno();
	
				// TODO: FALTA EVALUAR LA POSIBILIDAD DE RESTRINGIR FORMULARIOS DEPENDIENDO DE LOS DATOS DEL CLIENTE
				// RESTRICCIONES TEMPORALES
				if((!nmbre.equals("PEDIDO") && !nmbre.equals("DEVOLUCIÓN")) 
						|| (nmbre.equals("PEDIDO") && clnte.getDatosAdicionales() != null)
						|| (nmbre.equals("DEVOLUCIÓN") && clnte.getDatosAdicionales() != null)){
					// Si es un formulario transaccional lo muestra
					if(tpo.equals(Formulario.TRANSACCIONAL)){
						// Crea el botón y le asigna sus propiedades
						final Button mnu = new Button(context);
						mnu.setId(id);
						mnu.setText(nmbre);
						mnu.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								mnu.setEnabled(false);
								auditorFormulario.eventoFormularioSeleccionado(context);
								/*Intent intent = new Intent(context, ActividadFormularioTransaccion.class);
								intent.putExtra("Formulario", frmlrio);
								activity.startActivityForResult(intent, REQUEST_CODE);
								*/
							}
						});
						cntndor.addView(mnu);
					}
				}
			}
		} else {
			toolsApp.showLongMessage("No hay formularios disponibles!", context);
		}
	}
		
	// Permite consultar un formulario en la lista segun el id
	public Formulario buscarFormularioLista(int id_frmlrio){
		int nmro_frmlrios = this.formularios.size();
		Formulario frmlrio = null;
		for(int i = 0; i < nmro_frmlrios; i++){
			if(id_frmlrio == this.formularios.get(i).getId()){
				frmlrio = this.formularios.get(i);
			}
		}
		return frmlrio;
	}
	
	/*************************************************/
	// MÉTODOS QUE INVOLUCRAN UN SOLO FORMULARIO
	/*************************************************/
	
	// Permite asignar los formularios de la base de SQLite
	public Formulario cargarFormularioLocal(SQLiteRoutines sqliteRoutines, String dscrpcion_frmlrio, String tpo_frmlrio, int idFormulario) {
		this.formularioActivo = sqliteRoutines.routine_consultarFormularioLocal(dscrpcion_frmlrio, tpo_frmlrio, true, idFormulario);
		return this.formularioActivo;
	}
	
	// Permite visualizar el formulario activo
	// Si asgnar_vlores es true, reconstruye un formulario previamente diligenciado
	public void visualizarFormulario(
			final Context contexto, final View layoutView, LinearLayout contenedorFormulario,
			final HashMap<String, Campo> camposHash, final HashMap<String, View> contenedoresCamposHash, 
			final AuditorFormulario auditorFormulario, final AutocompletarCampo autocompletarCampo,
			OnItemSelectedListener onItemSelectedListener, OnCheckedChangeListener onCheckedChangeListener,
			boolean adcnar_TextWatcher
	) {	
		final Activity actividad = (Activity) contexto;
		List<Campo> campos = formularioActivo.getCampos();
		int numeroCampos = campos.size();
		
		// Recorre los Campos del Formulario
		for (int i = 0; i < numeroCampos; i++) {
			
			// Extrae el campo
			final Campo campo = campos.get(i);
			
			// Extrae los parametros del campo
			final int id = campo.getId();
			final String nombreCampo = campo.getNombre();
			String modoEdicion = campo.getModoEdicion();
			String tipoDato = campo.getTipoDato();
			final String datosAdicionales = campo.getDatosAdicionales();
			boolean visible = campo.isVisible();

			// Crea el contenedor de campo
			LinearLayout contenedorCampo = new LinearLayout(contexto);
			contenedorCampo.setOrientation(LinearLayout.VERTICAL);
			if(!visible){
				contenedorCampo.setVisibility(LinearLayout.GONE);
			}
			contenedoresCamposHash.put(String.valueOf(id), contenedorCampo);
			
			// Crea el Label del campo
			TextView textview = new TextView(contexto);
			textview.setTextAppearance(contexto, R.style.titulo_etiqueta_oscuro);
			textview.setPadding(0, 6, 0, 0);
			if (campo.isObligatorio()){
				textview.setText("(*)" + nombreCampo);
			} else {
				textview.setText(nombreCampo);
			}

			// Dependiendo del modo de edición crea el elemento.
			if (modoEdicion.equals("EditText") 
					|| modoEdicion.equals("AutoComplete")
					|| modoEdicion.equals("ScanCode")
					|| modoEdicion.equals("ListView")
					|| modoEdicion.equals("TextArea")) {
				
				// Crea un cuadro de edicion
				final EditText edittext = new EditText(contexto);
				camposHash.put(String.valueOf(id), campo);
				
				// Dependiendo del tipo de dato, filtra el edittext
				int inputType = -1 ;
				if(tipoDato.equals("String")){
					inputType = InputType.TYPE_CLASS_TEXT;
					
					// Restringe la longitud del editText
					int maxLength = 0;
					try{
						maxLength = Integer.parseInt(datosAdicionales);    
					}catch (Exception e) {
						// Default 200
						maxLength = 200;
					}
					textview.setText(textview.getText()+" (Max "+maxLength+" car.)");
					edittext.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});
					
				}else if (tipoDato.equals("Number")) {
					inputType = InputType.TYPE_CLASS_NUMBER;
					
				}else if (tipoDato.equals("Decimal")) {
					inputType = InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL;
					
				}else if (tipoDato.equals("Email")) {
					inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS;
					
				}else if (tipoDato.equals("Phone")) {
					inputType = InputType.TYPE_CLASS_PHONE;
					
				}else if (tipoDato.equals("Time")) {
					inputType = InputType.TYPE_CLASS_DATETIME | InputType.TYPE_DATETIME_VARIATION_TIME;
					
				}else if (tipoDato.equals("Date")) {
					inputType = InputType.TYPE_CLASS_DATETIME | InputType.TYPE_DATETIME_VARIATION_DATE;
					
				}else if (tipoDato.equals("Name")) {
					inputType = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME;
					
					// Restringe la longitud del editText
					int maxLength = 0;
					try {
						maxLength = Integer.parseInt(datosAdicionales);    
					} catch (Exception e) {
						// Default 150
						maxLength = 150;
					}
					textview.setText(textview.getText() + " (Max "+maxLength+" car.)");
					edittext.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});
				}
				edittext.setInputType(inputType);
				
				// Asigna el id para el edittext
				edittext.setId(id);
				
				// Adiciona un watcher
				if(adcnar_TextWatcher){
					edittext.addTextChangedListener(new TextWatcher() {
						
						@Override
						public void onTextChanged(CharSequence s, int start, int before, int count) {}
						
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}
						
						@Override
						public void afterTextChanged(Editable s) {
							auditorFormulario.validarEventoCampo(
								contexto, layoutView, campo, edittext, contenedoresCamposHash, camposHash
							);
						}
					});
				}
				
				// Dependiendo del tipo o modo, adiciona el listener
				if(tipoDato.equals("Time")) {					
					// TimePicker
					edittext.setKeyListener(null);
					edittext.setOnTouchListener(new OnTouchListener() {
				        @Override
				        public boolean onTouch(View v, MotionEvent event) {
				            if(MotionEvent.ACTION_UP == event.getAction()) {
				            	// Llama el timePicker
				            	// TODO: dependiendo del valor, calcula el unixtime
				            	// String HH:MM to milisegundos.
				            	long unixTime = 0;
				            	mostrarTimePicker(contexto, id, unixTime);
				            }
				            edittext.requestFocus();
				            return true;
				        }
				    });
				} else if(tipoDato.equals("Date")) {					
					// TimePicker
					// TODO
					if(id == 736){
						edittext.setVisibility(View.VISIBLE);
						contenedorCampo.setVisibility(View.VISIBLE);
					}
					edittext.setKeyListener(null);
					edittext.setOnTouchListener(new OnTouchListener() {
				        @Override
				        public boolean onTouch(View v, MotionEvent event) {
				            if(MotionEvent.ACTION_UP == event.getAction()) {
				            	// Llama el timePicker
				            	// TODO: dependiendo del valor, calcula el unixtime
				            	// String HH:MM to milisegundos.
				            	long unixDate = 0;
				            	mostrarDatePicker(contexto, id, unixDate);
				            }
				            edittext.requestFocus();
				            return true;
				        }
				    });
				} else if(modoEdicion.equals("AutoComplete")){
					// Si es AutoComplete, inicializa el objeto autocompletar
					autocompletarCampo.setIdFormulario(formularioActivo.getId());
					
					// Habilita la actividad de consulta.
					edittext.setHint("Pulse aquí");
					edittext.setKeyListener(null);
					edittext.setOnTouchListener(new OnTouchListener() {
				        @Override
				        public boolean onTouch(View v, MotionEvent event) {
				            if(MotionEvent.ACTION_UP == event.getAction()) {
								autocompletarCampo.setIdCampo(id);
								autocompletarCampo.setNombreCampo(nombreCampo);
								
				            	// TODO: Llama la actividad de consulta de elementos.
				            	/*Intent intent = new Intent(contexto, ActividadAutocompletarCampo.class);
				            	intent.putExtra("autocompletarCampo", autocompletarCampo);
				            	actividad.startActivityForResult(intent, REQUEST_AUTOCOMPLETE);*/
				            	autocompletarCampo.abrirActividadEspecifica(
				            			actividad, contexto, AutocompletarCampo.AUTOCOMPLETE, REQUEST_AUTOCOMPLETE, layoutView);
				            }
				            edittext.requestFocus();
				            return true;
				        }
				    });
				} else if (modoEdicion.equals("ScanCode")) {
					// Si es ScanCode, habilita la actividad de Zxing.
					edittext.setKeyListener(null);
					edittext.setOnTouchListener(new OnTouchListener() {
				        @Override
				        public boolean onTouch(View v, MotionEvent event) {
				            if(MotionEvent.ACTION_UP == event.getAction()) {
				            	// Llama la actividad para scanear codigos
				            	Intent intent = new Intent(contexto, ActividadEscanearCodigo.class);
				            	intent.putExtra("idCampo", id);
				            	actividad.startActivityForResult(intent, REQUEST_SCANCODE);
				            }
				            edittext.requestFocus();
				            return true;
				        }
				    });
				} else if (modoEdicion.equals("ListView")) {
					// Si es ListView, inicializa el objeto autocompletar
					autocompletarCampo.setIdFormulario(formularioActivo.getId());
					autocompletarCampo.setIdCampo(id);
					autocompletarCampo.setNombreCampo(nombreCampo);
					
					// Habilita la actividad de consulta.
					edittext.setHint("Pulse aqui");
					edittext.setKeyListener(null);
					edittext.setOnTouchListener(new OnTouchListener() {
				        @Override
				        public boolean onTouch(View v, MotionEvent event) {
				            if(MotionEvent.ACTION_UP == event.getAction()) {
				            	// TODO: Llama la actividad de consulta de elementos.
				            	/*Intent intent = new Intent(contexto, ActividadListaCampo.class);
				            	intent.putExtra("autocompletarCampo", autocompletarCampo);
				            	actividad.startActivityForResult(intent, REQUEST_LISTACAMPO);*/
				            	autocompletarCampo.abrirActividadEspecifica(
				            			actividad, contexto, AutocompletarCampo.LISTACAMPO, REQUEST_LISTACAMPO, layoutView);
				            }
				            edittext.requestFocus();
				            return true;
				        }
				    });
				} else if(modoEdicion.equals("TextArea")) {
					edittext.setSingleLine(false);
					edittext.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
					edittext.setLines(5);
					edittext.setGravity(Gravity.TOP | Gravity.LEFT);
				}
				
				// Adiciona el nombre y el elemento al contenedor
				contenedorCampo.addView(textview);
				contenedorCampo.addView(edittext);
				contenedorFormulario.addView(contenedorCampo);

			} else if(modoEdicion.equals("Spinner")) {
				// Crea una lista de seleccion
				final Spinner spinner = new Spinner(contexto);
				camposHash.put(String.valueOf(id), campo);

				// Crea la lista de elementos
				String[] lsta_elmntos = datosAdicionales.split(",");
				
				// Crea y adiciona el Adapter para el spinner
				ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(contexto, 
						android.R.layout.simple_spinner_item, lsta_elmntos);
				arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				spinner.setAdapter(arrayAdapter);
				
				// Asigna el id para el spinner
				spinner.setId(id);
				
				// Adiciona un listener al spinner
				spinner.setOnItemSelectedListener(onItemSelectedListener);
				
				// Adiciona el nombre y el elemento al contenedor
				contenedorCampo.addView(textview);
				contenedorCampo.addView(spinner);
				contenedorFormulario.addView(contenedorCampo);

			} else if(modoEdicion.equals("MultiSpinner")) {
				// Crea una lista de seleccion
				final MultiSelectionSpinner multiSpinner = new MultiSelectionSpinner(contexto);
				camposHash.put(String.valueOf(id), campo);

				// Crea la lista de elementos
				String[] lsta_elmntos = datosAdicionales.split(",");
				multiSpinner.setItems(lsta_elmntos);
				
				if(id==734){
					multiSpinner.setVisibility(View.VISIBLE);
					contenedorCampo.setVisibility(View.VISIBLE);
				}
				
				// Asigna el id para el spinner
				multiSpinner.setId(id);
				
				// Adiciona un listener al spinner
				multiSpinner.setOnItemSelectedListener(onItemSelectedListener);
				
				// Adiciona el nombre y el elemento al contenedor
				contenedorCampo.addView(textview);
				contenedorCampo.addView(multiSpinner);
				contenedorFormulario.addView(contenedorCampo);

			}else if(modoEdicion.equals("CheckBox")) {
				// Crea un checkbox
				CheckBox checkbox = new CheckBox(contexto);
				camposHash.put(String.valueOf(id), campo);

				// Asigna el id para el checkbox y lo adiciona al contenedor
				checkbox.setId(id);
				
				// Adiciona un listener al checkbox
				checkbox.setOnCheckedChangeListener(onCheckedChangeListener);
				
				// Adiciona el nombre y el elemento al contenedor
				contenedorCampo.addView(textview);
				contenedorCampo.addView(checkbox);
				contenedorFormulario.addView(contenedorCampo);
				
			} else if (modoEdicion.equals("Detalle")) {
				// Crea un button
				Button button = new Button(contexto);
				camposHash.put(String.valueOf(id), campo);
				
				// Asigna el id para el button y lo adiciona al contenedor
				button.setId(id);
				button.setText("DETALLE");
				
				// Adiciona el listener del botón
				button.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// Extrae elF formulario asociado.
						int id_frmlrio = Integer.parseInt(datosAdicionales);
						Formulario formularioAsociado = buscarFormularioLista(id_frmlrio);
						
						// Abre la nueva actividad para llenar el detalle
						auditorFormulario.eventoCampoDetalle(contexto, campo, formularioAsociado);
						/*Intent intent = new Intent(contexto, ActividadSubformulario.class);
		            	intent.putExtra("idCampo", id);
						intent.putExtra("Formulario", frmlrio);
						((Activity) contexto).startActivityForResult(intent, REQUEST_SUBFORMULARIO);*/
					}
				});
				
				// Adiciona el elemento al contenedor
				contenedorCampo.addView(textview);
				contenedorCampo.addView(button);
				contenedorFormulario.addView(contenedorCampo);
				
			} else if(modoEdicion.equals("TextView")) {
				// Crea un TextView Grande
				TextView campoTextView = new TextView(contexto);
				campoTextView.setTextAppearance(contexto, R.style.valor_etiqueta_oscuro);
				camposHash.put(String.valueOf(id), campo);

				// Asigna el id para el campoTextView y algunas propiedades.
				campoTextView.setId(id);
				if (datosAdicionales != null) {
					campoTextView.setText(datosAdicionales);
				}
			
				// Adiciona el nombre y el elemento al contenedor
				contenedorCampo.addView(textview);
				contenedorCampo.addView(campoTextView);
				contenedorFormulario.addView(contenedorCampo);
				
			} else if (modoEdicion.equals("AdjuntaFotos")) {

				// Crea un button
				Button button = new Button(contexto);
				camposHash.put(String.valueOf(id), campo);
				
				// Asigna el id para el button y lo adiciona al contenedor
				button.setId(id);
				button.setText("ADJUNTAR");


				// Adiciona el listener del botón
				button.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {

						// abre un dialogo de opciones para seleccionar las opciones
						// permite elegir la opcion de seleccionar fotos o tomar una nueva
						mostrarDialogOpciones(contexto, auditorFormulario, campo);
						// Abre la nueva actividad para adjuntar las fotos

					}
				});
				
				// Adiciona el elemento al contenedor
				contenedorCampo.addView(textview);
				contenedorCampo.addView(button);
				contenedorFormulario.addView(contenedorCampo);
			}else if (modoEdicion.equals("Button")) {
				// Crea un button
				Button button = new Button(contexto);
				camposHash.put(String.valueOf(id), campo);
				
				// Asigna el id para el button y lo adiciona al contenedor
				button.setId(id);
				button.setText(campo.getNombre());
				
				// Adiciona el listener del botón
				button.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// Abre la nueva actividad para adjuntar las fotos
						auditorFormulario.validarEventoCampo(
								contexto, layoutView, campo, null, contenedoresCamposHash, camposHash
							);
					}
				});

				//salvarObjetosContexto(true);
				
				// Adiciona el elemento al contenedor
				contenedorCampo.addView(textview);
				contenedorCampo.addView(button);
				contenedorFormulario.addView(contenedorCampo);
			}
		}
	}


	private void mostrarDialogOpciones(final Context context, final AuditorFormulario auditorFormulario , final Campo campo) {
		final CharSequence[] opciones={"Tomar Foto","Elegir de Galeria","Cancelar"};
		final AlertDialog.Builder builder=new AlertDialog.Builder(context);
		builder.setTitle("Elige una Opción");
		builder.setItems(opciones, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				if (opciones[i].equals("Tomar Foto")){

					auditorFormulario.eventoCampoAdjuntaFotos(context, campo , REQUEST_FOTO);
				}else{
					if (opciones[i].equals("Elegir de Galeria")){

						auditorFormulario.eventoCampoAdjuntaFotos(context, campo , GALERIA);
					}else{
						dialogInterface.dismiss();
					}
				}
			}
		});
		builder.show();
	}


	// Permite asignar los valores a los campos de la interfaz.
	public void formularioAInterfaz(
			Context contexto, View layoutView, HashMap<String, Campo> camposHash, Configuracion configApp){
		List<Campo> cmpos = formularioActivo.getCampos();
		int nmro_cmpos = cmpos.size();
		
		// Recorre los Campos del Formulario
		for (int i = 0; i < nmro_cmpos; i++) {
			
			// Extrae el campo
			final Campo cmpo = cmpos.get(i);
			
			// Extrae los parametros del campo
			int id = cmpo.getId();
			String mdo_edcion = cmpo.getModoEdicion();
			final String dtos_adcnles = cmpo.getDatosAdicionales();
			String vlor = cmpo.getValor();
			camposHash.put(String.valueOf(id), cmpo);

			// Dependiendo del modo de edición crea el elemento.
			if (mdo_edcion.equals("EditText") 
					|| mdo_edcion.equals("AutoComplete") 
					|| mdo_edcion.equals("ScanCode")
					|| mdo_edcion.equals("ListView")
					|| mdo_edcion.equals("TextArea")) {
				
				// Consulta un cuadro de edicion
				final EditText edittext = (EditText) layoutView.findViewById(id);
				
				// Asigna el valor siempre y cuando el campo sea diferente
				if (vlor != null && !edittext.getText().toString().equals(vlor) && !vlor.equals("null") ){
					edittext.setText(vlor);
				}

				if (!cmpo.isEditable()){
					edittext.setEnabled(false);
				}else{
					edittext.setEnabled(true);
				}

			} else if(mdo_edcion.equals("Spinner")) {
				// Consula la lista de seleccion
				Spinner spinner = (Spinner) layoutView.findViewById(id);

				// Extrae los elementos
				String[] lsta_elmntos = dtos_adcnles.split(",");

				// Asigna el valor
				if (vlor != null) {
					int nmro_vlres = lsta_elmntos.length;
					for (int j = 0; j < nmro_vlres; j++) {
						if(lsta_elmntos[j].equals(vlor)){
							spinner.setSelection(j);
							break;
						}
					}
				} else {
					// Por defecto selecciona la primer opcion
					spinner.setSelection(0);
				}
				if (!cmpo.isEditable()){
					spinner.setEnabled(false);
					spinner.setClickable(false);
				}else{
					spinner.setEnabled(true);
					spinner.setClickable(true);
				}
				
			} else if(mdo_edcion.equals("MultiSpinner")) {
				// Consula la lista de seleccion
				MultiSelectionSpinner multiSpinner = (MultiSelectionSpinner) layoutView.findViewById(id);

				// Extrae los elementos del valor
				if(vlor != null) {
					String[] valoreSeleccionados = vlor.split(", ");
	
					// Asigna el valor
					multiSpinner.setSelection(valoreSeleccionados);
				}

				if (!cmpo.isEditable()){
					multiSpinner.setEnabled(false);
					multiSpinner.setClickable(false);
				}else{
					multiSpinner.setEnabled(true);
					multiSpinner.setClickable(true);
				}

			} else if(mdo_edcion.equals("CheckBox")) {
				// Consulta un checkbox
				CheckBox checkbox = (CheckBox) layoutView.findViewById(id);
				
				// Asigna el valor
				if(vlor != null && vlor.equals("true")){
					checkbox.setChecked(true);
				}else{
					checkbox.setChecked(false);
				}
				if (!cmpo.isEditable()){
					checkbox.setEnabled(false);
					checkbox.setClickable(false);
				}else{
					checkbox.setEnabled(true);
					checkbox.setClickable(true);
				}
				
			} else if (mdo_edcion.equals("Detalle")) {
				// Consulta los datos en memoria
				@SuppressWarnings("unchecked")
				Vector<Vector<Campo>> dtos = (Vector<Vector<Campo>>) configApp.getMemoria(id);

				// Si no hay datos pero existe un valor, reconstruye los datos del detalle
				if(dtos == null && vlor != null && vlor.length() > 0){
					// Extrae el formulario asociado.
					int id_frmlrio = Integer.parseInt(dtos_adcnles);
					Formulario frmlrio = buscarFormularioLista(id_frmlrio);
					
					// Reconstruye los datos.
					configApp.setMemoria(id, jsonToCampoDetalle(vlor, frmlrio));
				}
				Button detalle = (Button) layoutView.findViewById(id);
				if (!cmpo.isEditable()){
					detalle.setEnabled(false);
					detalle.setClickable(false);
				}else{
					detalle.setEnabled(true);
					detalle.setClickable(true);
				}
			}else if (mdo_edcion.equals("Button")) {
				Log.d("sebas", "estoy acá " + cmpo.getNombre() + cmpo.isEditable());
				Button boton = (Button) layoutView.findViewById(id);
				if (cmpo.isEditable()){
					boton.setEnabled(true);
					boton.setClickable(true);
				}else{
					boton.setEnabled(false);
					boton.setClickable(false);
				}
			}  else if (mdo_edcion.equals("TextView")) {
				// Consulta un cuadro de texto
				TextView textView = (TextView) layoutView.findViewById(id);
				
				// Asigna el valor si no es un campo informativo
				if (!cmpo.isInformativo()){
					if(vlor != null) textView.setText(vlor);
				}
			} else if (mdo_edcion.equals("AdjuntaFotos")) {
				@SuppressWarnings("unchecked")
				Vector<String[]> datos = (Vector<String[]>) configApp.getMemoria(id);

				// Si no hay datos pero existe un valor, reconstruye los datos del detalle
				if(datos == null && vlor != null && vlor.length() > 0){				
					// Reconstruye los datos.
					configApp.setMemoria(id, jsonToCampoAdjuntaFotos(vlor));
				}
			}
		}
	}
	
	// Permite asignar los valores del Formulario(Interfaz) al Formulario(Objeto)
	public boolean interfazAFormulario(
			Context contexto, Configuracion configApp, boolean validarCampos,
			AuditorFormulario auditorFormulario, View layoutView, HashMap<String, View> contenedoresCamposHash) {
		Activity activity = (Activity) contexto;
		boolean asgncion_extsa = true;

		// Se obtiene el valor de Bandera de guardado
		boolean banderaGuardadoTemporal = formularioActivo.banderaGuardadoTemporal;

		// Extrae los campos
		List<Campo> cmpos = formularioActivo.getCampos();
		int nmro_cmpos = cmpos.size();
		
		// Recorre los Campos del Formulario
		for (int i = 0; i < nmro_cmpos; i++) {
			
			// Extrae el campo
			Campo campo = cmpos.get(i);
			
			// Extrae los parametros del campo
			int id = campo.getId();
			String mdo_edcion = campo.getModoEdicion();

			// Extrae el view de la actividad
			View view = activity.findViewById(id);
			
			// Dependiendo del modo de edicion, realiza el cast al view
			// Extrae el valor y lo guarda en el objeto
			String vlor = "";
			HashMap<String, String> rsltdo = null;
			if (mdo_edcion.equals("EditText") 
					|| mdo_edcion.equals("AutoComplete") 
					|| mdo_edcion.equals("ScanCode")
					|| mdo_edcion.equals("ListView")
					|| mdo_edcion.equals("TextArea")) {
				EditText edittext = (EditText) view;
				vlor = edittext.getText().toString();	
				if(edittext.getCurrentTextColor() == Color.RED){
					vlor = "";
				}
				if (campo.isEditable()){
					edittext.setEnabled(true);
				}else{
					edittext.setEnabled(false);
				}
			}else if(mdo_edcion.equals("Spinner")){
				Spinner spinner = (Spinner) view;

				vlor = (String)spinner.getSelectedItem();
				if (!campo.isEditable()){
					spinner.setEnabled(false);
					spinner.setClickable(false);
				}else{
					spinner.setEnabled(true);
					spinner.setClickable(true);
				}

			}else if(mdo_edcion.equals("MultiSpinner")){
				MultiSelectionSpinner multiSpinner = (MultiSelectionSpinner) view;
				vlor = (String)multiSpinner.getSelectedItemsAsString();
				if (!campo.isEditable()){
					multiSpinner.setEnabled(false);
					multiSpinner.setClickable(false);
				}else{
					multiSpinner.setEnabled(true);
					multiSpinner.setClickable(true);
				}
			}else if(mdo_edcion.equals("CheckBox")){
				CheckBox checkBox = (CheckBox) view;
				if(checkBox.isChecked()){
					vlor = "true";
				}else{
					vlor = "false";
				}
				if (!campo.isEditable()){
					checkBox.setEnabled(false);
					checkBox.setClickable(false);
				}else{
					checkBox.setEnabled(true);
					checkBox.setClickable(true);
				}
				
			}else if(mdo_edcion.equals("Detalle")){
				// Recorre el vector con el detalle,
				// Valida la información y genera un String JSON 
				Formulario formularioReferencia = buscarFormularioLista(Integer.parseInt(campo.getDatosAdicionales()));
				@SuppressWarnings("unchecked")
				Vector<Vector<Campo>> dtos = (Vector<Vector<Campo>>) configApp.getMemoria(id);
				rsltdo = campoDetalleToJson(campo.getNombre(),dtos, auditorFormulario, formularioReferencia, configApp);
				vlor = rsltdo.get("JSON");
				//toolsapp.showLongMessage("Datos: " + vlor, context);
				Button detalle = (Button) layoutView.findViewById(id);
				if (!campo.isEditable()){
					detalle.setEnabled(false);
					detalle.setClickable(false);
				}else{
					detalle.setEnabled(true);
					detalle.setClickable(true);
				}
				
			}else if (mdo_edcion.equals("Button")) {
				Log.d("sebas", "estoy acá " + campo.getNombre() + campo.isEditable());
				Button boton = (Button) layoutView.findViewById(id);
				if (!campo.isEditable()){
					boton.setEnabled(false);
					boton.setClickable(false);
				}else{
					boton.setEnabled(true);
					boton.setClickable(true);
				}
			} else if(mdo_edcion.equals("TextView")){
				TextView campoTextView = (TextView) view;
				vlor = (String)campoTextView.getText().toString();
				if(campoTextView.getCurrentTextColor() == Color.RED){
					vlor = "";
				}
			}else if(mdo_edcion.equals("AdjuntaFotos")){
				// Recorre el vector con el detalle,
				// Valida la información y genera un String JSON 
				@SuppressWarnings("unchecked")
				Vector<String[]> datos = (Vector<String[]>) configApp.getMemoria(id);
				vlor = campoAdjuntaFotosToJson(datos);
				toolsApp.showLongMessage("Datos: " + vlor, contexto);
			}

			if(vlor != null){
				if(!banderaGuardadoTemporal){
					// Verifica el valor del campo y el resultado de procesamiento si existe.
					if((validarCampos && campo.isObligatorio() && vlor.equals("") && campo.isVisible()  ) //&& campo.isVisible())
						|| (mdo_edcion.equals("Detalle") && validarCampos && vlor.equals("") && campo.isVisible())){
						// Verifica si existe algun resultado de procesamiento
						if(rsltdo != null ){
							toolsApp.showLongMessage(rsltdo.get("Mensaje"), contexto);
						} else {
							toolsApp.showLongMessage("El campo '"+campo.getNombre()+ "' es incorrecto!", contexto);
						}

						asgncion_extsa = false;
						break;
					}
				}
			}else{
				if((mdo_edcion.equals("Detalle") && campo.isObligatorio() && campo.isVisible())){
					toolsApp.showLongMessage(rsltdo.get("Mensaje"), contexto);
					asgncion_extsa = false;
					break;
				}
			}
			
			// Guardar Valor en Formulario(Objeto)
			campo.setValor(vlor);
		}
		return asgncion_extsa;
	}	
		
	// Permite mostrar un TimePicker
	private void mostrarTimePicker(Context contexto, int idCampo, long unixTime){
		FragmentActivity actividad = (FragmentActivity) contexto;
		DialogFragment newFragment = new TimePickerFragment();
		Bundle args = new Bundle();
		if (unixTime > 0){
			args.putBoolean("definirTiempo", true);
		}
		
		// Dependiendo del botón adiciona la fecha de inicio o fin
		args.putInt("idCampo", idCampo);
		args.putInt("hora", toolsApp.unixTimeToHours(unixTime));
		args.putInt("minutos", toolsApp.unixTimeToMinutes(unixTime));
		newFragment.setArguments(args);
		newFragment.show(actividad.getSupportFragmentManager(), "timePicker");
	}
	
	// Permite mostrar un DatePicker
	private void mostrarDatePicker(Context contexto, int idCampo, long unixDate){
		FragmentActivity actividad = (FragmentActivity) contexto;
		DialogFragment newFragment = new DatePickerFragment();
		Bundle args = new Bundle();
		if (unixDate > 0){
			args.putBoolean("definirFecha", true);
		}
		
		// Dependiendo del botón adiciona la fecha de inicio o fin
		args.putInt("idCampo", idCampo);
		args.putLong("fecha", unixDate);
		newFragment.setArguments(args);
		newFragment.show(actividad.getSupportFragmentManager(),"datePicker");
	}

	// Método que permite reconstruir el vector de datos de un campo detalle
	public Vector<Vector<Campo>> jsonToCampoDetalle(String valorJson, Formulario formulario){
		Vector<Vector<Campo>> datos = new Vector<Vector<Campo>>();
		
		try {
			String json = "";
			if (Configuracion.utilizarWEBSOCKET) {
				json = valorJson;
			} else {
				json = valorJson.replace("\\\\\"", "\\\"");
			}
			JSONArray detalleJson;
			
			if(json != null && !json.equals("") && !json.equals("null"))
				detalleJson = new JSONArray(json);
			else
				detalleJson = new JSONArray();
			
			int numeroElementos = detalleJson.length();
			
			for (int i = 0; i < numeroElementos; i++) {
				
				JSONObject formularioJson = detalleJson.getJSONObject(i);
				JSONArray camposJson = formularioJson.getJSONArray("campos");
				int numeroCampos = camposJson.length();
				
				Vector<Campo> camposPlantilla = formulario.getCamposPlantilla();
				
				for(int j=0;j<numeroCampos;j++){
					JSONObject campoJson = camposJson.getJSONObject(j);
					int idCampoJson = campoJson.getInt("idCampo");
					String valor = campoJson.getString("valor");
					
					// Recorre los camposPlantilla hasta encontrar uno válido
					Campo campo = camposPlantilla.get(j); 
					if(campo.getId() == idCampoJson && campo.isVisible() && !campo.isInformativo() && campo.getValor() == null){
						// Campo válido, setea el valor y marca la bandera para salir del ciclo
						campo.setValor(valor);
					}
				}
				
				// Adiciona los campos de un elemento al vector de datos
				datos.add(camposPlantilla);
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return datos;
	}
	
	// TODO: Comentar
	public HashMap<String, String> campoDetalleToJson(String nombreCampo,Vector<Vector<Campo>> vectorCampos, 
			AuditorFormulario auditorFormulario, Formulario formularioReferencia,Configuracion configApp){
		
		HashMap<String, String> resultado = new HashMap<String, String>();
		StringBuilder json = new StringBuilder();
		
		
		String mnsje = "";
		
		if(vectorCampos != null) {
			int numeroItems = vectorCampos.size();
			json.append("[");
			for(int i=0;i<numeroItems;i++){
				Vector<Campo> campos = vectorCampos.get(i);
				
				int nmro_cmpos = campos.size();
				for(int j = 0; j < nmro_cmpos; j++){
					Campo cmpo_item = campos.elementAt(j);
					String vlor = cmpo_item.getValor();
					
					// Verifica el valor
					if(cmpo_item.isObligatorio() && cmpo_item.isVisible() && (vlor == null || vlor.equals(""))){
						// Valor no válido
						mnsje = "El campo '" + cmpo_item.getNombre() + "' del item # " + (i+1) + " de '" + nombreCampo + "' es incorrecto!";
						resultado.put("Mensaje", mnsje);
						resultado.put("JSON","");
						return resultado;
					}
				}
				
				Formulario formulario = new Formulario(formularioReferencia.getId(), 
						formularioReferencia.getNombre(), formularioReferencia.getIcono(), 
						campos, formularioReferencia.getTipo(), formularioReferencia.isMultiple(), 
						formularioReferencia.getTablaAsociadas());
				
				if(i>0){
					json.append(",");
				}
				
				json.append("{");
				json.append(formulario.generarJson(configApp));
				json.append("}");
			}
			
			json.append("]");
		}else {
			mnsje = "El campo '" + nombreCampo + "' no tiene items!";
		}
		
		if(!json.toString().equals("")){
			// Crea el resultado
			resultado.put("JSON", json.toString());
		}
		
		resultado.put("Mensaje", mnsje);
		
		return resultado;
	}

	// Método que permite reconstruir el vector de datos de las fotos que se habían adjuntado.
	private Vector<String[]> jsonToCampoAdjuntaFotos(String valorJson) {
		Vector<String[]> datos = new Vector<String[]>();
		
		try {
			String json = "";
			if (Configuracion.utilizarWEBSOCKET) {
				json = valorJson;
			} else {
				json = valorJson.replace("\\\\\"", "\\\"");
			}
			JSONArray detalleJson;
			
			if(json != null && !json.equals("") && !json.equals("null"))
				detalleJson = new JSONArray(json);
			else
				detalleJson = new JSONArray();
			int numeroElementos = detalleJson.length();
			
			// Recorre los elementos
			for (int i = 0; i < numeroElementos; i++) {
				JSONArray datosImagenJson = detalleJson.getJSONArray(i);
			
				// Adiciona los campos de un elemento al vector de datos
				String[] datosImagen = {datosImagenJson.getString(0), datosImagenJson.getString(1)};
				datos.add(datosImagen);
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		// Retorno
		return datos;
	}
	
	// Método que permite generar un String Json a partir del un vector de datos
	private String campoAdjuntaFotosToJson(Vector<String[]> datos) {
		StringBuilder json = new StringBuilder();
		if(datos != null) {
			int numeroItems = datos.size();
			for(int i=0; i < numeroItems; i++){
				String[] datosImagen = datos.get(i);
				if(json.length() > 0) json.append(",");
				json.append("[\"" + datosImagen[0] + "\",\"" + datosImagen[1] + "\"]");
			}
			json.insert(0, "[").append("]");
		}
				
		// Retorno...
		return json.toString();
	}
}
