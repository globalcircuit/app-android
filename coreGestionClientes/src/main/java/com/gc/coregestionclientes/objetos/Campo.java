package com.gc.coregestionclientes.objetos;

import java.util.List;
import java.util.Vector;

import android.os.Parcel;
import android.os.Parcelable;

import com.gc.coregestionclientes.Configuracion;

public class Campo implements Parcelable{
	private int id;
	private String nmbre;
	private String mdo_edcion;
	private String tpo_dto;
	private String dtos_adcnles;
	private String vlor;
	
	// Características
	private boolean oblgtrio = false;
	private boolean vsble = true; 
	private boolean infrmtvo = false; 
	private boolean ttlza = false; 
	private boolean rsmen = false;
	private boolean editable = true;
	private List<String[]> camposAsociados;
	
	// Constructor
	public Campo (
			int id, String nmbre, String mdo_edcion, String tpo_dto, 
			String dtos_adcnles, 
			boolean oblgtrio, boolean vsble, boolean infrmtvo, boolean ttlza, boolean rsmen, 
			List<String[]> camposAsociados, boolean editable
	) {
		this.id = id;
		this.nmbre = nmbre;
		this.mdo_edcion = mdo_edcion;
		this.tpo_dto = tpo_dto;
		this.dtos_adcnles = dtos_adcnles;
		this.oblgtrio = oblgtrio;		
		this.vsble = vsble;
		this.infrmtvo = infrmtvo;
		this.ttlza = ttlza;
		this.rsmen = rsmen;
		this.camposAsociados = camposAsociados;
		this.editable = editable;
	}
	
	// Contructor parceable
	public Campo (Parcel parcel) {
		this.id = parcel.readInt();
		this.nmbre = parcel.readString();
		this.mdo_edcion = parcel.readString();
		this.tpo_dto = parcel.readString();
		this.dtos_adcnles = parcel.readString();
        this.oblgtrio = (parcel.readInt() == 0) ? false : true;
		this.vsble = (parcel.readInt() == 0) ? false : true;
		this.infrmtvo = (parcel.readInt() == 0) ? false : true;
		this.ttlza = (parcel.readInt() == 0) ? false : true;
		this.rsmen = (parcel.readInt() == 0) ? false : true;
		this.editable = (parcel.readInt() == 0) ? false : true;
    }
	
	// ?? Creator ??
	public static Creator<Campo> CREATOR = new Creator<Campo>() {
        public Campo createFromParcel(Parcel parcel) {
            return new Campo(parcel);
        }

        public Campo[] newArray(int size) {
            return new Campo[size];
        }
    };
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.id);
		dest.writeString(this.nmbre);
		dest.writeString(this.mdo_edcion);
		dest.writeString(this.tpo_dto);
		dest.writeString(this.dtos_adcnles);
		dest.writeInt(this.oblgtrio ? 1 : 0);
		dest.writeInt(this.vsble ? 1 : 0);
		dest.writeInt(this.infrmtvo ? 1 : 0);
		dest.writeInt(this.ttlza ? 1 : 0);
		dest.writeInt(this.rsmen ? 1 : 0);
		dest.writeInt(this.editable ? 1 : 0);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nmbre
	 */
	public String getNombre() {
		return nmbre;
	}

	/**
	 * @param nmbre the nmbre to set
	 */
	public void setNombre(String nmbre) {
		this.nmbre = nmbre;
	}

	/**
	 * @return the mdo_edcion
	 */
	public String getModoEdicion() {
		return mdo_edcion;
	}

	/**
	 * @param mdo_edcion the mdo_edcion to set
	 */
	public void setModoEdicion(String mdo_edcion) {
		this.mdo_edcion = mdo_edcion;
	}

	/**
	 * @return the tpo_dto
	 */
	public String getTipoDato() {
		return tpo_dto;
	}

	/**
	 * @param tpo_dto the tpo_dto to set
	 */
	public void setTipoDato(String tpo_dto) {
		this.tpo_dto = tpo_dto;
	}

	/**
	 * @return the dtos_adcnles
	 */
	public String getDatosAdicionales() {
		return dtos_adcnles;
	}

	/**
	 * @param dtos_adcnles the dtos_adcnles to set
	 */
	public void setDatosAdicionales(String dtos_adcnles) {
		this.dtos_adcnles = dtos_adcnles;
	}

	/**
	 * @return the vlor
	 */
	public String getValor() {
		return vlor;
	}

	/**
	 * @param vlor the vlor to set
	 */
	public void setValor(String vlor) {
		this.vlor = vlor;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	/**
	 * @return the oblgtrio
	 */
	public boolean isObligatorio() {
		return oblgtrio;
	}

	/**
	 * @param oblgtrio the oblgtrio to set
	 */
	public void setObligatorio(boolean oblgtrio) {
		this.oblgtrio = oblgtrio;
	}

	/**
	 * @return the vsble
	 */
	public boolean isVisible() {
		return vsble;
	}

	/**
	 * @param vsble the vsble to set
	 */
	public void setVisible(boolean vsble) {
		this.vsble = vsble;
	}

	/**
	 * @return the infrmtvo
	 */
	public boolean isInformativo() {
		return infrmtvo;
	}

	/**
	 * @param infrmtvo the infrmtvo to set
	 */
	public void setInformativo(boolean infrmtvo) {
		this.infrmtvo = infrmtvo;
	}

	/**
	 * @return the ttlza
	 */
	public boolean isTotaliza() {
		return ttlza;
	}

	/**
	 * @param ttlza the ttlza to set
	 */
	public void setTotaliza(boolean ttlza) {
		this.ttlza = ttlza;
	}

	/**
	 * @return the rsmen
	 */
	public boolean isResumen() {
		return rsmen;
	}

	/**
	 * @param rsmen the rsmen to set
	 */
	public void setResumen(boolean rsmen) {
		this.rsmen = rsmen;
	}
	
	public void setCamposAsociados(List<String[]> camposAsociados) {
		this.camposAsociados = camposAsociados;
	}
	
	public List<String[]> getCamposAsociados() {
		return camposAsociados;
	}
	
	// TODO: Comentarios y bloques de código mas claros
	public String generarJson(Configuracion aplicacion,ControladorFormularios controladorFormularios,Vector<Vector<Campo>> campos){
		StringBuilder campoJson = new StringBuilder();
		
		// Se arma cabecera de campo
		campoJson.append("{");
		campoJson.append("\"idCampo\": " +id + ",");
		campoJson.append("\"tipoDato\": \"" + tpo_dto + "\",");
		campoJson.append("\"modoEdicion\": \"" + mdo_edcion + "\",");
		campoJson.append("\"camposAsociados\": [");
		
		// Se valida que el campo tenga campos asociados
		if(camposAsociados != null){
			int numeroCamposAsociados = camposAsociados.size();
			
			// Recorre los campos asociados a cada formulario
			for(int k = 0;k<numeroCamposAsociados;k++){
				//Se obtiene campo asociado en posicion k
				String[] campoAsociado = camposAsociados.get(k);
				if(k>0) campoJson.append(",");
				
				campoJson.append("{");
				campoJson.append("\"tabla\": \"" + campoAsociado[0] + "\",");
				campoJson.append("\"nombreCampo\": \"" + campoAsociado[1] + "\"");
				campoJson.append("}");
			}
		}
		
		// Cierra arreglo de campos asociados
		campoJson.append("],");
		
		// Verifica si el campo es un "Detalle"
		/*if(mdo_edcion.equals("Detalle")){
			JSONArray jsonDetalle;
			int idSubformulario = Integer.parseInt(dtos_adcnles);
			
			try {
				if(vlor != null && !vlor.equals("") && !vlor.equals("null"))
					jsonDetalle = new JSONArray(vlor);
				else
					jsonDetalle = new JSONArray();
				
				int numeroDetalles = jsonDetalle.length();
				
				for(int i=0;i<numeroDetalles;i++){
					Formulario formulario = controladorFormularios.cargarFormularioLocal(
							aplicacion.getSqliteRoutines(), 
							""
							, Formulario.APLICACION, idSubformulario);
					
					// Se setean campos
					formulario.setCampos(campos.get(i));
					
					formulario.generarJson(aplicacion);
				}
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}*/
		
		// Adiciona el valor validando el tipo de campo
		if(tpo_dto.equals("Number") || tpo_dto.equals("Decimal")){
			campoJson.append("\"valor\": " + ((vlor == null || (vlor != null && vlor.equals(""))) ? "0" : vlor));				
		} else {
			String valor = (vlor != null ? vlor.replace("\"", "\\\"") : vlor);
			campoJson.append("\"valor\": \"" + valor + "\"");
		}
		campoJson.append("}");
		
		// Retorna Json del campo...
		return campoJson.toString();
	}
}
