package com.gc.coregestionclientes.objetos;

public class Usuario {
	private int id_clnte_global;
	private String nmbres_clnte_global;
	private String sdnmo;
	private boolean bd_adcnal;
	private int id_usrio;
	private String nmbres_usrio;
	private String codigo;
	private String login;
	private String pass;
	private boolean bandera_login;
	
	// TODO: Variables de Modulos, perfiles, BD etc.
	private boolean sncrnzar_BD = false;
	private int dmnsion_db_prncpal = -1;
	private boolean sncrnzar_BD_adcnal = false;
	private int dmnsion_db_adcnal = -1;
	private int intervaloSeguimiento = -1;
	
	/**
	 * @param id_clnte_global
	 * @param nmbres_clnte_global
	 * @param sdnmo
	 * @param id_usrio
	 * @param nmbres_usrio
	 */
	public Usuario(
			int id_clnte_global, String nmbres_clnte_global, String sdnmo, 
			boolean bd_adcnal, int id_usrio, String nmbres_usrio, String codigo,  int intervaloSeguimiento,
			String login, String pass, boolean bandera_login	)
		{
		this.id_clnte_global = id_clnte_global;
		this.nmbres_clnte_global = nmbres_clnte_global;
		this.sdnmo = sdnmo;
		this.bd_adcnal = bd_adcnal;
		this.id_usrio = id_usrio;
		this.nmbres_usrio = nmbres_usrio;
		this.codigo = codigo;
		this.intervaloSeguimiento = intervaloSeguimiento;
		this.login = login;
		this.pass = pass;
		this.bandera_login = bandera_login;
	}
	
	/**
	 * @param to set login
	 */
	public void setIdLogin(String login) {
		this.login = login;
	}
	
	/**
	 * @param to set pass
	 */
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	/**
	 * @param to set banderaLogin
	 */
	public void setIdBanderaLogin(boolean bandera_login) {
		this.bandera_login = bandera_login;
	}
	
	/**
	 * @return the user login
	 */
	public String getLogin() {
		return login;
	}
	
	/**
	 * @return the user pass
	 */
	public String getPass() {
		return pass;
	}
	
	/**
	 * @return the user's flag login
	 */
	public boolean getBanderaLogin() {
		return bandera_login;
	}

	/**
	 * @return the id_clnte_global
	 */
	public int getIdClienteGlobal() {
		return id_clnte_global;
	}

	/**
	 * @param id_clnte_global the id_clnte_global to set
	 */
	public void setIdClienteGlobal(int id_clnte_global) {
		this.id_clnte_global = id_clnte_global;
	}

	/**
	 * @return the nmbres_clnte_global
	 */
	public String getNombresClienteGlobal() {
		return nmbres_clnte_global;
	}

	/**
	 * @param nmbres_clnte_global the nmbres_clnte_global to set
	 */
	public void setNombresClienteGlobal(String nmbres_clnte_global) {
		this.nmbres_clnte_global = nmbres_clnte_global;
	}
	
	/**
	 * @return the sdnmo
	 */
	public String getSeudonimo() {
		return sdnmo;
	}

	/**
	 * @param sdnmo the sdnmo to set
	 */
	public void setSeudonimo(String sdnmo) {
		this.sdnmo = sdnmo;
	}

	/**
	 * @return the bd_adcnal
	 */
	public boolean isBDAdicional() {
		return bd_adcnal;
	}

	/**
	 * @param bd_adcnal the bd_adcnal to set
	 */
	public void setBDAdicional(boolean bd_adcnal) {
		this.bd_adcnal = bd_adcnal;
	}

	/**
	 * @return the id_usrio
	 */
	public int getId() {
		return id_usrio;
	}

	/**
	 * @param id_usrio the id_usrio to set
	 */
	public void setId(int id_usrio) {
		this.id_usrio = id_usrio;
	}

	/**
	 * @return the nmbres_usrio
	 */
	public String getNombres() {
		return nmbres_usrio;
	}

	/**
	 * @param nmbres_usrio the nmbres_usrio to set
	 */
	public void setNombres(String nmbres_usrio) {
		this.nmbres_usrio = nmbres_usrio;
	}
	
	/**
	 * @return the identificacion
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param identificacion the identificacion to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the sncrnzar_BD
	 */
	public boolean isSincronizarBD() {
		return sncrnzar_BD;
	}

	/**
	 * @param sncrnzar_BD the sncrnzar_BD to set
	 */
	public void setSincronizarBD(boolean sncrnzar_BD) {
		this.sncrnzar_BD = sncrnzar_BD;
	}

	/**
	 * @return the dimensionDBPrincipal
	 */
	public int getDimensionDBPrincipal() {
		return dmnsion_db_prncpal;
	}

	/**
	 * @param dimensionDBPrincipal the dimensionDBPrincipal to set
	 */
	public void setDimensionDBPrincipal(int dmnsion_db_prncpal) {
		this.dmnsion_db_prncpal = dmnsion_db_prncpal;
	}

	/**
	 * @return the sncrnzar_BD_adcnal
	 */
	public boolean isSincronizarBDAdicional() {
		return sncrnzar_BD_adcnal;
	}

	/**
	 * @param sncrnzar_BD_adcnal the sncrnzar_BD_adcnal to set
	 */
	public void setSincronizarBDAdicional(boolean sncrnzar_BD_adcnal) {
		this.sncrnzar_BD_adcnal = sncrnzar_BD_adcnal;
	}
	
	/**
	 * @return the dimensionDBAdicional
	 */
	public int getDimensionDBAdicional() {
		return dmnsion_db_adcnal;
	}

	/**
	 * @param dimensionDBAdicional the dimensionDBAdicional to set
	 */
	public void setDimensionDBAdicional(int dmnsion_db_adcnal) {
		this.dmnsion_db_adcnal = dmnsion_db_adcnal;
	}

	/**
	 * @return the intervaloSeguimiento
	 */
	public int getIntervaloSeguimiento() {
		return intervaloSeguimiento;
	}

	/**
	 * @param intervaloSeguimiento the intervaloSeguimiento to set
	 */
	public void setIntervaloSeguimiento(int intervaloSeguimiento) {
		this.intervaloSeguimiento = intervaloSeguimiento;
	}
}
