package com.gc.coregestionclientes.objetos;

import java.util.List;
import java.util.Vector;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;

import com.gc.coregestionclientes.Configuracion;
import com.gc.coregestionclientes.accesodatos.SQLiteRoutines;

public class Formulario implements Parcelable{

	// Variables Estaticas (Tipo de formulario)
	public static final String TRANSACCIONAL = "TRANSACCIONAL";
	public static final String APLICACION = "APLICACION";
	public static final String SUBFORMULARIO = "SUBFORMULARIO";
	
	// Parámetros
	private int id;
	private String nmbre;
	private byte[] icno;
	private Vector<Campo> cmpos;
	private String tpo;
	private boolean multiple;
	private List<String[]> tablasAsociadas;
	private String[] campoLlave;
	public boolean banderaGuardadoTemporal = false;
	
	// Constructores
	public Formulario (int id) {
		this.id = id;
	}
	public Formulario (int id, String nmbre, byte[] icno, Vector<Campo> cmpos, String tpo, boolean multiple, List<String[]> tablasAsociadas) {
		this.id = id;
		this.nmbre = nmbre;
		this.icno = icno;
		this.cmpos = cmpos;
		this.tpo = tpo;
		this.multiple = multiple;
		this.tablasAsociadas = tablasAsociadas;
	}
	
	// Contructor parcelable
	public Formulario (Parcel parcel) {
		this.id = parcel.readInt();
		this.nmbre = parcel.readString();
		cmpos = new Vector<Campo>();
		parcel.readList(cmpos, getClass().getClassLoader());
		this.tpo = parcel.readString();
		this.multiple = (parcel.readInt() == 0) ? false : true;
		//this.tablasAsociadas = parcel.readTypedList(tablasAsociadas, ta);
    }

	// Creator
	public static Creator<Formulario> CREATOR = new Creator<Formulario>() {
        public Formulario createFromParcel(Parcel parcel) {
            return new Formulario(parcel);
        }

        public Formulario[] newArray(int size) {
            return new Formulario[size];
        }
    };
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.id);
		dest.writeString(this.nmbre);
		dest.writeList(cmpos);
		dest.writeString(this.tpo);
		dest.writeInt(this.multiple ? 1 : 0);
	}
/*
	public boolean isBanderaGuardadoTemporal() {
		return banderaGuardadoTemporal;
	}

	public void setBanderaGuardadoTemporal(boolean banderaGuardadoTemporal) {
		this.banderaGuardadoTemporal = banderaGuardadoTemporal;
	}
*/
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nmbre
	 */
	public String getNombre() {
		return nmbre;
	}

	/**
	 * @param nmbre the nmbre to set
	 */
	public void setNombre(String nmbre) {
		this.nmbre = nmbre;
	}

	/**
	 * @return the icno
	 */
	public byte[] getIcono() {
		return icno;
	}

	/**
	 * @param icno the icno to set
	 */
	public void setIcono(byte[] icno) {
		this.icno = icno;
	}

	/**
	 * @return the cmpos
	 */
	public Vector<Campo> getCampos() {
		return cmpos;
	}
	
	// Método que permite transformar los campos de Vector a SparseArray
	public SparseArray<Campo> getCamposHash() {
		// Inicializa el objeto que almacenara los datos
		SparseArray<Campo> camposHash = new SparseArray<Campo>();
		
		// Recorre los campos
		int numeroCampos = cmpos.size();
		for(int indice = 0; indice < numeroCampos; indice++){
			Campo campo = cmpos.elementAt(indice);
			int idCampo = campo.getId();
			
			// Adiciona el valor a la nueva lista
			camposHash.put(idCampo, campo);
		}
		
		// Retorno...
		return camposHash;
	}
	
	public Vector<Campo> getCamposPlantilla(){
		// Inicializa el nuevo vector
		int nmro_cmpos = cmpos.size();
		Vector<Campo> cmpos_nvo = new Vector<Campo>(nmro_cmpos);
		
		// Recorre los campos
		for (int i = 0; i < nmro_cmpos; i++) {
			Campo cmpo = cmpos.get(i);
			
			// Crea el nuevo objeto Campo
			cmpos_nvo.add(
				new Campo(cmpo.getId(), cmpo.getNombre(), cmpo.getModoEdicion(), cmpo.getTipoDato(), 
					cmpo.getDatosAdicionales(), 
					cmpo.isObligatorio(), cmpo.isVisible(), cmpo.isInformativo(), cmpo.isTotaliza(), cmpo.isResumen(),
					cmpo.getCamposAsociados(), cmpo.isEditable()
				)
			);
		}
		return cmpos_nvo;
	}

	/**
	 * @param cmpos the cmpos to set
	 */
	public void setCampos(Vector<Campo> cmpos) {
		this.cmpos = cmpos;
	}
	
	// Método que permite transformar los campos de SparseArray a Vector para "setearlos" 
	public void setCamposHash(SparseArray<Campo> camposHash) {
		// Inicializa el objeto que almacenara los datos
		Vector<Campo> campos = new Vector<Campo>();
		
		// Recorre los campos
		int numeroCampos = camposHash.size();
		for(int indice = 0; indice < numeroCampos; indice++){
			Campo campo = camposHash.valueAt(indice);
			
			// Adiciona el valor a la nueva lista
			campos.addElement(campo);
		}
		this.cmpos = campos;
	}
	
	/**
	 * @return the tpo
	 */
	public String getTipo() {
		return tpo;
	}
	
	/**
	 * @param tpo_frmlrio the tpo to set
	 */
	public void setTipo(String tpo) {
		this.tpo = tpo;
	}
	
	/**
	 * @return the multiple
	 */
	public boolean isMultiple() {
		return multiple;
	}
	
	/**
	 * @param multiple the multiple to set
	 */
	public void setMultiple(boolean multiple) {
		this.multiple = multiple;
	}
	
	public List<String[]> getTablaAsociadas() {
		return tablasAsociadas;
	}
	
	public void setTablaAsociadas(List<String[]> tablasAsociadas) {
		this.tablasAsociadas = tablasAsociadas;
	}
	
	// Obtener idAdicional de formulario
	public String[] getCampoLlave(){
		return campoLlave;
	}
	
	// Setear campo idAdicional para detalles
	public void setCampoLlave(String[] campoLlave){
		this.campoLlave = campoLlave;
	}
	
	// TODO: 
	public String generarJson(Configuracion aplicacion){
		StringBuilder formularioJson = new StringBuilder();
		
		// Inicializa el encabezado del formulario
		formularioJson.append("\"idFormulario\": " + this.id + ",");
		formularioJson.append("\"nombreFormulario\": \"" + nmbre + "\",");
		formularioJson.append("\"tablasAsociadas\": [");
		 		
		// Se valida si el formulario tiene tablas asociadas
		if(tablasAsociadas != null){
			// Se recorre las tablas asociadas al formulario
			int numeroTablasAsociadas = tablasAsociadas.size();
			for(int i = 0;i<numeroTablasAsociadas;i++){
				if(i>0) formularioJson.append(",");
				
				formularioJson.append("\""+tablasAsociadas.get(i)[1]+"\"");
			}
		}
		
		// Cierra el arreglo de tablas asociadas
		formularioJson.append("],");
		
		// Inicia a concatenar campos
		formularioJson.append("\"campos\":[");
		
		// Se recorren los campos del formulario
		int numeroCampos = cmpos.size();
		for(int j = 0; j < numeroCampos; j++){
			// Obtiene campo en la posicion j
			Campo campo = cmpos.get(j);
			ControladorFormularios controladorFormularios = new ControladorFormularios();
			
			// Adiciona coma si el indice es mayor a cero
			if(j>0) formularioJson.append(",");
			
			// Genera un vector de datos si el campo de detalle
			Vector<Vector<Campo>> datosDetalle = null;
			if(campo.getModoEdicion().equals("Detalle")) {
				// TODO: TODO TODO 2015-04-22
				// Es un campo detalle, extrae la información asociada
				int idSubformulario = Integer.parseInt(campo.getDatosAdicionales());
				Formulario formularioReferenciaDetalle = ((SQLiteRoutines)aplicacion.getSqliteRoutines())
						.routine_consultarFormularioLocal(null, null, true, idSubformulario);
				
				// Genera un vector de datos con el detalle
				datosDetalle = controladorFormularios.jsonToCampoDetalle(campo.getValor(), formularioReferenciaDetalle);
			}
			
			// Se crea Json de campo
			formularioJson.append(campo.generarJson(aplicacion,controladorFormularios,datosDetalle));
		}
		
		// Cierra el arreglo de campos
		formularioJson.append("]");
		
		// Retorno...
		return formularioJson.toString();
	}
}
