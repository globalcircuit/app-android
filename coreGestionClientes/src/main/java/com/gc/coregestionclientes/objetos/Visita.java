package com.gc.coregestionclientes.objetos;

public class Visita {

	// Parametros
	protected int id;
	protected int idUsuario;
	protected long unixFechaHoraInicio;
	protected long unixFechaHoraFin = -1;

	// Constructor
	public Visita(int id, int idUsuario, long unixFechaHoraInicio) {
		this.id = id;
		this.idUsuario = idUsuario;
		this.unixFechaHoraInicio = unixFechaHoraInicio;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the id_usrio
	 */
	public int getIdUsuario() {
		return idUsuario;
	}

	/**
	 * @param id_usrio the id_usrio to set
	 */
	public void setIdUsuario(int id_usrio) {
		this.idUsuario = id_usrio;
	}

	/**
	 * @return the fcha_hra_incio
	 */
	public long getUnixFechaHoraIncio() {
		return unixFechaHoraInicio;
	}

	/**
	 * @param fcha_hra_incio the fcha_hra_incio to set
	 */
	public void setUnixFechaHoraIncio(long unixFechaHoraInicio) {
		this.unixFechaHoraInicio = unixFechaHoraInicio;
	}

	/**
	 * @return the fcha_hra_fin
	 */
	public long getUnixFechaHoraFin() {
		return unixFechaHoraFin;
	}

	/**
	 * @param fcha_hra_fin the fcha_hra_fin to set
	 */
	public void setUnixFechaHoraFin(long unixFechaHoraFin) {
		this.unixFechaHoraFin = unixFechaHoraFin;
	}
}
