package com.gc.coregestionclientes.objetos;

import android.os.Parcel;
import android.os.Parcelable;

import com.gc.coregestionclientes.librerias.ToolsApp;

public class Cliente implements Parcelable {
	
	private int id;
	private String codigo;
	private String nombre;
	private float latitud;
	private float longitud;
	private boolean activo;
	private String datosAdicionales;

	// Auxiliares
	ToolsApp toolsApp = new ToolsApp();
	
	// Constructor
	public Cliente (int id, String codigo, String nombre, float latitud, float longitud, 
			boolean activo, String datosAdicionales) {
		this.id = id;
		this.codigo = codigo;
		this.nombre = nombre;
		this.latitud = latitud;
		this.longitud = longitud;
		this.activo = activo;
		this.datosAdicionales = datosAdicionales;
	}
	
	// Contructor parceable
	public Cliente (Parcel parcel) {
		this.id = parcel.readInt();
		this.codigo = parcel.readString();
		this.nombre = parcel.readString();
		this.latitud = parcel.readFloat();
		this.longitud = parcel.readFloat();
		this.activo = (parcel.readInt() == 0) ? false : true;
		this.datosAdicionales = parcel.readString();
    }
	
	// Creator
	public static Creator<Cliente> CREATOR = new Creator<Cliente>() {
        public Cliente createFromParcel(Parcel parcel) {
            return new Cliente(parcel);
        }

        public Cliente[] newArray(int size) {
            return new Cliente[size];
        }
    };

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.id);
		dest.writeString(this.codigo);
		dest.writeString(this.nombre);
		dest.writeFloat(this.latitud);
		dest.writeFloat(this.longitud);
		dest.writeInt(this.activo ? 1 : 0);
		dest.writeString(this.datosAdicionales);
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	/**
	 * @return the nombre
	 */
	public String getNombres() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombres(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the latitud
	 */
	public float getLatitud() {
		return latitud;
	}

	/**
	 * @param latitud the latitud to set
	 */
	public void setLatitud(float latitud) {
		this.latitud = latitud;
	}

	/**
	 * @return the longitud
	 */
	public float getLongitud() {
		return longitud;
	}

	/**
	 * @param longitud the longitud to set
	 */
	public void setLongitud(float longitud) {
		this.longitud = longitud;
	}
	
	/**
	 * @return the activo
	 */
	public boolean isActivo() {
		return activo;
	}

	/**
	 * @param activo the activo to set
	 */
	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	/**
	 * @return the datosAdicionales
	 */
	public String getDatosAdicionales() {
		return datosAdicionales;
	}

	/**
	 * @param datosAdicionales the datosAdicionales to set
	 */
	public void setDatosAdicionales(String datosAdicionales) {
		this.datosAdicionales = datosAdicionales;
	}
	
	public String toJSONString(){
		StringBuilder dtos = new StringBuilder();
		dtos.append("{");
		dtos.append("\"Id\":" + this.id);
		dtos.append(", \"Codigo\":\"" + this.codigo + "\"");
		dtos.append(", \"Nombres\":\"" + this.nombre + "\"");
		dtos.append(", \"Latitud\":" + this.latitud);
		dtos.append(", \"Longitud\":" + this.longitud);
		dtos.append(", \"Activo\":" + toolsApp.booleanToBit(this.activo));
		dtos.append(", \"DatosAdicionales\":\"" + this.datosAdicionales + "\"");
		dtos.append("}");
		return dtos.toString();
	}
}
