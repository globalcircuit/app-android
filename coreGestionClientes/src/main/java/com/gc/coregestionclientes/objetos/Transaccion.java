package com.gc.coregestionclientes.objetos;

public class Transaccion {

	// Parametros
	private int id;
	private int id_tpo_trnsccion = -1;
	private Formulario frmlrio;
	private String fcha_hra_incio;
	private String fcha_hra_fin;
	
	// Constructor
	public Transaccion(int id, int id_tpo_trnsccion, Formulario frmlrio, String fcha_hra_incio) {
		this.id = id;
		this.id_tpo_trnsccion = id_tpo_trnsccion;
		this.frmlrio = frmlrio;
		this.fcha_hra_incio = fcha_hra_incio;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the id_frmlrio
	 */
	public Formulario getFormulario() {
		return frmlrio;
	}

	/**
	 * @param id_frmlrio the id_frmlrio to set
	 */
	public void setFormulario(Formulario frmlrio) {
		this.frmlrio = frmlrio;
	}

	/**
	 * @param id_tpo_trnsccion the id_tpo_trnsccion to set
	 */
	public void setIdTipoTransaccion(int id_tpo_trnsccion) {
		this.id_tpo_trnsccion = id_tpo_trnsccion;
	}

	/**
	 * @return the id_tpo_trnsccion
	 */
	public int getIdTipoTransaccion() {
		return id_tpo_trnsccion;
	}

	/**
	 * @return the fcha_hra_incio
	 */
	public String getFechaHoraInicio() {
		return fcha_hra_incio;
	}

	/**
	 * @param fcha_hra_incio the fcha_hra_incio to set
	 */
	public void setFechaHoraInicio(String fcha_hra_incio) {
		this.fcha_hra_incio = fcha_hra_incio;
	}

	/**
	 * @return the fcha_hra_fin
	 */
	public String getFechaHoraFin() {
		return fcha_hra_fin;
	}

	/**
	 * @param fcha_hra_fin the fcha_hra_fin to set
	 */
	public void setFechaHoraFin(String fcha_hra_fin) {
		this.fcha_hra_fin = fcha_hra_fin;
	}	
}
