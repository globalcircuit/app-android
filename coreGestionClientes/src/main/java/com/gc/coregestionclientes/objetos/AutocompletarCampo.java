package com.gc.coregestionclientes.objetos;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

public abstract class AutocompletarCampo implements Parcelable {
	public static final String AUTOCOMPLETE = "AUTOCOMPLETE";
	public static final String LISTACAMPO = "LISTACAMPO";
	
	// Atributos
	protected int idFormulario;
	protected int idCampo;
	protected String nombreCampo;

	// Constructor vacío
	public AutocompletarCampo() {}
	
	// Constructor para parcel
	public AutocompletarCampo(Parcel parcel) {
		this.idFormulario = parcel.readInt();
		this.idCampo = parcel.readInt();
		this.nombreCampo = parcel.readString();
    }
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.idFormulario);
		dest.writeInt(this.idCampo);
		dest.writeString(this.nombreCampo);
	}
	
	// Método que permite cambiar a la actividad específica para consulta
	public abstract void abrirActividadEspecifica(Activity actividad, Context contexto, String tipoActividad, int request, View layoutView);
	
	// Método que permite consultar la forma del autocomplete, con detalle o no
	public abstract int consultarModoFuncionamiento(Context contexto);

	// Método que permite consultar el número de coincidencias (Personalizable por cliente)
	public abstract int consultarNumeroItems(Context contexto, String patron, HashMap<String, String> datosAdicionales);

	// Método que permite consultar las coincidencias (Personalizable por cliente)
	public abstract Vector<LinkedHashMap<String, String>> consultarItems(Context contexto, String patron, HashMap<String, String> datosAdicionales);

	// Obtiene el nombre del campo del item sobre el cual se puede realizar algun tipo de filtro
	public abstract String obtenerNombreCampoFiltro(Context contexto);

	/**
	 * @return the idFormulario
	 */
	public int getIdFormulario() {
		return idFormulario;
	}

	/**
	 * @return the idCampo
	 */
	public int getIdCampo() {
		return idCampo;
	}

	/**
	 * @return the nombreCampo
	 */
	public String getNombreCampo() {
		return nombreCampo;
	}

	/**
	 * @param idFormulario the idFormulario to set
	 */
	public void setIdFormulario(int idFormulario) {
		this.idFormulario = idFormulario;
	}

	/**
	 * @param idCampo the idCampo to set
	 */
	public void setIdCampo(int idCampo) {
		this.idCampo = idCampo;
	}

	/**
	 * @param nombreCampo the nombreCampo to set
	 */
	public void setNombreCampo(String nombreCampo) {
		this.nombreCampo = nombreCampo;
	}
}
