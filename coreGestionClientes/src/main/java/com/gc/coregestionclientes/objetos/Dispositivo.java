package com.gc.coregestionclientes.objetos;


public class Dispositivo {
	// Manager del telefono, wifi y version
	private String imei;
	private String mac;
	private String vrsion;
	private String token;
	private String email;
	
	// Identificador del telefono, podrá ser el IMEI o la MAC (bndra_id_dspstvo)
	private String id;
	private static String tpo_id_dspstvo = "IMEI";

	public Dispositivo() {

	}

	/**
	 * @param
	 * @param imei
	 * @param mac
	 * @param vrsion
	 */
	public Dispositivo(String token, String imei, String mac, String vrsion) {
		super();
		this.imei = imei;
		this.mac = mac;
		this.token = token;
		this.vrsion = vrsion;
	}
	
	/**
	 * @return the id_dspstvo
	 */
	public String getId() {
		if(tpo_id_dspstvo.equals("IMEI")){
			id = imei;
		}else if(tpo_id_dspstvo.equals("MAC")){
			id = mac;
		}
		return id;
	}

	/**
	 * @return the vrsion
	 */
	public String getVersion() {
		return vrsion;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getVrsion() {
		return vrsion;
	}

	public void setVrsion(String vrsion) {
		this.vrsion = vrsion;
	}

	public void setId(String id) {
		this.id = id;
	}

	public static String getTpo_id_dspstvo() {
		return tpo_id_dspstvo;
	}

	public static void setTpo_id_dspstvo(String tpo_id_dspstvo) {
		Dispositivo.tpo_id_dspstvo = tpo_id_dspstvo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
