package com.gc.coregestionclientes.objetos;

public class Talonario {
	// Estados
	public final static String EN_USO = "EN USO";
	public final static String PENDIENTE = "PENDIENTE";
	
	// Parametros
	private String nmbre_tbla;
	private String nmbre_cmpo;
	private String estdo;
	private int incio = -1;
	private int fin = -1;
	private int cnsctvo_utlzdo = 0;
	
	// Auxiliar
	private Talonario tlnrio_rpsto = null;
	
	/**
	 * @param nmbre_tbla
	 * @param nmbre_cmpo
	 * @param estdo
	 * @param incio
	 * @param fin
	 * @param cnsctvo_utlzdo
	 */
	public Talonario(String nmbre_tbla, String nmbre_cmpo, String estdo,
			int incio, int fin, int cnsctvo_utlzdo) {
		super();
		this.nmbre_tbla = nmbre_tbla;
		this.nmbre_cmpo = nmbre_cmpo;
		this.estdo = estdo;
		this.incio = incio;
		this.fin = fin;
		this.cnsctvo_utlzdo = cnsctvo_utlzdo;
	}

	/**
	 * @return the nmbre_tbla
	 */
	public String getNombreTabla() {
		return nmbre_tbla;
	}

	/**
	 * @param nmbre_tbla the nmbre_tbla to set
	 */
	public void setNombreTabla(String nmbre_tbla) {
		this.nmbre_tbla = nmbre_tbla;
	}

	/**
	 * @return the nmbre_cmpo
	 */
	public String getNombreCampo() {
		return nmbre_cmpo;
	}

	/**
	 * @param nmbre_cmpo the nmbre_cmpo to set
	 */
	public void setNombreCampo(String nmbre_cmpo) {
		this.nmbre_cmpo = nmbre_cmpo;
	}

	/**
	 * @return the estdo
	 */
	public String getEstado() {
		return estdo;
	}

	/**
	 * @param estdo the estdo to set
	 */
	public void setEstado(String estdo) {
		this.estdo = estdo;
	}

	/**
	 * @return the incio
	 */
	public int getInicio() {
		return incio;
	}

	/**
	 * @param incio the incio to set
	 */
	public void setInicio(int incio) {
		this.incio = incio;
	}

	/**
	 * @return the fin
	 */
	public int getFin() {
		return fin;
	}

	/**
	 * @param fin the fin to set
	 */
	public void setFin(int fin) {
		this.fin = fin;
	}

	/**
	 * @return the cnsctvo_utlzdo
	 */
	public int getConsecutivoUtilizado() {
		return cnsctvo_utlzdo;
	}

	/**
	 * @param cnsctvo_utlzdo the cnsctvo_utlzdo to set
	 */
	public void setConsecutivoUtilizado(int cnsctvo_utlzdo) {
		this.cnsctvo_utlzdo = cnsctvo_utlzdo;
	}

	/**
	 * @return the tlnrio_rpsto
	 */
	public Talonario getTalonarioRepuesto() {
		return tlnrio_rpsto;
	}

	/**
	 * @param tlnrio_rpsto the tlnrio_rpsto to set
	 */
	public void setTalonarioRepuesto(Talonario tlnrio_rpsto) {
		this.tlnrio_rpsto = tlnrio_rpsto;
	}
	
	
}
