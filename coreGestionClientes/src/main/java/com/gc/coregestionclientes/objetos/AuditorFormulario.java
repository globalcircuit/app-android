package com.gc.coregestionclientes.objetos;

import java.util.HashMap;
import java.util.Vector;

import android.content.Context;
import android.view.View;

import com.gc.coregestionclientes.librerias.ToolsApp;

public abstract class AuditorFormulario {
	protected ControladorFormularios controladorFormularios;
	protected Formulario formulario;
	protected ToolsApp toolsApp;
	
	// Constructor
	public AuditorFormulario(Formulario formulario, String logicaInterpretador, 
			ControladorFormularios controladorFormularios) {
		this.formulario = formulario;
		this.controladorFormularios = controladorFormularios;
		this.toolsApp = new ToolsApp();
	}

	// Setea formulario
	public void setFormulario(Formulario formulario) {
		this.formulario = formulario;
	}
	
	/*******************************************************/
	/***************** METODOS GENERALES *******************/
	/*******************************************************/
	// Método que se llama cuando un formulario es seleccionado
	public abstract void eventoFormularioSeleccionado(Context contexto);
	
	// Método que inicializa algunos datos del formulario si es necesario
	public abstract void inicializarCamposFormulario(Context contexto,HashMap<String, View> contenedoresCamposHash);
	
	// Método que permite validar un campo de un formulario
	public abstract void validarEventoCampo(Context contexto, View layoutView, Campo campoEvento, View view, 
			HashMap<String, View> contenedoresCamposHash, HashMap<String, Campo> camposHash);
	
	// Método que se llama cuando se toca un campo de un formulario
	public abstract void eventoCampoDetalle(Context contexto, Campo campoEvento, Formulario formularioAsociado);
	
	// Método que se llama cuando se toca un campo para adjuntar fotos
	public abstract void eventoCampoAdjuntaFotos(Context contexto, Campo campoEvento , int tipoPeticion);

	// Método que se llama cuando se autocompleta un campo
	public abstract void eventoAutoComplete(Context contexto, View layoutView, Campo campoEvento, View view, HashMap<String, String> objetoDatos);
	
	// Método que permite ejecutar una rutina adicional dependiendo del formulario.
	// Se dispara cuando se elimina un item de un campo tipo "detalle" y tiene 
	// como objetivo eliminar rastros que se tenga en BD
	public abstract void eliminarRastroItem(Context contexto, View layoutView);
	
	// Método que permite ejecutar alguna acción adicional 
	// cuando se guarda un formulario con algún campo tipo "detalle"
	public abstract void finalizarDetalle(View layoutView);
	
	// Método que permite ejecutar alguna acción adicional 
	// en el momento de finalizar un formulario.
	public abstract void validarEventoFinFormulario(Context contexto, View layoutView);
	
	// Método que valida la existencia de un item en la lista asociada 
	// a un campo tipo "detalle" del formulario en curso
	public abstract int validarExistenciaItem(Context contexto, Campo campo, String _valor, Vector<Vector<Campo>> datos);

	// TODO:
	public abstract void actualizaCampoCalculado(
			View layoutView, HashMap<String, View> contenedoresCamposHash, float total);
	
}
