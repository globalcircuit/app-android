package com.gc.coregestionclientes.objetos;

import java.util.Hashtable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.gc.coregestionclientes.Configuracion;
import com.gc.coregestionclientes.accesodatos.ConsumeWebService;
import com.gc.coregestionclientes.accesodatos.SQLiteRoutines;
import com.gc.coregestionclientes.librerias.JSONManager;

public class ControladorTalonarios {
	private SQLiteRoutines sqliteRoutines;
	private Hashtable<String, Talonario> tlnrios_mmria = new Hashtable<String, Talonario>();
	
	// Constructor
	public ControladorTalonarios(SQLiteRoutines sqliteRoutines) {
		this.sqliteRoutines = sqliteRoutines;
	}
	
	// Cargar Talonario y Talonario de repuesto
	public void cargarTalonario(String nmbre_tbla, String nmbre_cmpo){
		// Consulta un talonario en USO
		Talonario tlnrio = sqliteRoutines.routine_cargarTalonario(nmbre_tbla, nmbre_cmpo, Talonario.EN_USO);
		
		// Verifica si hay talonario en uso
		if(tlnrio != null){
			// Hay talonario en uso
			Log.i(Configuracion.tagLog, "Talonario en USO cargado!: " + 
					nmbre_tbla+"-> "+tlnrio.getInicio()+","+tlnrio.getFin()
			);
			
			// Consulta un talonario (De repuesto)
			Talonario tlnrio_rpsto = sqliteRoutines.routine_cargarTalonario(nmbre_tbla, nmbre_cmpo, Talonario.PENDIENTE);
			
			// Verifica si hay talonario (De repuesto)
			if(tlnrio_rpsto != null){
				// Hay talonario (De repuesto)
				// Se lo adiciona al talonario en uso
				tlnrio.setTalonarioRepuesto(tlnrio_rpsto);
			}
			
			// Carga el talonario en memoria
			tlnrios_mmria.put(nmbre_tbla+nmbre_cmpo, tlnrio);
		}else{
			// No hay talonario en uso
			// Consulta un talonario PENDIENTE
			tlnrio = sqliteRoutines.routine_cargarTalonario(nmbre_tbla, nmbre_cmpo, Talonario.PENDIENTE);

			// Verifica si hay talonario PENDIENTE
			if(tlnrio != null){
				// Hay talonario PENDIENTE
				Log.i(Configuracion.tagLog, "Talonario Pendiente cargado!: " + 
						nmbre_tbla+"-> "+tlnrio.getInicio()+","+tlnrio.getFin()
				);
								
				// Activa consecutivo
				tlnrio.setConsecutivoUtilizado(tlnrio.getInicio() - 1);
				Log.i(Configuracion.tagLog, "Activa consecutivo base!: " +tlnrio.getConsecutivoUtilizado());

				// Actualiza  el estado
				sqliteRoutines.routine_actualizarTalonario(tlnrio, Talonario.EN_USO);
				tlnrio.setEstado(Talonario.EN_USO);
				
				// Carga el talonario en memoria
				tlnrios_mmria.put(nmbre_tbla+nmbre_cmpo, tlnrio);

			}else{
				// No hay talonario PENDIENTE
				Log.i(Configuracion.tagLog, "No hay talonarios!!: "+nmbre_tbla);
			}

		}
	}
	
	// Verificar Consecutivo
	public boolean solicitarNuevoTalonario(
			Context context, Configuracion configApp, String nmbre_tbla, String nmbre_cmpo) {
		boolean slctar_tlnrio;
		
		Talonario tlnrio = tlnrios_mmria.get(nmbre_tbla+nmbre_cmpo);
		
		if(tlnrio != null){
			// Extrae los datos del talonario
			int cnsctvo_actual = tlnrio.getConsecutivoUtilizado();
			int incio = tlnrio.getInicio();
			int fin = tlnrio.getFin();
			
			// Verifica si tiene talonario de repuesto
			if(tlnrio.getTalonarioRepuesto() != null){
				// Si tiene
				slctar_tlnrio = false;
			}else{
				// No tiene
				// Verifica si el talonario ha superado el 50% de los cnsctvos
				if((cnsctvo_actual > incio) && (cnsctvo_actual <= fin) && ((cnsctvo_actual-incio) >= (fin-incio)*0.50)){
					// Lo ha superado, debe solicitar un nuevo talonario
					slctar_tlnrio = true;
				} else {
					slctar_tlnrio = false;
				}
			}
		}else{
			// Solicitar talonario
			Log.i(Configuracion.tagLog, "Solicitar Talonario!: "+nmbre_tbla);
			slctar_tlnrio = true;
		}
		
		if(slctar_tlnrio){
			Log.i(Configuracion.tagLog, "Solicitar Talonario!: "+nmbre_tbla);

			// Consumir servicio para solicitar talonario
			Hashtable<String, String> prmtros_adcnles = new Hashtable<String, String>();
			prmtros_adcnles.put("IdClienteGlobal", String.valueOf(configApp.getUsuario().getIdClienteGlobal()));
			prmtros_adcnles.put("IdDispositivo", configApp.getDispositivo().getId());
			prmtros_adcnles.put("IdUsuario", String.valueOf(configApp.getUsuario().getId()));
			prmtros_adcnles.put("NombreTabla", nmbre_tbla);
			prmtros_adcnles.put("NombreCampo", nmbre_cmpo);
			ConsumeWebService consumeWS = new ConsumeWebService(context);
			consumeWS.iniciarProceso("solicitarNuevoTalonario", prmtros_adcnles, true,
					"Solicitando Talonario", "callback_solicitarNuevoTalonario");
		}
		
		return slctar_tlnrio;
	}
	
	// Obtener consecutivo
	public int obtenerConsecutivo(String nmbre_tbla, String nmbre_cmpo) {
		String key = nmbre_tbla + nmbre_cmpo;
		int cnsctvo = -1;
		Talonario tlnrio = tlnrios_mmria.get(key);
		
		// Extrae los datos del talonario
		int cnsctvo_utlzdo = tlnrio.getConsecutivoUtilizado();
		int fin = tlnrio.getFin();
		
		// Consulta si el cnsctvo ya sobrepaso los disponibles
		if(cnsctvo_utlzdo == fin){
			// Supero los límites
			// Verifica si tiene talonario de repuesto
			Talonario tlnrio_rpsto = tlnrio.getTalonarioRepuesto();
			if(tlnrio_rpsto != null){
				// Si tiene
				// Elimina el talonario actual
				sqliteRoutines.routine_eliminarTalonario(tlnrio);
				Log.i(Configuracion.tagLog, "El talonario En USO fue eliminado!: "+nmbre_tbla);

				// Activa consecutivo	 
				cnsctvo = tlnrio_rpsto.getInicio(); 
				tlnrio_rpsto.setConsecutivoUtilizado(cnsctvo);
				
				// Actualiza en BD
				sqliteRoutines.routine_actualizarTalonario(tlnrio_rpsto, Talonario.EN_USO);
				Log.i(Configuracion.tagLog, "Se obtuvo consecutivo " + cnsctvo + " de: "+nmbre_tbla);

			}else{
				// No tiene
				// No puede asignar un nuevo consecutivo hasta
				// que descargue un talonario nuevo
				Log.i(Configuracion.tagLog, "El talonario supero los límites!: "+nmbre_tbla);
			}
		}else{
			// No superó los límites
			// Incrementa el consecutivo
			cnsctvo = cnsctvo_utlzdo + 1;
			tlnrio.setConsecutivoUtilizado(cnsctvo);
			
			// Actualiza en BD
			sqliteRoutines.routine_actualizarTalonario(tlnrio, Talonario.EN_USO);
			Log.i(Configuracion.tagLog, "Se obtuvo consecutivo " + cnsctvo + " de: "+nmbre_tbla);
		}
		
		// Libera de memoria el talonario
		tlnrios_mmria.remove(key);
		return cnsctvo;
	}

	public void guardarTalonarioNuevo(JSONManager jsonManager) {
		try {
			// Extrae el único registro 
			JSONArray jsonArray = jsonManager.getRegistros(0);
			JSONObject dtos_tlnrio = jsonArray.getJSONObject(0);
			
			// Extrae los datos del talonario
			String nmbre_tbla = dtos_tlnrio.getString("NombreTabla");
			String nmbre_cmpo = dtos_tlnrio.getString("NombreCampo");
			int incio = Integer.parseInt(dtos_tlnrio.getString("Inicio"));
			int fin = Integer.parseInt(dtos_tlnrio.getString("Fin"));
			
			// Guarda el nuevo talonario
			sqliteRoutines.routine_guardarNuevoTalonario(nmbre_tbla, nmbre_cmpo, incio, fin, Talonario.PENDIENTE);
			Log.i(Configuracion.tagLog, "Talonario Nuevo Almacenado!: "+nmbre_tbla+"-> "+incio+","+fin);

			// Verifica si el actual talonario en memoria tiene consecutivo,
			// Si no es asi carga en memoria el talonario nuevo
			String key = nmbre_tbla + nmbre_cmpo;
			if(!tlnrios_mmria.isEmpty()){
				Talonario tlnrio = tlnrios_mmria.get(key);
				
				// Extrae los datos del talonario
				int cnsctvo_mmria = tlnrio.getConsecutivoUtilizado();
				int fin_mmria = tlnrio.getFin();
				
				// Verifica si el cnsctvo ya sobrepaso los disponibles
				if(cnsctvo_mmria == fin_mmria){
					cargarTalonario(nmbre_tbla, nmbre_cmpo);
				}
			}else{
				cargarTalonario(nmbre_tbla, nmbre_cmpo);
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
