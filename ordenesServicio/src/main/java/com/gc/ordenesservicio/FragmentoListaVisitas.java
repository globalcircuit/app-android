package com.gc.ordenesservicio;

import java.util.List;
import java.util.Vector;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gc.coregestionclientes.objetos.Campo;
import com.gc.coregestionclientes.objetos.Formulario;
import com.gc.ordenesservicio.accesodatos.CustomSQLiteRoutinesOS;
import com.gc.ordenesservicio.accesodatos.SQLiteRoutinesOS;
import com.gc.ordenesservicio.objetos.VisitaOS;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class FragmentoListaVisitas extends ListFragment{

	private AplicacionOrdenesServicio aplicacion;
	
	// Objetos
	private CustomAdapterTransacciones adapter;
	private Context contextoActividad;
	private List<VisitaOS> listaVisitasOS;
	private Formulario formularioReferencia;
	
	// Parametros
	private OnVisitaSeleccionada listenerTransaccionSeleccionada;
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		
		// Obtiene el contexto desde donde se creo 
		contextoActividad = getActivity();
		
		// Instancia objeto de aplicación
		aplicacion = ((AplicacionOrdenesServicio)contextoActividad.getApplicationContext());

		// Extrae datos adicionales
		formularioReferencia = getArguments().getParcelable("formulario");		
		inicializarListaVisitas();
	}
	
	// Consulta e inicializa la lista de visitas
	public void inicializarListaVisitas(){
		listaVisitasOS = ((SQLiteRoutinesOS) aplicacion.getConfiguracionApp().getSqliteRoutines()).
				routine_consultarVisitasOrden(
						aplicacion.getOrdenServicioActiva().getId(),
						(CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines(),
						formularioReferencia,
						aplicacion.getConfiguracionApp(),
						false
				);
		if(listaVisitasOS != null){
			// Inicializa el adaptador de visitas
			adapter = new CustomAdapterTransacciones(contextoActividad, listaVisitasOS);
			setListAdapter(adapter);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragmento_lista_visitas, container, false);
		return view;
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		this.listenerTransaccionSeleccionada.onVisitaSeleccionada(listaVisitasOS.get(position));	
	}
	
	// Interface que debe ser implementada en la actividad que lo utilice
	public interface OnVisitaSeleccionada {
        void onVisitaSeleccionada(VisitaOS visita);
    }
	
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.listenerTransaccionSeleccionada = (OnVisitaSeleccionada)activity;
        }
        catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " debe implementar OnTransaccionSeleccionada");
        }
    }
    
	// Clase personalizada para listar las transacciones
	private class CustomAdapterTransacciones extends BaseAdapter implements
			Filterable {
		private Context contexto;
		private List<VisitaOS> list;
		private VisitaOS visitaOS;
		private LayoutInflater inflater = null;

		// Constructor
		public CustomAdapterTransacciones(Context contexto, List<VisitaOS> list) {
			this.contexto = contexto;
			this.list = list;
			this.inflater = (LayoutInflater) this.contexto
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		// Retorna el número de items en la lista
		public int getCount() {
			return list.size();
		}

		// Permite obtener el objeto en la posición
		public Object getItem(int position) {
			visitaOS = list.get(position);
			return visitaOS;
		}
		
		// Permite obtener el estilo de layout previamente definido y coloca los
		// valores de la fila que serán mostrados
		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;
			if (convertView == null)
				vi = inflater.inflate(R.layout.row_visitas, parent, false);

			// Obtiene los view donde será colocada la infromación
			TextView row_estadoUltimaVisita = (TextView) vi.findViewById(R.id.row_estadoUltimaVisita);
			TextView row_indiceVisita = (TextView) vi.findViewById(R.id.row_indiceVisita);
			LinearLayout row_contenedorResumenVisita = (LinearLayout) vi.findViewById(R.id.row_contenedorResumenVisita);
			row_contenedorResumenVisita.removeAllViews();

			// Valida si es la primera fila para agregar texView de Ultima Visita
			if(position == 0){
				row_estadoUltimaVisita.setVisibility(TextView.VISIBLE);
			} else {
				row_estadoUltimaVisita.setVisibility(TextView.GONE);
			}
			
			// Obtiene los datos en la posición del arreglo
			Formulario form = list.get(position).getFormulario();

			// Setea los campos
			row_indiceVisita.setText(String.valueOf(list.size() - position));
			
			// Recorre los campos del formulario asociado a la transacción
			Vector<Campo> campos = form.getCampos();
			int numeroCampos = campos.size();
			for(int j = 0; j < numeroCampos; j++){
				Campo campo = campos.elementAt(j);
				String nombreCampo = campo.getNombre();
				String vlor = null;

				// Verifica si el campo se muestra en el resumen
				if(campo.isResumen()){
					// Se muetsra en el resumen
					vlor = (campo.getValor() == null || campo.getValor().equals("")
							? "(No definido)" : campo.getValor());
					TextView textView = new TextView(contextoActividad);
					textView.setText(nombreCampo + ": " + vlor);
					row_contenedorResumenVisita.addView(textView);
					
					// Si es el primer TextView lo coloca en BOLD
					if (j == 0) {
						textView.setTextAppearance(contextoActividad, R.style.titulo_etiqueta_claro);
					} else {
						textView.setTextAppearance(contextoActividad, R.style.valor_etiqueta_claro);
					}
				}
			}
			return vi;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public void notifyDataSetChanged() {
			super.notifyDataSetChanged();
		}
		
		@Override
		public Filter getFilter() {
			return null;
		}
	}
}
