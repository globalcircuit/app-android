package com.gc.ordenesservicio;

import java.util.HashMap;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.objetos.AuditorFormulario;
import com.gc.coregestionclientes.objetos.Campo;
import com.gc.coregestionclientes.objetos.ControladorFormularios;
import com.gc.coregestionclientes.objetos.Formulario;
import com.gc.ordenesservicio.objetos.AuditorFormularioOS;
import com.gc.ordenesservicio.objetos.AutocompletarCampoOS;

public class ActividadDetalleVisita extends FragmentActivity {
	// Objetos
	private AplicacionOrdenesServicio aplicacion;
	private ControladorFormularios controladorFormularios = new ControladorFormularios();
	private AuditorFormulario auditorFormulario;
	private Formulario formulario;
	private ToolsApp toolsApp = new ToolsApp();
	private boolean modoEdicion;
	
	// Elementos Gráficos	
	private LinearLayout contenedorFormulario;
	//private LayoutInflater inflater = null;
	private View layoutView = null;
	
	// Objetos auxiliares 
	private HashMap<String, Campo> camposHash = new HashMap<String, Campo>();
	private HashMap<String, View> contenedoresCamposHash = new HashMap<String, View>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*inflater = (LayoutInflater) (ActividadVisita.this).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutView = inflater.inflate(R.layout.actividad_visita, null);*/
		setContentView(R.layout.actividad_detalle_visita);
		layoutView = getWindow().getDecorView().findViewById(android.R.id.content);
		
		// Inicializa elemntos graficos.
		contenedorFormulario = (LinearLayout)findViewById(R.id.contenedor_formulario);
		
		// Obtiene el objeto con la configuracion de la app
		aplicacion = ((AplicacionOrdenesServicio)getApplicationContext());
		
		// Extrae los datos del intent
		//modoEdicion = getIntent().getBooleanExtra("MODO_EDICION", false);
		// Mostrar
		mostrarFormulario();
		
		// yeah
	}
	
	
	// Inicializa y muestra el formulario
	private void mostrarFormulario() {	
		// Carga el formulario de visitas o tareas realizadas.
		formulario = controladorFormularios.cargarFormularioLocal(
				aplicacion.getConfiguracionApp().getSqliteRoutines(), 
				"DETALLE VISITA", Formulario.APLICACION, -1);
		// Inicializa el auditor
		controladorFormularios.cargarFormulariosLocales(aplicacion.getConfiguracionApp().getSqliteRoutines(), 
				Formulario.SUBFORMULARIO);
		
		// Inicializa el auditor
		auditorFormulario = new AuditorFormularioOS(formulario, null, controladorFormularios);
		
		// Crea dinámicamente el formulario
		controladorFormularios.visualizarFormulario(
				this, layoutView, contenedorFormulario, camposHash, contenedoresCamposHash, 
				auditorFormulario, new AutocompletarCampoOS(), 
				onItemSelectedListener, onCheckedChangeListener, true);
		
		// Inicializa campos
		auditorFormulario.inicializarCamposFormulario(this,contenedoresCamposHash);
	}

	// Listener para los Spinner
	AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			// Extrae el campo segun el view (Spinner)
			Campo cmpo_evnto = camposHash.get(String.valueOf(arg0.getId()));
			auditorFormulario.validarEventoCampo(ActividadDetalleVisita.this, layoutView, cmpo_evnto, 
					arg0, contenedoresCamposHash, camposHash);	
		}
		
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {}
	};
	
	// Listener para los Checkbox
	CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// Extrae el campo segun el view (Checkbox)
			Campo cmpo_evnto = camposHash.get(String.valueOf(buttonView.getId()));
			auditorFormulario.validarEventoCampo(ActividadDetalleVisita.this, layoutView, cmpo_evnto, 
					buttonView, contenedoresCamposHash, camposHash);
		}
	};
	
	public void onBackPressed() {
		// Se deshabilita el funcionamiento del back
	};
	
	// Guarda los datos en el objeto
	private boolean guardarDatosVisita() {
		// Asigna los valores del formulario
		boolean asignacionExitosa = controladorFormularios.interfazAFormulario(
				this, aplicacion.getConfiguracionApp(), true, auditorFormulario, 
				layoutView, contenedoresCamposHash);

		// Verifica si la asignación fue exitosa
		if(asignacionExitosa){
			// Exitosa,	
			aplicacion.getOrdenServicioActiva().getVisitaActiva().setDatos(true);
			return true;
		}
		return false;
	}


	// Método que maneja las acciones de los botones de layout
	public void ejecutarAccionBoton(View view) {
		switch(view.getId()){
		case R.id.boton_guardarFormulario:
			if(guardarDatosVisita()) { 
				// Salva el contexto de los objetos
				aplicacion.salvarObjetosContexto(true);
				
				// Guardó datos, finaliza la actividad
				finish();
			}
			break;
		case R.id.boton_cancelarFormulario:
			// Mensaje de verificación
			toolsApp.alertDialogYESNO(this, "¿Está seguro de CANCELAR este formulario?", 
					null, null, new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
					    	// Deshabilita el botón
				        	((AlertDialog) dialog).getButton(which).setEnabled(false);

				        	// Dependiendo del botón presionado, se ejecuta la acción
					        switch (which){
						        case DialogInterface.BUTTON_POSITIVE:
									aplicacion.getOrdenServicioActiva().getVisitaActiva().setFormulario(null);
									finish();	
						        break;
					        }
						}
					});
			break;
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
		switch (requestCode) {
			case ControladorFormularios.REQUEST_AUTOCOMPLETE:
				// Analiza el resultado
				if(resultCode == RESULT_OK){   			
					// Obtiene el valor retornado
					if(data != null){
						int idCampo = data.getIntExtra("idCampo", -1);
						HashMap<String, String> objetoDatos = (HashMap<String, String>) data.getSerializableExtra("valor");					
						
						// Si es un valor válido, lo setea
						if(objetoDatos != null && idCampo > 0){
							EditText editText = (EditText)layoutView.findViewById(idCampo);
							Campo campoEvento = camposHash.get(String.valueOf(idCampo));
							String nombreCampo = campoEvento.getNombre();
							String valor = objetoDatos.get(nombreCampo);
							editText.setText(valor);
							
							// Genera evento autocomplete
							auditorFormulario.eventoAutoComplete(ActividadDetalleVisita.this, layoutView, campoEvento, editText, objetoDatos);	
						}
					} 
				}
				break;
			case ControladorFormularios.REQUEST_SUBFORMULARIO:
				// Analiza el resultado
				if(resultCode == RESULT_OK){   			
					// Obtiene el valor retornado
					if(data != null) {
						// Listener para el campo tipo "Detalle"
						int idCampo = data.getIntExtra("idCampo", -1);
						Campo campoEvento = camposHash.get(String.valueOf(idCampo));
						auditorFormulario.validarEventoCampo(ActividadDetalleVisita.this, layoutView, campoEvento, 
								null, contenedoresCamposHash, camposHash);	
					} 
				}				
				break;
			case ControladorFormularios.REQUEST_SCANCODE:
				// Analiza el resultado
				if(resultCode == RESULT_OK){   			
					// Obtiene el valor retornado
					if(data != null){
						String valor = data.getStringExtra("SCAN_RESULT");
				        //String format = data.getStringExtra("SCAN_RESULT_FORMAT");
						int idCampo = data.getIntExtra("idCampo", -1);
						// Si es un valor válido, lo setea
						if(valor != null && idCampo > 0){
							EditText editText = (EditText)layoutView.findViewById(idCampo);
							editText.setText(valor);
						}
				    } 
				}				
				break;
			case ControladorFormularios.REQUEST_LISTACAMPO:
				// Analiza el resultado
				if(resultCode == RESULT_OK){   			
					// Obtiene el valor retornado
					if(data != null){
						int idCampo = data.getIntExtra("idCampo", -1);
						String valor = data.getStringExtra("valor");
						
						// Si es un valor válido, lo setea
						if(valor != null && idCampo > 0){
							EditText editText = (EditText)layoutView.findViewById(idCampo);
							editText.setText(valor);
						}
					} 
				}
				break;
			case ControladorFormularios.REQUEST_ADJUNTAFOTOS:
				// Analiza el resultado
				if(resultCode == RESULT_OK){   			
					// Obtiene el valor retornado
					if(data != null){
						int idCampo = data.getIntExtra("idCampo", -1);
						String valor = data.getStringExtra("valor");
						
						// TODO: Si es un valor válido, lo setea
						if(valor != null && idCampo > 0){
							toolsApp.showLongMessage(valor, this);
						}
					} 
				}
				break;
			default:
				break;
		}
	}
}
