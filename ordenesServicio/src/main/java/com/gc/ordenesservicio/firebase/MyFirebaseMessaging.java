package com.gc.ordenesservicio.firebase;

import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import com.gc.ordenesservicio.ActividadLogin;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessaging  extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.v("Mensaje", "From: " + remoteMessage.getNotification().getTitle());
        Log.v("Mensaje2", "From: " + remoteMessage.getNotification().getBody());
        try {
            handleNow(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void handleNow(String notificationtitle, String notificationBody ) throws Exception {

        Intent intentFCM = new Intent("fcm");
        intentFCM.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Intent intent;
        intent = new Intent(this, ActividadLogin.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_IMMUTABLE);
        // Construye y muestra la notificación

        String messagetitle = notificationtitle;
        String messagetBody = notificationBody;

        Tools.buildNotification(
                this,
                messagetitle,
                 messagetBody,
                pendingIntent,
                Tools.CHANNEL_ALERT, 1, false, false);
       // intentFCM.putExtra("extraObject", publication);




    // Enviar respuesta al interesado
        intentFCM.putExtra("asdasd", "asdasd");
        intentFCM.putExtra("event", "fcm");
        //LocalBroadcastManager.getInstance(contexto).sendBroadcast(intentFCM);
    }
}
