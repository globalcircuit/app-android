package com.gc.ordenesservicio.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import androidx.core.app.NotificationCompat;

import com.gc.ordenesservicio.R;

class Tools {

    public static final int ID_NOTIFICACION = 1;
    public static final int ID_NOTIFICACION_FCM = 2;
    public static final int CHANNEL_ALERT = 1;
    public static final int CHANNEL_INFORM = 2;

    public static Notification buildNotification(
            Context context, String titleNotification,String messageNotification, PendingIntent pendingIntent,
            int channel, int notificationId, boolean returnObject, boolean downloadIcon){
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = null;
        String channelName = null;
        int importance = -1;
        int priority = -1;
        boolean vibracion = false;
        boolean sonido = false;

        // Dependiendo del canal, configura los parámtros
        switch (channel) {
            case CHANNEL_ALERT:
                channelId = "channel-01";
                channelName = "Channel Garden Alert";
                importance = NotificationManager.IMPORTANCE_HIGH;
                priority = Notification.PRIORITY_HIGH;
                vibracion = true;
                sonido = true;
                break;
            case CHANNEL_INFORM:
                channelId = "channel-02";
                channelName = "Channel Garden Inform";
                importance = NotificationManager.IMPORTANCE_HIGH;
                priority = Notification.PRIORITY_HIGH;
                vibracion = false;
                sonido = false;
                break;
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            mChannel.enableVibration(vibracion);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(mChannel);
            }
        } else {
            mBuilder.setPriority(priority);
            if (sonido) {
                mBuilder.setSound(soundUri);
            }
        }
        mBuilder.setContentTitle(titleNotification)
                .setContentText(messageNotification)
                .setDefaults(Notification.DEFAULT_LIGHTS)
                .setAutoCancel(true);

        // Textos largos
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.bigText(messageNotification);
        mBuilder.setStyle(bigTextStyle);

        if(downloadIcon){
            mBuilder.setSmallIcon(android.R.drawable.stat_sys_download);
        } else {
            mBuilder.setSmallIcon(R.drawable.ic_launcher);
        }

        if (vibracion){
            mBuilder.setVibrate(new long[]{0, 500, 1000});
        }

        if (pendingIntent != null) {
            mBuilder.setContentIntent(pendingIntent);
        }

        // Retorno...
        if (returnObject) {
            return mBuilder.build();
        } else {
            notificationManager.notify(notificationId, mBuilder.build());
            return null;
        }
    }
}
