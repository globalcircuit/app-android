package com.gc.ordenesservicio;

import java.text.DecimalFormat;
import java.util.Vector;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.objetos.Campo;

public class ActividadResumenSubformulario extends ListActivity {

	// Objetos
	private AplicacionOrdenesServicio aplicacion;
	private ToolsApp toolsApp = new ToolsApp();
	private Vector<Vector<Campo>> dtos = null;
	private CustomAdapterResumen adapter;
	
	// Parámetros
	private int idCampo = -1;
	private int indiceItem = -1;
	private DecimalFormat formatoMoneda = new DecimalFormat("$###,###");
	
	// Elementos Gráficos
	private TextView ttal_rsmen;
	private TextView vlor_ttal_rsmen;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.actividad_resumen_subformulario);
		
		// Obtiene el objeto con la configuracion de la app
		aplicacion = ((AplicacionOrdenesServicio)getApplicationContext());
		
		// Inicializa elementos gráficos
		ttal_rsmen = (TextView)findViewById(R.id.ttal_rsmen);
		vlor_ttal_rsmen = (TextView)findViewById(R.id.vlor_ttal_rsmen);
		
		// Extrae los datos del intent
		idCampo = getIntent().getIntExtra("idCampo", -1);
		
		// Inicializar datos
		inicializarDatos();

		// Registrar Menu contextual.
	    registerForContextMenu(getListView());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.actividad_resumen_subformulario, menu);
		return true;
	}
	
	@Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
    	super.onCreateContextMenu(menu, v, menuInfo);   		
    	// Obtiene la información de la lista
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
        
        // Indice seleccionado
        indiceItem = info.position;
        
    	// Permite crear el menu teniendo en cuenta la plantilla creada.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_contextual_resumen_subformulario, menu);
    }
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
        // Indice seleccionado
        indiceItem = position;
        
        // Muestra el menu contextual
        v.showContextMenu(); 
	}

	@Override
    public boolean onContextItemSelected(MenuItem item) {		
        switch (item.getItemId()) {
        	case R.id.opcion_ver_item_detalle:
        		Intent resultIntent = new Intent();
        		resultIntent.putExtra("indiceItem", indiceItem);
        		setResult(RESULT_OK, resultIntent);
            	finish();
        		return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
	
	// Inicializa los datos
	@SuppressWarnings("unchecked")
	private void inicializarDatos() {
		// Verifica si existen datos 
		dtos = (Vector<Vector<Campo>>) aplicacion.getConfiguracionApp().getMemoria(idCampo);
		
		// Inicializa el adapter
		if(dtos != null && dtos.size() > 0){
			adapter = new CustomAdapterResumen(this, dtos);
			setListAdapter(adapter);
			
			// Calcula el total
			calcularTotal();
		} else {
			toolsApp.showLongMessage("No hay items!!", this);
			finish();
		}
	}
	
	// Clase customizada para la visualizacion del resumen de los items
	private class CustomAdapterResumen extends BaseAdapter implements
			Filterable {
		private Activity activity;
		private Vector<Vector<Campo>> list;
		private Vector<Campo> item;
		private LayoutInflater inflater = null;

		// Constructor
		public CustomAdapterResumen(Activity activity, Vector<Vector<Campo>> list) {
			this.activity = activity;
			this.list = list;
			this.inflater = (LayoutInflater) this.activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		// Retorna el número de items en el vector
		public int getCount() {
			return list.size();
		}

		// Permite obtener el objeto en la posición
		public Object getItem(int position) {
			item = list.get(position);
			return item;
		}

		// Permite obtener el estilo de layout previamente definido y coloca los
		// valores de la fila
		// que serán mostrados
		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;
			if (convertView == null)
				vi = inflater.inflate(R.layout.row_detalle_resumen, parent, false);

			// Obtiene los view donde será colocada la información
			TextView row_rsmen_cnsctvo_item = (TextView) vi.findViewById(R.id.row_rsmen_cnsctvo_item);
			LinearLayout row_rsmen_cntndor_infrmcion = (LinearLayout) vi.findViewById(R.id.row_rsmen_cntndor_infrmcion);
			row_rsmen_cntndor_infrmcion.removeAllViews();
			TextView row_rsmen_sbttal = (TextView) vi.findViewById(R.id.row_rsmen_sbttal);

			// Obtiene los datos en la posición dada
			Vector<Campo> item = list.get(position);
			
			// Setea los valores
			row_rsmen_cnsctvo_item.setText(String.valueOf(position+1));

			// Recorre los campos del item
			int numeroCampos = item.size();
			for(int j = 0; j < numeroCampos; j++){
				Campo campoItem = item.elementAt(j);
				String nombreCampo = campoItem.getNombre();
				String vlor = null;

				// Verifica si el campo se muestra en el resumen
				if (campoItem.isResumen() && campoItem.isTotaliza()) {
					// Campo que totaliza
					vlor = (campoItem.getValor() == null || campoItem.getValor().equals("")
							? "0" : campoItem.getValor());
					row_rsmen_sbttal.setVisibility(TextView.VISIBLE);
					row_rsmen_sbttal.setText(formatoMoneda.format(Float.parseFloat(vlor)));
					
				} else if(campoItem.isResumen()){
					// Se muetsra en el resumen
					vlor = (campoItem.getValor() == null || campoItem.getValor().equals("")
							? "(No definido)" : campoItem.getValor());
					TextView textView = new TextView(ActividadResumenSubformulario.this);
					textView.setText(nombreCampo + ": " + vlor);
					row_rsmen_cntndor_infrmcion.addView(textView);
					
					// Si es el primer TextView lo coloca en BOLD
					if (j == 0) {
						textView.setTextAppearance(ActividadResumenSubformulario.this, R.style.titulo_etiqueta_claro);
					} else {
						textView.setTextAppearance(ActividadResumenSubformulario.this, R.style.valor_etiqueta_claro);
					}
				}
			}
			return vi;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public Filter getFilter() {
			return null;
		}
	}
	
	// Permite calcular el total del detalle
	private void calcularTotal() {
		float total = 0;
		int numeroItems = 0;
		
		// Recorre los items y totaliza teniendo en cuenta el campo destinado para eso
		numeroItems = dtos.size();
		for (int i = 0; i < numeroItems; i++) {
			Vector<Campo> item = dtos.elementAt(i);
			int numeroCampos = item.size();
			
			// Recorre los campos del item
			for(int j = 0; j < numeroCampos; j++){
				Campo campoItem = item.elementAt(j);
				
				// Verifica si el campo se totaliza
				if(campoItem.isTotaliza()){
					String vlor = campoItem.getValor();
					
					// Verificia si tiene o no valor
					if(vlor == null || vlor.equals("")){
						toolsApp.showLongMessage(
							"No se puede generar el resumen, el item #"+ (i+1) + " no es válido.", this);
						return;
					} else {
						total += Float.parseFloat(vlor);
					}
				}
			}
		}
		
		// Actualiza el valor total
		if(total > 0){
			ttal_rsmen.setVisibility(TextView.VISIBLE);
			vlor_ttal_rsmen.setVisibility(TextView.VISIBLE);
			vlor_ttal_rsmen.setText(formatoMoneda.format(total));
		}
	}
}
