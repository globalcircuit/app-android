package com.gc.ordenesservicio.objetos;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.gc.coregestionclientes.ActividadAutocompletarCampo;
import com.gc.coregestionclientes.ActividadListaCampo;
import com.gc.coregestionclientes.objetos.AutocompletarCampo;
import com.gc.ordenesservicio.AplicacionOrdenesServicio;
import com.gc.ordenesservicio.accesodatos.CustomSQLiteRoutinesOS;
import com.gc.ordenesservicio.accesodatos.SQLiteRoutinesOS;

public class AutocompletarCampoOS extends AutocompletarCampo {
	
	public AutocompletarCampoOS() {}
	
	public AutocompletarCampoOS(Parcel parcel) {
		super(parcel);
	}
	
	public static Creator<AutocompletarCampo> CREATOR = new Creator<AutocompletarCampo>() {
		public AutocompletarCampo createFromParcel(Parcel parcel) {
			return new AutocompletarCampoOS(parcel);
		}

		public AutocompletarCampo[] newArray(int size) {
			return new AutocompletarCampoOS[size];
		}
	};

	@Override
	public int consultarModoFuncionamiento(Context contexto) {
		int modo = 1;
		return modo;
	}

	@Override
	public int consultarNumeroItems(Context contexto, String patron, HashMap<String, String> datosAdicionales) {
		AplicacionOrdenesServicio aplicacion = (AplicacionOrdenesServicio) contexto.getApplicationContext();
		int numeroItems = 0;
		
		//Formularios de detalle de solicitud/instalados de productos crown
		if ((idFormulario == 101 && idCampo == 1096) || (idFormulario == 103 && idCampo == 1130)) {
			//Cantidad de Repuestos Crown buscados por código/descripción
			numeroItems = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
					.royal_consultarCantidadCodigo(patron);
		}		
		
		if ((idFormulario == 35 && idCampo == 322) || (idFormulario == 58 && idCampo == 678)) {
			
			// Repuestos
			numeroItems = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
					.almatec_consultarTotalRepuestosPatron(patron);
		} else if (idFormulario == 71){ 
			if(idCampo == 770) {
				// Proyectos
				numeroItems = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
						.mepal_consultarTotalProyectosPatron(patron);
			} else if(idCampo == 771) {
				// Pedidos
				numeroItems = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
						.mepal_consultarTotalPedidosPatron(patron, datosAdicionales);
			} else if(idCampo == 772) {
				// Articulos
				numeroItems = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
						.mepal_consultarTotalArticulosPatron(patron, datosAdicionales);
			} 
		} else if ((idFormulario == 98 && idCampo == 1024) || (idFormulario == 99 && idCampo == 1020)) {
			// Repuestos
			numeroItems = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
					.agenciaAlemana_consultarTotalRepuestosPatron(patron);
		}

		// Consulta coexito
		if ((idFormulario == 118 && idCampo == 1556)) {
			// Repuestos
			numeroItems = ((SQLiteRoutinesOS) aplicacion.getConfiguracionApp().getSqliteRoutines())
					.consultarTotalClientePatron(patron);
		}

		// Consulta coexito
		if ((idFormulario == 112 && idCampo == 1573)) {
			// Repuestos
			numeroItems = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
					.consultarTotalEnergitecaPatron(patron);
		}

		// Consulta coexito
		if ((idFormulario == 112 && idCampo == 1579)) {
			// Repuestos
			numeroItems = ((SQLiteRoutinesOS) aplicacion.getConfiguracionApp().getSqliteRoutines())
					.consultarTotalClientePatron(patron);
		}
		
		// Retorno
		return numeroItems;
	}

	@Override
	public Vector<LinkedHashMap<String, String>> consultarItems(Context contexto, String patron, HashMap<String, String> datosAdicionales) {
		AplicacionOrdenesServicio aplicacion = (AplicacionOrdenesServicio) contexto.getApplicationContext();
		Vector<LinkedHashMap<String, String>> items = null;
		if (idFormulario == 41 || idFormulario == 42) {
			if (idCampo == 387 || idCampo == 367) {
				// Montacargas
				items = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
						.almatec_consultarMontacargas(nombreCampo);
			}
		}

		// Consulta clientes por patron
		// Consulta coexito
		if ((idFormulario == 118)) {
			if (idCampo == 1556) {
				items = ((SQLiteRoutinesOS) aplicacion.getConfiguracionApp().getSqliteRoutines())
						.consultaClienteVector(patron);
			}
		}

		if ((idFormulario == 112)) {
			if (idCampo == 1573) {
				items = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
						.consultaEnergitecaVector(patron);
			}else if (idCampo == 1579) {
				items = ((SQLiteRoutinesOS) aplicacion.getConfiguracionApp().getSqliteRoutines())
						.consultaClienteVector(patron); //
			}
		}

		//Consulta de respuestos crown instalados(101)/solicitados(103)
		if ((idFormulario == 101 || idFormulario == 103) ) {
			if (idCampo == 1096 || idCampo == 1130) {
				// repuestos
				items = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
						.royal_consultarRepuestosCrown(patron);
			}
		}
		
		// Consulta los repuestos
		if ((idFormulario == 35 && idCampo == 322) || (idFormulario == 58 && idCampo == 678)) {
			// repuestos
			items = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
					.almatec_consultarRepuestosPatron(patron);
		}else if ((idFormulario == 98 && idCampo == 1024) || (idFormulario == 99 && idCampo == 1020)) {
			// repuestos
			items = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
					.agenciaAlemana_consultarRepuestosPatron(patron);
		}
		
		// MEPAL
		if (idFormulario == 70){ 
			if(idCampo == 767) {
				// Actividades
				items = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
						.mepal_consultarListaActividades();
			}
		} else if (idFormulario == 71){ 
			if(idCampo == 770) {
				// Proyectos
				items = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
						.mepal_consultarProyectosPatron(patron);
			} else if(idCampo == 771) {
				// Pedidos
				items = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
						.mepal_consultarPedidosPatron(patron, datosAdicionales);
			} else if(idCampo == 772) {
				// Articulos
				items = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
						.mepal_consultarArticulosPatron(patron, datosAdicionales);
			}
		}
		
		// Retorno...
		return items;
	}

	@Override
	public String obtenerNombreCampoFiltro(Context contexto) {
		// MEPAL
		if (idFormulario == 70){ 
			if(idCampo == 767) {
				return "Actividad";
			}
		}
		return null;
	}

	@Override
	public void abrirActividadEspecifica(Activity actividad, Context contexto, String tipoActividad, int request, View layoutView) {
		Intent intent = null;
		HashMap<String, String> datosAdicionales = new HashMap<>();
		
		// Dependiendo del formulario y del campo, genera objeto con datos adicionales.
		// MEPAL
		if (idFormulario == 71){ 
			if(idCampo == 771) {
				// Pedidos, Extrae el campo de proyecto para filtrar los pedidos
				String codigoProyecto = ((TextView)layoutView.findViewById(770)).getText().toString();
				datosAdicionales.put("codigoProyecto", codigoProyecto);
			} else if(idCampo == 772) {
				// Articulos
				String codigoProyecto = ((TextView)layoutView.findViewById(770)).getText().toString();
				String codigoPedido = ((EditText)layoutView.findViewById(771)).getText().toString();
				datosAdicionales.put("codigoProyecto", codigoProyecto);
				datosAdicionales.put("codigoPedido", codigoPedido);
			}
		}
		
		// Dependiendo del tipoActividad, inicializa el intent
		if (tipoActividad.equals(AUTOCOMPLETE)){
	    	intent = new Intent(contexto, ActividadAutocompletarCampo.class);
		} else if (tipoActividad.equals(LISTACAMPO)){
	    	intent = new Intent(contexto, ActividadListaCampo.class);
		}
		
    	// Llama la actividad de consulta de elementos.
		if(intent != null){
			intent.putExtra("autocompletarCampo", this);
			intent.putExtra("datosAdicionales", datosAdicionales);
			actividad.startActivityForResult(intent, request);
		}
	}

}
