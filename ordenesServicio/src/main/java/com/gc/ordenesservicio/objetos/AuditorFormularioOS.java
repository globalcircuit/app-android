package com.gc.ordenesservicio.objetos;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

//import com.gc.ordenesservicio.ActividadFirma.onFirma;
import com.gc.coregestionclientes.objetos.AuditorFormulario;
import com.gc.coregestionclientes.objetos.Campo;
import com.gc.coregestionclientes.objetos.Cliente;
import com.gc.coregestionclientes.objetos.ControladorFormularios;
import com.gc.coregestionclientes.objetos.Formulario;
import com.gc.ordenesservicio.ActividadFirma;
import com.gc.ordenesservicio.ActividadGaleriaImagenes;
import com.gc.ordenesservicio.ActividadSubformulario;
import com.gc.ordenesservicio.ActividadVisita;
import com.gc.ordenesservicio.AplicacionOrdenesServicio;
import com.gc.ordenesservicio.R;
import com.gc.ordenesservicio.accesodatos.CustomSQLiteRoutinesOS;
import com.gc.ordenesservicio.accesodatos.SQLiteRoutinesOS;
import com.gc.ordenesservicio.novedades.ServicioNovedades;
import com.kyanogen.signatureview.SignatureView;


public class AuditorFormularioOS extends AuditorFormulario
{

    private static final int COD_FOTO = 20;

	public AuditorFormularioOS(Formulario formulario,
							   String logicaInterpretador,
							   ControladorFormularios controladorFormularios) {
		super(formulario, logicaInterpretador, controladorFormularios);
	}

	@Override
	public void eventoFormularioSeleccionado(Context contexto) {}

	public void onFirma (String path, String nombre)
	{
		removerFragmento();

	}
	private void removerFragmento()
	{

	}

	@Override
	public void eventoAutoComplete(Context contexto, View layoutView, Campo campoEvento, View view, HashMap<String, String> objetoDatos){
		AplicacionOrdenesServicio aplicacion = (AplicacionOrdenesServicio) contexto.getApplicationContext();
		String valor = null;
		EditText editText = null;

		// Solicitud Repuestos - Carvajal Espacios - ALMATEC
		if (formulario.getId() == 35) {
			switch (campoEvento.getId()) {
				case 322:
					// Es el campo Código
					editText = (EditText) view;
					valor = (String) editText.getText().toString();

					// Busca coincidencias en la base de datos
					HashMap<String, String> repuesto = ((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp()
							.getCustomSqliteRoutines()).almatec_consultarRepuesto(valor, objetoDatos);
					String descripcionArticulo = "";
					String subinventario = "";
					String localizador = "";
					String tipoUnidad = "";
					String uen = "";
					String bodega = "";

					// Verifica si el repuesto no es nulo
					if(repuesto != null){
						// Item existe, extrae datos
						descripcionArticulo = repuesto.get("descripcionArticulo");
						subinventario = repuesto.get("subinventario");
						localizador = repuesto.get("localizador");
						tipoUnidad = repuesto.get("tipoUnidad");
						uen = repuesto.get("uen");
						bodega = repuesto.get("bodega");

						// Cambia el color del texto
						editText.setTextColor(contexto.getResources().getColor(R.color.DarkGreen));
					} else {
						// Cambia el color del texto
						editText.setTextColor(Color.RED);
					}

					// Setea los datos en los campos del formulario
					((EditText)layoutView.findViewById(323)).setText(descripcionArticulo);
					((TextView)layoutView.findViewById(673)).setText(subinventario);
					((TextView)layoutView.findViewById(674)).setText(localizador);
					((TextView)layoutView.findViewById(675)).setText(tipoUnidad);
					((TextView)layoutView.findViewById(676)).setText(uen);
					((TextView)layoutView.findViewById(677)).setText(bodega);
					break;
			}
			return;
		}

		// Instalación Repuestos - Carvajal Espacios - ALMATEC
		if (formulario.getId() == 58) {
			switch (campoEvento.getId()) {
				case 678:
					// Es el campo Código
					editText = (EditText) view;
					valor = (String) editText.getText().toString();

					// Busca coincidencias en la base de datos
					HashMap<String, String> repuesto = ((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp()
							.getCustomSqliteRoutines()).almatec_consultarRepuesto(valor, objetoDatos);
					String descripcionArticulo = "";
					String subinventario = "";
					String localizador = "";
					String tipoUnidad = "";
					String uen = "";
					String bodega = "";

					// Verifica si el repuesto no es nulo
					if(repuesto != null){
						// Item existe, extrae datos
						descripcionArticulo = repuesto.get("descripcionArticulo");
						subinventario = repuesto.get("subinventario");
						localizador = repuesto.get("localizador");
						tipoUnidad = repuesto.get("tipoUnidad");
						uen = repuesto.get("uen");
						bodega = repuesto.get("bodega");

						// Cambia el color del texto
						editText.setTextColor(contexto.getResources().getColor(R.color.DarkGreen));
					} else {
						// Cambia el color del texto
						editText.setTextColor(Color.RED);
					}

					// Setea los datos en los campos del formulario
					((TextView)layoutView.findViewById(679)).setText(descripcionArticulo);
					((TextView)layoutView.findViewById(680)).setText(subinventario);
					((TextView)layoutView.findViewById(681)).setText(localizador);
					((TextView)layoutView.findViewById(682)).setText(tipoUnidad);
					((TextView)layoutView.findViewById(683)).setText(uen);
					((TextView)layoutView.findViewById(684)).setText(bodega);
					break;
			}
			return;
		}

		// Instalación Repuestos - Agencia Alemana
		if (formulario.getId() == 98) {
			switch (campoEvento.getId()) {
				case 1024:
					// Es el campo Código
					editText = (EditText) view;
					valor = (String) editText.getText().toString();

					// Busca coincidencias en la base de datos
					HashMap<String, String> articulos = ((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp()
							.getCustomSqliteRoutines()).agenciaAlemana_consultarRepuesto(valor, objetoDatos);
					String codigoSAP = "";
					String codigoBarras = "";
					String referenciaFabricante = "";
					String descripcion = "";
					String grupo = "";

					// Verifica si el repuesto no es nulo
					if(articulos != null) {
						// Item existe, extrae datos
						codigoBarras = articulos.get("codigoBarras");
						referenciaFabricante = articulos.get("referenciaFabricante");
						descripcion = articulos.get("descripcion");
						codigoSAP = articulos.get("codigoSAP");
						grupo = articulos.get("grupo");

						// Cambia el color del texto
						editText.setTextColor(contexto.getResources().getColor(R.color.DarkGreen));
						//
					} else {
						// Cambia el color del texto
						editText.setTextColor(Color.RED);
					}

					// Setea los datos en los campos del formulario
					((TextView)layoutView.findViewById(1024)).setText(referenciaFabricante);
					((TextView)layoutView.findViewById(1025)).setText(codigoSAP);
					((TextView)layoutView.findViewById(1045)).setText(codigoBarras);
					((TextView)layoutView.findViewById(1070)).setText(grupo);
					Log.d("sebas", "dd: " + referenciaFabricante);
					if( referenciaFabricante.equals("012345678") || referenciaFabricante == "012345678"){
						((TextView)layoutView.findViewById(1026)).setEnabled(true);
						((TextView)layoutView.findViewById(1026)).setText(descripcion);
					}else{
						((TextView)layoutView.findViewById(1026)).setEnabled(false);
						((TextView)layoutView.findViewById(1026)).setText(descripcion);
					}
					break;
			}
			return;
		}
		// Instalación Repuestos - Agencia Alemana
		if (formulario.getId() == 99) {
			switch (campoEvento.getId()) {
				case 1020:
					// Es el campo Código
					editText = (EditText) view;
					valor = (String) editText.getText().toString();

					// Busca coincidencias en la base de datos
					HashMap<String, String> articulos = ((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp()
							.getCustomSqliteRoutines()).agenciaAlemana_consultarRepuesto(valor, objetoDatos);
					String codigoSAP = "";
					String codigoBarras = "";
					String referenciaFabricante = "";
					String descripcion = "";
					String grupo = "";

					// Verifica si el repuesto no es nulo
					if(articulos != null){
						// Item existe, extrae datos
						codigoSAP = articulos.get("codigoSAP");
						codigoBarras = articulos.get("codigoBarras");
						referenciaFabricante = articulos.get("referenciaFabricante");
						descripcion = articulos.get("descripcion");
						grupo = articulos.get("grupo");

						// Cambia el color del texto
						editText.setTextColor(contexto.getResources().getColor(R.color.DarkGreen));
					} else {
						// Cambia el color del texto
						editText.setTextColor(Color.RED);
					}

					// Setea los datos en los campos del formulario
					((TextView)layoutView.findViewById(1020)).setText(referenciaFabricante);
					((TextView)layoutView.findViewById(1021)).setText(codigoSAP);

					((TextView)layoutView.findViewById(1065)).setText(codigoBarras);
					((TextView)layoutView.findViewById(1069)).setText(grupo);

					if( referenciaFabricante.equals("012345678") || referenciaFabricante == "012345678"){
						((TextView)layoutView.findViewById(1022)).setEnabled(true);
						((TextView)layoutView.findViewById(1022)).setText(descripcion);
					}else{
						((TextView)layoutView.findViewById(1022)).setEnabled(false);
						((TextView)layoutView.findViewById(1022)).setText(descripcion);
					}
					break;
			}
			return;
		}
	}

	public static String cargaFormularioDinamico(Context contexto){
		String nombreFormulario = "TAREAS / VISITAS";
		Activity actividad = (Activity) contexto;
		AplicacionOrdenesServicio aplicacion = (AplicacionOrdenesServicio) actividad.getApplicationContext();
		OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();
		JSONObject jsonObject;
		String datosAdicionales;
		int idCliennteGlobal = aplicacion.getConfiguracionApp().getUsuario().getIdClienteGlobal();

		if(idCliennteGlobal == 14){
			// Consulta los datos de la orden y los setea en los campos
			int  idOrden = ordenServicio.getId();
			//String tipoServicio = ordenServicio.

			datosAdicionales = ordenServicio.getDatosAdicionales();

			//Cambio de estado
			try{
				//Convierto los datos adicionales de una orden de servicio a un objeto tipo JSON
				jsonObject = new JSONObject(datosAdicionales);
				String tipoServicio = jsonObject.getString("tipoOrden");

				if (tipoServicio.equals("PREVENTIVO")) {
					nombreFormulario = "TAREAS / VISITAS PREVENTIVO"; // cambio en esta linea
					//nombreFormulario ="TAREA/PRUEBA"; // cambio en esta linea
				}else if (tipoServicio.equals("ALISTAMIENTO")) {
					nombreFormulario = "TAREAS / VISITAS ALISTAMIENTO";
				}

			}catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if(idCliennteGlobal == 16){
			try {
				datosAdicionales = aplicacion.getOrdenServicioActiva().getDatosAdicionales();
				if(datosAdicionales != null) {
					jsonObject = new JSONObject(datosAdicionales);
					String actividadSelecionada = jsonObject.getString("actividad");

					// Se valida que formulario desea ver
					if(actividadSelecionada.equals("Visita a Energiteca")){
						nombreFormulario = "TAREAS / VISITA ENERGITECA";
					}else if(actividadSelecionada.equals("Capacitación Interna (Coéxito)")){
						nombreFormulario = "TAREAS / VISITAS CAPACITACIÓN INTERNA";
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		return nombreFormulario;
	}

	@Override
	public void inicializarCamposFormulario(Context contexto, HashMap<String, View> contenedoresCamposHash) {
		Activity actividad = (Activity) contexto;
		AplicacionOrdenesServicio aplicacion = (AplicacionOrdenesServicio) actividad.getApplicationContext();

		// NUEVA ORDEN DE SERVICIO - Coexito
		if (formulario.getId() == 118) {
			OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();

			// Consulta los datos de la orden y los setea en los campos
			String idOrden = String.valueOf(ordenServicio.getId());
			String fechaCreacion = toolsApp.unixDateToString(ordenServicio.getUnixFechaCreacion(), 2, true);
			((TextView)actividad.findViewById(1559)).setText(idOrden);
			((TextView)actividad.findViewById(1558)).setText(fechaCreacion);

			return;
		}

		// ORDEN DE SERVICIO ACTIVA - Coexito
		if (formulario.getId() == 119) {
			try {
				String datosAdicionales = aplicacion.getOrdenServicioActiva().getDatosAdicionales();
				if(datosAdicionales != null) {
					JSONObject jsonObject = new JSONObject(datosAdicionales);
					OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();
					Cliente cliente = aplicacion.getConfiguracionApp().getClienteActivo();

					//int idOrden = ordenServicio.getId();
					String codigoCliente = String.valueOf(cliente.getCodigo());
					String nombreCliente = String.valueOf(cliente.getNombres());
					String fechaCreacion = toolsApp.unixDateToString(ordenServicio.getUnixFechaCreacion(), 2, true);
					String actividadSelecionada = jsonObject.getString("actividad");

					//((TextView)actividad.findViewById(1564)).setText(idOrden);
					((TextView)actividad.findViewById(1562)).setText(nombreCliente);
					((TextView)actividad.findViewById(1561)).setText(codigoCliente);
					((TextView)actividad.findViewById(1563)).setText(fechaCreacion);
					((TextView)actividad.findViewById(1560)).setText(actividadSelecionada);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return;
		}
		// TAREAS / VISITAS CAPACITACIÓN INTERNA - Coexito
		if (formulario.getId() == 120) {
			OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();

			// Setea el número de tarea y la fecha de inicio
			((TextView)actividad.findViewById(1567)).setText(String.valueOf(ordenServicio.getVisitaActiva().getId()));
			((TextView)actividad.findViewById(1568)).setText(String.valueOf(
					toolsApp.unixDateToString(ordenServicio.getVisitaActiva().getUnixFechaHoraIncio(), 2, true)));

			return;
		}
		// TAREAS / VISITAS Energiteca- Coexito
		if (formulario.getId() == 112) {
			OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();

			// Setea el número de tarea y la fecha de inicio
			((TextView)actividad.findViewById(1578)).setText(String.valueOf(ordenServicio.getVisitaActiva().getId()));
			((TextView)actividad.findViewById(1527)).setText(String.valueOf(
					toolsApp.unixDateToString(ordenServicio.getVisitaActiva().getUnixFechaHoraIncio(), 2, true)));

			return;
		}
		// NUEVA ORDEN DE SERVICIO - Carvajal Espacios - ALMATEC
		if (formulario.getId() == 41) {
			OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();

			// Consulta los datos de la orden y los setea en los campos
			String idOrden = String.valueOf(ordenServicio.getId());
			String fechaCreacion = toolsApp.unixDateToString(ordenServicio.getUnixFechaCreacion(), 2, true);
			((TextView)actividad.findViewById(385)).setText(idOrden);
			((TextView)actividad.findViewById(386)).setText(fechaCreacion);

			// TODO: Muestra los campos de fallas
			/*SparseArray<Campo> camposHash = formulario.getCamposHash();
			for(int indiceIdCampo = 818; indiceIdCampo <= 819; indiceIdCampo++){
				LinearLayout ll_campo = (LinearLayout) contenedoresCamposHash.get(String.valueOf(indiceIdCampo));
				Campo campo = camposHash.get(indiceIdCampo);
				if (ll_campo != null && campo != null){
					ll_campo.setVisibility(LinearLayout.VISIBLE);
					campo.setVisible(true);
				}
			}*/

			// Carga la lista de falla de sistemas
			String[] fallasSistema = ((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
					.almatec_consultarFallasSistema();

			// Crea y adiciona el Adapter para el spinner
			if (fallasSistema != null) {
				ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(contexto,
						R.layout.textview_spinner, fallasSistema);
				arrayAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
				((Spinner)actividad.findViewById(818)).setAdapter(arrayAdapter);
			}
			return;
		}
		//Instalación y solicitud de repuestos Agencia Alemana
		if (formulario.getId() == 98) {
			//Se setea el código de barras como oculto para no generar problemas
			TextView codigoBarras = (TextView)actividad.findViewById(1045);
			codigoBarras.setEnabled(false);
			codigoBarras.setVisibility(View.GONE);
			return;
		}

		if (formulario.getId() == 99) {
			//Se setea el código de barras como oculto para no generar problemas
			TextView codigoBarras = (TextView)actividad.findViewById(1065);
			codigoBarras.setEnabled(false);
			codigoBarras.setVisibility(View.GONE);
			return;
		}

		if (formulario.getId() == 96) {
			OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();
			String datosAdicionales = ordenServicio.getDatosAdicionales();

			/* Manera de ocultar un campo gráficamente.
			SparseArray<Campo> camposHash = formulario.getCamposHash();
			LinearLayout ll_campo = (LinearLayout) contenedoresCamposHash.get(String.valueOf(1046));
				Campo campo = camposHash.get(1046);
				if (ll_campo != null && campo != null){
					ll_campo.setVisibility(LinearLayout.INVISIBLE);
					campo.setVisible(false);
				}*/

			// Consulta los datos de la orden y los setea en los campos
			String fechaCreacion = toolsApp.unixDateToString(ordenServicio.getUnixFechaCreacion(), 2, true);

			//Se debería mostrar son los datos específicos del cliente mediante la siguiente línea:
			LinkedHashMap <String, String> datoEspecialCliente = ordenServicio.getDatosEspecCliente();

			if(datoEspecialCliente != null){
				// Se obtiene los datos del objeto de datos especiales del cliente
				String codigoVisitaProgramada = datoEspecialCliente.get("Código Visita");
				String fechaProgramadaInicio = datoEspecialCliente.get("Fecha Programada Inicio");
				String fechaProgramadaFin = datoEspecialCliente.get("Fecha Programada Fin");

				//Cambio de estado
				try{
					//Convierto los datos adicionales de una orden de servicio a un objeto tipo JSON
					JSONObject jsonObject = new JSONObject(datosAdicionales);

					//Declaración de variables para usar
					String codigoOT = jsonObject.getString("codigoOT").toUpperCase();
					String codigoTecnico = aplicacion.getConfiguracionApp().getUsuario().getCodigo().toString();
					boolean serial = jsonObject.isNull( "codigoEquipo" );

					//validación para saber si un serial viene vacío y así poder setearlos en la orden
					if(serial){
						//Los campos serial, modelo y marca se habilitan, respectivamente
						((EditText)actividad.findViewById(1145)).setEnabled(true);
						((EditText)actividad.findViewById(1148)).setEnabled(true);
						((EditText)actividad.findViewById(1149)).setEnabled(true);
					}else{
						//Los campos serial, modelo y marca se deshabilitan, respectivamente
						((EditText)actividad.findViewById(1145)).setEnabled(false);
						((EditText)actividad.findViewById(1148)).setEnabled(false);
						((EditText)actividad.findViewById(1149)).setEnabled(false);
					}

					// Obtiene el estado de la programación visita
					String estado = ((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp()
							.getCustomSqliteRoutines()).agenciaAlemana_obtenerEstadoVisita( codigoOT, codigoVisitaProgramada, codigoTecnico);

					// Se asignan los valores al formulario
					//((TextView)actividad.findViewById(1152)).setText(estado);

					String estadoValidar = datoEspecialCliente.get("Estado");

					//FALTA IMPLEMENTAR BIEN ESTO
					if( estadoValidar.equals("FINALIZADA") || estadoValidar.equals("CANCELADA")){

						int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
						//Calendario para la fecha de los hitos
						Calendar cal = Calendar.getInstance();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
						String fecha = sdf.format(cal.getTime());
						String toLog = "Entré a una visita ya finalizada/cancelada a las " + fecha + " con el idOrden " + idOrdenServicioLog;
						toolsApp.generaLog(toLog);

						Button boton_finalizar_orden = (Button)actividad.findViewById(R.id.boton_finalizar_orden);
						boton_finalizar_orden.setEnabled(false);
						boton_finalizar_orden.setVisibility(View.GONE);
						Button boton_detalle = (Button)actividad.findViewById(R.id.boton_detalle);
						boton_detalle.setEnabled(false);
						boton_detalle.setVisibility(View.GONE);
					}else{

					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


				// Se asignan los valores al formulario
				//((TextView)actividad.findViewById(1152)).setText(estado);
				((TextView)actividad.findViewById(1153)).setText(codigoVisitaProgramada);
				((TextView)actividad.findViewById(1154)).setText(fechaProgramadaInicio);
				((TextView)actividad.findViewById(1155)).setText(fechaProgramadaFin);
			}

			((TextView)actividad.findViewById(1047)).setText(fechaCreacion);
			return;

		}
		// ORDEN DE SERVICIO ACTIVA - Carvajal Espacios - ALMATEC
		if (formulario.getId() == 42) {
			try {
				String datosAdicionales = aplicacion.getOrdenServicioActiva().getDatosAdicionales();
				if(datosAdicionales != null){
					JSONObject jsonObject = new JSONObject(datosAdicionales);
					OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();
					Cliente cliente = aplicacion.getConfiguracionApp().getClienteActivo();

					// Consulta los datos de la orden y los setea en los campos
					//String idOrden = String.valueOf(ordenServicio.getId());
					String codigoCliente = String.valueOf(cliente.getCodigo());
					String fechaCreacion = toolsApp.unixDateToString(ordenServicio.getUnixFechaCreacion(), 2, true);
					String serialMontacarga = jsonObject.getString("serialMontacarga");
					String serialVerificado = jsonObject.getString("serialVerificado");
					String codigoFallaSistema = jsonObject.getString("codigoFallaSistema");
					/*String descripcion 		= jsonObject.getString("descripcion");
					String modelo 			= jsonObject.getString("modelo");
					String marca 			= jsonObject.getString("marca");
					String tipoOrdenServicio 	= jsonObject.getString("tipoOrdenServicio");
					String tipoServicio 	= jsonObject.getString("tipoServicio");
					String descripcionFalla = jsonObject.getString("descripcionFalla");
					String contacto 		= jsonObject.getString("contacto");
					String direccion 		= jsonObject.getString("direccion");
					String telefono 		= jsonObject.getString("telefono");*/
					String estado 			= jsonObject.getString("estado");
					EditText serialVerificadoView = (EditText)actividad.findViewById(367);

					//((TextView)actividad.findViewById(363)).setText(idOrden);
					((TextView)actividad.findViewById(364)).setText(codigoCliente);
					((TextView)actividad.findViewById(365)).setText(fechaCreacion);
					((TextView)actividad.findViewById(366)).setText(serialMontacarga);

					if(serialVerificado != null && !serialVerificado.equals("null")){
						// Ya existe serial de verificacion
						serialVerificadoView.setText(serialVerificado);
						serialVerificadoView.setEnabled(false);
					} else {
						if (serialVerificadoView.getText().toString().equals("")){
							// Serial del montacarga por defecto como serial de verificacion
							serialVerificadoView.setText(serialMontacarga);
						}
					}


					// Carga la lista de falla de sistemas
					String[] fallasSistema = ((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
							.almatec_consultarFallasSistema();

					// Crea y adiciona el Adapter para el spinner
					if (fallasSistema != null) {
						ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(contexto,
								R.layout.textview_spinner, fallasSistema);
						arrayAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
						((Spinner)actividad.findViewById(816)).setAdapter(arrayAdapter);


						// Se inicializa el spiner
						Spinner spinnerCodigoFalla = ((Spinner)actividad.findViewById(816));

						// Asigna el valor
						int numeroValoresFallasSistema = fallasSistema.length;

						// Recorre el vector fallas  sistema
						for (int j = 0; j < numeroValoresFallasSistema; j++) {

							if(fallasSistema[j].equals(codigoFallaSistema)){
								// Se muestra el valor del spinner
								spinnerCodigoFalla.setSelection(j);
								break;
							}
						}
					}
					/*((TextView)actividad.findViewById(368)).setText(descripcion);
					((TextView)actividad.findViewById(369)).setText(modelo);
					((TextView)actividad.findViewById(370)).setText(marca);
					((TextView)actividad.findViewById(371)).setText(tipoOrdenServicio);
					((TextView)actividad.findViewById(397)).setText(tipoServicio);
					((TextView)actividad.findViewById(372)).setText(descripcionFalla);
					((TextView)actividad.findViewById(398)).setText(contacto);
					((TextView)actividad.findViewById(399)).setText(direccion);
					((TextView)actividad.findViewById(400)).setText(telefono);*/
					((TextView)actividad.findViewById(374)).setText(estado);

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			// Muestra los campos de fallas
			/*SparseArray<Campo> camposHash = formulario.getCamposHash();
			for(int indiceIdCampo = 816; indiceIdCampo <= 817; indiceIdCampo++){
				LinearLayout ll_campo = (LinearLayout) contenedoresCamposHash.get(String.valueOf(indiceIdCampo));
				Campo campo = camposHash.get(indiceIdCampo);
				if (ll_campo != null && campo != null){
					ll_campo.setVisibility(LinearLayout.VISIBLE);
					campo.setVisible(true);
				}
			}*/

			return;
		}

		//Detalles de herramientas y repuestos Agencia Alemana
		if (formulario.getId() == 105) {
			/*
			// Setea los datos en los campos del formulario fecha inicio y numTarea
			OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();

			//Obtengo las herramientas para luego setear en el multispinner
			 String[] herramientas = ((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp()
					.getCustomSqliteRoutines()).routine_obtenerHerramientas();

			 //Obtengo las repuestos para luego setear en el multispinner
			 String[] repuestos = ((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp()
						.getCustomSqliteRoutines()).routine_obtenerRepuestos();

	        //Se crean los ArrayAdapter para setear los campos del multispinner basados en las herramientas
			 //y repuestos de la consulta
			ArrayAdapter<String> arrayAdapterHerramientas= new ArrayAdapter<String>(contexto,
					R.layout.textview_spinner, herramientas);

			ArrayAdapter<String> arrayAdapterRepuestos= new ArrayAdapter<String>(contexto,
					R.layout.textview_spinner, repuestos);

			// Adicina el adapter al multispinner
			arrayAdapterHerramientas.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
			((Spinner)actividad.findViewById(1160)).setAdapter(arrayAdapterHerramientas);


			// Adicina el adapter al multispinner
			arrayAdapterRepuestos.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
			((Spinner)actividad.findViewById(1161)).setAdapter(arrayAdapterRepuestos);
	            */

			return;
		}

		//Detalles de formulario de prueba (copia de formulario con id 43)
		if (formulario.getId() == 102) {
			// Setea los datos en los campos del formulario fecha inicio y numTarea
			OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();
			((TextView)actividad.findViewById(1099)).setText(String.valueOf(ordenServicio.getVisitaActiva().getId()));
			((TextView)actividad.findViewById(1100)).setText(String.valueOf(
					toolsApp.unixDateToString(ordenServicio.getVisitaActiva().getUnixFechaHoraIncio(), 2, true)));
			return;
		}

		// TAREAS / VISITAS - Agencia Alemana
		if (formulario.getId() == 95) {
			OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();
			// Setea el número de tarea y la fecha de inicio
			((TextView)actividad.findViewById(1066)).setText(String.valueOf(ordenServicio.getVisitaActiva().getId()));
			((TextView)actividad.findViewById(1068)).setText(String.valueOf(
					toolsApp.unixDateToString(ordenServicio.getVisitaActiva().getUnixFechaHoraIncio(), 2, true)));

			String codigoOT= ordenServicio.getDatosEspecCliente().get("CódigoOT");
			String codVisita = ordenServicio.getDatosEspecCliente().get("Código Visita");
			((TextView)actividad.findViewById(1162)).setText( codigoOT );
			((TextView)actividad.findViewById(1193)).setText( codVisita );

			//Seteo opcional de el edittext observación
			((TextView)actividad.findViewById(1041)).setEnabled(false);

			return;
		}

		// TAREAS / VISITAS - Agencia Alemana Detalle preventivo
		if (formulario.getId() == 107) {  // cambio en esta linea
			OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();
			// Setea el número de tarea y la fecha de inicio
			((TextView)actividad.findViewById(1318)).setText(String.valueOf(ordenServicio.getVisitaActiva().getId()));
			((TextView)actividad.findViewById(1321)).setText(String.valueOf(
					toolsApp.unixDateToString(ordenServicio.getVisitaActiva().getUnixFechaHoraIncio(), 2, true)));

			String codigoOT= ordenServicio.getDatosEspecCliente().get("CódigoOT");
			String codVisita = ordenServicio.getDatosEspecCliente().get("Código Visita");
			((TextView)actividad.findViewById(1319)).setText( codigoOT );
			((TextView)actividad.findViewById(1320)).setText( codVisita );

			//Seteo del edittext observaciones
			((TextView)actividad.findViewById(1337)).setEnabled(false);

			return;
		}

		// TAREAS / VISITAS - Agencia Alemana Detalle preventivo
		if (formulario.getId() == 108)
		{

			OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();

			// Setea el número de tarea y la fecha de inicio
			((TextView)actividad.findViewById(1358)).setText(String.valueOf(ordenServicio.getVisitaActiva().getId()));
			((TextView)actividad.findViewById(1361)).setText(String.valueOf(
					toolsApp.unixDateToString(ordenServicio.getVisitaActiva().getUnixFechaHoraIncio(), 2, true)));

			String codigoOT= ordenServicio.getDatosEspecCliente().get("CódigoOT");
			String codVisita = ordenServicio.getDatosEspecCliente().get("Código Visita");
			((TextView)actividad.findViewById(1359)).setText( codigoOT );
			((TextView)actividad.findViewById(1360)).setText( codVisita );

			//Seteo del edittext observaciones
			((TextView)actividad.findViewById(1436)).setEnabled(false);

			return;
		}

		// TAREAS / VISITAS - Carvajal Espacios - ALMATEC
		if (formulario.getId() == 43) {
			OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();
			Campo campoTipoOrdenServicio = ordenServicio.getFormulario().getCamposHash().get(371);
			((View)actividad.findViewById(971)).setVisibility(View.VISIBLE);
			// Setea el número de tarea y la fecha de inicio
			((TextView)actividad.findViewById(375)).setText(String.valueOf(ordenServicio.getVisitaActiva().getId()));
			((TextView)actividad.findViewById(376)).setText(String.valueOf(
					toolsApp.unixDateToString(ordenServicio.getVisitaActiva().getUnixFechaHoraIncio(), 2, true)));

			LinearLayout ll_cmpo = (LinearLayout) contenedoresCamposHash.get(String.valueOf(734));
			// Setea campo diagnostico tecnico si tipo de servico de la orden es mantenimiento
			if(campoTipoOrdenServicio.getValor().contains("PROGRAMADO") || campoTipoOrdenServicio.getValor().contains("MTTO")){
				((TextView)actividad.findViewById(381)).setText(campoTipoOrdenServicio.getValor());
				((View)actividad.findViewById(734)).setVisibility(View.VISIBLE);
				ll_cmpo.setVisibility(View.VISIBLE);
			}else{
				((View)actividad.findViewById(734)).setVisibility(View.GONE);
				ll_cmpo.setVisibility(View.GONE);
			}


			// Carga el estado
			try {
				String datosAdicionales = aplicacion.getOrdenServicioActiva().getDatosAdicionales();
				if(datosAdicionales != null){
					JSONObject jsonObject = new JSONObject(datosAdicionales);
					String estado = jsonObject.getString("estado");

					// Extrae los elementos
					Spinner spinnerEstado = ((Spinner)actividad.findViewById(401));

					// Asigna el valor
					int numeroElementos = spinnerEstado.getAdapter().getCount();
					for (int j = 0; j < numeroElementos; j++) {
						if(((String)spinnerEstado.getAdapter().getItem(j)).equals(estado)){
							spinnerEstado.setSelection(j);
							break;
						}
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return;
		}

		// DETALLE ACTIVIDAD - Carvajal Espacios - ALMATEC
		if(formulario.getId() == 40){
			// Setea los datos en los campos del formulario
			((TextView)actividad.findViewById(359)).setText(toolsApp.getDateTimeNOW());
			return;
		}

		// ACTIVIDADES ADMINISTRATIVAS - Carvajal Espacios - ALMATEC
		if(formulario.getId() == 50){
			// Setea los datos en los campos del formulario
			((TextView)actividad.findViewById(567)).setText(toolsApp.getDateTimeNOW());
			((TextView)actividad.findViewById(619)).setText(String.valueOf(aplicacion.getActividadAdministrativaActiva()
					.getId()));
			return;
		}

		/*******************************************/
		// Condicionales Mepal
		/*******************************************/
		// ORDEN DE SERVICIO ACTIVA - Carvajal Espacios - MEPAL
		if (formulario.getId() == 68) {
			SparseArray<Campo> camposHash = formulario.getCamposHash();
			OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();
			try {
				String datosAdicionales = ordenServicio.getDatosAdicionales();
				if(datosAdicionales != null){
					JSONObject jsonObject = new JSONObject(datosAdicionales);
					String estado = jsonObject.getString("estado");
					((TextView)actividad.findViewById(761)).setText(estado);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			// Consulta los datos de la orden y los setea en los campos
			((TextView)actividad.findViewById(786)).setText(
					toolsApp.unixDateToString(ordenServicio.getUnixFechaCreacion(), 1, true));

			// Dependiendo del tipo de orden, oculta algunos campos
			String tipoOrden = ((TextView)actividad.findViewById(814)).getText().toString();
			if (tipoOrden.equals("Proyecto")) {
				// Oculta el campo de servicio
				LinearLayout ll_campo = (LinearLayout) contenedoresCamposHash.get(String.valueOf(747));
				Campo campo = camposHash.get(747);
				if (ll_campo != null && campo != null){
					ll_campo.setVisibility(LinearLayout.GONE);
					campo.setVisible(false);
				}
				// TODO mostraría los campos de proyecto??
			} else if (tipoOrden.equals("Servicio")){
				// Oculta los campos de proyecto
				for(int indiceIdCampo = 744; indiceIdCampo <= 746; indiceIdCampo++){
					LinearLayout ll_campo = (LinearLayout) contenedoresCamposHash.get(String.valueOf(indiceIdCampo));
					Campo campo = camposHash.get(indiceIdCampo);
					if (ll_campo != null && campo != null){
						ll_campo.setVisibility(LinearLayout.GONE);
						campo.setVisible(false);
					}
				}
				// TODO mostraría el campo de servicio??
			}

			// Dependiendo de si aplica o no el protocolo, oculta algunos campos
			String aplica = ((TextView)actividad.findViewById(748)).getText().toString();
			if (aplica.equals("NO")) {
				// Oculta los campos de protocolo
				for(int indiceIdCampo = 749; indiceIdCampo <= 760; indiceIdCampo++){
					LinearLayout ll_campo = (LinearLayout) contenedoresCamposHash.get(String.valueOf(indiceIdCampo));
					Campo campo = camposHash.get(indiceIdCampo);
					if (ll_campo != null && campo != null){
						ll_campo.setVisibility(LinearLayout.GONE);
						campo.setVisible(false);
					}
				}
			}

			return;
		}

		// TAREAS / VISITAS - Carvajal Espacios - MEPAL
		if (formulario.getId() == 69) {
			SparseArray<Campo> camposHash = formulario.getCamposHash();
			OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();

			// Setea el número de tarea y la fecha de inicio
			((TextView)actividad.findViewById(762)).setText(String.valueOf(ordenServicio.getVisitaActiva().getId()));
			((TextView)actividad.findViewById(763)).setText(String.valueOf(
					toolsApp.unixDateToString(ordenServicio.getVisitaActiva().getUnixFechaHoraIncio(), 2, true)));

			// TODO: Muestra el campo para adjuntar fotos de actividades
			View contenedorCampo = contenedoresCamposHash.get(String.valueOf(825));
			if(contenedorCampo != null){
				contenedorCampo.setVisibility(LinearLayout.VISIBLE);
			}
			camposHash.get(825).setVisible(true);

			return;
		}

		// DETALLE ACTIVIDAD - Carvajal Espacios - MEPAL
		if(formulario.getId() == 70){
			// Setea los datos en los campos del formulario
			((TextView)actividad.findViewById(784)).setText(toolsApp.getDateTimeNOW());
			return;
		}
		if(formulario.getId() == 97){
			// Setea los datos en los campos del formulario
			((TextView)actividad.findViewById(1016)).setText(toolsApp.getDateTimeNOW());
			return;
		}

		// DETALLE RECLAMOS - Carvajal Espacios - MEPAL
		if(formulario.getId() == 71){
			OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();
			String datosAdicionales = ordenServicio.getDatosAdicionales();
			if(datosAdicionales != null){
				try {
					JSONObject jsonObject = new JSONObject(datosAdicionales);
					String idProyecto = jsonObject.getString("idProyecto");
					((TextView)actividad.findViewById(770)).setText(idProyecto);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return;
		}
	}

	@Override
	public void validarEventoCampo(Context contexto, View layoutView,
								   Campo campoEvento, View view,
								   final HashMap<String, View> contenedoresCamposHash,
								   HashMap<String, Campo> camposHash) {
		Activity actividad = (Activity) contexto;
		AplicacionOrdenesServicio aplicacion = (AplicacionOrdenesServicio) contexto.getApplicationContext();
		String valor = null;
		Spinner spinner = null;
		CheckBox checkbox = null;
		TextView textView = null;
		EditText editText = null;
		Button button = null;
		String fechaHora = null;
		String fechaHoraVisita = null;
		long unixFechaHoraVisita = -1;
		long unixFechaHora = -1;
		long unixFechaHoraActual = -1;
		List<Campo> cmpos = formulario.getCampos();
		int nmro_cmpos = cmpos.size();
		SignatureView signatureView;

		aplicacion.salvarObjetosContexto(true);

		//Temporal D:
		final View layoutTemporal = layoutView;

		// DETALLE ACTIVIDAD validación fecha inicio y fin - Carvajal Espacios
		/*if (formulario.getId() == 40) {
			OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();
			switch (campoEvento.getId()) {
				case 360:
					// Es el campo 'Unidades'
					// Extrae el valor del view
					editText = (EditText) view;
					valor = editText.getText().toString();
					if(valor != null && !valor.equals("")){
						String fechaInicio  = toolsApp.getDateNOW();
						fechaHora = fechaInicio+" "+valor;
						unixFechaHora = toolsApp.stringToUnixDate(fechaHora,true);
						unixFechaHoraActual = toolsApp.getUnixDateNOW(true,0,0,0);
						unixFechaHoraVisita = ordenServicio.getVisitaActiva().getUnixFechaHoraIncio();

						// Se valida que la fecha digitada por el técnico es menor a la fecha actual
						if(unixFechaHoraVisita > unixFechaHora || unixFechaHoraActual > unixFechaHora){
							toolsApp.showLongMessage("El rango de horas no es válido!", contexto);
							editText.setText("");
						}
					}
				break;
				case 361:

					// Es el campo 'Unidades'
					// Extrae el valor del view
					editText = (EditText) view;
					valor = editText.getText().toString();
					if(valor != null && !valor.equals("")){
						String fechaFin  = toolsApp.getDateNOW();
						fechaHora = fechaFin+" "+valor;
						unixFechaHora = toolsApp.stringToUnixDate(fechaHora,true);
						unixFechaHoraActual = toolsApp.getUnixDateNOW(true,0,0,0);
						unixFechaHoraVisita = ordenServicio.getVisitaActiva().getUnixFechaHoraIncio();

						// Se valida que la fecha digitada por el técnico es menor a la fecha actual
						if(unixFechaHoraVisita > unixFechaHora || unixFechaHoraActual > unixFechaHora){
							toolsApp.showLongMessage("El rango de horas no es válido!", contexto);
							editText.setText("");
						}
					}
				break;
			}
			return;
		}*/


		// Formulario de detalle para instalación(101)/solicitud(103) de repuestos crown
		if (formulario.getId() == 101 || formulario.getId() == 103) {

			String codigoRepuesto = "";
			String descripcion = "";
			HashMap<String, String> repuesto = null;

			//Switch para manejar el evento de los campos del formulario 101 y del 103 respectivamente
			switch (campoEvento.getId()) {
				case 1096:
					editText = (EditText) view;
					valor = editText.getText().toString();

					if(valor.equals("")){

					}else {
						// Consulta los datos del repuesto
						repuesto = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
								.royal_consultarRepuestoCrownCodigo(valor);

						if (repuesto != null) {
							// Si existe un repuesto, se obtienen los datos y se setean los valores.
							codigoRepuesto = repuesto.get("codigo");
							descripcion = repuesto.get("descripcion");
						}else {
							// Cambia el color del texto si no se encuentra el repuesto
							editText.setTextColor(Color.RED);
						}
						((TextView)layoutView.findViewById(1097)).setText(descripcion);
					}
					break;

				case 1130:
					editText = (EditText) view;
					valor = editText.getText().toString();

					if(valor.equals("")){

					}else {
						// Consulta los datos del repuesto
						repuesto = ((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
								.royal_consultarRepuestoCrownCodigo(valor);

						if (repuesto != null) {
							// Si existe un repuesto, se obtienen los datos y se setean los valores.
							codigoRepuesto = repuesto.get("codigo");
							descripcion = repuesto.get("descripcion");
						}else {
							// Cambia el color del texto si no se encuentra el repuesto
							editText.setTextColor(Color.RED);
						}
						((TextView)layoutView.findViewById(1131)).setText(descripcion);
					}
					break;

			}
			return;

		}

		// NUEVA ORDEN DE SERVICIO - Carvajal Espacios - ALMATEC
		if (formulario.getId() == 41) {
			switch (campoEvento.getId()) {
				case 387:
					// Es el campo Serial Montacarga
					editText = (EditText) view;
					valor = editText.getText().toString();

					// Consulta los datos del montacarga y los setea en los campos
					HashMap<String, String> montacarga =
							((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
									.almatec_consultarMontacarga(valor);
					Cliente cliente = null;
					String codigoCliente = null;
					String descripcion = "";
					String modelo = "";
					String marca = "";
					String tipoServicio = "";
					String direccionInstalacion = "";

					// Verifica si el montacarga no es nulo
					if(montacarga != null){
						// Existe, extrae datos
						codigoCliente = montacarga.get("codigoCliente");
						descripcion = montacarga.get("descripcion");
						modelo = montacarga.get("modelo");
						marca = montacarga.get("marca");
						tipoServicio = montacarga.get("tipo");
						direccionInstalacion = montacarga.get("direccionInstalacion");

						// Consultar el cliente asociado al montacarga seleccionado.
						cliente = ((SQLiteRoutinesOS)aplicacion.getConfiguracionApp().getSqliteRoutines())
								.routine_consultarCliente(-1, codigoCliente);

						// Cambia el color del texto
						editText.setTextColor(contexto.getResources().getColor(R.color.DarkGreen));
					} else {
						// Cambia el color del texto
						editText.setTextColor(Color.RED);
					}

					// Valida si hay un cliente válido o no
					if (cliente != null) {
						// Setea el cliente en la orden de servicio.
						aplicacion.getOrdenServicioActiva().setIdCliente(cliente.getId());
						((TextView)layoutView.findViewById(R.id.lbel_nmbre_clnte)).setText(cliente.getNombres());

						// Setea los datos en los campos del formulario
						((TextView)layoutView.findViewById(388)).setText(descripcion);
						((TextView)layoutView.findViewById(389)).setText(modelo);
						((TextView)layoutView.findViewById(390)).setText(marca);
						((TextView)layoutView.findViewById(394)).setText(direccionInstalacion);

						// Carga el tipo de servicio si lo hay
						Spinner spinnerTipoServicio = ((Spinner)actividad.findViewById(396));
						int numeroElementos = spinnerTipoServicio.getAdapter().getCount();
						for (int j = 0; j < numeroElementos; j++) {
							if(tipoServicio != null && !tipoServicio.equals("") &&
									((String)spinnerTipoServicio.getAdapter().getItem(j)).equals(tipoServicio)){
								spinnerTipoServicio.setSelection(j);
								break;
							}
						}
					} else {

					}
					break;
				case 391:
					// Es el campo tipo de orden se servicio
					spinner = (Spinner) view;
					valor = (String) spinner.getSelectedItem();

					// Si el valor es CERRADO cambia el estado de la orden a FINALIZADO, de lo contrario queda EN CURSO
					if (valor.equals("MANTENIMIENTO PROGRAMADO")) {
						((EditText)actividad.findViewById(392)).setText(valor);
					} else {
						((EditText)actividad.findViewById(392)).setText("");
					}
					break;
				case 818:
					// Es el campo fallas de sistema
					spinner = (Spinner) view;
					valor = (String) spinner.getSelectedItem();

					// Carga la lista de fallas
					String[] fallas = ((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
							.almatec_consultarFallas(valor);
					ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(contexto,
							R.layout.textview_spinner, fallas);

					// Crea y adiciona el Adapter para el spinner
					arrayAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
					((Spinner)actividad.findViewById(819)).setAdapter(arrayAdapter);
					break;
			}
			return;
		}
		// Repuestos Agencia Alemana
		if (formulario.getId() == 98) {
			switch (campoEvento.getId()) {

				case 1045:
					// Es el campo Código
					editText = (EditText) view;
					valor = (String) editText.getText().toString();

					// Busca coincidencias en la base de datos
					HashMap<String, String> articulos = ((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp()
							.getCustomSqliteRoutines()).agenciaAlemana_consultarRepuestoXCodigoBarras(valor);
					String codigoSAP = "";
					String codigoBarras = "";
					String referenciaFabricante = "";
					String descripcion = "";
					String grupo = "";

					// Verifica si el repuesto no es nulo
					if(articulos != null){
						// Item existe, extrae datos
						codigoSAP = articulos.get("codigoSAP");
						referenciaFabricante = articulos.get("referenciaFabricante");
						descripcion = articulos.get("descripcion");
						grupo = articulos.get("grupo");

						// Cambia el color del texto
						editText.setTextColor(contexto.getResources().getColor(R.color.DarkGreen));
					} else {
						// Cambia el color del texto
						editText.setTextColor(Color.RED);
					}
					// Setea los datos en los campos del formulario
					((TextView)layoutView.findViewById(1024)).setText(referenciaFabricante);
					((TextView)layoutView.findViewById(1025)).setText(codigoSAP);

					if( !referenciaFabricante.equals("012345678") && referenciaFabricante != "012345678"){
						((TextView)layoutView.findViewById(1026)).setText(descripcion);
						((TextView)layoutView.findViewById(1026)).setEnabled(false);
					}else{
						((TextView)layoutView.findViewById(1026)).setEnabled(true);
					}

					((TextView)layoutView.findViewById(1070)).setText(grupo);
				break;
			}
			return;
		}

		// Repuestos Agencia Alemana
		if (formulario.getId() == 118) {
			switch (campoEvento.getId()) {

				case 1556:
					// Es el campo Serial Montacarga
					editText = (EditText) view;
					valor = editText.getText().toString();
					Cliente cliente = null;

					// Consultar el cliente.
					cliente = ((SQLiteRoutinesOS)aplicacion.getConfiguracionApp().getSqliteRoutines())
							.routine_consultarCliente(-1, valor);

					// Valida si hay un cliente válido o no
					if (cliente != null) {
						// Setea el cliente en la orden de servicio.
						aplicacion.getOrdenServicioActiva().setIdCliente(cliente.getId());

						// Setea los datos en los campos del formulario
						((TextView)layoutView.findViewById(1557)).setText(cliente.getNombres());

					} else {

					}
					break;
			}
			return;
		}
        //Auditor de campos para el formulario 102 visita orden de servicio Carvajal espacios
        if (formulario.getId() == 102	) {

			final int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
			String toLog = "";
			//Calendario para la fecha de los hitos
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

			//Obtengo todos los campos del formulario
			final SparseArray<Campo> campos = formulario.getCamposHash();
			switch (campoEvento.getId()) {
				case 3090:
					String fechaFirmaDigital = sdf.format(cal.getTime());
					toLog = "Se presionó firmaDigital a las " + fechaFirmaDigital + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					Intent intent = new Intent(contexto, ActividadFirma.class);
					int orden = aplicacion.getOrdenServicioActiva().getId();
					int visita = aplicacion.getOrdenServicioActiva().getVisitaActiva().getId();
					intent.putExtra("id_Orden", orden);
					intent.putExtra("id_Visita", visita);
					contexto.startActivity(intent);
					break;
			}
		}

		// Repuestos Agencia Alemana
		if (formulario.getId() == 112	) {
			final int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
			String toLog = "";
			//Calendario para la fecha de los hitos
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

			//Obtengo todos los campos del formulario
			final SparseArray<Campo> campos = formulario.getCamposHash();
			switch (campoEvento.getId()) {

				case 1573:
					// Es el campo Serial Montacarga
					editText = (EditText) view;
					valor = editText.getText().toString();
					HashMap<String, String> datosEnergiteca = null;

					// Consultar el cliente.
					datosEnergiteca = ((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
							.coexito_consultarEnergiteca(valor);

					// Valida si hay un cliente válido o no
					if (datosEnergiteca != null) {
						// Se obtiene los datos de la energiteca
						String correo1 = datosEnergiteca.get("correo1");
						String correo2 = datosEnergiteca.get("correo2");
						String ciudad = datosEnergiteca.get("ciudad");

						// Setea los datos en los campos del formulario
						((EditText)layoutView.findViewById(1575)).setText(correo1);
						((EditText)layoutView.findViewById(1576)).setText(correo2);
						((TextView)layoutView.findViewById(1574)).setText(ciudad);

					} else {

					}
					break;

				//case del botón firma Digital
				case 1617:
					String fechaFirmaDigital = sdf.format(cal.getTime());
					toLog = "Se presionó firmaDigital a las " + fechaFirmaDigital + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					Intent intent = new Intent(contexto, ActividadFirma.class);
					int orden = aplicacion.getOrdenServicioActiva().getId();
					int visita = aplicacion.getOrdenServicioActiva().getVisitaActiva().getId();
					intent.putExtra("id_Orden",orden);
					intent.putExtra("id_Visita",visita);
					contexto.startActivity(intent);
					break;
				case 1586:
					toolsApp.alertDialogYESNO(contexto, "Desea realizar la lista de checkeo"
					, "Sí", "No",new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// Deshabilita el botón
							((AlertDialog) dialog).getButton(which).setEnabled(false);
							// Dependiendo del botón presionado, se ejecuta la acción
							switch (which){
								case DialogInterface.BUTTON_POSITIVE:

									//optimizar con un contador
									for(int i = 0; i < campos.size(); i++) {
										int key = campos.keyAt(i);
										if(key > 1586  && key < 1667){
											//obtengo el objeto por la llave y se pone invisible
											Campo campo = campos.get(key);
											LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
											campo.setVisible(true);
											objetoLayaout.setVisibility(LinearLayout.VISIBLE);
										}

									}//fin for
									break;
								}
							}
						}
					);//Fin Dialog
					break;
			}
			return;
		}
		//TAREAS/REPUESTOS Agencia Alemana
		if (formulario.getId() == 95) {
			//String que cambiará el estado de las visitas al
			String estado = "";
			final int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();

			final String datosAdicionales = aplicacion.getOrdenServicioActiva().getDatosAdicionales();
			final LinkedHashMap <String, String> datosEspecificos = aplicacion.getOrdenServicioActiva().getDatosEspecCliente();

			//obtengo el valor del campo Serial Verificación
			String valorQR = (String) ((EditText)layoutView.findViewById(1071)).getText().toString();
			String serialEditText = (String) ((EditText)layoutView.findViewById(1033)).getText().toString().toUpperCase();

			//Obtengo todos los campos del formulario
			SparseArray<Campo> campos = formulario.getCamposHash();

			//Obtengo los campos hash para mostrar/ocultar al relizar la acción
			Campo campoIniciarMovilizacion = campos.get(1179);
			Campo campoArribo = campos.get(1180);

			//Obtengo los layout donde están los botones que necesito mostrar/ocultar
			LinearLayout layoutIniciarMov= (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoIniciarMovilizacion.getId()));
			LinearLayout layoutArribo = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoArribo.getId()));

			//Calendario para la fecha de los hitos
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			//String que imprimirá en el log información de lo que hacen lo usuarios finales con la aplicación
			String toLog =  "";

			switch (campoEvento.getId()) {

				//case del botón firma Digital
				case 1457:
					String fechaFirmaCorrectivo = sdf.format(cal.getTime());
					toLog = "Hundí en FirmaDigital a las " + fechaFirmaCorrectivo + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					Intent intent = new Intent(contexto, ActividadFirma.class);
					int orden = aplicacion.getOrdenServicioActiva().getId();
					int visita = aplicacion.getOrdenServicioActiva().getVisitaActiva().getId();
					intent.putExtra("id_Orden", orden);
					intent.putExtra("id_Visita", visita);
					contexto.startActivity(intent);
				break;

				//Case del botón de inicio de movilización
				case 1179:
					String fechaHitoUno = sdf.format(cal.getTime());

					//Inicio del hito # 1
					String hitoUnoInicio = "ACT001-" + fechaHitoUno;
					((TextView) layoutTemporal.findViewById(1182)).setText(hitoUnoInicio);

					//Muestro/escondo los campos y sus respectivos layouts
					campoArribo.setVisible(true);
					campoIniciarMovilizacion.setVisible(false);
					layoutArribo.setVisibility(LinearLayout.VISIBLE);
					layoutIniciarMov.setVisibility(LinearLayout.GONE);
					toLog = "Se presionó InicioMovilización a las " + fechaHitoUno + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

				break;

				//Case del botón de Arribo cliente
				case 1180:

					//obtengo la fecha con el formato del calendario
					String fechaHitoDos = sdf.format(cal.getTime());

					toLog = "Se presionó Arribo cliente a las " + fechaHitoDos + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					//fin del hito # 1
					String hitoUnoFin = ((TextView) layoutTemporal.findViewById(1182)).getText().toString();
					hitoUnoFin += "-" + fechaHitoDos;
					((TextView) layoutTemporal.findViewById(1182)).setText(hitoUnoFin);

					//Inicio del hito # 2
					String hitoDosInicio = "ACT002-" + fechaHitoDos;
					((TextView) layoutTemporal.findViewById(1183)).setText(hitoDosInicio);

					//optimizar con un contador
					for (int i = 0; i < campos.size(); i++) {
						int key = campos.keyAt(i);
						if ((key == 1177) || (key == 1033) || (key == 1176)) {

							//obtengo el objeto por la llave y se pone invisible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(true);
							objetoLayaout.setVisibility(LinearLayout.VISIBLE);
							// sebas ((EditText)layoutView.findViewById(1071)).setText("");

						} else if (key == 1180) {
							//obtengo el objeto por la llave y se pone visible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(false);
							objetoLayaout.setVisibility(LinearLayout.GONE);
						}//fin if/else
					}//fin for
					break;

				case 1178:
					//Segundo Verificar
					String fechaSegundoVerificar = sdf.format(cal.getTime());
					toLog = "Se presionó Segundo Verificar a las " + fechaSegundoVerificar + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					// Valida que sea diferente de null o de vacío
					if (valorQR != null && !valorQR.equals("") && serialEditText != null && !serialEditText.equals("")) {
						Toast.makeText(contexto, "Llene un solo campo, por favor", Toast.LENGTH_LONG).show();
					} else if (valorQR != null && !valorQR.equals("")) {
						//Separo los datos escaneados por el lectorQR
						String[] valorMontacarga = valorQR.split("\r\n");

						// Se obtiene el serial, que siempre estará en la primera posición del codigo QR
						String serialLector = valorMontacarga[0].toUpperCase();

						//Creo un string que contendrá todos los datos leídos por el lectorQR para pintarlos
						//En la casilla de información
						String datosQR = "";

						//Creo un string con todos los datos leídos del lector, independientemente del tamaño
						//para poder pintar los datos en el mensaje.
						for (int i = 0; i < valorMontacarga.length; i++) {
							datosQR += valorMontacarga[i].toString().toUpperCase() + "\n";
						}

						try {
							//Convierto los datos adicionales de una orden de servicio a un objeto tipo JSON
							JSONObject jsonObject = new JSONObject(datosAdicionales);

							//Obtengo las variables que necesito para las condiciones y requerimientos
							String serialDatosOrden = jsonObject.getString("codigoEquipo").toUpperCase();

							//Valido si el serial captado por el lectorQR es igual al serial de los datos en la orden de servicio.
							if (!serialLector.equals(serialDatosOrden)) {
								((TextView) layoutView.findViewById(1041)).setText("Datos escaneados: " + datosQR);
							} else if (serialLector.equals(serialDatosOrden)) {
								((TextView) layoutView.findViewById(1041)).setText("");
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else if (serialEditText != null && !serialEditText.equals("")) {

						//string que contendrá la información de aviso a mostrar
						String datos = "Datos escaneados: ";

						datos += serialEditText + "\n";

						try {
							//Convierto los datos adicionales de una orden de servicio a un objeto tipo JSON
							JSONObject jsonObject = new JSONObject(datosAdicionales);

							//Obtengo las variables que necesito para las condiciones y requerimientos
							String serialDatosOrden = jsonObject.getString("codigoEquipo").toUpperCase();

							//Valido si el serial captado por el lectorQR es igual al serial de los datos en la orden de servicio.
							if (!serialEditText.equals(serialDatosOrden)) {
								((TextView) layoutView.findViewById(1041)).setText(datos);
							} else if (serialEditText.equals(serialDatosOrden)) {
								((TextView) layoutView.findViewById(1041)).setText("");
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					//Cada que se pulse el botón, se setean los campos en vacío
					((EditText) layoutView.findViewById(1071)).setText("");
					((EditText) layoutView.findViewById(1033)).setText("");
					break;

				//case del botón iniciar retorno
				case 1191:

					String fechaHitoSeis = sdf.format(cal.getTime());

					toLog = "Se presionó iniciar retorno a las " + fechaHitoSeis + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					//fin del hito # 5
					String hitoCincoFin = ((TextView) layoutTemporal.findViewById(1186)).getText().toString();
					hitoCincoFin += "-" + fechaHitoSeis;
					((TextView) layoutTemporal.findViewById(1186)).setText(hitoCincoFin);

					//Inicio del hito # 6
					String hitoSeisInicio = "ACT006-" + fechaHitoSeis;
					((TextView) layoutTemporal.findViewById(1187)).setText(hitoSeisInicio);

					for (int i = 0; i < campos.size(); i++) {
						int key = campos.keyAt(i);
						if ((key == 1192)) {

							//obtengo el objeto por la llave y se pone invisible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(true);
							campo.setEditable(true);
							objetoLayaout.setVisibility(LinearLayout.VISIBLE);
							((Button) layoutTemporal.findViewById(key)).setEnabled(true);
							((Button) layoutTemporal.findViewById(key)).setClickable(true);
						} else {
							//obtengo el objeto por la llave y se pone visible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(false);
							objetoLayaout.setVisibility(LinearLayout.GONE);
						}//fin if/else
					}//fin for

					break;

				//case del botón fin retorno
			case 1192:
					estado = "FINALIZADA";
					((TextView) layoutView.findViewById(1194)).setText(estado);

					String fechaHitoSeisFin = sdf.format(cal.getTime());

					toLog = "Se presionó fin Retorno a las " + fechaHitoSeisFin + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					//fin del hito # 6
					String hitoSeisFin = ((TextView) layoutTemporal.findViewById(1187)).getText().toString();
					hitoSeisFin += "-" + fechaHitoSeisFin;
					((TextView) layoutTemporal.findViewById(1187)).setText(hitoSeisFin);

					for (int i = 0; i < campos.size(); i++) {
						int key = campos.keyAt(i);
						if ((key == 1182) || (key == 1183) || (key == 1184) || (key == 1185) || (key == 1186)
								|| (key == 1187) || (key == 1194)) {

							//obtengo el objeto por la llave y se pone invisible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(true);
							objetoLayaout.setVisibility(LinearLayout.VISIBLE);
						} else {
							//obtengo el objeto por la llave y se pone visible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(false);
							objetoLayaout.setVisibility(LinearLayout.GONE);
						}//fin if/else
					}//fin for

				break;


				//case del botón firmar orden de servicio
				case 1190:

					//obtengo los datos de los campos para saber sin están vacíos o no

					String nombreFirma = (String) ((EditText) layoutView.findViewById(1188)).getText().toString().toUpperCase();
					String documentoFirma = (String) ((EditText) layoutView.findViewById(1189)).getText().toString().toUpperCase();

					// Valida que sea diferente de null o de vacío
					if (nombreFirma != null && !nombreFirma.equals("") && documentoFirma != null && !documentoFirma.equals("")) {

						String fechaHitoCinco = sdf.format(cal.getTime());

						toLog = "Se presionó firmar a las " + fechaHitoCinco + " con el idOrden " + idOrdenServicioLog;
						toolsApp.generaLog(toLog);

						//fin del hito # 4
						String hitoCuatroFin = ((TextView) layoutTemporal.findViewById(1185)).getText().toString();
						hitoCuatroFin += "-" + fechaHitoCinco;
						((TextView) layoutTemporal.findViewById(1185)).setText(hitoCuatroFin);

						//Inicio del hito # 5
						String hitoCincoInicio = "ACT005-" + fechaHitoCinco;
						((TextView) layoutTemporal.findViewById(1186)).setText(hitoCincoInicio);


						for (int i = 0; i < campos.size(); i++) {
							int key = campos.keyAt(i);
							if ((key == 1191)) {

								//obtengo el objeto por la llave y se pone invisible
								Campo campo = campos.get(key);
								LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
								campo.setVisible(true);
								campo.setEditable(true);
								objetoLayaout.setVisibility(LinearLayout.VISIBLE);
								((Button) layoutTemporal.findViewById(key)).setEnabled(true);
								((Button) layoutTemporal.findViewById(key)).setClickable(true);
							} else {
								//obtengo el objeto por la llave y se pone visible
								Campo campo = campos.get(key);
								LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
								campo.setVisible(false);
								objetoLayaout.setVisibility(LinearLayout.GONE);
							}//fin if/else
						}//fin for

					} else if (nombreFirma != null && !nombreFirma.equals("")) {
						Toast.makeText(contexto, "Escriba el documento, por favor", Toast.LENGTH_LONG).show();
					} else if (documentoFirma != null && !documentoFirma.equals("")) {
						Toast.makeText(contexto, "Escriba el nombre, por favor", Toast.LENGTH_LONG).show();
					} else {
						Toast.makeText(contexto, "Digite los datos, por favor", Toast.LENGTH_LONG).show();
					}
				break;


				//case del botón finalizar Mantenimiento
				case 1181:

					String fechaBotonFirmar = sdf.format(cal.getTime());
					boolean flag = controladorFormularios.interfazAFormulario(
							contexto, aplicacion.getConfiguracionApp(), true, this,
							layoutView, contenedoresCamposHash);

					toLog = "Se presionó en finalizar mantenimiento a las " + fechaBotonFirmar
							+ " con el idOrden " + idOrdenServicioLog + " y tenía la bandera en " + flag;
					toolsApp.generaLog(toLog);

					if(flag) {
						//Se pregunta si de verdad quiere finalizar el movimiento
						toolsApp.alertDialogYESNO(contexto, "¿Desea finalizar el movimiento?"
								, "Sí", "No", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										// Deshabilita el botón
										((AlertDialog) dialog).getButton(which).setEnabled(false);
										// Dependiendo del botón presionado, se ejecuta la acción
										switch (which) {
											case DialogInterface.BUTTON_POSITIVE:

												//Obtengo todos los campos del formulario
												SparseArray<Campo> campos = formulario.getCamposHash();

												//optimizar con un contador
												for (int i = 0; i < campos.size(); i++) {
													int key = campos.keyAt(i);
													String modoEdicion = campos.get(key).getModoEdicion();

													//Muestro los campos de firmar la orden de servicio
													if ((key == 1188) || (key == 1189) || (key == 1190) || (key == 1457) || (key == 1458)) {

														//obtengo el objeto por la llave y se pone invisible
														Campo campo = campos.get(key);
														LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
														campo.setVisible(true);
														objetoLayaout.setVisibility(LinearLayout.VISIBLE);

														//Desctivo los campos dependiendo de su tipo
													} else if (modoEdicion.equals("EditText")) {
														//obtengo el objeto por la llave y se pone visible
														((EditText) layoutTemporal.findViewById(key)).setEnabled(false);
														Campo campo = campos.get(key);
														campo.setEditable(false);
													} else if (modoEdicion.equals("MultiSpinner")) {
														//obtengo el objeto por la llave y se pone visible
														((Spinner) layoutTemporal.findViewById(key)).setEnabled(false);
														((Spinner) layoutTemporal.findViewById(key)).setClickable(false);
														Campo campo = campos.get(key);
														campo.setEditable(false);
													} else if (modoEdicion.equals("Button") ) {
														//obtengo el objeto por la llave y se pone visible
														((Button) layoutTemporal.findViewById(key)).setEnabled(false);
														((Button) layoutTemporal.findViewById(key)).setClickable(false);
													} else if (modoEdicion.equals("Detalle")) {
														//obtengo el objeto por la llave y se pone visible
														((Button) layoutTemporal.findViewById(key)).setEnabled(false);
														((Button) layoutTemporal.findViewById(key)).setClickable(false);
														Campo campo = campos.get(key);
														campo.setEditable(false);
													} else if (modoEdicion.equals("ScanCode")) {
														//obtengo el objeto por la llave y se pone visible
														((EditText) layoutTemporal.findViewById(key)).setEnabled(false);
														Campo campo = campos.get(key);
														campo.setEditable(false);
													} else if (modoEdicion.equals("Spinner")) {
														//obtengo el objeto por la llave y se pone visible
														((Spinner) layoutTemporal.findViewById(key)).setEnabled(false);
														((Spinner) layoutTemporal.findViewById(key)).setClickable(false);
														Campo campo = campos.get(key);
														campo.setEditable(false);
													}

												}//fin for

												//Fecha actual para los hitos
												Calendar cal = Calendar.getInstance();
												SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SS");

												String fechaHitoCuatro = sdf.format(cal.getTime());

												//fin del hito # 3
												String hitoTresFin = ((TextView) layoutTemporal.findViewById(1184)).getText().toString();
												hitoTresFin += "-" + fechaHitoCuatro;
												((TextView) layoutTemporal.findViewById(1184)).setText(hitoTresFin);

												//Inicio del hito # 4
												String hitoCuatroInicio = "ACT004-" + fechaHitoCuatro;
												((TextView) layoutTemporal.findViewById(1185)).setText(hitoCuatroInicio);

												break;
										}
									}
								}
						);//Fin Dialog
					}
					break;

				case 1177:
					//Primer Verificar

					String fechaPrimerVerificar = sdf.format(cal.getTime());
					toLog = "Se presionó Primer Verificar a las " + fechaPrimerVerificar + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					// Valida que sea diferente de null o de vacío
					if( valorQR != null && !valorQR.equals("") && serialEditText != null && !serialEditText.equals("")){
						Toast.makeText(contexto, "Llene un solo campo, por favor", Toast.LENGTH_LONG).show();
					}else if(valorQR != null && !valorQR.equals("") ){
						//Separo los datos escaneados por el lectorQR
						String[] valorMontacarga = valorQR.split("\r\n");

						// Se obtiene el serial, que siempre estará en la primera posición del codigo QR
						String serialLector = valorMontacarga[0].toUpperCase();

						//Creo un string que contendrá todos los datos leídos por el lectorQR para pintarlos
						//En la casilla de información
						String datosQR = "";

						//Creo un string con todos los datos leídos del lector, independientemente del tamaño
						//para poder pintar los datos en el mensaje.
						for(int i = 0; i < valorMontacarga.length; i++){
							datosQR += valorMontacarga[i].toString().toUpperCase() +"\n";
						}

						try{
							//Convierto los datos adicionales de una orden de servicio a un objeto tipo JSON
							JSONObject jsonObject = new JSONObject(datosAdicionales);

							//Obtengo las variables que necesito para las condiciones y requerimientos
							String serialDatosOrden = jsonObject.getString("codigoEquipo").toUpperCase();
							String codigoOT =  jsonObject.getString("CódigoOT").toString();
							String codVisita =  datosEspecificos.get("Código Visita").toString();

							//Obtengo la fecha actual para iniciar el hito y hacer la resta.
							long hitoTresInicioUnix = toolsApp.getUnixDateNOW(true,0,0,0);

							String fechaHitoTres= sdf.format(cal.getTime());

							//fin del hito # 2
							String hitoDosFin = ((TextView)layoutTemporal.findViewById(1183)).getText().toString();
							hitoDosFin += "-" + fechaHitoTres;
							((TextView)layoutTemporal.findViewById(1183)).setText(hitoDosFin);

							//Inicio del hito # 3
							String hitoTresInicio = "ACT003-" + fechaHitoTres;
							((TextView)layoutTemporal.findViewById(1184)).setText(hitoTresInicio);

							//Valido si el serial captado por el lectorQR es igual al serial de los datos en la orden de servicio.
							if( !serialLector.equals(serialDatosOrden) ){
								toolsApp.alertDialogYESNO(contexto, "El equipo a intervenir no coincide con el programado en la OT#:" + codigoOT  +  " Visita#:"+  codVisita +  "\n "
												+ "Los datos escaneados son: \n" + datosQR
												+ "\n ¿Desea continuar?"
										, "Sí", "No",new DialogInterface.OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog, int which) {
												// Deshabilita el botón
												((AlertDialog) dialog).getButton(which).setEnabled(false);
												// Dependiendo del botón presionado, se ejecuta la acción
												switch (which){
													case DialogInterface.BUTTON_POSITIVE:
														SparseArray<Campo> campos = formulario.getCamposHash();

														for(int i = 0; i < campos.size(); i++) {
															int key = campos.keyAt(i);
															if( (key == 1177) || (key == 1176) || (key == 1179) || (key == 1180)
																	|| (key == 1188) || (key == 1189) || (key == 1190)
																	|| (key == 1191) || (key == 1192) || (key == 1072)
																	|| (key == 1071)  || (key == 1182) || (key == 1183)
																	|| (key == 1184) || (key == 1185) || (key == 1186)
																	|| (key == 1187) || (key == 1194) || (key == 1457) || (key == 1458) )
															{

																//obtengo el objeto por la llave y se pone invisible
																Campo campo = campos.get(key);
																LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
																campo.setVisible(false);
																objetoLayaout.setVisibility(LinearLayout.GONE);

															}else{
																//obtengo el objeto por la llave y se pone visible
																Campo campo = campos.get(key);
																LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
																campo.setVisible(true);
																objetoLayaout.setVisibility(LinearLayout.VISIBLE);
															}//fin if/else
														}//fin for

														break;
												}
											}
										}
								);//Fin Dialog
							}else if( serialLector.equals(serialDatosOrden) ) {
								//Si es igual, oculto los campos de verificación  y muestro los datos del formulario

								for(int i = 0; i < campos.size(); i++) {
									int key = campos.keyAt(i);
									if( (key == 1177) || (key == 1176) || (key == 1179) || (key == 1180) || (key == 1188)
											|| (key == 1189) || (key == 1190) || (key == 1191) || (key == 1192) || (key == 1072)
											|| (key == 1071)  || (key == 1182) || (key == 1183) || (key == 1184) || (key == 1185)
											|| (key == 1186) || (key == 1187) || (key == 1194) || (key == 1457) || (key == 1458) )
									{

										//obtengo el objeto por la llave y se pone invisible
										Campo campo = campos.get(key);
										LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
										campo.setVisible(false);
										objetoLayaout.setVisibility(LinearLayout.GONE);
										// sebas ((EditText)layoutView.findViewById(1071)).setText("");

									}else{
										//obtengo el objeto por la llave y se pone visible
										Campo campo = campos.get(key);
										LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
										campo.setVisible(true);
										objetoLayaout.setVisibility(LinearLayout.VISIBLE);
									}//fin if/else
								}//fin for
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}else if(serialEditText != null && !serialEditText.equals("")){

						//string que contendrá la información de aviso a mostrar
						String datos= "Datos escaneados: ";

						datos += serialEditText +"\n";

						try{
							//Convierto los datos adicionales de una orden de servicio a un objeto tipo JSON
							JSONObject jsonObject = new JSONObject(datosAdicionales);

							//Obtengo las variables que necesito para las condiciones y requerimientos
							String serialDatosOrden = jsonObject.getString("codigoEquipo").toUpperCase();
							String codVisita = datosEspecificos.get("Código Visita");
							String codigoOT = jsonObject.getString("codigoOT");

							String fechaHitoTres= sdf.format(cal.getTime());

							//fin del hito # 2
							String hitoDosFin = ((TextView)layoutTemporal.findViewById(1183)).getText().toString();
							hitoDosFin += "-" + fechaHitoTres;
							((TextView)layoutTemporal.findViewById(1183)).setText(hitoDosFin);

							//Inicio del hito # 3
							String hitoTresInicio = "ACT003-" + fechaHitoTres;
							((TextView)layoutTemporal.findViewById(1184)).setText(hitoTresInicio);


							//Valido si el serial captado por el lectorQR es igual al serial de los datos en la orden de servicio.
							if( !serialEditText.equals(serialDatosOrden) ){
								toolsApp.alertDialogYESNO(contexto, "El equipo a intervenir no coincide con el programado en la OT#:" + codigoOT  +  " Visita#:"+  codVisita +  "\n "
												+ "Los datos escaneados son: \n" + datos
												+ "\n ¿Desea continuar?"
										, "Sí", "No",new DialogInterface.OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog, int which) {
												// Deshabilita el botón
												((AlertDialog) dialog).getButton(which).setEnabled(false);
												// Dependiendo del botón presionado, se ejecuta la acción
												switch (which){
													case DialogInterface.BUTTON_POSITIVE:
														SparseArray<Campo> campos = formulario.getCamposHash();

														for(int i = 0; i < campos.size(); i++) {
															int key = campos.keyAt(i);
															if( (key == 1177) || (key == 1176) || (key == 1179) || (key == 1180) || (key == 1188)
																	|| (key == 1189) || (key == 1190) || (key == 1191) || (key == 1192) || (key == 1072)
																	|| (key == 1071)  || (key == 1182) || (key == 1183) || (key == 1184) || (key == 1185)
																	|| (key == 1186) || (key == 1187) || (key == 1194) || (key == 1457) || (key == 1458) )
															{

																//obtengo el objeto por la llave y se pone invisible
																Campo campo = campos.get(key);
																LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
																campo.setVisible(false);
																objetoLayaout.setVisibility(LinearLayout.GONE);
																// sebas ((EditText)layoutView.findViewById(1071)).setText("");

															}else{
																//obtengo el objeto por la llave y se pone visible
																Campo campo = campos.get(key);
																LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
																campo.setVisible(true);
																objetoLayaout.setVisibility(LinearLayout.VISIBLE);
															}//fin if/else
														}//fin for

														break;
												}
											}
										}
								);//Fin Dialog
							}else if( serialEditText.equals(serialDatosOrden) ) {
								//Si es igual, oculto los campos de verificación  y muestro los datos del formulario

								for(int i = 0; i < campos.size(); i++) {
									int key = campos.keyAt(i);
									if( (key == 1177) || (key == 1176) || (key == 1179) || (key == 1180) || (key == 1188) || (key == 1189)
											|| (key == 1190) || (key == 1191) || (key == 1192) || (key == 1072) || (key == 1071)
											|| (key == 1182) || (key == 1183) || (key == 1184) || (key == 1185) || (key == 1186)
											|| (key == 1187) || (key == 1194) || (key == 1457) || (key == 1458) )
									{

										//obtengo el objeto por la llave y se pone invisible
										Campo campo = campos.get(key);
										LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
										campo.setVisible(false);
										objetoLayaout.setVisibility(LinearLayout.GONE);
										// sebas ((EditText)layoutView.findViewById(1071)).setText("");

									}else{
										//obtengo el objeto por la llave y se pone visible
										Campo campo = campos.get(key);
										LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
										campo.setVisible(true);
										objetoLayaout.setVisibility(LinearLayout.VISIBLE);
									}//fin if/else
								}//fin for
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					//Cada que se pulse el botón, se setean los campos en vacío
					((EditText)layoutView.findViewById(1071)).setText("");
					((EditText)layoutView.findViewById(1033)).setText("");

					break;
			}
			return;
		}

		//TAREAS/VISITAS Agencia Alemana Detalle Preventivo
		if (formulario.getId() == 107) { // cambio en esta linea
			//String que cambiará el estado de las visitas al
			String estado = "";
			String toLog = "";
			final int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
			final String datosAdicionales = aplicacion.getOrdenServicioActiva().getDatosAdicionales();
			final LinkedHashMap <String, String> datosEspecificos = aplicacion.getOrdenServicioActiva().getDatosEspecCliente();

			//obtengo el valor del campo Serial Verificación
			String valorQR = (String) ((EditText)layoutView.findViewById(1338)).getText().toString();
			String serialEditText = (String) ((EditText)layoutView.findViewById(1339)).getText().toString().toUpperCase();

			//Obtengo todos los campos del formulario
			SparseArray<Campo> campos = formulario.getCamposHash();

			//Obtengo los campos hash para mostrar/ocultar al relizar la acción
			Campo campoIniciarMovilizacion = campos.get(1312);
			Campo campoArribo = campos.get(1313);

			//Obtengo los layout donde están los botones que necesito mostrar/ocultar
			LinearLayout layoutIniciarMov= (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoIniciarMovilizacion.getId()));
			LinearLayout layoutArribo = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoArribo.getId()));

			//Calendario para la fecha de los hitos
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			switch (campoEvento.getId())
			{
				//case del botón firma Digital
				case 1459:
					String fechaFirmaDigital = sdf.format(cal.getTime());
					toLog = "Se presionó firmaDigital a las " + fechaFirmaDigital + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					Intent intent = new Intent(contexto, ActividadFirma.class);
					int orden = aplicacion.getOrdenServicioActiva().getId();
					int visita = aplicacion.getOrdenServicioActiva().getVisitaActiva().getId();
					intent.putExtra("id_Orden",orden);
					intent.putExtra("id_Visita",visita);
					contexto.startActivity(intent);
					break;

				//Lista de Subíndices
				case 1461:
					//Bandera para validar si ya se ha finalizado el mantenimiento
					boolean banderaFinalizar = ((Button)layoutTemporal.findViewById(1341)).isEnabled();
						if(banderaFinalizar) {
							String seleccion = ((Spinner) actividad.findViewById(1461)).getSelectedItem().toString();
							if (!seleccion.equals("") && seleccion != null) {

								String fechaListaSubIndices = sdf.format(cal.getTime());
								toLog = "Se presionó la listaSubIndices a las " + fechaListaSubIndices
										+ " con el idOrden " + idOrdenServicioLog + " y seleccionó " + seleccion;
								toolsApp.generaLog(toLog);

								int key;
								Campo campoTemporal;
								LinearLayout objeto;

								switch (seleccion) {
									case "SISTEMA HIDRAÚLICO":

										for (int i = 0; i < campos.size(); i++) {
											key = campos.keyAt(i);
											if ((key == 1198) || (key == 1199) || (key == 1200) || (key == 1201) || (key == 1202) || (key == 1203)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else if ((key == 1318) || (key == 1319) || (key == 1320) || (key == 1321)
													|| (key == 1322) || (key == 1323) || (key == 1324) || (key == 1325) || (key == 1326) || (key == 1327)
													|| (key == 1328) || (key == 1329) || (key == 1330) || (key == 1331) || (key == 1332) || (key == 1333)
													|| (key == 1196) || (key == 1335) || (key == 1336) || (key == 1337) || (key == 1339) || (key == 1340)
													|| (key == 1341) || (key == 1461)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												//campoTemporal.setVisible(false);
												objeto.setVisibility(LinearLayout.GONE);
											}

										}

										break;

									case "SISTEMA ELÉCTRICO":

										for (int i = 0; i < campos.size(); i++) {
											key = campos.keyAt(i);

											if ((key == 1205) || (key == 1206) || (key == 1207)
													|| (key == 1208) || (key == 1209) || (key == 1210) || (key == 1211)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else if ((key == 1318) || (key == 1319) || (key == 1320) || (key == 1321)
													|| (key == 1322) || (key == 1323) || (key == 1324) || (key == 1325) || (key == 1326) || (key == 1327)
													|| (key == 1328) || (key == 1329) || (key == 1330) || (key == 1331) || (key == 1332) || (key == 1333)
													|| (key == 1196) || (key == 1335) || (key == 1336) || (key == 1337) || (key == 1339) || (key == 1340)
													|| (key == 1341) || (key == 1461)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												//campoTemporal.setVisible(false);
												objeto.setVisibility(LinearLayout.GONE);
											}

										}

										break;

									case "RUEDAS":

										for (int i = 0; i < campos.size(); i++) {
											key = campos.keyAt(i);

											if ((key == 1213) || (key == 1214) || (key == 1215)
													|| (key == 1216) || (key == 1217) || (key == 1218) || (key == 1219)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else if ((key == 1318) || (key == 1319) || (key == 1320) || (key == 1321)
													|| (key == 1322) || (key == 1323) || (key == 1324) || (key == 1325) || (key == 1326) || (key == 1327)
													|| (key == 1328) || (key == 1329) || (key == 1330) || (key == 1331) || (key == 1332) || (key == 1333)
													|| (key == 1196) || (key == 1335) || (key == 1336) || (key == 1337) || (key == 1339) || (key == 1340)
													|| (key == 1341) || (key == 1461)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												//campoTemporal.setVisible(false);
												objeto.setVisibility(LinearLayout.GONE);
											}

										}

										break;

									case "CHASIS":

										for (int i = 0; i < campos.size(); i++) {
											key = campos.keyAt(i);

											if ((key == 1221) || (key == 1222) || (key == 1223)
													|| (key == 1224) || (key == 1225) || (key == 1226)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else if ((key == 1318) || (key == 1319) || (key == 1320) || (key == 1321)
													|| (key == 1322) || (key == 1323) || (key == 1324) || (key == 1325) || (key == 1326) || (key == 1327)
													|| (key == 1328) || (key == 1329) || (key == 1330) || (key == 1331) || (key == 1332) || (key == 1333)
													|| (key == 1196) || (key == 1335) || (key == 1336) || (key == 1337) || (key == 1339) || (key == 1340)
													|| (key == 1341) || (key == 1461)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												//campoTemporal.setVisible(false);
												objeto.setVisibility(LinearLayout.GONE);
											}

										}

										break;

									case "FRENOS":

										for (int i = 0; i < campos.size(); i++) {
											key = campos.keyAt(i);

											if ((key == 1228) || (key == 1229) || (key == 1230)
													|| (key == 1231) || (key == 1232) || (key == 1233)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else if ((key == 1318) || (key == 1319) || (key == 1320) || (key == 1321)
													|| (key == 1322) || (key == 1323) || (key == 1324) || (key == 1325) || (key == 1326) || (key == 1327)
													|| (key == 1328) || (key == 1329) || (key == 1330) || (key == 1331) || (key == 1332) || (key == 1333)
													|| (key == 1196) || (key == 1335) || (key == 1336) || (key == 1337) || (key == 1339) || (key == 1340)
													|| (key == 1341) || (key == 1461)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												//campoTemporal.setVisible(false);
												objeto.setVisibility(LinearLayout.GONE);
											}

										}

										break;
									case "REVISIÓN DE OPERACIÓN":

										for (int i = 0; i < campos.size(); i++) {
											key = campos.keyAt(i);

											if ((key == 1235) || (key == 1236) || (key == 1237) || (key == 1238)
													|| (key == 1239) || (key == 1240) || (key == 1241) || (key == 1242)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else if ((key == 1318) || (key == 1319) || (key == 1320) || (key == 1321)
													|| (key == 1322) || (key == 1323) || (key == 1324) || (key == 1325) || (key == 1326) || (key == 1327)
													|| (key == 1328) || (key == 1329) || (key == 1330) || (key == 1331) || (key == 1332) || (key == 1333)
													|| (key == 1196) || (key == 1335) || (key == 1336) || (key == 1337) || (key == 1339) || (key == 1340)
													|| (key == 1341) || (key == 1461)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												//campoTemporal.setVisible(false);
												objeto.setVisibility(LinearLayout.GONE);
											}

										}

										break;


									case "TRACCIÓN":

										for (int i = 0; i < campos.size(); i++) {
											key = campos.keyAt(i);

											if ((key == 1244) || (key == 1245) || (key == 1246)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else if ((key == 1318) || (key == 1319) || (key == 1320) || (key == 1321)
													|| (key == 1322) || (key == 1323) || (key == 1324) || (key == 1325) || (key == 1326) || (key == 1327)
													|| (key == 1328) || (key == 1329) || (key == 1330) || (key == 1331) || (key == 1332) || (key == 1333)
													|| (key == 1196) || (key == 1335) || (key == 1336) || (key == 1337) || (key == 1339) || (key == 1340)
													|| (key == 1341) || (key == 1461)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												//campoTemporal.setVisible(false);
												objeto.setVisibility(LinearLayout.GONE);
											}

										}

										break;


									case "MÁSTIL":

										for (int i = 0; i < campos.size(); i++) {
											key = campos.keyAt(i);

											if ((key == 1248) || (key == 1249) || (key == 1250) || (key == 1251)
													|| (key == 1252) || (key == 1253) || (key == 1254) || (key == 1255)
													|| (key == 1256) || (key == 1257)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else if ((key == 1318) || (key == 1319) || (key == 1320) || (key == 1321)
													|| (key == 1322) || (key == 1323) || (key == 1324) || (key == 1325) || (key == 1326) || (key == 1327)
													|| (key == 1328) || (key == 1329) || (key == 1330) || (key == 1331) || (key == 1332) || (key == 1333)
													|| (key == 1196) || (key == 1335) || (key == 1336) || (key == 1337) || (key == 1339) || (key == 1340)
													|| (key == 1341) || (key == 1461)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												//campoTemporal.setVisible(false);
												objeto.setVisibility(LinearLayout.GONE);
											}

										}

										break;


									case "DIRECCIÓN":

										for (int i = 0; i < campos.size(); i++) {
											key = campos.keyAt(i);

											if ((key == 1259) || (key == 1260) || (key == 1261)
													|| (key == 1262) || (key == 1263) || (key == 1264) || (key == 1265)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else if ((key == 1318) || (key == 1319) || (key == 1320) || (key == 1321)
													|| (key == 1322) || (key == 1323) || (key == 1324) || (key == 1325) || (key == 1326) || (key == 1327)
													|| (key == 1328) || (key == 1329) || (key == 1330) || (key == 1331) || (key == 1332) || (key == 1333)
													|| (key == 1196) || (key == 1335) || (key == 1336) || (key == 1337) || (key == 1339) || (key == 1340)
													|| (key == 1341) || (key == 1461)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												//campoTemporal.setVisible(false);
												objeto.setVisibility(LinearLayout.GONE);
											}

										}

										break;


									case "MARCHA":

										for (int i = 0; i < campos.size(); i++) {
											key = campos.keyAt(i);

											if ((key == 1267) || (key == 1268) || (key == 1269) || (key == 1270)
													|| (key == 1271) || (key == 1272) || (key == 1273) || (key == 1274)
													|| (key == 1275) || (key == 1276) || (key == 1277) || (key == 1278)
													|| (key == 1279) || (key == 1280) || (key == 1281)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else if ((key == 1318) || (key == 1319) || (key == 1320) || (key == 1321)
													|| (key == 1322) || (key == 1323) || (key == 1324) || (key == 1325) || (key == 1326) || (key == 1327)
													|| (key == 1328) || (key == 1329) || (key == 1330) || (key == 1331) || (key == 1332) || (key == 1333)
													|| (key == 1196) || (key == 1335) || (key == 1336) || (key == 1337) || (key == 1339) || (key == 1340)
													|| (key == 1341) || (key == 1461)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												//campoTemporal.setVisible(false);
												objeto.setVisibility(LinearLayout.GONE);
											}

										}

										break;


									case "BATERÍA":

										for (int i = 0; i < campos.size(); i++) {
											key = campos.keyAt(i);

											if ((key == 1283) || (key == 1284) || (key == 1285)
													|| (key == 1286) || (key == 1287) || (key == 1288)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else if ((key == 1318) || (key == 1319) || (key == 1320) || (key == 1321)
													|| (key == 1322) || (key == 1323) || (key == 1324) || (key == 1325) || (key == 1326) || (key == 1327)
													|| (key == 1328) || (key == 1329) || (key == 1330) || (key == 1331) || (key == 1332) || (key == 1333)
													|| (key == 1196) || (key == 1335) || (key == 1336) || (key == 1337) || (key == 1339) || (key == 1340)
													|| (key == 1341) || (key == 1461)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												//campoTemporal.setVisible(false);
												objeto.setVisibility(LinearLayout.GONE);
											}

										}

										break;


									case "CARGADOR":

										for (int i = 0; i < campos.size(); i++) {
											key = campos.keyAt(i);

											if ((key == 1289) || (key == 1290) || (key == 1291)
													|| (key == 1292) || (key == 1293) || (key == 1294)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else if ((key == 1318) || (key == 1319) || (key == 1320) || (key == 1321)
													|| (key == 1322) || (key == 1323) || (key == 1324) || (key == 1325) || (key == 1326) || (key == 1327)
													|| (key == 1328) || (key == 1329) || (key == 1330) || (key == 1331) || (key == 1332) || (key == 1333)
													|| (key == 1196) || (key == 1335) || (key == 1336) || (key == 1337) || (key == 1339) || (key == 1340)
													|| (key == 1341) || (key == 1461)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												//campoTemporal.setVisible(false);
												objeto.setVisibility(LinearLayout.GONE);
											}

										}

										break;


									case "EQUIPOS ADICIONALES":

										for (int i = 0; i < campos.size(); i++) {
											key = campos.keyAt(i);

											if ((key == 1296) || (key == 1297) || (key == 1298) || (key == 1299)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else if ((key == 1318) || (key == 1319) || (key == 1320) || (key == 1321)
													|| (key == 1322) || (key == 1323) || (key == 1324) || (key == 1325) || (key == 1326) || (key == 1327)
													|| (key == 1328) || (key == 1329) || (key == 1330) || (key == 1331) || (key == 1332) || (key == 1333)
													|| (key == 1196) || (key == 1335) || (key == 1336) || (key == 1337) || (key == 1339) || (key == 1340)
													|| (key == 1341) || (key == 1461)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												//campoTemporal.setVisible(false);
												objeto.setVisibility(LinearLayout.GONE);
											}

										}

										break;


									case "CAMBIO DE ACEITE":

										for (int i = 0; i < campos.size(); i++) {
											key = campos.keyAt(i);

											if ((key == 1301) || (key == 1302) || (key == 1303)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else if ((key == 1318) || (key == 1319) || (key == 1320) || (key == 1321)
													|| (key == 1322) || (key == 1323) || (key == 1324) || (key == 1325) || (key == 1326) || (key == 1327)
													|| (key == 1328) || (key == 1329) || (key == 1330) || (key == 1331) || (key == 1332) || (key == 1333)
													|| (key == 1196) || (key == 1335) || (key == 1336) || (key == 1337) || (key == 1339) || (key == 1340)
													|| (key == 1341) || (key == 1461)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												//campoTemporal.setVisible(false);
												objeto.setVisibility(LinearLayout.GONE);
											}

										}

										break;


									case "CAMBIOS DE FILTROS":

										for (int i = 0; i < campos.size(); i++) {
											key = campos.keyAt(i);

											if ((key == 1305) || (key == 1306) || (key == 1307) || (key == 1308)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else if ((key == 1318) || (key == 1319) || (key == 1320) || (key == 1321)
													|| (key == 1322) || (key == 1323) || (key == 1324) || (key == 1325) || (key == 1326) || (key == 1327)
													|| (key == 1328) || (key == 1329) || (key == 1330) || (key == 1331) || (key == 1332) || (key == 1333)
													|| (key == 1196) || (key == 1335) || (key == 1336) || (key == 1337) || (key == 1339) || (key == 1340)
													|| (key == 1341) || (key == 1461)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												//campoTemporal.setVisible(false);
												objeto.setVisibility(LinearLayout.GONE);
											}

										}

										break;


									case "DEMOSTRACIÓN":

										for (int i = 0; i < campos.size(); i++) {
											key = campos.keyAt(i);

											if ((key == 1310) || (key == 1311)) {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else if ((key == 1318) || (key == 1319) || (key == 1320) || (key == 1321)
													|| (key == 1322) || (key == 1323) || (key == 1324) || (key == 1325) || (key == 1326) || (key == 1327)
													|| (key == 1328) || (key == 1329) || (key == 1330) || (key == 1331) || (key == 1332) || (key == 1333)
													|| (key == 1196) || (key == 1335) || (key == 1336) || (key == 1337) || (key == 1339) || (key == 1340)
													|| (key == 1341) || (key == 1461)) {

												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												campoTemporal.setVisible(true);
												objeto.setVisibility(LinearLayout.VISIBLE);
											} else {
												campoTemporal = campos.get(key);
												objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
												//campoTemporal.setVisible(false);
												objeto.setVisibility(LinearLayout.GONE);
											}

										}

										break;
								}


							}
						}

				break;

				//Case del botón de inicio de movilización
				case 1312:
					String fechaHitoUno = sdf.format(cal.getTime());

					//Inicio del hito # 1
					String hitoUnoInicio = "ACT001-" + fechaHitoUno;
					((TextView)layoutTemporal.findViewById(1345)).setText(hitoUnoInicio);

					//Muestro/escondo los campos y sus respectivos layouts
					campoArribo.setVisible(true);
					campoIniciarMovilizacion.setVisible(false);
					layoutArribo.setVisibility(LinearLayout.VISIBLE);
					layoutIniciarMov.setVisibility(LinearLayout.GONE);

					toLog = "Se presionó InicioMovilización a las " + fechaHitoUno + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

				break;

				//Case del botón de Arribo cliente
				case 1313:

					//obtengo la fecha con el formato del calendario
					String fechaHitoDos = sdf.format(cal.getTime());

					toLog = "Se presionó Arribo cliente a las " + fechaHitoDos + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					//fin del hito # 1
					String hitoUnoFin = ((TextView)layoutTemporal.findViewById(1345)).getText().toString();
					hitoUnoFin += "-" + fechaHitoDos;
					((TextView)layoutTemporal.findViewById(1345)).setText(hitoUnoFin);

					//Inicio del hito # 2
					String hitoDosInicio = "ACT002-" + fechaHitoDos;
					((TextView)layoutTemporal.findViewById(1346)).setText(hitoDosInicio);

					//optimizar con un contador
					for(int i = 0; i < campos.size(); i++) {
						int key = campos.keyAt(i);
						if( (key == 1314)  || (key == 1339) ||  (key == 1317) ){

							//obtengo el objeto por la llave y se pone invisible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(true);
							objetoLayaout.setVisibility(LinearLayout.VISIBLE);


						}else{
							//obtengo el objeto por la llave y se pone visible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(false);
							objetoLayaout.setVisibility(LinearLayout.GONE);
						}//fin if/else
					}//fin for
					break;

				case 1340:
					//Segundo Validar
					String fechaSegundoValidar= sdf.format(cal.getTime());
					toLog = "Se presionó Segundo Validar a las " + fechaSegundoValidar + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					// Valida que sea diferente de null o de vacío
					if( valorQR != null && !valorQR.equals("") && serialEditText != null && !serialEditText.equals("")){
						Toast.makeText(contexto, "Llene un solo campo, por favor", Toast.LENGTH_LONG).show();
					}else if( valorQR != null && !valorQR.equals("") ){
						//Separo los datos escaneados por el lectorQR
						String[] valorMontacarga = valorQR.split("\r\n");

						// Se obtiene el serial, que siempre estará en la primera posición del codigo QR
						String serialLector = valorMontacarga[0].toUpperCase();

						//Creo un string que contendrá todos los datos leídos por el lectorQR para pintarlos
						//En la casilla de información
						String datosQR = "";

						//Creo un string con todos los datos leídos del lector, independientemente del tamaño
						//para poder pintar los datos en el mensaje.
						for(int i = 0; i < valorMontacarga.length; i++){
							datosQR += valorMontacarga[i].toString().toUpperCase() +"\n";
						}

						try{
							//Convierto los datos adicionales de una orden de servicio a un objeto tipo JSON
							JSONObject jsonObject = new JSONObject(datosAdicionales);

							//Obtengo las variables que necesito para las condiciones y requerimientos
							String serialDatosOrden = jsonObject.getString("codigoEquipo").toUpperCase();

							//Valido si el serial captado por el lectorQR es igual al serial de los datos en la orden de servicio.
							if( !serialLector.equals(serialDatosOrden) ){
								((TextView)layoutView.findViewById(1337)).setText("Datos escaneados: "+ datosQR);
							}else if( serialLector.equals(serialDatosOrden) ) {
								((TextView)layoutView.findViewById(1337)).setText("");
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}else if(serialEditText != null && !serialEditText.equals("")){

						//string que contendrá la información de aviso a mostrar
						String datos= "Datos escaneados: ";

						datos += serialEditText +"\n";

						try{
							//Convierto los datos adicionales de una orden de servicio a un objeto tipo JSON
							JSONObject jsonObject = new JSONObject(datosAdicionales);

							//Obtengo las variables que necesito para las condiciones y requerimientos
							String serialDatosOrden = jsonObject.getString("codigoEquipo").toUpperCase();

							//Valido si el serial captado por el lectorQR es igual al serial de los datos en la orden de servicio.
							if( !serialEditText.equals(serialDatosOrden) ){
								((TextView)layoutView.findViewById(1337)).setText( datos );
							}else if( serialEditText.equals(serialDatosOrden) ) {
								((TextView)layoutView.findViewById(1337)).setText("");
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					//Cada que se pulse el botón, se setean los campos en vacío
					((EditText)layoutView.findViewById(1338)).setText("");
					((EditText)layoutView.findViewById(1339)).setText("");
					break;

				//case del botón iniciar retorno
				case 1351:

					String fechaHitoSeis= sdf.format(cal.getTime());

					toLog = "Se presionó iniciar retorno a las " + fechaHitoSeis + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					//fin del hito # 5
					String hitoCincoFin = ((TextView)layoutTemporal.findViewById(1349)).getText().toString();
					hitoCincoFin += "-" + fechaHitoSeis;
					((TextView)layoutTemporal.findViewById(1349)).setText(hitoCincoFin);

					//Inicio del hito # 6
					String hitoSeisInicio = "ACT006-" + fechaHitoSeis;
					((TextView)layoutTemporal.findViewById(1350)).setText(hitoSeisInicio);

					for(int i = 0; i < campos.size(); i++) {
						int key = campos.keyAt(i);
						if( (key == 1352) ){

							//obtengo el objeto por la llave y se pone invisible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(true);
							campo.setEditable(true);
							objetoLayaout.setVisibility(LinearLayout.VISIBLE);
							((Button)layoutTemporal.findViewById(key)).setEnabled(true);
							((Button)layoutTemporal.findViewById(key)).setClickable(true);
						}else {
							//obtengo el objeto por la llave y se pone visible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(false);
							objetoLayaout.setVisibility(LinearLayout.GONE);
						}//fin if/else
					}//fin for

					break;

				//case del botón fin retorno
				case 1352:
					estado = "FINALIZADA";
					((TextView)layoutView.findViewById(1354)).setText(estado);

					String fechaHitoSeisFin= sdf.format(cal.getTime());

					toLog = "Se presionó fin retorno a las " + fechaHitoSeisFin + " con el idOrden " + idOrdenServicioLog + " y me puso el estado en " + estado;
					toolsApp.generaLog(toLog);

					//fin del hito # 6
					String hitoSeisFin = ((TextView)layoutTemporal.findViewById(1350)).getText().toString();
					hitoSeisFin += "-" + fechaHitoSeisFin;
					((TextView)layoutTemporal.findViewById(1350)).setText(hitoSeisFin);

					for(int i = 0; i < campos.size(); i++) {
						int key = campos.keyAt(i);
						if( (key == 1345) || (key ==1346) || (key == 1347) || (key ==1348) || (key == 1349)
								|| (key ==1350) || (key ==1354) ){

							//obtengo el objeto por la llave y se pone invisible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(true);
							objetoLayaout.setVisibility(LinearLayout.VISIBLE);
						}else {
							//obtengo el objeto por la llave y se pone visible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(false);
							objetoLayaout.setVisibility(LinearLayout.GONE);
						}//fin if/else
					}//fin for

				break;


				//case del botón firmar orden de servicio
				case 1344:

					//obtengo los datos de los campos para saber sin están vacíos o no
					String nombreFirma = (String) ((EditText)layoutView.findViewById(1342)).getText().toString().toUpperCase();
					String documentoFirma = (String) ((EditText)layoutView.findViewById(1343)).getText().toString().toUpperCase();

					// Valida que sea diferente de null o de vacío
					if( nombreFirma != null && !nombreFirma.equals("") && documentoFirma != null && !documentoFirma.equals("")){

						    String fechaHitoCinco= sdf.format(cal.getTime());

							toLog = "Se presionó firmar a las " + fechaHitoCinco + " con el idOrden " + idOrdenServicioLog;
							toolsApp.generaLog(toLog);


							//fin del hito # 4
							String hitoCuatroFin = ((TextView)layoutTemporal.findViewById(1348)).getText().toString();
							hitoCuatroFin += "-" + fechaHitoCinco;
							((TextView)layoutTemporal.findViewById(1348)).setText(hitoCuatroFin);

							//Inicio del hito # 5
							String hitoCincoInicio = "ACT005-" + fechaHitoCinco;
							((TextView)layoutTemporal.findViewById(1349)).setText(hitoCincoInicio);

							for(int i = 0; i < campos.size(); i++) {
								   int key = campos.keyAt(i);
								   if( (key == 1351) ){

									   //obtengo el objeto por la llave y se pone invisible
										Campo campo = campos.get(key);
										LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
										campo.setVisible(true);
									   	campo.setEditable(true);
										objetoLayaout.setVisibility(LinearLayout.VISIBLE);
										((Button)layoutTemporal.findViewById(key)).setEnabled(true);
										((Button)layoutTemporal.findViewById(key)).setClickable(true);
								   }else {
									   //obtengo el objeto por la llave y se pone visible
										Campo campo = campos.get(key);
										LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
										campo.setVisible(false);
										objetoLayaout.setVisibility(LinearLayout.GONE);
								   }//fin if/else
								}//fin for

					}else if( nombreFirma != null && !nombreFirma.equals("") ){
						Toast.makeText(contexto, "Escriba el documento, por favor", Toast.LENGTH_LONG).show();
					}else if( documentoFirma != null && !documentoFirma.equals("") ){
						Toast.makeText(contexto, "Escriba el nombre, por favor", Toast.LENGTH_LONG).show();
					}else{
						Toast.makeText(contexto, "Digite los datos, por favor", Toast.LENGTH_LONG).show();
					}
					break;


				//case del botón finalizar Mantenimiento
				case 1341:

					boolean flag = controladorFormularios.interfazAFormulario(
							contexto, aplicacion.getConfiguracionApp(), true, this,
							layoutView, contenedoresCamposHash);
					String fechaBotonFirmar = sdf.format(cal.getTime());
					toLog = "Se presionó en finalizar mantenimiento a las " + fechaBotonFirmar
							+ " con el idOrden " + idOrdenServicioLog + " y tenía la bandera en " + flag;
					toolsApp.generaLog(toLog);

						if(flag) {
							//Se pregunta si de verdad quiere finalizar el movimiento
							toolsApp.alertDialogYESNO(contexto, "¿Desea finalizar el movimiento?"
									, "Sí", "No", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											// Deshabilita el botón
											((AlertDialog) dialog).getButton(which).setEnabled(false);
											// Dependiendo del botón presionado, se ejecuta la acción
											switch (which) {
												case DialogInterface.BUTTON_POSITIVE:

													//Obtengo todos los campos del formulario
													SparseArray<Campo> campos = formulario.getCamposHash();

													//optimizar con un contador
													for (int i = 0; i < campos.size(); i++) {
														int key = campos.keyAt(i);
														String modoEdicion = campos.get(key).getModoEdicion();

														//Muestro los campos de firmar la orden de servicio
														if ((key == 1342) || (key == 1343) || (key == 1344) || (key == 1459) || (key == 1460)) {

															//obtengo el objeto por la llave y se pone invisible
															Campo campo = campos.get(key);
															LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
															campo.setVisible(true);
															objetoLayaout.setVisibility(LinearLayout.VISIBLE);

															//Desctivo los campos dependiendo de su tipo
														} else if (modoEdicion.equals("EditText")) {
															//obtengo el objeto por la llave y se pone visible
															((EditText) layoutTemporal.findViewById(key)).setEnabled(false);
															Campo campo = campos.get(key);
															campo.setEditable(false);
														} else if (modoEdicion.equals("MultiSpinner")) {
															//obtengo el objeto por la llave y se pone visible
															((Spinner) layoutTemporal.findViewById(key)).setEnabled(false);
															((Spinner) layoutTemporal.findViewById(key)).setClickable(false);
															Campo campo = campos.get(key);
															campo.setEditable(false);
														} else if (modoEdicion.equals("Button")) {
															//obtengo el objeto por la llave y se pone visible
															((Button) layoutTemporal.findViewById(key)).setEnabled(false);
															((Button) layoutTemporal.findViewById(key)).setClickable(false);
															Campo campo = campos.get(key);
															campo.setEditable(false);
														} else if (modoEdicion.equals("Detalle")) {
															//obtengo el objeto por la llave y se pone visible
															((Button) layoutTemporal.findViewById(key)).setEnabled(false);
															((Button) layoutTemporal.findViewById(key)).setClickable(false);
															Campo campo = campos.get(key);
															campo.setEditable(false);
														} else if (modoEdicion.equals("ScanCode")) {
															//obtengo el objeto por la llave y se pone visible
															((EditText) layoutTemporal.findViewById(key)).setEnabled(false);
															Campo campo = campos.get(key);
															campo.setEditable(false);
														} else if (modoEdicion.equals("Spinner")) {
															//obtengo el objeto por la llave y se pone visible
															((Spinner) layoutTemporal.findViewById(key)).setEnabled(false);
															((Spinner) layoutTemporal.findViewById(key)).setClickable(false);
															Campo campo = campos.get(key);
															campo.setEditable(false);
														}

													}//fin for

													//Fecha actual para los hitos
													Calendar cal = Calendar.getInstance();
													SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SS");

													String fechaHitoCuatro = sdf.format(cal.getTime());

													//fin del hito # 3
													String hitoTresFin = ((TextView) layoutTemporal.findViewById(1347)).getText().toString();
													hitoTresFin += "-" + fechaHitoCuatro;
													((TextView) layoutTemporal.findViewById(1347)).setText(hitoTresFin);

													//Inicio del hito # 4
													String hitoCuatroInicio = "ACT004-" + fechaHitoCuatro;
													((TextView) layoutTemporal.findViewById(1348)).setText(hitoCuatroInicio);

													break;
											}
										}
									}
							);//Fin Dialog
						}
					break;

				case 1317:
					//Primer Validar
					String fechaPrimerValidar= sdf.format(cal.getTime());
					toLog = "Se presionó Primer Validar a las " + fechaPrimerValidar + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					// Valida que sea diferente de null o de vacío
					if( valorQR != null && !valorQR.equals("") && serialEditText != null && !serialEditText.equals("")){
						Toast.makeText(contexto, "Llene un solo campo, por favor", Toast.LENGTH_LONG).show();
					}else if(valorQR != null && !valorQR.equals("") ){
						//Separo los datos escaneados por el lectorQR
						String[] valorMontacarga = valorQR.split("\r\n");

						// Se obtiene el serial, que siempre estará en la primera posición del codigo QR
						String serialLector = valorMontacarga[0].toUpperCase();

						//Creo un string que contendrá todos los datos leídos por el lectorQR para pintarlos
						//En la casilla de información
						String datosQR = "";

						//Creo un string con todos los datos leídos del lector, independientemente del tamaño
						//para poder pintar los datos en el mensaje.
						for(int i = 0; i < valorMontacarga.length; i++){
							datosQR += valorMontacarga[i].toString().toUpperCase() +"\n";
						}

						try{
							//Convierto los datos adicionales de una orden de servicio a un objeto tipo JSON
							JSONObject jsonObject = new JSONObject(datosAdicionales);

							//Obtengo las variables que necesito para las condiciones y requerimientos
							String serialDatosOrden = jsonObject.getString("codigoEquipo").toUpperCase();
							String codigoOT =  jsonObject.getString("CódigoOT").toString();
							String codVisita =  datosEspecificos.get("Código Visita").toString();

							String fechaHitoTres= sdf.format(cal.getTime());

							//fin del hito # 2
							String hitoDosFin = ((TextView)layoutTemporal.findViewById(1346)).getText().toString();
							hitoDosFin += "-" + fechaHitoTres;
							((TextView)layoutTemporal.findViewById(1346)).setText(hitoDosFin);

							//Inicio del hito # 3
							String hitoTresInicio = "ACT003-" + fechaHitoTres;
							((TextView)layoutTemporal.findViewById(1347)).setText(hitoTresInicio);

							//Valido si el serial captado por el lectorQR es igual al serial de los datos en la orden de servicio.
							if( !serialLector.equals(serialDatosOrden) ){
								toolsApp.alertDialogYESNO(contexto, "El equipo a intervenir no coincide con el programado en la OT#:" + codigoOT  +  " Visita#:"+  codVisita +  "\n "
												+ "Los datos escaneados son: \n" + datosQR
												+ "\n ¿Desea continuar?"
										, "Sí", "No",new DialogInterface.OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog, int which) {
												// Deshabilita el botón
												((AlertDialog) dialog).getButton(which).setEnabled(false);
												// Dependiendo del botón presionado, se ejecuta la acción
												switch (which){
													case DialogInterface.BUTTON_POSITIVE:
														SparseArray<Campo> campos = formulario.getCamposHash();

														for(int i = 0; i < campos.size(); i++) {
															int key = campos.keyAt(i);
															if( (key == 1317) || (key == 1314) || (key == 1312) || (key == 1313) || (key == 1342) || (key == 1343) || (key == 1344)
																	|| (key == 1351) || (key == 1352) || (key == 1338)  || (key == 1345) || (key == 1346) || (key == 1347) || (key == 1348)
																	|| (key == 1349) || (key == 1350) || (key == 1354) || (key == 1459) || (key == 1460) ){

																//obtengo el objeto por la llave y se pone invisible
																Campo campo = campos.get(key);
																LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
																campo.setVisible(false);
																objetoLayaout.setVisibility(LinearLayout.GONE);

															}else{
																//obtengo el objeto por la llave y se pone visible
																Campo campo = campos.get(key);
																LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
																campo.setVisible(true);
																objetoLayaout.setVisibility(LinearLayout.VISIBLE);
															}//fin if/else
														}//fin for

														break;
												}
											}
										}
								);//Fin Dialog
							}else if( serialLector.equals(serialDatosOrden) ) {
								//Si es igual, oculto los campos de verificación  y muestro los datos del formulario

								for(int i = 0; i < campos.size(); i++) {
									int key = campos.keyAt(i);
									if( (key == 1317) || (key == 1314) || (key == 1312) || (key == 1313) || (key == 1342) || (key == 1343) || (key == 1344)
											|| (key == 1351) || (key == 1352) || (key == 1338)  || (key == 1345) || (key == 1346) || (key == 1347) || (key == 1348)
											|| (key == 1349) || (key == 1350) || (key == 1354) || (key == 1459) || (key == 1460) ){

										//obtengo el objeto por la llave y se pone invisible
										Campo campo = campos.get(key);
										LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
										campo.setVisible(false);
										objetoLayaout.setVisibility(LinearLayout.GONE);


									}else{
										//obtengo el objeto por la llave y se pone visible
										Campo campo = campos.get(key);
										LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
										campo.setVisible(true);
										objetoLayaout.setVisibility(LinearLayout.VISIBLE);
									}//fin if/else
								}//fin for
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}else if(serialEditText != null && !serialEditText.equals("")){

						//string que contendrá la información de aviso a mostrar
						String datos= "Datos escaneados: ";

						datos += serialEditText +"\n";

						try{
							//Convierto los datos adicionales de una orden de servicio a un objeto tipo JSON
							JSONObject jsonObject = new JSONObject(datosAdicionales);

							//Obtengo las variables que necesito para las condiciones y requerimientos
							String serialDatosOrden = jsonObject.getString("codigoEquipo").toUpperCase();
							String codVisita = datosEspecificos.get("Código Visita");
							String codigoOT = jsonObject.getString("codigoOT");

							String fechaHitoTres= sdf.format(cal.getTime());

							//fin del hito # 2
							String hitoDosFin = ((TextView)layoutTemporal.findViewById(1346)).getText().toString();
							hitoDosFin += "-" + fechaHitoTres;
							((TextView)layoutTemporal.findViewById(1346)).setText(hitoDosFin);

							//Inicio del hito # 3
							String hitoTresInicio = "ACT003-" + fechaHitoTres;
							((TextView)layoutTemporal.findViewById(1347)).setText(hitoTresInicio);


							//Valido si el serial captado por el lectorQR es igual al serial de los datos en la orden de servicio.
							if( !serialEditText.equals(serialDatosOrden) ){
								toolsApp.alertDialogYESNO(contexto, "El equipo a intervenir no coincide con el programado en la OT#:" + codigoOT  +  " Visita#:"+  codVisita +  "\n "
												+ "Los datos escaneados son: \n" + datos
												+ "\n ¿Desea continuar?"
										, "Sí", "No",new DialogInterface.OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog, int which) {
												// Deshabilita el botón
												((AlertDialog) dialog).getButton(which).setEnabled(false);
												// Dependiendo del botón presionado, se ejecuta la acción
												switch (which){
													case DialogInterface.BUTTON_POSITIVE:
														SparseArray<Campo> campos = formulario.getCamposHash();

														for(int i = 0; i < campos.size(); i++) {
															int key = campos.keyAt(i);
															if( (key == 1317) || (key == 1314) || (key == 1312) || (key == 1313) || (key == 1342) || (key == 1343) || (key == 1344)
																	|| (key == 1351) || (key == 1352) || (key == 1338)  || (key == 1345) || (key == 1346) || (key == 1347) || (key == 1348)
																	|| (key == 1349) || (key == 1350) || (key == 1354) || (key == 1459) || (key == 1460) ){

																//obtengo el objeto por la llave y se pone invisible
																Campo campo = campos.get(key);
																LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
																campo.setVisible(false);
																objetoLayaout.setVisibility(LinearLayout.GONE);


															}else{
																//obtengo el objeto por la llave y se pone visible
																Campo campo = campos.get(key);
																LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
																campo.setVisible(true);
																objetoLayaout.setVisibility(LinearLayout.VISIBLE);
															}//fin if/else
														}//fin for

														break;
												}
											}
										}
								);//Fin Dialog
							}else if( serialEditText.equals(serialDatosOrden) ) {
								//Si es igual, oculto los campos de verificación  y muestro los datos del formulario

								for(int i = 0; i < campos.size(); i++) {
									int key = campos.keyAt(i);
									if( (key == 1317) || (key == 1314) || (key == 1312) || (key == 1313) || (key == 1342) || (key == 1343) || (key == 1344)
											|| (key == 1351) || (key == 1352) || (key == 1338)  || (key == 1345) || (key == 1346) || (key == 1347) || (key == 1348)
											|| (key == 1349) || (key == 1350) || (key == 1354) || (key == 1459) || (key == 1460) ){

										//obtengo el objeto por la llave y se pone invisible
										Campo campo = campos.get(key);
										LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
										campo.setVisible(false);
										objetoLayaout.setVisibility(LinearLayout.GONE);


									}else{
										//obtengo el objeto por la llave y se pone visible
										Campo campo = campos.get(key);
										LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
										campo.setVisible(true);
										objetoLayaout.setVisibility(LinearLayout.VISIBLE);
									}//fin if/else
								}//fin for
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					//Cada que se pulse el botón, se setean los campos en vacío
					((EditText)layoutView.findViewById(1338)).setText("");
					((EditText)layoutView.findViewById(1339)).setText("");

					break;
			}
			return;
		}

		//TAREAS/REPUESTOS Agencia Alemana
		if (formulario.getId() == 108) {
			//String que cambiará el estado de las visitas al
			String estado = "";

			String toLog = "";
			final int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();

			final String datosAdicionales = aplicacion.getOrdenServicioActiva().getDatosAdicionales();
			final LinkedHashMap <String, String> datosEspecificos = aplicacion.getOrdenServicioActiva().getDatosEspecCliente();

			//obtengo el valor del campo Serial Verificación
			String valorQR = (String) ((EditText)layoutView.findViewById(1437)).getText().toString();
			String serialEditText = (String) ((EditText)layoutView.findViewById(1438)).getText().toString().toUpperCase();

			//Obtengo todos los campos del formulario
			SparseArray<Campo> campos = formulario.getCamposHash();

			//Obtengo los campos hash para mostrar/ocultar al relizar la acción
			Campo campoIniciarMovilizacion = campos.get(1355);
			Campo campoArribo = campos.get(1356);

			//Obtengo los layout donde están los botones que necesito mostrar/ocultar
			LinearLayout layoutIniciarMov= (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoIniciarMovilizacion.getId()));
			LinearLayout layoutArribo = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoArribo.getId()));

			//Calendario para la fecha de los hitos
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

			switch (campoEvento.getId())
			{
				case 1454:
					//Bandera para validar si ya se ha finalizado el mantenimiento
					boolean banderaFinalizar = ((Button)layoutTemporal.findViewById(1441)).isEnabled();
					if(banderaFinalizar) {
						String seleccion = ((Spinner) actividad.findViewById(1454)).getSelectedItem().toString();
						if (!seleccion.equals("") && seleccion != null && seleccion != "") {
							// declaraciones
							String fechaListaSubIndices = sdf.format(cal.getTime());
							toLog = "Se presionó la listaSubIndices a las " + fechaListaSubIndices
									+ " con el idOrden " + idOrdenServicioLog + " y seleccionó " + seleccion;
							toolsApp.generaLog(toLog);
							int key;
							Campo campoTemporal;
							LinearLayout objeto;
							switch (seleccion) {
								case "SISTEMA HIDRÁULICO":

									for (int i = 0; i < campos.size(); i++) {
										key = campos.keyAt(i);
										if ((key == 1377) || (key == 1378) || (key == 1379) || (key == 1380) || (key == 1381)) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else if ((key == 1358) || (key == 1359) || (key == 1360) || (key == 1361) || (key == 1362)
												|| (key == 1363) || (key == 1364) || (key == 1365) || (key == 1366) || (key == 1367)
												|| (key == 1368) || (key == 1369) || (key == 1370) || (key == 1371) || (key == 1372)
												|| (key == 1373) || (key == 1374) || (key == 1375) || (key == 1454) || (key == 1434)
												|| (key == 1435) || (key == 1436) || (key == 1438) || (key == 1440) || (key == 1441)
												) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											//campoTemporal.setVisible(false);
											objeto.setVisibility(LinearLayout.GONE);

										}

									}


									break;
								case "SISTEMA ELÉCTRICO":

									for (int i = 0; i < campos.size(); i++) {
										key = campos.keyAt(i);
										if ((key == 1383) || (key == 1384) || (key == 1385) || (key == 1386) || (key == 1387) || (key == 1388)) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else if ((key == 1358) || (key == 1359) || (key == 1360) || (key == 1361) || (key == 1362)
												|| (key == 1363) || (key == 1364) || (key == 1365) || (key == 1366) || (key == 1367)
												|| (key == 1368) || (key == 1369) || (key == 1370) || (key == 1371) || (key == 1372)
												|| (key == 1373) || (key == 1374) || (key == 1375) || (key == 1454) || (key == 1434)
												|| (key == 1435) || (key == 1436) || (key == 1438) || (key == 1440) || (key == 1441)
												) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											//campoTemporal.setVisible(false);
											objeto.setVisibility(LinearLayout.GONE);

										}

									}


									break;

								case "RUEDAS":

									for (int i = 0; i < campos.size(); i++) {
										key = campos.keyAt(i);
										if ((key == 1390) || (key == 1391)) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else if ((key == 1358) || (key == 1359) || (key == 1360) || (key == 1361) || (key == 1362)
												|| (key == 1363) || (key == 1364) || (key == 1365) || (key == 1366) || (key == 1367)
												|| (key == 1368) || (key == 1369) || (key == 1370) || (key == 1371) || (key == 1372)
												|| (key == 1373) || (key == 1374) || (key == 1375) || (key == 1454) || (key == 1434)
												|| (key == 1435) || (key == 1436) || (key == 1438) || (key == 1440) || (key == 1441)
												) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											//campoTemporal.setVisible(false);
											objeto.setVisibility(LinearLayout.GONE);
										}

									}

									break;
								case "FRENOS":

									for (int i = 0; i < campos.size(); i++) {
										key = campos.keyAt(i);
										if ((key == 1393) || (key == 1394)) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else if ((key == 1358) || (key == 1359) || (key == 1360) || (key == 1361) || (key == 1362)
												|| (key == 1363) || (key == 1364) || (key == 1365) || (key == 1366) || (key == 1367)
												|| (key == 1368) || (key == 1369) || (key == 1370) || (key == 1371) || (key == 1372)
												|| (key == 1373) || (key == 1374) || (key == 1375) || (key == 1454) || (key == 1434)
												|| (key == 1435) || (key == 1436) || (key == 1438) || (key == 1440) || (key == 1441)
												) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											//campoTemporal.setVisible(false);
											objeto.setVisibility(LinearLayout.GONE);
										}

									}

									break;
								case "TRACCION":
									for (int i = 0; i < campos.size(); i++) {
										key = campos.keyAt(i);
										if ((key == 1396) || (key == 1397)) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else if ((key == 1358) || (key == 1359) || (key == 1360) || (key == 1361) || (key == 1362)
												|| (key == 1363) || (key == 1364) || (key == 1365) || (key == 1366) || (key == 1367)
												|| (key == 1368) || (key == 1369) || (key == 1370) || (key == 1371) || (key == 1372)
												|| (key == 1373) || (key == 1374) || (key == 1375) || (key == 1454) || (key == 1434)
												|| (key == 1435) || (key == 1436) || (key == 1438) || (key == 1440) || (key == 1441)
												) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											//campoTemporal.setVisible(false);
											objeto.setVisibility(LinearLayout.GONE);
										}

									}
									break;
								case "BATERIA":

									for (int i = 0; i < campos.size(); i++) {
										key = campos.keyAt(i);
										if ((key == 1399) || (key == 1400) || (key == 1401)) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else if ((key == 1358) || (key == 1359) || (key == 1360) || (key == 1361) || (key == 1362)
												|| (key == 1363) || (key == 1364) || (key == 1365) || (key == 1366) || (key == 1367)
												|| (key == 1368) || (key == 1369) || (key == 1370) || (key == 1371) || (key == 1372)
												|| (key == 1373) || (key == 1374) || (key == 1375) || (key == 1454) || (key == 1434)
												|| (key == 1435) || (key == 1436) || (key == 1438) || (key == 1440) || (key == 1441)
												) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											//campoTemporal.setVisible(false);
											objeto.setVisibility(LinearLayout.GONE);
										}
									}

									break;
								case "CARGADOR":
									for (int i = 0; i < campos.size(); i++) {
										key = campos.keyAt(i);
										if ((key == 1403) || (key == 1404)) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else if ((key == 1358) || (key == 1359) || (key == 1360) || (key == 1361) || (key == 1362)
												|| (key == 1363) || (key == 1364) || (key == 1365) || (key == 1366) || (key == 1367)
												|| (key == 1368) || (key == 1369) || (key == 1370) || (key == 1371) || (key == 1372)
												|| (key == 1373) || (key == 1374) || (key == 1375) || (key == 1454) || (key == 1434)
												|| (key == 1435) || (key == 1436) || (key == 1438) || (key == 1440) || (key == 1441)
												) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											//campoTemporal.setVisible(false);
											objeto.setVisibility(LinearLayout.GONE);
										}
									}
									break;
								case "MÁSTIL":
									for (int i = 0; i < campos.size(); i++) {
										key = campos.keyAt(i);
										if ((key == 1406) || (key == 1407) || (key == 1408)) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else if ((key == 1358) || (key == 1359) || (key == 1360) || (key == 1361) || (key == 1362)
												|| (key == 1363) || (key == 1364) || (key == 1365) || (key == 1366) || (key == 1367)
												|| (key == 1368) || (key == 1369) || (key == 1370) || (key == 1371) || (key == 1372)
												|| (key == 1373) || (key == 1374) || (key == 1375) || (key == 1454) || (key == 1434)
												|| (key == 1435) || (key == 1436) || (key == 1438) || (key == 1440) || (key == 1441)
												) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											//campoTemporal.setVisible(false);
											objeto.setVisibility(LinearLayout.GONE);
										}
									}
									break;
								case "REVISIÓN DE OPERACIÓN":
									for (int i = 0; i < campos.size(); i++) {
										key = campos.keyAt(i);
										if ((key == 1410) || (key == 1411) || (key == 1412) || (key == 1413)
												|| (key == 1414) || (key == 1415) || (key == 1416) || (key == 1417)) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else if ((key == 1358) || (key == 1359) || (key == 1360) || (key == 1361) || (key == 1362)
												|| (key == 1363) || (key == 1364) || (key == 1365) || (key == 1366) || (key == 1367)
												|| (key == 1368) || (key == 1369) || (key == 1370) || (key == 1371) || (key == 1372)
												|| (key == 1373) || (key == 1374) || (key == 1375) || (key == 1454) || (key == 1434)
												|| (key == 1435) || (key == 1436) || (key == 1438) || (key == 1440) || (key == 1441)
												) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											//campoTemporal.setVisible(false);
											objeto.setVisibility(LinearLayout.GONE);
										}
									}
									break;

								case "ADICIONALES":
									for (int i = 0; i < campos.size(); i++) {
										key = campos.keyAt(i);
										if ((key == 1419) || (key == 1420) || (key == 1421) || (key == 1422) || (key == 1423)
												|| (key == 1424) || (key == 1425) || (key == 1426) || (key == 1427) || (key == 1428)
												|| (key == 1429)) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else if ((key == 1358) || (key == 1359) || (key == 1360) || (key == 1361) || (key == 1362)
												|| (key == 1363) || (key == 1364) || (key == 1365) || (key == 1366) || (key == 1367)
												|| (key == 1368) || (key == 1369) || (key == 1370) || (key == 1371) || (key == 1372)
												|| (key == 1373) || (key == 1374) || (key == 1375) || (key == 1454) || (key == 1434)
												|| (key == 1435) || (key == 1436) || (key == 1438) || (key == 1440) || (key == 1441)
												) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											//campoTemporal.setVisible(false);
											objeto.setVisibility(LinearLayout.GONE);
										}
									}
									break;
								case "DEMOSTRACIÓN":
									for (int i = 0; i < campos.size(); i++) {
										key = campos.keyAt(i);
										if ((key == 1431) || (key == 1432) || (key == 1433)) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else if ((key == 1358) || (key == 1359) || (key == 1360) || (key == 1361) || (key == 1362)
												|| (key == 1363) || (key == 1364) || (key == 1365) || (key == 1366) || (key == 1367)
												|| (key == 1368) || (key == 1369) || (key == 1370) || (key == 1371) || (key == 1372)
												|| (key == 1373) || (key == 1374) || (key == 1375) || (key == 1454) || (key == 1434)
												|| (key == 1435) || (key == 1436) || (key == 1438) || (key == 1440) || (key == 1441)
												) {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											campoTemporal.setVisible(true);
											objeto.setVisibility(LinearLayout.VISIBLE);

										} else {
											campoTemporal = campos.get(key);
											objeto = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campoTemporal.getId()));
											//campoTemporal.setVisible(false);
											objeto.setVisibility(LinearLayout.GONE);
										}
									}

									break;

							}
						}
					}
				break;

				//Case del botón de inicio de movilización
				case 1355:
					String fechaHitoUno = sdf.format(cal.getTime());

					//Inicio del hito # 1
					String hitoUnoInicio = "ACT001-" + fechaHitoUno;
					((TextView)layoutTemporal.findViewById(1445)).setText(hitoUnoInicio);

					//Muestro/escondo los campos y sus respectivos layouts
					campoArribo.setVisible(true);
					campoIniciarMovilizacion.setVisible(false);
					layoutArribo.setVisibility(LinearLayout.VISIBLE);
					layoutIniciarMov.setVisibility(LinearLayout.GONE);

					toLog = "Se presionó InicioMovilización a las " + fechaHitoUno + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					break;

				//Case del botón de Arribo cliente
				case 1356:

					//obtengo la fecha con el formato del calendario
					String fechaHitoDos = sdf.format(cal.getTime());

					toLog = "Se presionó Arribo cliente a las " + fechaHitoDos + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					//fin del hito # 1
					String hitoUnoFin = ((TextView)layoutTemporal.findViewById(1445)).getText().toString();
					hitoUnoFin += "-" + fechaHitoDos;
					((TextView)layoutTemporal.findViewById(1445)).setText(hitoUnoFin);

					//Inicio del hito # 2
					String hitoDosInicio = "ACT002-" + fechaHitoDos;
					((TextView)layoutTemporal.findViewById(1446)).setText(hitoDosInicio);

					//optimizar con un contador
					for(int i = 0; i < campos.size(); i++) {
						int key = campos.keyAt(i);
						if( (key == 1439) || (key == 1438) ||  (key == 1357) ){

							//obtengo el objeto por la llave y se pone invisible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(true);
							objetoLayaout.setVisibility(LinearLayout.VISIBLE);
							// sebas ((EditText)layoutView.findViewById(1437)).setText("");

						}else{
							//obtengo el objeto por la llave y se pone visible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(false);
							objetoLayaout.setVisibility(LinearLayout.GONE);
						}//fin if/else
					}//fin for
				break;

				case 1440:
					//Segundo Verificar
					String fechaSegundoValidar= sdf.format(cal.getTime());
					toLog = "Se presionó Segundo Validar a las " + fechaSegundoValidar + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);
					// Valida que sea diferente de null o de vacío
					if( valorQR != null && !valorQR.equals("") && serialEditText != null && !serialEditText.equals("")){
						Toast.makeText(contexto, "Llene un solo campo, por favor", Toast.LENGTH_LONG).show();
					}else if( valorQR != null && !valorQR.equals("") ){
						//Separo los datos escaneados por el lectorQR
						String[] valorMontacarga = valorQR.split("\r\n");

						// Se obtiene el serial, que siempre estará en la primera posición del codigo QR
						String serialLector = valorMontacarga[0].toUpperCase();

						//Creo un string que contendrá todos los datos leídos por el lectorQR para pintarlos
						//En la casilla de información
						String datosQR = "";

						//Creo un string con todos los datos leídos del lector, independientemente del tamaño
						//para poder pintar los datos en el mensaje.
						for(int i = 0; i < valorMontacarga.length; i++){
							datosQR += valorMontacarga[i].toString().toUpperCase() +"\n";
						}

						try{
							//Convierto los datos adicionales de una orden de servicio a un objeto tipo JSON
							JSONObject jsonObject = new JSONObject(datosAdicionales);

							//Obtengo las variables que necesito para las condiciones y requerimientos
							String serialDatosOrden = jsonObject.getString("codigoEquipo").toUpperCase();

							//Valido si el serial captado por el lectorQR es igual al serial de los datos en la orden de servicio.
							if( !serialLector.equals(serialDatosOrden) ){
								((TextView)layoutView.findViewById(1436)).setText("Datos escaneados: "+ datosQR);
							}else if( serialLector.equals(serialDatosOrden) ) {
								((TextView)layoutView.findViewById(1436)).setText("");
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}else if(serialEditText != null && !serialEditText.equals("")){

						//string que contendrá la información de aviso a mostrar
						String datos= "Datos escaneados: ";

						datos += serialEditText +"\n";

						try{
							//Convierto los datos adicionales de una orden de servicio a un objeto tipo JSON
							JSONObject jsonObject = new JSONObject(datosAdicionales);

							//Obtengo las variables que necesito para las condiciones y requerimientos
							String serialDatosOrden = jsonObject.getString("codigoEquipo").toUpperCase();

							//Valido si el serial captado por el lectorQR es igual al serial de los datos en la orden de servicio.
							if( !serialEditText.equals(serialDatosOrden) ){
								((TextView)layoutView.findViewById(1436)).setText( datos );
							}else if( serialEditText.equals(serialDatosOrden) ) {
								((TextView)layoutView.findViewById(1436)).setText("");
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					//Cada que se pulse el botón, se setean los campos en vacío
					((EditText)layoutView.findViewById(1437)).setText("");
					((EditText)layoutView.findViewById(1438)).setText("");
					break;

				//case del botón iniciar retorno
				case 1451:

					String fechaHitoSeis= sdf.format(cal.getTime());

					toLog = "Se presionó iniciar retorno a las " + fechaHitoSeis + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					//fin del hito # 5
					String hitoCincoFin = ((TextView)layoutTemporal.findViewById(1449)).getText().toString();
					hitoCincoFin += "-" + fechaHitoSeis;
					((TextView)layoutTemporal.findViewById(1449)).setText(hitoCincoFin);

					//Inicio del hito # 6
					String hitoSeisInicio = "ACT006-" + fechaHitoSeis;
					((TextView)layoutTemporal.findViewById(1450)).setText(hitoSeisInicio);

					for(int i = 0; i < campos.size(); i++) {
						int key = campos.keyAt(i);
						if( (key == 1452) ){

							//obtengo el objeto por la llave y se pone invisible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(true);
							campo.setEditable(true);
							objetoLayaout.setVisibility(LinearLayout.VISIBLE);
							((Button)layoutTemporal.findViewById(key)).setEnabled(true);
							((Button)layoutTemporal.findViewById(key)).setClickable(true);
						}else {
							//obtengo el objeto por la llave y se pone visible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(false);
							objetoLayaout.setVisibility(LinearLayout.GONE);
						}//fin if/else
					}//fin for

					break;

				//case del botón fin retorno
				case 1452:
					estado = "FINALIZADA";
					((TextView)layoutView.findViewById(1453)).setText(estado);

					String fechaHitoSeisFin= sdf.format(cal.getTime());

					toLog = "Se presionó fin retorno a las " + fechaHitoSeisFin + " con el idOrden " + idOrdenServicioLog + " y me puso el estado en " + estado;
					toolsApp.generaLog(toLog);

					//fin del hito # 6
					String hitoSeisFin = ((TextView)layoutTemporal.findViewById(1450)).getText().toString();
					hitoSeisFin += "-" + fechaHitoSeisFin;
					((TextView)layoutTemporal.findViewById(1450)).setText(hitoSeisFin);

					for(int i = 0; i < campos.size(); i++) {
						int key = campos.keyAt(i);
						if( (key == 1445) || (key ==1446) || (key == 1447) || (key == 1448) || (key == 1449)
								|| (key ==1450) || (key ==1453) ){

							//obtengo el objeto por la llave y se pone invisible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(true);
							objetoLayaout.setVisibility(LinearLayout.VISIBLE);
						}else {
							//obtengo el objeto por la llave y se pone visible
							Campo campo = campos.get(key);
							LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
							campo.setVisible(false);
							objetoLayaout.setVisibility(LinearLayout.GONE);
						}//fin if/else
					}//fin for

				break;


				//case del botón firmar orden de servicio
				case 1444:
					//obtengo los datos de los campos para saber sin están vacíos o no
					String nombreFirma = (String) ((EditText)layoutView.findViewById(1442)).getText().toString().toUpperCase();
					String documentoFirma = (String) ((EditText)layoutView.findViewById(1443)).getText().toString().toUpperCase();

					// Valida que sea diferente de null o de vacío
					if( nombreFirma != null && !nombreFirma.equals("") && documentoFirma != null && !documentoFirma.equals("")){

						    String fechaHitoCinco= sdf.format(cal.getTime());

							toLog = "Se presionó firmar a las " + fechaHitoCinco + " con el idOrden " + idOrdenServicioLog;
							toolsApp.generaLog(toLog);

							//fin del hito # 4
							String hitoCuatroFin = ((TextView)layoutTemporal.findViewById(1448)).getText().toString();
							hitoCuatroFin += "-" + fechaHitoCinco;
							((TextView)layoutTemporal.findViewById(1448)).setText(hitoCuatroFin);

							//Inicio del hito # 5
							String hitoCincoInicio = "ACT005-" + fechaHitoCinco;
							((TextView)layoutTemporal.findViewById(1449)).setText(hitoCincoInicio);

							for(int i = 0; i < campos.size(); i++) {
								   int key = campos.keyAt(i);
								   if( (key == 1451) ){

									   //obtengo el objeto por la llave y se pone invisible
										Campo campo = campos.get(key);
										LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
										campo.setVisible(true);
									   	campo.setEditable(true);
										objetoLayaout.setVisibility(LinearLayout.VISIBLE);
										((Button)layoutTemporal.findViewById(key)).setEnabled(true);
										((Button)layoutTemporal.findViewById(key)).setClickable(true);
								   }else {
									   //obtengo el objeto por la llave y se pone visible
										Campo campo = campos.get(key);
										LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
										campo.setVisible(false);
										objetoLayaout.setVisibility(LinearLayout.GONE);
								   }//fin if/else
								}//fin for

					}else if( nombreFirma != null && !nombreFirma.equals("") ){
						Toast.makeText(contexto, "Escriba el documento, por favor", Toast.LENGTH_LONG).show();
					}else if( documentoFirma != null && !documentoFirma.equals("") ){
						Toast.makeText(contexto, "Escriba el nlombre, por favor", Toast.LENGTH_LONG).show();
					}else{
						Toast.makeText(contexto, "Digite los datos, por favor", Toast.LENGTH_LONG).show();
					}
					break;

				//case del botón firma Digital
				case 1456:
					String fechaFirmaDigital = sdf.format(cal.getTime());
					toLog = "Se presionó firmaDigital a las " + fechaFirmaDigital + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					Intent intent = new Intent(contexto, ActividadFirma.class);
					int orden = aplicacion.getOrdenServicioActiva().getId();
					int visita = aplicacion.getOrdenServicioActiva().getVisitaActiva().getId();
					intent.putExtra("id_Orden",orden);
					intent.putExtra("id_Visita",visita);
					contexto.startActivity(intent);
				break;

				//case del botón finalizar Mantenimiento
				case 1441:
					boolean flag = controladorFormularios.interfazAFormulario(
							contexto, aplicacion.getConfiguracionApp(), true, this,
							layoutView, contenedoresCamposHash);

					String fechaBotonFirmar = sdf.format(cal.getTime());
					toLog = "Se presionó en finalizar mantenimiento a las " + fechaBotonFirmar
							+ " con el idOrden " + idOrdenServicioLog + " y tenía la bandera en " + flag;
					toolsApp.generaLog(toLog);

					if(flag) {
							//Se pregunta si de verdad quiere finalizar el movimiento
							toolsApp.alertDialogYESNO(contexto, "¿Desea finalizar el movimiento?"
									, "Sí", "No", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											// Deshabilita el botón
											((AlertDialog) dialog).getButton(which).setEnabled(false);
											// Dependiendo del botón presionado, se ejecuta la acción
											switch (which) {
												case DialogInterface.BUTTON_POSITIVE:

													//Obtengo todos los campos del formulario
													SparseArray<Campo> campos = formulario.getCamposHash();

													//optimizar con un contador
													for (int i = 0; i < campos.size(); i++) {
														int key = campos.keyAt(i);
														String modoEdicion = campos.get(key).getModoEdicion();

														//Muestro los campos de firmar la orden de servicio
														if ((key == 1442) || (key == 1443) ||
																(key == 1444) || (key == 1456) || (key == 1455)) {

															//obtengo el objeto por la llave y se pone invisible
															Campo campo = campos.get(key);
															LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
															campo.setVisible(true);
															objetoLayaout.setVisibility(LinearLayout.VISIBLE);

															//Desctivo los campos dependiendo de su tipo
														} else if (modoEdicion.equals("EditText")) {
															//obtengo el objeto por la llave y se pone visible
															((EditText) layoutTemporal.findViewById(key)).setEnabled(false);
															Campo campo = campos.get(key);
															campo.setEditable(false);
														} else if (modoEdicion.equals("MultiSpinner")) {
															//obtengo el objeto por la llave y se pone visible
															((Spinner) layoutTemporal.findViewById(key)).setEnabled(false);
															((Spinner) layoutTemporal.findViewById(key)).setClickable(false);
															Campo campo = campos.get(key);
															campo.setEditable(false);
														} else if (modoEdicion.equals("Button")) {
															//obtengo el objeto por la llave y se pone visible
															((Button) layoutTemporal.findViewById(key)).setEnabled(false);
															((Button) layoutTemporal.findViewById(key)).setClickable(false);
															Campo campo = campos.get(key);
															campo.setEditable(false);
														} else if (modoEdicion.equals("Detalle")) {
															//obtengo el objeto por la llave y se pone visible
															((Button) layoutTemporal.findViewById(key)).setEnabled(false);
															((Button) layoutTemporal.findViewById(key)).setClickable(false);
															Campo campo = campos.get(key);
															campo.setEditable(false);
														} else if (modoEdicion.equals("ScanCode")) {
															//obtengo el objeto por la llave y se pone visible
															((EditText) layoutTemporal.findViewById(key)).setEnabled(false);
															Campo campo = campos.get(key);
															campo.setEditable(false);
														} else if (modoEdicion.equals("Spinner")) {
															//obtengo el objeto por la llave y se pone visible
															((Spinner) layoutTemporal.findViewById(key)).setEnabled(false);
															((Spinner) layoutTemporal.findViewById(key)).setClickable(false);
															Campo campo = campos.get(key);
															campo.setEditable(false);
														}

													}//fin for

													//Fecha actual para los hitos
													Calendar cal = Calendar.getInstance();
													SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SS");

													String fechaHitoCuatro = sdf.format(cal.getTime());

													//fin del hito # 3
													String hitoTresFin = ((TextView) layoutTemporal.findViewById(1447)).getText().toString();
													hitoTresFin += "-" + fechaHitoCuatro;
													((TextView) layoutTemporal.findViewById(1447)).setText(hitoTresFin);

													//Inicio del hito # 4
													String hitoCuatroInicio = "ACT004-" + fechaHitoCuatro;
													((TextView) layoutTemporal.findViewById(1448)).setText(hitoCuatroInicio);

												break;
											}
										}
									}
							);//Fin Dialog
						}
				break;

				case 1439:
					//Primer Verificar

					String fechaPrimerValidar= sdf.format(cal.getTime());
					toLog = "Se presionó Primer Validar a las " + fechaPrimerValidar + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					// Valida que sea diferente de null o de vacío
					if( valorQR != null && !valorQR.equals("") && serialEditText != null && !serialEditText.equals("")){
						Toast.makeText(contexto, "Llene un solo campo, por favor", Toast.LENGTH_LONG).show();
					}else if(valorQR != null && !valorQR.equals("") ){
						//Separo los datos escaneados por el lectorQR
						String[] valorMontacarga = valorQR.split("\r\n");

						// Se obtiene el serial, que siempre estará en la primera posición del codigo QR
						String serialLector = valorMontacarga[0].toUpperCase();

						//Creo un string que contendrá todos los datos leídos por el lectorQR para pintarlos
						//En la casilla de información
						String datosQR = "";

						//Creo un string con todos los datos leídos del lector, independientemente del tamaño
						//para poder pintar los datos en el mensaje.
						for(int i = 0; i < valorMontacarga.length; i++){
							datosQR += valorMontacarga[i].toString().toUpperCase() +"\n";
						}

						try{
							//Convierto los datos adicionales de una orden de servicio a un objeto tipo JSON
							JSONObject jsonObject = new JSONObject(datosAdicionales);

							//Obtengo las variables que necesito para las condiciones y requerimientos
							String serialDatosOrden = jsonObject.getString("codigoEquipo").toUpperCase();
							String codigoOT =  jsonObject.getString("CódigoOT").toString();
							String codVisita =  datosEspecificos.get("Código Visita").toString();

							//Obtengo la fecha actual para iniciar el hito y hacer la resta.
							long hitoTresInicioUnix = toolsApp.getUnixDateNOW(true,0,0,0);

							String fechaHitoTres= sdf.format(cal.getTime());

							//fin del hito # 2
							String hitoDosFin = ((TextView)layoutTemporal.findViewById(1446)).getText().toString();
							hitoDosFin += "-" + fechaHitoTres;
							((TextView)layoutTemporal.findViewById(1446)).setText(hitoDosFin);

							//Inicio del hito # 3
							String hitoTresInicio = "ACT003-" + fechaHitoTres;
							((TextView)layoutTemporal.findViewById(1447)).setText(hitoTresInicio);

							//Valido si el serial captado por el lectorQR es igual al serial de los datos en la orden de servicio.
							if( !serialLector.equals(serialDatosOrden) ){
								toolsApp.alertDialogYESNO(contexto, "El equipo a intervenir no coincide con el programado en la OT#:" + codigoOT  +  " Visita#:"+  codVisita +  "\n "
												+ "Los datos escaneados son: \n" + datosQR
												+ "\n ¿Desea continuar?"
										, "Sí", "No",new DialogInterface.OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog, int which) {
												// Deshabilita el botón
												((AlertDialog) dialog).getButton(which).setEnabled(false);
												// Dependiendo del botón presionado, se ejecuta la acción
												switch (which){
													case DialogInterface.BUTTON_POSITIVE:
														SparseArray<Campo> campos = formulario.getCamposHash();

														for(int i = 0; i < campos.size(); i++) {
															int key = campos.keyAt(i);
															if( (key == 1439) || (key == 1357) || (key == 1355) || (key == 1356)
																	|| (key == 1442) || (key == 1443) || (key == 1444)
																	|| (key == 1451) || (key == 1452) || (key == 1437) || (key == 1456)
																	|| (key == 1445) || (key == 1446) || (key == 1447) || (key == 1448)
																	|| (key == 1449) || (key == 1450) || (key == 1453) || (key == 1443)
																	|| (key == 1442) || (key == 1452) || (key == 1451)){

																//obtengo el objeto por la llave y se pone invisible
																Campo campo = campos.get(key);
																LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
																campo.setVisible(false);
																objetoLayaout.setVisibility(LinearLayout.GONE);

															}else{
																//obtengo el objeto por la llave y se pone visible
																Campo campo = campos.get(key);
																LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
																campo.setVisible(true);
																objetoLayaout.setVisibility(LinearLayout.VISIBLE);
															}//fin if/else
														}//fin for

														break;
												}
											}
										}
								);//Fin Dialog
							}else if( serialLector.equals(serialDatosOrden) ) {
								//Si es igual, oculto los campos de verificación  y muestro los datos del formulario

								for(int i = 0; i < campos.size(); i++) {
									int key = campos.keyAt(i);
									if( (key == 1439) || (key == 1357) || (key == 1355) || (key == 1356)
											|| (key == 1442) || (key == 1443) || (key == 1444)
											|| (key == 1451) || (key == 1452) || (key == 1437) || (key == 1456)
											|| (key == 1445) || (key == 1446) || (key == 1447) || (key == 1448)
											|| (key == 1449) || (key == 1450) || (key == 1453) || (key == 1443)
											|| (key == 1442) || (key == 1452) || (key == 1451)){

										//obtengo el objeto por la llave y se pone invisible
										Campo campo = campos.get(key);
										LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
										campo.setVisible(false);
										objetoLayaout.setVisibility(LinearLayout.GONE);

									}else{
										//obtengo el objeto por la llave y se pone visible
										Campo campo = campos.get(key);
										LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
										campo.setVisible(true);
										objetoLayaout.setVisibility(LinearLayout.VISIBLE);
									}//fin if/else
								}//fin for
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}else if(serialEditText != null && !serialEditText.equals("")){

						//string que contendrá la información de aviso a mostrar
						String datos= "Datos escaneados: ";

						datos += serialEditText +"\n";

						try{
							//Convierto los datos adicionales de una orden de servicio a un objeto tipo JSON
							JSONObject jsonObject = new JSONObject(datosAdicionales);

							//Obtengo las variables que necesito para las condiciones y requerimientos
							String serialDatosOrden = jsonObject.getString("codigoEquipo").toUpperCase();
							String codVisita = datosEspecificos.get("Código Visita");
							String codigoOT = jsonObject.getString("codigoOT");

							String fechaHitoTres= sdf.format(cal.getTime());

							//fin del hito # 2
							String hitoDosFin = ((TextView)layoutTemporal.findViewById(1446)).getText().toString();
							hitoDosFin += "-" + fechaHitoTres;
							((TextView)layoutTemporal.findViewById(1446)).setText(hitoDosFin);

							//Inicio del hito # 3
							String hitoTresInicio = "ACT003-" + fechaHitoTres;
							((TextView)layoutTemporal.findViewById(1447)).setText(hitoTresInicio);


							//Valido si el serial captado por el lectorQR es igual al serial de los datos en la orden de servicio.
							if( !serialEditText.equals(serialDatosOrden) ){
								toolsApp.alertDialogYESNO(contexto, "El equipo a intervenir no coincide con el programado en la OT#:" + codigoOT  +  " Visita#:"+  codVisita +  "\n "
												+ "Los datos escaneados son: \n" + datos
												+ "\n ¿Desea continuar?"
										, "Sí", "No",new DialogInterface.OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog, int which) {
												// Deshabilita el botón
												((AlertDialog) dialog).getButton(which).setEnabled(false);
												// Dependiendo del botón presionado, se ejecuta la acción
												switch (which){
													case DialogInterface.BUTTON_POSITIVE:
														SparseArray<Campo> campos = formulario.getCamposHash();

														for(int i = 0; i < campos.size(); i++) {
															int key = campos.keyAt(i);
															if( (key == 1439) || (key == 1357) || (key == 1355) || (key == 1356)
																	|| (key == 1442) || (key == 1443) || (key == 1444)
																	|| (key == 1451) || (key == 1452) || (key == 1437) || (key == 1456)
																	|| (key == 1445) || (key == 1446) || (key == 1447) || (key == 1448)
																	|| (key == 1449) || (key == 1450) || (key == 1453) || (key == 1443)
																	|| (key == 1442) || (key == 1452) || (key == 1451)){

																//obtengo el objeto por la llave y se pone invisible
																Campo campo = campos.get(key);
																LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
																campo.setVisible(false);
																objetoLayaout.setVisibility(LinearLayout.GONE);
																// sebas ((EditText)layoutView.findViewById(1437)).setText("");

															}else{
																//obtengo el objeto por la llave y se pone visible
																Campo campo = campos.get(key);
																LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
																campo.setVisible(true);
																objetoLayaout.setVisibility(LinearLayout.VISIBLE);
															}//fin if/else
														}//fin for

														break;
												}
											}
										}
								);//Fin Dialog
							}else if( serialEditText.equals(serialDatosOrden) ) {
								//Si es igual, oculto los campos de verificación  y muestro los datos del formulario

								for(int i = 0; i < campos.size(); i++) {
									int key = campos.keyAt(i);
									if( (key == 1439) || (key == 1357) || (key == 1355) || (key == 1356)
											|| (key == 1442) || (key == 1443) || (key == 1444)
											|| (key == 1451) || (key == 1452) || (key == 1437) || (key == 1456)
											|| (key == 1445) || (key == 1446) || (key == 1447) || (key == 1448)
											|| (key == 1449) || (key == 1450) || (key == 1453) || (key == 1443)
											|| (key == 1442) || (key == 1452) || (key == 1451)){

										//obtengo el objeto por la llave y se pone invisible
										Campo campo = campos.get(key);
										LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
										campo.setVisible(false);
										objetoLayaout.setVisibility(LinearLayout.GONE);
										// sebas ((EditText)layoutView.findViewById(1437)).setText("");

									}else{
										//obtengo el objeto por la llave y se pone visible
										Campo campo = campos.get(key);
										LinearLayout objetoLayaout = (LinearLayout) contenedoresCamposHash.get(String.valueOf(campo.getId()));
										campo.setVisible(true);
										objetoLayaout.setVisibility(LinearLayout.VISIBLE);
									}//fin if/else
								}//fin for
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					//Cada que se pulse el botón, se setean los campos en vacío
					((EditText)layoutView.findViewById(1437)).setText("");
					((EditText)layoutView.findViewById(1438)).setText("");

					break;
			}
			return;
		}

		if (formulario.getId() == 96) {
			//	String datosAdicionales = aplicacion.getOrdenServicioActiva().getDatosAdicionales();
			//LinkedHashMap <String, String> datosEspecificos = aplicacion.getOrdenServicioActiva().getDatosEspecCliente();

			switch (campoEvento.getId()) {

			}
			return;
		}
		// Repuestos Agencia Alemana
		if (formulario.getId() == 99) {
			switch (campoEvento.getId()) {

				case 1065:
					// Es el campo Código
					editText = (EditText) view;
					valor = (String) editText.getText().toString();

					// Busca coincidencias en la base de datos
					HashMap<String, String> articulos = ((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp()
							.getCustomSqliteRoutines()).agenciaAlemana_consultarRepuestoXCodigoBarras(valor);
					String codigoSAP = "";
					String codigoBarras = "";
					String referenciaFabricante = "";
					String descripcion = "";
					String grupo = "";

					// Verifica si el repuesto no es nulo
					if(articulos != null){
						// Item existe, extrae datos
						codigoSAP = articulos.get("codigoSAP");
						referenciaFabricante = articulos.get("referenciaFabricante");
						descripcion = articulos.get("descripcion");
						grupo = articulos.get("grupo");

						// Cambia el color del texto
						editText.setTextColor(contexto.getResources().getColor(R.color.DarkGreen));
					} else {
						// Cambia el color del texto
						editText.setTextColor(Color.RED);
					}

					// Setea los datos en los campos del formulario
					((TextView)layoutView.findViewById(1020)).setText(referenciaFabricante);
					((TextView)layoutView.findViewById(1021)).setText(codigoSAP);
					((TextView)layoutView.findViewById(1069)).setText(grupo);

					if( !referenciaFabricante.equals("012345678") && referenciaFabricante != "012345678"){
						((TextView)layoutView.findViewById(1022)).setText(descripcion);
						((TextView)layoutView.findViewById(1022)).setEnabled(false);
					}else{
						((TextView)layoutView.findViewById(1022)).setEnabled(true);
					}
					break;
			}
			return;
		}
		// ORDEN DE SERVICIO ACTIVA - Carvajal Espacios - ALMATEC
		if (formulario.getId() == 42) {
			switch (campoEvento.getId()) {
				case 816:
					// Es el campo fallas de sistema
					spinner = (Spinner) view;
					valor = (String) spinner.getSelectedItem();

					// Carga la lista de fallas
					String[] fallas = ((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
							.almatec_consultarFallas(valor);
					ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(contexto,
							R.layout.textview_spinner, fallas);
					// Crea y adiciona el Adapter para el spinner
					arrayAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
					((Spinner)actividad.findViewById(817)).setAdapter(arrayAdapter);

					// Se obtiene el valor de datos adicionales
					String datosAdicionales = aplicacion.getOrdenServicioActiva().getDatosAdicionales();

					// Se valida que datos adicionales sea diferente de null
					if(datosAdicionales != null){
						try {
							JSONObject jsonObject = new JSONObject(datosAdicionales);
							String codigoFalla = jsonObject.getString("codigoFalla");

							// Valia si no es vacio
							if(codigoFalla!=null && !codigoFalla.equals("")){
								int numeroValoresfallas = fallas.length;

								// Recorre el vector fallas  sistema
								for (int j = 0; j < numeroValoresfallas; j++) {

									if(fallas[j].equals(codigoFalla)){
										((Spinner)actividad.findViewById(817)).setSelection(j);
										break;
									}
								}
							}

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					break;
			}
			return;
		}

		// TAREAS / VISITAS - Carvajal Espacios - ALMATEC
		if (formulario.getId() == 43) {
			switch (campoEvento.getId()) {
				case 401:
					// Es el campo Estado
					spinner = (Spinner) view;
					valor = (String) spinner.getSelectedItem();


					LinearLayout ll_cmpo = (LinearLayout) contenedoresCamposHash.get(String.valueOf(735));
					// Si el valor es CERRADO cambia el estado de la orden a FINALIZADO, de lo contrario queda EN CURSO
					if (valor.equals("CERRADO")) {
						aplicacion.getOrdenServicioActiva().setEstado(OrdenServicio.FINALIZADA);
						aplicacion.getOrdenServicioActiva().setUnixFechaFinalizacion(
								toolsApp.getUnixDateNOW(true, 0, 0, 0));

						ll_cmpo.setVisibility(View.VISIBLE);
						((CheckBox)layoutView.findViewById(735)).setVisibility(View.VISIBLE);
					} else {
						aplicacion.getOrdenServicioActiva().setEstado(OrdenServicio.ENCURSO);
						aplicacion.getOrdenServicioActiva().setUnixFechaFinalizacion(-1);
						ll_cmpo.setVisibility(View.GONE);
						((CheckBox)layoutView.findViewById(735)).setVisibility(View.GONE);
					}
					break;
			}
			return;
		}

		// Solicitud Repuestos - Carvajal Espacios - ALMATEC
		/*if (formulario.getId() == 35) {
			switch (campoEvento.getId()) {
				case 322:
					// Es el campo Código
					editText = (EditText) view;
					valor = (String) editText.getText().toString();

					// Busca coincidencias en la base de datos
					HashMap<String, String> repuesto = ((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp()
							.getCustomSqliteRoutines()).almatec_consultarRepuesto(valor);
					String descripcionArticulo = "";
					String subinventario = "";
					String localizador = "";
					String tipoUnidad = "";
					String uen = "";
					String bodega = "";

					// Verifica si el repuesto no es nulo
					if(repuesto != null){
						// Item existe, extrae datos
						descripcionArticulo = repuesto.get("descripcionArticulo");
						subinventario = repuesto.get("subinventario");
						localizador = repuesto.get("localizador");
						tipoUnidad = repuesto.get("tipoUnidad");
						uen = repuesto.get("uen");
						bodega = repuesto.get("bodega");

						// Cambia el color del texto
						editText.setTextColor(contexto.getResources().getColor(R.color.DarkGreen));
					} else {
						// Cambia el color del texto
						editText.setTextColor(Color.RED);
					}

					// Setea los datos en los campos del formulario
					((EditText)layoutView.findViewById(323)).setText(descripcionArticulo);
					((TextView)layoutView.findViewById(673)).setText(subinventario);
					((TextView)layoutView.findViewById(674)).setText(localizador);
					((TextView)layoutView.findViewById(675)).setText(tipoUnidad);
					((TextView)layoutView.findViewById(676)).setText(uen);
					((TextView)layoutView.findViewById(677)).setText(bodega);
				break;
			}
			return;
		}

		// Instalación Repuestos - Carvajal Espacios - ALMATEC
		if (formulario.getId() == 58) {
			switch (campoEvento.getId()) {
				case 678:
					// Es el campo Código
					editText = (EditText) view;
					valor = (String) editText.getText().toString();

					// Busca coincidencias en la base de datos
					HashMap<String, String> repuesto = ((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp()
							.getCustomSqliteRoutines()).almatec_consultarRepuesto(valor);
					String descripcionArticulo = "";
					String subinventario = "";
					String localizador = "";
					String tipoUnidad = "";
					String uen = "";
					String bodega = "";

					// Verifica si el repuesto no es nulo
					if(repuesto != null){
						// Item existe, extrae datos
						descripcionArticulo = repuesto.get("descripcionArticulo");
						subinventario = repuesto.get("subinventario");
						localizador = repuesto.get("localizador");
						tipoUnidad = repuesto.get("tipoUnidad");
						uen = repuesto.get("uen");
						bodega = repuesto.get("bodega");

						// Cambia el color del texto
						editText.setTextColor(contexto.getResources().getColor(R.color.DarkGreen));
					} else {
						// Cambia el color del texto
						editText.setTextColor(Color.RED);
					}

					// Setea los datos en los campos del formulario
					((TextView)layoutView.findViewById(679)).setText(descripcionArticulo);
					((TextView)layoutView.findViewById(680)).setText(subinventario);
					((TextView)layoutView.findViewById(681)).setText(localizador);
					((TextView)layoutView.findViewById(682)).setText(tipoUnidad);
					((TextView)layoutView.findViewById(683)).setText(uen);
					((TextView)layoutView.findViewById(684)).setText(bodega);
				break;
			}
			return;
		}*/

		/*******************************************/
		// Condicionales Mepal
		/*******************************************/
		// TAREAS / VISITAS - Carvajal Espacios - MEPAL
		if (formulario.getId() == 69) {
			switch (campoEvento.getId()) {
				case 765:
					// Es el campo contiene reclamo
					spinner = (Spinner) view;
					valor = (String) spinner.getSelectedItem();

					LinearLayout contenedorDetalleReclamos = (LinearLayout) contenedoresCamposHash.get(String.valueOf(766));
					Campo campoDetalleReclamos = formulario.getCamposHash().get(766);
					LinearLayout contenedorFotosReclamos = (LinearLayout) contenedoresCamposHash.get(String.valueOf(826));
					Campo campoFotosReclamos = formulario.getCamposHash().get(826);

					// Si el valor es SI muestra el botón de reclamos
					if (valor.equals("SI")) {
						contenedorDetalleReclamos.setVisibility(View.VISIBLE);
						campoDetalleReclamos.setVisible(true);
						contenedorFotosReclamos.setVisibility(View.VISIBLE);
						campoFotosReclamos.setVisible(true);
					} else {
						contenedorDetalleReclamos.setVisibility(View.GONE);
						campoDetalleReclamos.setVisible(false);
						contenedorFotosReclamos.setVisibility(View.GONE);
						campoFotosReclamos.setVisible(false);
					}
					break;
				case 783:
					// Es el campo Estado
					spinner = (Spinner) view;
					valor = (String) spinner.getSelectedItem();

					//LinearLayout ll_cmpo = (LinearLayout) contenedoresCamposHash.get(String.valueOf(735));
					// Si el valor es CERRADO cambia el estado de la orden a FINALIZADO, de lo contrario queda EN CURSO
					if (valor.startsWith("CERRADO")) {
						aplicacion.getOrdenServicioActiva().setEstado(OrdenServicio.FINALIZADA);
						aplicacion.getOrdenServicioActiva().setUnixFechaFinalizacion(
								toolsApp.getUnixDateNOW(true, 0, 0, 0));

						/*ll_cmpo.setVisibility(View.VISIBLE);
						((CheckBox)layoutView.findViewById(735)).setVisibility(View.VISIBLE);*/
					} else {
						aplicacion.getOrdenServicioActiva().setEstado(OrdenServicio.ENCURSO);
						aplicacion.getOrdenServicioActiva().setUnixFechaFinalizacion(-1);
						/*ll_cmpo.setVisibility(View.GONE);
						((CheckBox)layoutView.findViewById(735)).setVisibility(View.GONE);*/
					}
					break;
			}
			return;
		}

		// Es el formulario de Reclamos
		if (formulario.getId() == 71) {
			switch (campoEvento.getId()) {
				case 772:
					// Es el campo Código Artículo
					editText = (EditText) view;
					valor = (String) editText.getText().toString();

					// Busca coincidencias en la base de datos
					HashMap<String, String> articulo = ((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp()
							.getCustomSqliteRoutines()).mepal_consultarArticulo(valor);
					String descripcionArticulo = "";
					String cantidadSol = "";
					String proveedor = "";
					String ordenCompraEnvio = "";
					String billTo = "";
					String shipTo = "";

					// Verifica si el articulo no es nulo
					if(articulo != null){
						// Item existe, extrae datos
						descripcionArticulo = articulo.get("descripcionArticulo");
						cantidadSol = articulo.get("cantidadSolicitada");
						proveedor = articulo.get("proveedor");
						ordenCompraEnvio = articulo.get("ordenCompraEnvio");
						billTo = articulo.get("billTo");
						shipTo = articulo.get("shipTo");

						// Cambia el color del texto
						editText.setTextColor(contexto.getResources().getColor(R.color.DarkGreen));
					} else {
						// Cambia el color del texto
						editText.setTextColor(Color.RED);
					}

					// Setea los datos en los campos del formulario
					((TextView)layoutView.findViewById(773)).setText(descripcionArticulo);
					((TextView)layoutView.findViewById(820)).setText(cantidadSol);
					((TextView)layoutView.findViewById(775)).setText(proveedor);
					((TextView)layoutView.findViewById(776)).setText(ordenCompraEnvio);
					((TextView)layoutView.findViewById(823)).setText(shipTo);
					((TextView)layoutView.findViewById(824)).setText(billTo);

					break;
				case 777:
					// Es el campo cantidad
					editText = (EditText) view;
					valor = (String) editText.getText().toString();

					// Valida que la cantidad sea menor o igual a la cantidad solicitada,
					// de lo contrario invalida el campo
					if(valor != null && !valor.equals("")){
						try {
							int cantidad = Integer.parseInt(valor);
							int cantidadSolicitada = Integer.parseInt(((TextView)layoutView.findViewById(820)).getText().toString());
							if(cantidad <= cantidadSolicitada){
								editText.setTextColor(contexto.getResources().getColor(R.color.DarkGreen));
							} else {
								editText.setTextColor(Color.RED);
							}
						} catch (Exception e) {
							editText.setTextColor(Color.RED);
						}
					}
					break;
			}
			return;
		}
	}

	@Override
	public void eventoCampoDetalle(Context contexto, Campo cmpo_evnto, Formulario formularioAsociado) {
		Intent intent = new Intent(contexto, ActividadSubformulario.class);
		intent.putExtra("idCampo", cmpo_evnto.getId());
		intent.putExtra("nombreCampo", cmpo_evnto.getNombre());
		intent.putExtra("Formulario", formularioAsociado);
		((Activity) contexto).startActivityForResult(intent, ControladorFormularios.REQUEST_SUBFORMULARIO);
	}

	/*
	Implementacion e la clase abstracta donde se llama la actividad ActividadGaleriaImagen a la espera de su resultado
	con el tipo de peticion el id del campo que la invoca y el nombre
	 */
	@Override
	public void eventoCampoAdjuntaFotos(Context contexto, Campo campoEvento, int tipoPeticion){
		Intent intent = new Intent(contexto, ActividadGaleriaImagenes.class);
		intent.putExtra("tipoPeticion",tipoPeticion);
		intent.putExtra("idCampo", campoEvento.getId());
		intent.putExtra("nombreCampo", campoEvento.getNombre());

		((Activity) contexto).startActivityForResult(intent, ControladorFormularios.REQUEST_ADJUNTAFOTOS);
	}


	@Override
	public void eliminarRastroItem(Context contexto, View layoutView) {

	}

	@Override
	public void finalizarDetalle(View layoutView) {

	}

	@Override
	public void validarEventoFinFormulario(Context contexto, View layoutView) {
		Activity actividad = (Activity) contexto;
		AplicacionOrdenesServicio aplicacion = (AplicacionOrdenesServicio) actividad.getApplicationContext();
		OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();
		SparseArray<Campo> camposHash = formulario.getCamposHash();


		// ORDEN SERVICIO ACTIVA o TAREAS VISITAS DESDE EL HISTORIAL - Carvajal Espacios - ALMATEC
		if (formulario.getId() == 42 || formulario.getId() == 43) {
			// Actualiza estado de la orden en BD
			((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
					.almatec_actualizarEstadoOrdenServicio(ordenServicio);
			return;
		}

		// ORDEN SERVICIO ACTIVA o TAREAS VISITAS DESDE EL HISTORIAL - Carvajal Espacios - MEPAL
		if (formulario.getId() == 68 || formulario.getId() == 69) {
			// Actualiza estado de la orden en BD
			((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
					.mepal_actualizarEstadoOrdenServicio(ordenServicio);
			return;
		}

		// FINALIZACION ORDEN TRABAJO AGENCIA ALEMANA
		if (formulario.getId() == 96) {

			String nombre = String.valueOf((int) aplicacion.getOrdenServicioActiva().getId());
			nombre = nombre + "_";
			nombre = nombre + String.valueOf((int) aplicacion.getOrdenServicioActiva().getVisitaActiva().getId());
			nombre = nombre + ".png";

			//String directorio =File.separator + "GC" + File.separator + "databases" + File.separator;
			String pathSD = Environment.getExternalStorageDirectory()
					.getAbsolutePath() + "/GC/" + "/Firma/" ;   // ruta

			String directorio = pathSD+nombre;

			File fichero = new File(directorio);

			if(fichero.exists()) {
				Intent intentoServicioNovedades = new Intent(contexto, ServicioNovedades.class);
				intentoServicioNovedades.putExtra("tipoIntent", "FIRMA");
				intentoServicioNovedades.putExtra("nombre", nombre); // ya esta
				intentoServicioNovedades.putExtra("ruta", pathSD);  // ya esta
				contexto.startService(intentoServicioNovedades);
			}

			String datosAdicionales = aplicacion.getOrdenServicioActiva().getDatosAdicionales();
			LinkedHashMap <String, String> datosEspecificos = aplicacion.getOrdenServicioActiva().getDatosEspecCliente();
			String estado = "FINALIZADA";
			try{
				//Convierto los datos adicionales de una orden de servicio a un objeto tipo JSON
				JSONObject jsonObject = new JSONObject(datosAdicionales);

				//Obtengo los codigo de ot y visita para poner en los hitos
				String codVisita = datosEspecificos.get("Código Visita");
				String codigoOT = jsonObject.getString("codigoOT");
				String codigoTecnico = aplicacion.getConfiguracionApp().getUsuario().getCodigo().toString();


				// Actualiza el estado a nivel de base de datos.
				((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp()
						.getCustomSqliteRoutines()).agenciaAlemana_actualizarEstadoVisita(estado, codigoOT, codVisita, codigoTecnico);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}
		// FINALIZACION ORDEN TRABAJO COEXITO
		if (formulario.getId() == 119) {

			String nombre = String.valueOf((int) aplicacion.getOrdenServicioActiva().getId());
			nombre = nombre + "_";
			nombre = nombre + String.valueOf((int) aplicacion.getOrdenServicioActiva().getVisitaActiva().getId());
			nombre = nombre + ".png";

			//String directorio =File.separator + "GC" + File.separator + "databases" + File.separator;
			String pathSD = Environment.getExternalStorageDirectory()
					.getAbsolutePath() + "/GC/" + "/Firma/" ;   // ruta

			String directorio = pathSD+nombre;

			File fichero = new File(directorio);

			if(fichero.exists()) {
				Intent intentoServicioNovedades = new Intent(contexto, ServicioNovedades.class);
				intentoServicioNovedades.putExtra("tipoIntent", "FIRMA");
				intentoServicioNovedades.putExtra("nombre", nombre); // ya esta
				intentoServicioNovedades.putExtra("ruta", pathSD);  // ya esta
				contexto.startService(intentoServicioNovedades);
			}

			// Actualiza estado de la orden en BD
			((CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
					.coexito_actualizarEstadoOrdenServicio(ordenServicio);
			return;
		}
	}

	@Override
	public int validarExistenciaItem(Context contexto, Campo campo, String _valor,
									 Vector<Vector<Campo>> datos) {
		boolean campoLlave = false;

		// Dependiendo del formulario y el campo indica si es llave
		if(formulario.getId() == 35 && campo.getId() == 322) {
			// Formulario de repuestos de almatec y campo código
			campoLlave = true;
		}

		// Verifica si el campo es llave del item
		if(campoLlave) {
			// Verifica si existen datos
			if(datos != null && datos.size() > 0){
				// Si hay datos, recorre los items y verifica si el valor dado ya existe.
				int numeroItems = datos.size();
				for(int i = 0; i < numeroItems; i++){
					Vector<Campo> item = datos.elementAt(i);

					// Recorre los campos del Item
					int numeroCampos = item.size();
					for(int j = 0; j < numeroCampos; j++){
						Campo cmpo_item = item.elementAt(j);
						int idCampo = cmpo_item.getId();
						String valor = cmpo_item.getValor();

						// Valida si el item con el campo indicado ya existe
						if (idCampo == campo.getId() && valor != null && valor.equals(_valor)) {
							// El item existe.
							return i;
						}
					}
				}
			}
		}
		return -1;
	}

	@Override
	public void actualizaCampoCalculado(View layoutView,
										HashMap<String, View> cmpos_cntndres_view, float total) {

	}


	public void eventoInicioServicio(Context contexto,
									 HashMap<String, View> contenedoresCamposHash, HashMap<String, Campo> camposHash) {
		// ORDEN DE SERVICIO ACTIVA - Carvajal Espacios - ALMATEC
		if (formulario.getId() == 42) {
			// Muestra los campos del formulario que se encuentran ocultos.
			/*camposHash.get(String.valueOf(373)).setVisible(true);
			contenedoresCamposHash.get(String.valueOf(373)).setVisibility(View.VISIBLE);*/
			/*camposHash.get(String.valueOf(374)).setVisible(true);
			contenedoresCamposHash.get(String.valueOf(374)).setVisibility(View.VISIBLE);*/
			return;
		}
	}


}
