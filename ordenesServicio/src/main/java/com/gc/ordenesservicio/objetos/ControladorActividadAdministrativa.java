package com.gc.ordenesservicio.objetos;

import java.util.List;
import java.util.Vector;

import com.gc.coregestionclientes.objetos.Campo;
import com.gc.coregestionclientes.objetos.Formulario;
import com.gc.ordenesservicio.AplicacionOrdenesServicio;

public class ControladorActividadAdministrativa {
	
	// Genera Json con los datos de la actividad administrativa
	public static String generaraJson(ActividadAdministrativa actividadAdministrativa, AplicacionOrdenesServicio aplicacion){
		StringBuilder actividadJson = new StringBuilder(); //TODO:
		actividadJson.insert(0, "{").append("}");
		String json;
		
		// Verifica si existe actividadAdministrativa
		if (actividadAdministrativa == null) return null;
		
		int idActividadAdministrativa = actividadAdministrativa.getId();
		int idUsuario = actividadAdministrativa.getIdUsuario();
		long unixFechaInicio = actividadAdministrativa.getUnixFechaInicio();
		long unixFechaFin = actividadAdministrativa.getUnixFechaFinalizacion();
		Formulario formulario = actividadAdministrativa.getFormulario();
		
		if(formulario == null) return null;
		
		// Comienza a construir el JSON
		json = "{" +
		   "\"idActividadAdministrativa\":" + idActividadAdministrativa + "," +
		   "\"idUsuario\": " + idUsuario + "," +
		   "\"unixFechaInicio\": " + unixFechaInicio + "," +
		   "\"unixFechaFin\": " + unixFechaFin + "," +
		   "\"formulario\": { " + 
		   "\"idFormulario\": " + formulario.getId() + "," +
		   "\"nombreFormulario\": \"" + formulario.getNombre() + "\"," +
		   "\"tablasAsociadas\":[ ";
		
		List<String[]> tablasAsociadas = formulario.getTablaAsociadas();
		int numeroTablasAsociadas = tablasAsociadas.size();
		
		// Se valida si el formulario tiene tablas
		if(tablasAsociadas != null && numeroTablasAsociadas>0){
			// Se recorre las tablas asociadas al formulario
			for(int i = 0;i<numeroTablasAsociadas;i++){
				
				// Inicia INSERT y le concatena el nombre de la tabla destino
				json += "\""+tablasAsociadas.get(i)[1]+"\"";
				
				if(i<numeroTablasAsociadas-1){
					// Añade coma
					json += ",";
				}
			}
		}
		json += "],";
		json += "\"campos\":[ ";
		
		Vector<Campo> campos = formulario.getCampos();
		int numeroCampos = campos.size();
		
		// Recorre los campos del formulario
		for(int j = 0;j<numeroCampos;j++){
			Campo campo = campos.get(j);
			String valorCampo = campo.getValor();
			
			json += "{";
			json += "\"idCampo\": " +campo.getId() + "," +
					"\"tipoDato\": \"" + campo.getTipoDato() + "\"," +
					"\"modoEdicion\": \"" + campo.getModoEdicion() + "\"," +
					"\"camposAsociados\": ["; 
			
			List<String[]> camposAsociados = campo.getCamposAsociados();
			
			// Se valida que el campo tenga campos asociados
			if(camposAsociados != null){
				int numeroCamposAsociados = camposAsociados.size();

				// Recorre los campos asociados a cada formulario
				for(int k = 0;k<numeroCamposAsociados;k++){
					String[] campoAsociado = camposAsociados.get(k);
					
					json += "{" +
							"\"tabla\": \"" + campoAsociado[0] + "\"," +
							"\"nombreCampo\": \"" + campoAsociado[1] + "\"";
					
					json += "}";
					
					if(j<numeroCamposAsociados-1){
						// Añade coma
						json += ",";
					}
				}
			}
			json += "],";
			if(campo.getTipoDato().equals("Number")
					|| campo.getTipoDato().equals("Decimal")){
				json += "\"valor\": " + campo.getValor();
			}else{
				String valor = (valorCampo != null ? valorCampo.replace("\"", "\\\"") : valorCampo);
				json += "\"valor\": \"" + valor + "\"";
			}
			json += "}";
			
			if(j<numeroCampos-1){
				// Añade coma
				json += ",";
			}
		}
				
		json += "]}}"; 
		
		// Retorno..
		return json;
	}	
}