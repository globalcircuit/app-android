package com.gc.ordenesservicio.objetos;

import com.gc.coregestionclientes.objetos.Formulario;

public class ActividadAdministrativa {
	// Atributos 
	private int id;
	private int idUsuario;
	private long unixFechaInicio;
	private long unixFechaFinalizacion;
	
	// Auxiliares
	private Formulario formulario;
	
	// Método Constructor
	public ActividadAdministrativa(int id, int idUsuario,
			long unixFechaInicio, long unixFechaFinalizacion, Formulario formulario){
	
		this.id = id;
		this.idUsuario = idUsuario;
		this.unixFechaInicio = unixFechaInicio;
		this.unixFechaFinalizacion = unixFechaFinalizacion;
		this.formulario = formulario;
	}
	
	/*
	 * Métodos para obtener atributos
	 */
	
	// Retorna id de la actividad administrativa
	public int getId(){
		return id;
	}
		
	// Retorna idUsuarioCreacion
	public int getIdUsuario(){
		return idUsuario;
	}
	
	
	// Retorna fecha de inicio programada
	public long getUnixFechaInicio(){
		return unixFechaInicio;
	}
	
	// Retorna fecha de finalizacion programada
	public long getUnixFechaFinalizacion(){
		return unixFechaFinalizacion;
	}
	
	// Retorna formulario
	public Formulario getFormulario(){
		return formulario;
	}
	
	/*
	 * Métodos para actualizar atributos
	 */
	
	// Actualiza id de la actividad administrativa
	public void setId(int id){
		this.id = id;
	}
	
	
	// Actualiza idUsuarioCreacion
	public void setIdUsuarioCreacion(int idUsuario){
		this.idUsuario = idUsuario;
	}
	
	
	// Actualiza fecha de inicio programada
	public void setUnixFechaInicio(long unixFechaInicio){
		this.unixFechaInicio = unixFechaInicio;
	}
	
	// Actualiza fecha de finalizacion programada
	public void setUnixFechaFinalizacion(long unixFechaFinalizacion){
		this.unixFechaFinalizacion = unixFechaFinalizacion;
	}
	
	// Actualiza formulario
	public void setFormulario(Formulario formulario){
		this.formulario = formulario;
	}
}
