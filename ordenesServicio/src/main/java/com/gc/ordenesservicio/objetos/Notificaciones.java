package com.gc.ordenesservicio.objetos;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import android.widget.RemoteViews;

import com.gc.ordenesservicio.R;

public class Notificaciones {
	
	@SuppressLint("NewApi")
	public static void generarNotificacion(
			Context contexto, int numeroNotificacion, String titulo, 
			String nombreCliente, String fechaCreacion, String serialMontacarga) {

		// Construye la notificacion utilizando un layout personalizado
		RemoteViews remoteViews = new RemoteViews(contexto.getPackageName(), R.layout.notificacion_almatec);
 
		// Open NotificationView Class on Notification Click
		/*Intent intent = new Intent(this, NotificationView.class);
		// Send data to NotificationView Class
		intent.putExtra("title", strtitle);
		intent.putExtra("text", strtext);
		// Open NotificationView.java Activity
		PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);*/

		//Intent intento = new Intent(contexto, ActividadLoginUsuario.class);
		Intent intentRestart = contexto.getPackageManager().getLaunchIntentForPackage(contexto.getPackageName());
		//intentRestart.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Tarea que se ejecuta cuando se pulsa la notificación (en este caso Dummy)
        PendingIntent pIntent = PendingIntent.getActivity(
                contexto, 0, intentRestart, Intent.FLAG_ACTIVITY_CLEAR_TOP);
 
        // Inicializa notificacion
        Notification notification;

        // Dependiendo de la version del android crea la notificación
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
        	notification = new NotificationCompat.Builder(contexto)
	        .setSmallIcon(R.drawable.ic_launcher)
	        .setAutoCancel(true)
	        .setContentTitle(titulo)
			.setTicker(titulo)
	        .setContentIntent(pIntent)
  			.setDefaults(NotificationCompat.DEFAULT_ALL) 
	        .build();
	        
        	// Adiciona remoteViews y actualiza los textos
	        notification.bigContentView = remoteViews;
			remoteViews.setImageViewResource(R.id.imagen_logo, R.drawable.ic_launcher);
			remoteViews.setTextViewText(R.id.valor_titulo, titulo);
			remoteViews.setTextViewText(R.id.valor_nombrecliente, nombreCliente);
			remoteViews.setTextViewText(R.id.valor_fechaCreacion, fechaCreacion);
			remoteViews.setTextViewText(R.id.valor_serialMontacarga, serialMontacarga);
        } else {
			notification = new NotificationCompat.Builder(contexto)
	        .setSmallIcon(R.drawable.ic_launcher)
	        .setAutoCancel(true)
	        .setContentTitle(titulo)
			.setTicker(titulo)
			.setSubText(serialMontacarga)
			.setContentText(nombreCliente)
	        .setContentIntent(pIntent)
  			.setDefaults(NotificationCompat.DEFAULT_ALL) 
			.setStyle(new NotificationCompat.BigTextStyle().bigText(
					nombreCliente +  "\n" + fechaCreacion + "\n" + serialMontacarga)).build();
        }
        
		// Muestra la notificacion
		NotificationManager notificationmanager = (NotificationManager) contexto.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationmanager.notify(numeroNotificacion, notification);
	}
}
