package com.gc.ordenesservicio.objetos;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;

import com.gc.coregestionclientes.objetos.Formulario;

public class OrdenServicio {
	// Constantes
	public static String PENDIENTE 	= "PENDIENTE";
	public static String ENCURSO 	= "EN CURSO";
	public static String CANCELADA 	= "CANCELADA";
	public static String FINALIZADA	= "FINALIZADA";

	// Atributos
	private int id = -1;
	private int idCliente = -1;
	private long unixFechaCreacion = -1;
	private long unixFechaFinalizacion = -1;
	private String estado = null;
	
	// Auxiliares
	private Formulario formulario;
	private String datosAdicionales;
	private int idVisitaActiva = -1;
	private List<VisitaOS> visitas = new ArrayList<VisitaOS>();
	private int idUsuarioCreacion;
	private String justificacion;
	private String observacion;
	
	//Atributo nuevo para el seteo de información de Agencia Alemana
	private LinkedHashMap <String, String>  datosEspecCliente;
	


	// Constructor
	public OrdenServicio(int id, int idCliente, long unixFechaCreacion, String estado, String datosAdicionales, int idUsuarioCreacion) {
		this.id = id;
		this.idCliente = idCliente;
		this.unixFechaCreacion = unixFechaCreacion;
		this.estado = estado;
		this.datosAdicionales = datosAdicionales;
		this.idUsuarioCreacion = idUsuarioCreacion;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param datosEspecCliente the id to set
	 */
	
	public void setDatosEspecCliente( LinkedHashMap <String, String>  datosEspecCliente) {
		this.datosEspecCliente = datosEspecCliente;
	}
	/**
	 * @return the id
	 */
	public LinkedHashMap <String, String>  getDatosEspecCliente() {
		return datosEspecCliente;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the idCliente
	 */
	public int getIdCliente() {
		return idCliente;
	}

	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	/**
	 * @return the unixFechaCreacion
	 */
	public long getUnixFechaCreacion() {
		return unixFechaCreacion;
	}

	/**
	 * @param unixFechaCreacion the unixFechaCreacion to set
	 */
	public void setUnixFechaCreacion(long unixFechaCreacion) {
		this.unixFechaCreacion = unixFechaCreacion;
	}

	/**
	 * @return the unixFechaFinalizacion
	 */
	public long getUnixFechaFinalizacion() {
		return unixFechaFinalizacion;
	}

	/**
	 * @param unixFechaFinalizacion the unixFechaFinalizacion to set
	 */
	public void setUnixFechaFinalizacion(long unixFechaFinalizacion) {
		this.unixFechaFinalizacion = unixFechaFinalizacion;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the formulario
	 */
	public Formulario getFormulario() {
		return formulario;
	}

	/**
	 * @param formulario the formulario to set
	 */
	public void setFormulario(Formulario formulario) {
		this.formulario = formulario;
	}

	/**
	 * @return the datosAdicionales
	 */
	public String getDatosAdicionales() {
		return datosAdicionales;
	}

	/**
	 * @param datosAdicionales the datosAdicionales to set
	 */
	public void setDatosAdicionales(String datosAdicionales) {
		this.datosAdicionales = datosAdicionales;
	}
	
	/**
	 * @return the visitaActiva
	 */
	public VisitaOS getVisitaActiva() {
		for(int i = 0; i < visitas.size(); i++){
			if(visitas.get(i).getId() == idVisitaActiva)
				return visitas.get(i);
		}
		return null;
	}

	/**
	 * @param idVisitaActiva the visitaActiva to set
	 */
	public void setIdVisitaActiva(int idVisitaActiva) {
		this.idVisitaActiva = idVisitaActiva;
	}

	/**
	 * @return the visitas
	 */
	public List<VisitaOS> getVisitas() {
		return visitas;
	}

	/**
	 * @param visitas the visitas to set
	 */
	public void setVisitas(List<VisitaOS> visitas) {
		this.visitas = visitas;
	}
	
	// Permite adicionar una visita al arreglo
	public void adicionarVisita(VisitaOS visita) {
		this.visitas.add(visita);
	}
	
	// Permite obtener una visita según su id
	public VisitaOS obtenerVisita(int idVisita) {
		return visitas.get(idVisita);
	}

	/**
	 * @param idUsuarioCreacion
	 */
	// Permite setear id de usaurio
	public void setIdUsuarioCreacion (int idUsuarioCreacion) {
		this.idUsuarioCreacion=idUsuarioCreacion;
	}
	
	// permite obtener idUsuarioCreacion
	public int getIdUsuarioCreacion(){
		return idUsuarioCreacion;
	}
	
	/**
	 * @param justificacion 
	 */
	public void setJustificacion (String justificacion) {
		this.justificacion=justificacion;
	}
	
	// permite obtener idUsuarioCreacion
	public String getJustificacion(){
		return justificacion;
	}
	
	/**
	 * @param observacion 
	 */
	public void setObservacion (String observacion) {
		this.observacion=observacion;
	}
	
	// permite obtener idUsuarioCreacion
	public String getObservacion(){
		return observacion;
	}
}
