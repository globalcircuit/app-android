package com.gc.ordenesservicio.objetos;

import java.util.List;

import com.gc.coregestionclientes.objetos.Formulario;
import com.gc.ordenesservicio.AplicacionOrdenesServicio;

public class ControladorOrdenServicio {

	public static String generarJson(OrdenServicio ordenServicio, AplicacionOrdenesServicio aplicacion){
		String json = "";
		
		// Verifica si existe ordenServicio
		if (ordenServicio == null) return null;
		
		int idOrdenServicio = ordenServicio.getId();
		int idUsuario = ordenServicio.getIdUsuarioCreacion();
		long unixFechaInicio = ordenServicio.getUnixFechaCreacion();
		long unixFechaFin = ordenServicio.getUnixFechaFinalizacion();
		String observacion = ordenServicio.getObservacion();
		String justificacion = ordenServicio.getJustificacion();
		String estado = ordenServicio.getEstado();
		Formulario formulario = ordenServicio.getFormulario();
		
		if(formulario == null) return null;
		
		// Comienza a construir el JSON
		
		StringBuilder ordenJson = new StringBuilder(json); //TODO:
		ordenJson.append("{");
		ordenJson.append("\"idOrdenServicio\":" + idOrdenServicio + ",");
		ordenJson.append("\"idUsuarioCreacion\":" + idUsuario + ",");
		ordenJson.append("\"unixFechaCreacion\":" + unixFechaInicio + ",");
		ordenJson.append("\"unixFechaFinalizacion\":" + unixFechaFin + ",");
		ordenJson.append("\"observacion\":\"" + observacion + "\",");
		ordenJson.append("\"justificacion\":\"" + justificacion + "\",");
		ordenJson.append("\"estado\":\"" + estado + "\",");
		ordenJson.append("\"formulario\": {");
		
		ordenJson.append(formulario.generarJson(aplicacion.getConfiguracionApp()));
		
		ordenJson.append("},");
		
		// Se arma el Json de la visitas
		ordenJson.append("\"visitas\": [");
		
		// Se obtiene las visitas de la orden
		List<VisitaOS> visitas = ordenServicio.getVisitas();
		
		// Valida si la orden tiene visitas
		if(visitas != null){
			int numeroVisitas = visitas.size();
			
			//Se recorre las visitas de la orden
			for(int l = 0;l<numeroVisitas;l++){
				VisitaOS visita = visitas.get(l);
				if(l>0) ordenJson.append(",");
				
				String visitaJson = ControladorVisitaOS.generarJson(visita, aplicacion);
				ordenJson.append(visitaJson);
			}
		}
		
		
		ordenJson.append("]");
		ordenJson.append("}");
		
		return ordenJson.toString();
	}
}
