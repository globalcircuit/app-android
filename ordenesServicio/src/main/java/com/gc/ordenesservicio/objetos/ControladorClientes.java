package com.gc.ordenesservicio.objetos;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Base64;
import android.util.Log;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

import com.gc.coregestionclientes.Configuracion;
import com.gc.coregestionclientes.accesodatos.ConsumeWebService;
import com.gc.coregestionclientes.accesodatos.CustomSQLiteRoutines;
import com.gc.coregestionclientes.librerias.JSONManager;
import com.gc.coregestionclientes.librerias.ParseJSON;
import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.objetos.Campo;
import com.gc.coregestionclientes.objetos.Cliente;
import com.gc.coregestionclientes.objetos.Formulario;
import com.gc.ordenesservicio.AplicacionOrdenesServicio;
import com.gc.ordenesservicio.R;
import com.gc.ordenesservicio.accesodatos.SQLiteRoutinesOS;

public class ControladorClientes {
	
	// Parametros
	private List<Cliente> clntes;
	private SQLiteRoutinesOS sqliteRoutines;
	private ToolsApp toolsApp = new ToolsApp();
	
	// Constructor
	public ControladorClientes (SQLiteRoutinesOS sqliteRoutines) {
		this.sqliteRoutines = sqliteRoutines;
	}
	public ControladorClientes () {}
	
	/*******************************************************************/
	// MÉTODOS PARA CARGA Y VISUALIZACIÓN DE CLIENTES Y DATOS DE CLIENTE
	/*******************************************************************/
	
	// DEPRECATED: Realiza la asignación de los clientes desde un JSON Web
	public void asignarClientesWeb(JSONManager jsonManager){
		try {
			JSONArray jsonArray = jsonManager.getRegistros(0);
			int nmro_clntes = jsonArray.length();
			this.clntes = new ArrayList<Cliente>(nmro_clntes);
			
			// Recorre los formularios
			for (int i = 0; i < nmro_clntes; i++) {
				// Extrae el cliente JSON del JSONArray
				final JSONObject clnte = jsonArray.getJSONObject(i);
				
				// Extrae los parametros del cliente
				int id = Integer.parseInt(clnte.getString("Id"));
				String cdgo = clnte.getString("Codigo");
				String nmbres = clnte.getString("Nombres");
				float lttud = Float.parseFloat(clnte.getString("Latitud"));
				float lngtud = Float.parseFloat(clnte.getString("Longitud"));
				boolean actvo = toolsApp.bitToBoolean(clnte.getInt("Activo"));
				
				// Crea el cliente			
				this.clntes.add( new Cliente(id, cdgo, nmbres, lttud, lngtud, actvo, null));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	// Realiza la asignacion de los clientes desde la BD Sqlite
	public void asignarClientesLocales(CustomSQLiteRoutines customSqliteRoutines){
		this.clntes = sqliteRoutines.routine_consultarClientesLocales();
	}
	
	// Permite obtener la lista de clientes
	public List<Cliente> getListaClientes() {
		return clntes;
	}
	
	// Permite visualizar los datos de un cliente
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void visualizarDatosCliente(Context context, TableLayout cntndor_dtos, Cliente clnte) {	
		
		// Parametros generales para los texview
		TableRow.LayoutParams params = new TableRow.LayoutParams(
		        LayoutParams.WRAP_CONTENT,      
		        LayoutParams.WRAP_CONTENT
		);
		params.setMargins(5, 0, 5, 0);
		
		// Extrae los datos adicionales del cliente
		String dtos_adcnles = clnte.getDatosAdicionales();
		
		// Verifica si existen datos adicionales
		if(dtos_adcnles != null && !dtos_adcnles.equals("")){
			// Parsea el JSON y retorna un hashmap
			LinkedHashMap<String, Object> datos = (LinkedHashMap<String, Object>)(
					new ParseJSON(dtos_adcnles.getBytes())
			).datosJSON();
			
			// Recorre el HashMap
			Iterator<?> it = datos.entrySet().iterator();
			while (it.hasNext()) {
				LinkedHashMap.Entry pairs = (LinkedHashMap.Entry)it.next();
				
				// Extrae el prmtro
				String nmbre_prmtro = (String) pairs.getKey();	
				String vlor_prmtro = (String) pairs.getValue();
	
				// Crea una nueva fila
				TableRow fla = new TableRow(context);
			
				// Revisa si el nombre del parámetro es un separador
				if(nmbre_prmtro.equals("Separador")){
					TextView textview = new TextView(context);
					textview.setTextAppearance(context, R.style.titulo_etiqueta_oscuro);
					textview.setText("--------------------");
					fla.addView(textview);
				}else{			
					// Crea el Label del parametro
					TextView textview = new TextView(context);
					textview.setTextAppearance(context, R.style.titulo_etiqueta_oscuro);
					textview.setText(nmbre_prmtro+":");
					textview.setTypeface(null, Typeface.BOLD);
					fla.addView(textview);
					
					// Crea valor
					TextView textview_vlor = new TextView(context);
					textview_vlor.setTextAppearance(context, R.style.valor_etiqueta_oscuro);
					textview_vlor.setText(vlor_prmtro);
					textview_vlor.setLayoutParams(params);
					fla.addView(textview_vlor);
				}
				
				// Adiciona la fila al contenedor
				cntndor_dtos.addView(fla);
				
				it.remove(); // avoids a ConcurrentModificationException
			}
		}else{
			// No hay datos adicionales
			TextView textview = new TextView(context);
			textview.setTextAppearance(context, R.style.titulo_etiqueta_oscuro);
			textview.setText("No hay datos adicionales.");
			cntndor_dtos.addView(textview);
			toolsApp.showLongMessage("No hay datos adicionales!", context);
		}
		
		// Extrae la programación de visita del cliente
		/*VisitaProgramada vsta_prgrmda = clnte.getVisitaProgramadaPendiente();
		
		// Verifica si existe
		if(vsta_prgrmda != null){
			// Existe una programación de visita
			// Parsea el JSON y retorna un hashmap
			String dtos_prgrmcion = vsta_prgrmda.toJSONString();
			LinkedHashMap<String, Object> hashmap_prgrmcion = (LinkedHashMap<String, Object>)(
					new ParseJSON(dtos_prgrmcion.getBytes())
			).datosJSON();
			
			// Recorre el HashMap
			Iterator<?> it = hashmap_prgrmcion.entrySet().iterator();
			while (it.hasNext()) {
				LinkedHashMap.Entry pairs = (LinkedHashMap.Entry)it.next();
				
				// Extrae el prmtro
				String nmbre_prmtro = (String) pairs.getKey();	
				String vlor_prmtro = (String) pairs.getValue();
	
				// Crea una nueva fila
				TableRow fla = new TableRow(context);
		
				// Crea el Label del parametro
				TextView textview = new TextView(context);
				textview.setText(nmbre_prmtro+":");
				textview.setTypeface(null, Typeface.BOLD);
				fla.addView(textview);
				
				// Crea valor
				TextView textview_vlor = new TextView(context);
				textview_vlor.setText(vlor_prmtro);
				textview_vlor.setLayoutParams(params);
				fla.addView(textview_vlor);
				
				// Adiciona la fila al contenedor
				cntndor_dtos_prgrmcion.addView(fla);
				
				it.remove(); // avoids a ConcurrentModificationException
			}
		} else {
			// No existe una programación
			TextView textview = new TextView(context);
			textview.setText("No hay información disponible.");
			cntndor_dtos_prgrmcion.addView(textview);
		}*/
	}

	/*************************************************/
	// MÉTODOS PARA LA CREACIÓN O ACTUALIZACIÓN DE CLIENTES
	/*************************************************/
	// Cosulta los clientes creados localmente que aún no se han enviado
	// mdo_clnslta = 1: Consulta todos los clientes pendientes
	// mdo_clnslta = 2: Consulta el cliente especificiado
	public List<Cliente> consultarClientesNoSincronizados(int mdo_clnslta, Cliente clnte){
		List<Cliente> clntes = sqliteRoutines.routine_consultarClientesNoSincronizados(mdo_clnslta, clnte);
		return clntes ;
	}
	
	// Método que permite desactivar un cliente
	public void desactivarCliente(int id_clnte) {
		sqliteRoutines.routine_desactivarCliente(id_clnte);
		Log.i(AplicacionOrdenesServicio.tag_app, "Cliente Desactivado!: " + id_clnte);
	}
	
	// Método que permite marcar un cliente creado localmente como sincronizado
	public void clienteSincronizado(int id_clnte) {
		sqliteRoutines.routine_actualizarBanderaSincronizacionCliente(id_clnte);
		Log.i(AplicacionOrdenesServicio.tag_app, "Cliente Sincronizado!: " + id_clnte);
	}
	
	// Método que permite crear un cliente local
	public void asignarDatosCliente(Cliente clnte, Formulario frmlrio) {
		// TODO: Hacerlo generico ....
		Campo nmbres = frmlrio.getCampos().get(0);
		clnte.setNombres(nmbres.getValor());
		sqliteRoutines.routine_crearCliente(clnte);
		//return clnte;
	}
	
	// Método que permite enviar los datos a la nube
	public void enviarDatosCliente(Context context, Configuracion configApp, Cliente clnte, boolean showProgress) {
		try {	
			// Consumir WEBService
			ConsumeWebService consumeWS =  new ConsumeWebService(context);
			Hashtable<String, String> prmtros_adcnles = new Hashtable<String, String>();
			prmtros_adcnles.put("Seudonimo", configApp.getUsuario().getSeudonimo());
			prmtros_adcnles.put("IdClienteGlobal", String.valueOf(configApp.getUsuario().getIdClienteGlobal()));
			prmtros_adcnles.put("IdDispositivo", String.valueOf(configApp.getDispositivo().getId()));
			prmtros_adcnles.put("IdUsuario", String.valueOf(configApp.getUsuario().getId()));

			// TODO generar un string json con los datos basicos.
	    	String dtos_clnte = Base64.encodeToString(clnte.toJSONString().getBytes("ISO-8859-1"), Base64.DEFAULT);
			prmtros_adcnles.put("DatosCliente", dtos_clnte);
			if(showProgress){
				consumeWS.iniciarProceso(
						"crearCliente", prmtros_adcnles, true, "Enviando Datos", "callback_crearActualizarCliente");
			}else{
				consumeWS.iniciarProceso(
						"crearCliente", prmtros_adcnles, false, null, "callback_crearActualizarCliente");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
