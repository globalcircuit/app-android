package com.gc.ordenesservicio.objetos;

import com.gc.coregestionclientes.objetos.Formulario;
import com.gc.ordenesservicio.AplicacionOrdenesServicio;

public class ControladorVisitaOS {

	public static String generarJson(VisitaOS visita, AplicacionOrdenesServicio aplicacion){
		String json = "";
		
		// Verifica si existe visita
		if (visita == null) return null;
		
		int idVisita = visita.getId();
		int idUsuario = visita.getIdUsuario();
		long unixFechaInicio = visita.getUnixFechaHoraIncio();
		long unixFechaFin = visita.getUnixFechaHoraFin();
		Formulario formulario = visita.getFormulario();
		
		if(formulario == null) return null;
		
		// Comienza a construir el JSON
		
		StringBuilder visitaJson = new StringBuilder(json); //TODO:
		visitaJson.append("{");
		visitaJson.append("\"idVisita\":" + idVisita + ",");
		visitaJson.append("\"idUsuario\":" + idUsuario + ",");
		visitaJson.append("\"unixFechaInicio\":" + unixFechaInicio + ",");
		visitaJson.append("\"unixFechaFin\":" + unixFechaFin + ",");
		visitaJson.append("\"formulario\": {");
		
		visitaJson.append(formulario.generarJson(aplicacion.getConfiguracionApp()));
		
		visitaJson.append("}");
		visitaJson.append("}");
		
		return visitaJson.toString();
	}
	
}
