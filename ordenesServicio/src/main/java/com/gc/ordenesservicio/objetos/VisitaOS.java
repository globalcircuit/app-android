package com.gc.ordenesservicio.objetos;

import com.gc.coregestionclientes.objetos.Formulario;
import com.gc.coregestionclientes.objetos.Visita;

public class VisitaOS extends Visita {

	// Atributos
	private int idOrdenServicio = -1;

	// Auxiliares
	private Formulario formulario;
	private boolean datos = false;
	private boolean modoEdicion = false;
	private String estadoVisita;

	// Constructor
	public VisitaOS(int id, int idUsuario, long unixFechaHoraInicio, int idOrdenServicio, Formulario formulario) {
		super(id, idUsuario, unixFechaHoraInicio);
		this.idOrdenServicio = idOrdenServicio;
		this.formulario = formulario;
	}

	/**
	 * @return the idOrdenServicio
	 */
	public int getIdOrdenServicio() {
		return idOrdenServicio;
	}

	/**
	 * @param idOrdenServicio the idOrdenServicio to set
	 */
	public void setIdOrdenServicio(int idOrdenServicio) {
		this.idOrdenServicio = idOrdenServicio;
	}

	/**
	 * @return the Formulario
	 */
	public Formulario getFormulario() {
		return formulario;
	}

	/**
	 * @param formulario the formulario to set
	 */
	public void setFormulario(Formulario formulario) {
		this.formulario = formulario;
	}

	/**
	 * @param datos the datos to set
	 */
	public void setDatos(boolean datos) {
		this.datos = datos;
	}

	/**
	 * @return the datos
	 */
	public boolean contieneDatos() {
		return datos;
	}

	/**
	 * @return the modoEdicion
	 */
	public boolean getModoEdicion(){
		return modoEdicion;
	}

	/**
	 * @param modoEdicion the modoEdicion to set
	 */
	public void setModoEdicion(boolean modoEdicion) {
		this.modoEdicion = modoEdicion;
	}

	public String getEstadoVisita() {
		return estadoVisita;
	}

	public void setEstadoVisita(String estadoVisita) {
		this.estadoVisita = estadoVisita;
	}
}