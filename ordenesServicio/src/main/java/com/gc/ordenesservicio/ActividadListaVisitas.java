package com.gc.ordenesservicio;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.BatteryManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.objetos.Cliente;
import com.gc.ordenesservicio.accesodatos.CustomSQLiteRoutinesOS;
import com.gc.ordenesservicio.accesodatos.SQLiteRoutinesOS;
import com.gc.ordenesservicio.objetos.OrdenServicio;

public class ActividadListaVisitas extends Activity {
	
	// Objetos
	private AplicacionOrdenesServicio aplicacion;
	private ToolsApp toolsApp = new ToolsApp();
	private CustomAdapterOrdenes adapter;
	private OrdenServicio ordenSeleccionada;
	
	//Nuevo LinkedHashmap para mostrar los datos de las órdenes de servicio
	private Vector < LinkedHashMap <String, String> > arrayOrdenesxVisitas;
	
	//Objeto lista de órdenes de servicio que será seteado en iniciarOrdenes()
	private List <OrdenServicio> listaOrdenesServicio;
	
	// Elementos Gráficos y Menu
	private EditText campo_filtroVisitas;
	private ListView lista_ordenesVisitas;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.actividad_lista_visitas);

		// Inicializa el Objeto con la configuracion de la App para ser utilizado
		aplicacion = ((AplicacionOrdenesServicio)getApplicationContext());

		// Inicializa elementos gráficos
		campo_filtroVisitas = (EditText) findViewById(R.id.campo_filtro_visitas);
		campo_filtroVisitas.addTextChangedListener(filterTextWatcher);
	
		// Inicializa el objeto de la aplicación
		aplicacion = ((AplicacionOrdenesServicio) getApplicationContext());
		
		lista_ordenesVisitas = (ListView)findViewById(R.id.lista_ordenes_visitas);
		lista_ordenesVisitas.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// Extrae el objeto seleccionado
				LinkedHashMap<String, String> item = (LinkedHashMap<String, String>)lista_ordenesVisitas.getAdapter().getItem(position);
				ordenSeleccionada = seleccionarOrdenServicio( Integer.parseInt( arrayOrdenesxVisitas.get(position).get("Orden #") ) );

				//Poner if del null
				// Consulta la programación de visitas de una orden de servicio para, posteriormente, seteársela a una orden
				LinkedHashMap <String, String> datosEspecificos = 
						((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
						.routine_obtenerProgramacionVisita( arrayOrdenesxVisitas.get(position).get("CódigoOT"), arrayOrdenesxVisitas.get(position).get("Código Visita") );
				
				//Seteo de la programacion a la orden de servicio seleccionada
				ordenSeleccionada.setDatosEspecCliente(datosEspecificos);
								
				// Coloca la orden activa a nivel de aplicacion
				aplicacion.setOrdenServicioActiva(ordenSeleccionada);
				
				// Abre la orden de servicio
				abrirActividadOrdenServicio(ordenSeleccionada);				
			}			
		});
		registerForContextMenu(lista_ordenesVisitas);
	}
	
	private OrdenServicio seleccionarOrdenServicio( int id ){
		OrdenServicio orden = null;
		
		Iterator<OrdenServicio> it= listaOrdenesServicio.iterator();
		 
		while(it.hasNext()) {
		 
			OrdenServicio os = it.next();
			int idOrden = os.getId();
			if ( idOrden == id) {
				orden = os;
			}
		}
		return orden;
	}
	
	public void actualizar(View view){

		//Calendario para la fecha de los hitos
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String fecha = sdf.format(cal.getTime());
		String toLog = "Hundí en las flechas de actualizar a las " + fecha;
		toolsApp.generaLog(toLog);

    	// Solicita reinicio de la app para reenvío de información nueva del servidor
    	Intent intentRestart = 
    		getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
    	intentRestart.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intentRestart);

		finish();
	}

	@Override protected void onStop() {
		super.onStop();

		// Salva el contexto de los objetos
		aplicacion.salvarObjetosContexto(false);
	};
	
	@Override
	protected void onResume() {
		super.onResume();
		
		// Lista las ordenes de servicio
		inicializarListaOrdenes();

		// Verifica la existencia de una orden de servicio activa en memoria.
		OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();
		if (ordenServicio != null) {
			// Existe la orden, redirecciona a la actividad de orden
			abrirActividadOrdenServicio(ordenServicio);
		}
	}
	
	@Override
	public void finish() {
		super.finish();
	}
	
	@Override
	protected void onDestroy() {
		campo_filtroVisitas.removeTextChangedListener(filterTextWatcher);
		super.onDestroy();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	// TextWatcher para filtrar las ordenes
	private TextWatcher filterTextWatcher = new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			// Permite filtrar el TextView a medida que va cambiando
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if(adapter != null){
					adapter.getFilter().filter(s);
				}
			}
		};
		
		// Inicializa las órdenes de servicio con sus visitas-
		private void inicializarListaOrdenes() {
			
			// Consultar ordenes de servicio
			listaOrdenesServicio = 
					((SQLiteRoutinesOS)aplicacion.getConfiguracionApp().getSqliteRoutines())
					.routine_consultarOrdenesServicio(
							(CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines(),
							aplicacion.getConfiguracionApp());

			// Se obtiene el id cliente global del usuario
			int idClienteGlobal = aplicacion.getConfiguracionApp().getUsuario().getIdClienteGlobal();

			// Consultar ordenes las órdenes x visitas
			arrayOrdenesxVisitas =
					((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
							.routine_consultarOrdenesPersonalizado(idClienteGlobal);
					
			// Inicializa la lista de órdenes de servicio
			if(listaOrdenesServicio != null && listaOrdenesServicio.size() > 0){
				adapter = new CustomAdapterOrdenes(this, arrayOrdenesxVisitas);
				
				//Aquí se añadiría la línea para un adaptador genérico, donde se piten las cosas mediantes llave-valor
				//Algo como adapter = new CustomAdapterOrdenesVisitas(this, ordenesServicio);
				
				lista_ordenesVisitas.setAdapter(adapter);
				Log.i(AplicacionOrdenesServicio.tag_app, "Órdenes cargadas con éxito!");
			} else {
				lista_ordenesVisitas.setAdapter(null);
				toolsApp.showLongMessage("No hay órdenes de servicio!", this);
			}
		}
		
		// Método que permite abrir la actividad con la información de la orden de servicio
		private void abrirActividadOrdenServicio(OrdenServicio ordenSeleccionada) {		
			// Consulta la informacion del cliente y lo setea a nivel de aplicacion
			Cliente cliente = ((SQLiteRoutinesOS)aplicacion.getConfiguracionApp().getSqliteRoutines())
					.routine_consultarCliente(ordenSeleccionada.getIdCliente(), null);
			if (cliente != null) {
				aplicacion.getConfiguracionApp().setClienteActivo(cliente);
				
				// Abre la Orden
				Intent intent = new Intent(this, ActividadOrdenServicio.class);
				startActivity(intent);
			} else {
				// Cliente no asignado
				toolsApp.alertDialogYES(this, null, 
					"El cliente asociado a la orden # " + ordenSeleccionada.getId() + " no está asignado.", null);
			}
		}
		
		// Clase customizda para la visualizacion de las órdenes
		private class CustomAdapterOrdenes extends BaseAdapter implements Filterable {	
			private Activity activity;
			private Vector<LinkedHashMap<String, String>> list;
			private LinkedHashMap<String, String> item;
			private LayoutInflater inflater = null;
			protected Vector<LinkedHashMap<String, String>> listaOrigen;

			// Constructor
			public CustomAdapterOrdenes(Activity activity, Vector<LinkedHashMap<String, String>> list) {
				this.activity = activity;
				this.list = list;
				this.inflater = (LayoutInflater) this.activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}

			// Retorna el número de items en la lista
			public int getCount() {
				return list.size();
			}

			// Permite obtener el objeto en la posición
			public Object getItem(int position) {
				item = list.get(position);
				return item;
			}

			// Permite obtener el estilo de layout previamente definido y coloca los
			// valores de la fila que serán mostrados
			public View getView(int position, View convertView, ViewGroup parent) {
				View vi = convertView;
				if (convertView == null)
					vi = inflater.inflate(R.layout.row_consulta_elementos, parent, false);
				
					
					// Obtiene los view donde será colocada la información
					LinearLayout row_rsmen_cntndor_infrmcion = (LinearLayout) vi.findViewById(R.id.row_rsmen_cntndor_infrmcion);
					row_rsmen_cntndor_infrmcion.removeAllViews();
					
					LinkedHashMap<String, String> item = list.get(position);
					
					// Recorre los campos del item
					// Recorre el HashMap
					Iterator<?> it = item.entrySet().iterator();
					int indice = 0;
					while (it.hasNext()) {
						@SuppressWarnings("rawtypes")
						LinkedHashMap.Entry atributo = (LinkedHashMap.Entry)it.next();
						
						// Extrae los datos
						String nombreCampo = (String) atributo.getKey();	
						String valor = (String) atributo.getValue();
	
						// Setea los valroes
						TextView textView = new TextView(ActividadListaVisitas.this);
						textView.setText(nombreCampo + ": " + valor);
						
						// Si es el primer TextView lo coloca en BOLD
						if (indice==0  ) {
							textView.setTextAppearance(ActividadListaVisitas.this, R.style.titulo_etiqueta_claro);
						} else if( indice == ( item.size() - 1)  ){
							textView.setTextAppearance(ActividadListaVisitas.this, R.style.valor_etiqueta_claro);
							textView.setGravity(Gravity.RIGHT);
						} else {
							textView.setTextAppearance(ActividadListaVisitas.this, R.style.valor_etiqueta_claro);
						}
						
						// Adiciona a la fila
						row_rsmen_cntndor_infrmcion.addView(textView);
	
						// Incrementa contador
						indice++;
					}
					// Retorno...
					return vi;
			}

			@Override
			public long getItemId(int position) {
				return position;
			}

			@Override
			public Filter getFilter() {
				return new Filter() {

					@Override
					protected FilterResults performFiltering(CharSequence constraint) {
						final FilterResults oReturn = new FilterResults();
						/*
						final FilterResults oReturn = new FilterResults();
						final ArrayList<OrdenServicio> listaFiltrada = new ArrayList<OrdenServicio>();
						if (listaOrigen == null){
							listaOrigen = list;
						}
						
						// Recorre la lista original y dependiendo del filtro aplica el criterio
						if (listaOrigen != null && listaOrigen.size() > 0) {
							for (OrdenServicio clnte : listaOrigen) {
								if (String.valueOf(clnte.getId())
										.contains(constraint.toString().toLowerCase(Locale.getDefault()))
								) {
									listaFiltrada.add(clnte);
								}
							}
						}

						oReturn.values = listaFiltrada;*/
						return oReturn;
					}

					@SuppressWarnings("unchecked")
					@Override
					protected void publishResults(CharSequence constraint,
							FilterResults results) {
						//list = (ArrayList<OrdenServicio>) results.values;
						//notifyDataSetChanged();
					}
				};
			}

			public void notifyDataSetChanged() {
				super.notifyDataSetChanged();
			}
			
			
		}

	// Método que permite iniciar la actividad para crear una orden.
	public void crearNuevaOrden(View view) {
		// TODO:
		Intent intent = new Intent(this, ActividadNuevaOrdenServicio.class);
		startActivity(intent);
		/*Intent intent = new Intent(this, ActividadListaClientes.class);
		startActivity(intent);*/
	}
}
