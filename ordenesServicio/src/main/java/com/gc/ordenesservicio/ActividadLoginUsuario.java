package com.gc.ordenesservicio;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.objetos.AuditorFormulario;
import com.gc.coregestionclientes.objetos.ControladorFormularios;
import com.gc.ordenesservicio.novedades.ModuloSeguimiento;
import com.gc.ordenesservicio.novedades.ServicioNovedades;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class ActividadLoginUsuario extends Activity {
	
	// Objetos
	private AplicacionOrdenesServicio aplicacion;
	private EditText etLogin;
	private EditText etPass;
	private ToolsApp toolsApp = new ToolsApp();
	private ControladorFormularios controladorFormularios = new ControladorFormularios();
	private AuditorFormulario auditorFormulario;
	private HashMap<String, View> contenedoresCamposHash = new HashMap<String, View>();
	//private LayoutInflater inflater = null;
	private View layoutView = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.actividad_login_usuario);
		layoutView = getWindow().getDecorView().findViewById(android.R.id.content);

		// Inicializa el Objeto con la configuracion de la App para ser utilizado
		aplicacion = ((AplicacionOrdenesServicio)getApplicationContext());
		
		//Se ejecuta el método que me calcula el nivel de batería y da un mensaje tipo TOAST
		nivelBateria();
		
		//Inicialización de variables.
		etLogin = (EditText)findViewById(R.id.etUsuario);
        etPass = (EditText)findViewById(R.id.etPass);
        
        //Set de variables
        etLogin.setText( aplicacion.getConfiguracionApp().getUsuario().getLogin() );
	}
	
	//Método que me calculo el nivel de batería y me muestra un mensaje
	//dependiendo si está por debajo o por arriba del 20%
	//Se debe implementar lo que se quiera que se haga cuando esté
	//por debajo del 20%
    public void nivelBateria () 
    { 
        try 
        { 
           IntentFilter batIntentFilter = 
              new IntentFilter(Intent.ACTION_BATTERY_CHANGED); 
            Intent battery = 
               this.registerReceiver(null, batIntentFilter); 
            int nivelBateria = battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1); 
            
            //Si nivel de batería es inferior a 20, se muestra el mensaje de cargar.
            if( nivelBateria < 20 ){
                Toast.makeText(this, "Por favor, cargue su celular", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this, "El nivel está bien", Toast.LENGTH_LONG).show();
            }

        } 
        catch (Exception e) 
        {            
           Toast.makeText(getApplicationContext(), 
                    "Error al obtener estado de la batería", 
                    Toast.LENGTH_SHORT).show(); 
        }
    }

	@Override protected void onStop() {
		super.onStop();
		/*
		// Se valida que formulario no este vacio
		if(aplicacion.getOrdenServicioActiva() != null) {
			if(aplicacion.getOrdenServicioActiva().getVisitaActiva() != null) {
				if(aplicacion.getOrdenServicioActiva().getVisitaActiva().getFormulario() != null) {
					aplicacion.getOrdenServicioActiva().getVisitaActiva().getFormulario().banderaGuardadoTemporal = true;
					// error aqui guardarDatosVisita()
					if (guardarDatosVisita()) {
						// Salva el contexto de los objetos
						aplicacion.salvarObjetosContexto(true);

						//Manejo de string para pintar en el log
						int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
						//Calendario para la fecha de los hitos
						Calendar cal = Calendar.getInstance();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
						String fecha = sdf.format(cal.getTime());
						String toLog = "Entré a onStop de una visita a las " + fecha + " con el idOrden " + idOrdenServicioLog;
						toolsApp.generaLog(toLog);

					}
				}
			}

		}
		// Salva el contexto de los objetos
		aplicacion.salvarObjetosContexto(true);
		*/
	};

	public void verificar(View v) {
		String user = etLogin.getText().toString();
		String clave = etPass.getText().toString();
		//Si la clave del usuario es igual a la clave digitada en el editText de password
		//accede a la siguiente actividad
		if ( clave.equals(aplicacion.getConfiguracionApp().getUsuario().getPass() ) && user.equals( aplicacion.getConfiguracionApp().getUsuario().getLogin() ) ) {
			//Intent i = new Intent(this, ActividadListaOrdenesServicio.class);
			Intent intentoServicioNovedades = new Intent(this, ServicioNovedades.class);
			intentoServicioNovedades.putExtra("tipoIntent", "novedades");
			startService(intentoServicioNovedades);
			//Calendario para la fecha de los hitos
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String fecha = sdf.format(cal.getTime());
			String toLog = "Me loggié a las " + fecha;
			toolsApp.generaLog(toLog);

			Intent i = new Intent(this, ActividadListaVisitas.class);
			startActivity(i);
		}else{
			Toast.makeText(this, "Contraseña o usuario incorrecto", Toast.LENGTH_LONG).show();
	        etLogin.setText( aplicacion.getConfiguracionApp().getUsuario().getLogin() );
			etPass.setText("");
		}
	}

	// Guarda los datos en el objeto
	private boolean guardarDatosVisita() {
		// Asigna los valores del formulario
		boolean asignacionExitosa = controladorFormularios.interfazAFormulario(
				this, aplicacion.getConfiguracionApp(), true, auditorFormulario,
				layoutView, contenedoresCamposHash);

		// Verifica si la asignación fue exitosa
		if(asignacionExitosa){

			int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
			//Calendario para la fecha de los hitos
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String fecha = sdf.format(cal.getTime());
			String toLog = "guardé el formulario a las " + fecha + " con el idOrden " + idOrdenServicioLog;
			toolsApp.generaLog(toLog);

			// Exitosa,
			aplicacion.getOrdenServicioActiva().getVisitaActiva().setDatos(true);
			return true;
		}else{
			//Manejo de string para pintar en el log
			int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
			//Calendario para la fecha de los hitos
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String fecha = sdf.format(cal.getTime());
			String toLog = "No pude guardar el formulario a las " + fecha + " con el idOrden " + idOrdenServicioLog;
			toolsApp.generaLog(toLog);
		}
		return false;
	}
}
