package com.gc.ordenesservicio;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gc.coregestionclientes.interfaces.OnTaskCompleted;
import com.gc.coregestionclientes.librerias.JSONManager;
import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.objetos.Campo;
import com.gc.coregestionclientes.objetos.ControladorFormularios;
import com.gc.coregestionclientes.objetos.ControladorTalonarios;
import com.gc.coregestionclientes.objetos.Formulario;
import com.gc.ordenesservicio.accesodatos.CustomSQLiteRoutinesOS;
import com.gc.ordenesservicio.accesodatos.SQLiteRoutinesOS;
import com.gc.ordenesservicio.novedades.ModuloSeguimiento;
import com.gc.ordenesservicio.novedades.ServicioNovedades;
import com.gc.ordenesservicio.objetos.AuditorFormularioOS;
import com.gc.ordenesservicio.objetos.AutocompletarCampoOS;
import com.gc.ordenesservicio.objetos.OrdenServicio;
import com.gc.ordenesservicio.objetos.VisitaOS;

public class ActividadOrdenServicio extends FragmentActivity implements OnTaskCompleted {

	// Objetos
	private AplicacionOrdenesServicio aplicacion;
	private ControladorTalonarios controladorTalonarios;
	private ToolsApp toolsApp = new ToolsApp();
	private ControladorFormularios controladorFormularios = new ControladorFormularios();
	private AuditorFormularioOS auditorFormulario;
	private Formulario formulario;
	private OrdenServicio ordenServicio;
	private VisitaOS visita;

	// Elementos gráficos
	private Button boton_iniciar_orden;
	private Button boton_finalizar_orden;

	private Button boton_detalle;

	private TextView lbel_nmbre_clnte;
	private LinearLayout contenedor_ordenServicio;
	private LinearLayout contenedor_visitasOrdenServicio;
	//private LayoutInflater inflater = null;
	private View layoutView = null;
	private MenuItem menuItemVerHistorial;

	// Objetos auxiliares
	private HashMap<String, Campo> camposHash = new HashMap<String, Campo>();
	private HashMap<String, View> contenedoresCamposHash = new HashMap<String, View>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*inflater = (LayoutInflater) (ActividadOrdenServicio.this).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutView = inflater.inflate(R.layout.actividad_orden_servicio, null);*/
		setContentView(R.layout.actividad_orden_servicio);
		layoutView = getWindow().getDecorView().findViewById(android.R.id.content);

		// Inicializa elementos gráficos
		boton_iniciar_orden = (Button)findViewById(R.id.boton_iniciar_orden);

		boton_finalizar_orden = (Button)findViewById(R.id.boton_finalizar_orden);
		boton_finalizar_orden.setEnabled(true);

		boton_detalle = (Button)findViewById(R.id.boton_detalle_visita);
		boton_detalle.setEnabled(false);

		//Cosas para probar:
		//boton_iniciar_orden.setEnabled(true);
		boton_iniciar_orden.setVisibility((View.GONE));
		boton_detalle.setVisibility((View.GONE));
		//boton_iniciar_orden.setVisibility((View.GONE));


		contenedor_ordenServicio = (LinearLayout)findViewById(R.id.contenedor_ordenServicio);
		contenedor_visitasOrdenServicio = (LinearLayout) findViewById(R.id.contenedor_visitasOrdenServicio);

		// Inicializa el objeto de la aplicación y controlador de ordenes de servicio
		aplicacion = ((AplicacionOrdenesServicio) getApplicationContext());

		// Inicializa el nombre del cliente
		lbel_nmbre_clnte = (TextView)findViewById(R.id.lbel_nmbre_clnte);
		lbel_nmbre_clnte.setText(aplicacion.getConfiguracionApp().getClienteActivo().getNombres());

		// Verifica la existencia de la orden de servicio.
		ordenServicio = aplicacion.getOrdenServicioActiva();
		if (ordenServicio != null) {
			// Existe la orden, verifica la existencia de una visita activa
			if(ordenServicio.getVisitaActiva() != null){
				// Valida que el formulario contenga informaciòn

				// Obtiene la bandera de guardado temporal
				//boolean visitaTemporal = ordenServicio.getVisitaActiva().getFormulario().banderaGuardadoTemporal;
				if(ordenServicio.getVisitaActiva().getFormulario()!= null && ordenServicio.getVisitaActiva().getFormulario().banderaGuardadoTemporal) {
					// llamo visita y valido si la bandera e
					abrirActividadVisita();
				} else {
					// Existe la visita, habilita y deshabilita botones
					Log.i(AplicacionOrdenesServicio.tag_app, "Existe una visita en memoria: " + ordenServicio.getVisitaActiva().getId());
					boton_iniciar_orden.setEnabled(false);
					boton_finalizar_orden.setEnabled(true);

					int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
					//Calendario para la fecha de los hitos
					Calendar cal = Calendar.getInstance();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					String fecha = sdf.format(cal.getTime());
					String toLog = "Cargué una visita en memoria a las " + fecha + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					// Muestra el contenedor de visitas/tareas
					contenedor_visitasOrdenServicio.setVisibility(LinearLayout.VISIBLE);
				}
			}
			// Incializa el controlador de talonarios
			controladorTalonarios = new ControladorTalonarios(aplicacion.getConfiguracionApp().getSqliteRoutines());
			controladorTalonarios.cargarTalonario("visitasOrden", "id");
			boolean slctar_tlnrio = controladorTalonarios.solicitarNuevoTalonario(
					this, aplicacion.getConfiguracionApp(), "visitasOrden", "id");
			if(!slctar_tlnrio) {
				// Muestra Formulario
				mostrarFormulario();
			}
		} else {
			// No existe!,
			toolsApp.showLongMessage("Error al cargar orden de servicio!", this);
		}

	}


	private void mostrarFormulario() {
		formulario = ordenServicio.getFormulario();
		if(formulario != null){
			// Existía formulario, inicializa controlador
			controladorFormularios.setFormularioActivo(formulario);
		} else {
			// Inicializa el controlador del Formulario activo
			formulario = controladorFormularios.cargarFormularioLocal(
					aplicacion.getConfiguracionApp().getSqliteRoutines(),
					"ORDEN DE SERVICIO ACTIVA", Formulario.APLICACION, -1
			);

			// Setea el formulario en la orden
			ordenServicio.setFormulario(formulario);
		}

		// Carga formularios auxiliares
		controladorFormularios.cargarFormulariosLocales(aplicacion.getConfiguracionApp().getSqliteRoutines(),
				Formulario.SUBFORMULARIO);

		// Inicializa el auditor
		auditorFormulario = new AuditorFormularioOS(formulario, null, controladorFormularios);

		// Crea dinámicamente el formulario
		controladorFormularios.visualizarFormulario(
				this, layoutView, contenedor_ordenServicio, camposHash, contenedoresCamposHash,
				auditorFormulario, new AutocompletarCampoOS(),
				onItemSelectedListener, onCheckedChangeListener, true);

		// Pasa los datos del formulario Objeto a la interfaz
		controladorFormularios.formularioAInterfaz(this, layoutView, camposHash, aplicacion.getConfiguracionApp());

	}

	@Override
	protected void onResume() {
		if (ordenServicio != null){
			// Inicializa campos
			auditorFormulario.inicializarCamposFormulario(this,contenedoresCamposHash);
			if(ordenServicio.getVisitaActiva()==null){
				//Borrar
				iniciarVisitaOrdenServicio();
			}
		}
		super.onResume();
	};

	// Listener para los Spinner
	AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
								   long arg3) {
			// Extrae el campo segun el view (Spinner)
			Campo cmpo_evnto = camposHash.get(String.valueOf(arg0.getId()));
			auditorFormulario.validarEventoCampo(ActividadOrdenServicio.this, layoutView, cmpo_evnto,
					arg0, contenedoresCamposHash, camposHash);
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {}
	};

	// Listener para los Checkbox
	CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// Extrae el campo segun el view (Checkbox)
			Campo cmpo_evnto = camposHash.get(String.valueOf(buttonView.getId()));
			auditorFormulario.validarEventoCampo(ActividadOrdenServicio.this, layoutView, cmpo_evnto,
					buttonView, contenedoresCamposHash, camposHash);
		}
	};

	// OnClick de los botones de la actividad
	public void ejecutarAccionBoton(View view) {
		// Dependiendo del view, ejecuta la acción
		switch (view.getId()) {
			case R.id.boton_iniciar_orden:
				iniciarVisitaOrdenServicio();
				int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
				//Calendario para la fecha de los hitos
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				String fecha = sdf.format(cal.getTime());
				String toLog = "Inicié la orden a las " + fecha + " con el idOrden " + idOrdenServicioLog;
				toolsApp.generaLog(toLog);
				break;
			case R.id.boton_finalizar_orden:
				boton_finalizar_orden.setEnabled(false);
				toolsApp.alertDialogYESNO(this, "¿Está seguro de FINALIZAR esta visita?", null, null,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// Deshabilita el botón
								((AlertDialog) dialog).getButton(which).setEnabled(false);

								// Dependiendo del botón presionado, se ejecuta la acción
								switch (which){
									case DialogInterface.BUTTON_POSITIVE:
										int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
										//Calendario para la fecha de los hitos
										Calendar cal = Calendar.getInstance();
										SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
										String fecha = sdf.format(cal.getTime());
										String toLog = "Hundí en Finalizar Visita a las " + fecha + " con el idOrden " + idOrdenServicioLog;
										toolsApp.generaLog(toLog);
										finalizarVisitaOrdenServicio();
										break;
								}

								// Habilita el boton de fin
								boton_finalizar_orden.setEnabled(true);
							}
						});
				break;
		}
	}
	//cambiar a private
	// Método que se ejecuta cuando se inicia el servicio de la orden
	public void iniciarVisitaOrdenServicio() {
		int consecutivo = controladorTalonarios.obtenerConsecutivo("visitasOrden", "id");
		if(consecutivo > 0) {
			// Crea una nueva visita
			visita = new VisitaOS(
					consecutivo, aplicacion.getConfiguracionApp().getUsuario().getId(),
					toolsApp.getUnixDateNOW(true, 0, 0, 0), ordenServicio.getId(), null);
			ordenServicio.adicionarVisita(visita);
			ordenServicio.setIdVisitaActiva(visita.getId());

			// Salva el contexto de los objetos
			aplicacion.salvarObjetosContexto(true);

			// Llamar la clase que inicia y programa ejecución del servicio de localización

			Intent intentoServicioNovedades = new Intent(this, ServicioNovedades.class);
			intentoServicioNovedades.putExtra("tipoIntent", "GPS");
			intentoServicioNovedades.putExtra("quienSolicita", ModuloSeguimiento.INICIO_VISITA);
			startService(intentoServicioNovedades);

			// Cambia estado de botones y ejecuta evento de inicio de servicio
			//boton_iniciar_orden.setEnabled(false);
			boton_finalizar_orden.setEnabled(true);
			contenedor_visitasOrdenServicio.setVisibility(LinearLayout.VISIBLE);
			auditorFormulario.eventoInicioServicio(this, contenedoresCamposHash, camposHash);
		} else {
			finish();
			toolsApp.showLongMessage(
					"No hay talonarios para trabajar Offline, asegúrese de tener internet!", this);
		}
	}

	// Método que se ejecuta cuando se finaliza el servicio de la orden
	private void finalizarVisitaOrdenServicio() {
		// Verifica que se haya ingresado los datos de una nueva TAREA / VISITA
		if (ordenServicio.getVisitaActiva() == null || !ordenServicio.getVisitaActiva().contieneDatos() || aplicacion.getOrdenServicioActiva().getVisitaActiva().getEstadoVisita().equals("CANCELADA")) {
			toolsApp.showLongMessage(
					"No se ha diligenciado una TAREA / VISITA!", this);
			int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
			//Calendario para la fecha de los hitos
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String fecha = sdf.format(cal.getTime());
			String toLog = "Hundieron en finalizar visita y no se había diligenciado nada/faltaba algo a las " + fecha + " con el idOrden " + idOrdenServicioLog;
			toolsApp.generaLog(toLog);
		} else {
			// Asigna los valores del formulario
			boolean asignacionExitosa = controladorFormularios.interfazAFormulario(
					this, aplicacion.getConfiguracionApp(), true, auditorFormulario,
					layoutView, contenedoresCamposHash);

			// Verifica si la asignación fue exitosa
			if(asignacionExitosa){
				// Exitosa,
				// finaliza la visita y actualiza la orden
				ordenServicio.getVisitaActiva().setUnixFechaHoraFin(toolsApp.getUnixDateNOW(true, 0, 0, 0));
				((SQLiteRoutinesOS) aplicacion.getConfiguracionApp().getSqliteRoutines())
						.guardarOrdenServicio(aplicacion, aplicacion.getConfiguracionApp(), ordenServicio);

				// Procesa el evento de finalización del formulario
				auditorFormulario.validarEventoFinFormulario(this, layoutView);

				// Llamar la clase que inicia y programa ejecución del servicio de localización
				Intent intentoServicioNovedades = new Intent(this, ServicioNovedades.class);
				intentoServicioNovedades.putExtra("tipoIntent", "GPS");
				intentoServicioNovedades.putExtra("quienSolicita", ModuloSeguimiento.FIN_VISITA);
				startService(intentoServicioNovedades);

				// Verifica si hay conexión
				if(toolsApp.isConnected(this)){
					// Si hay conexión, Llama servicio para enviar datos pendientes
					Intent intent = new Intent(this, ServicioNovedades.class);
					intent.putExtra("tipoIntent", "WAMP");
					startService(intent);


					if(aplicacion.getFirma()!="" && aplicacion.getFirma() != null){
						Intent intentoServicioNovedadesFirma = new Intent(this, ServicioNovedades.class);
						intentoServicioNovedadesFirma.putExtra("tipoIntent", "FIRMA");
						intentoServicioNovedadesFirma.putExtra("nombre",aplicacion.getFirma()); // ya esta
						intentoServicioNovedadesFirma.putExtra("ruta",aplicacion.getRutaFirma());  // ya esta
						startService(intentoServicioNovedadesFirma);

						aplicacion.setFirma("");
						aplicacion.setRutaFirma("");
					}


				}
				int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
				//Calendario para la fecha de los hitos
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				String fecha = sdf.format(cal.getTime());
				String toLog = "Finalizó y guardó la orden a las " + fecha + " con el idOrden " + idOrdenServicioLog;
				toolsApp.generaLog(toLog);

				// Limpia objetos
				aplicacion.limpiarObjetosAplicacion();
				toolsApp.showLongMessage("Datos guardados!", this);

				// Salva el contexto de los objetos
				aplicacion.salvarObjetosContexto(false);



				// Finaliza actividad
				finish();
			}
		}
	}

	public void abrirDetalleVisita(View view) {
		int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
		//Calendario para la fecha de los hitos
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String fecha = sdf.format(cal.getTime());
		String toLog = "Hundí en TAREAS/VISITAS a las " + fecha + " con el idOrden " + idOrdenServicioLog;
		toolsApp.generaLog(toLog);
		abrirActividadVisita();
	}

	public void abrirActividadVisita(){

		Intent intent = new Intent(this, ActividadVisita.class);

		// Verifica si la visita activa ya tiene un formulario
		if (ordenServicio.getVisitaActiva().getFormulario() != null) {
			intent.putExtra("MODO_EDICION", true);
			int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
			//Calendario para la fecha de los hitos
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String fecha = sdf.format(cal.getTime());
			String toLog = "Pasé por abrirActividadVisita a las " + fecha + " con el idOrden " + idOrdenServicioLog;
			toolsApp.generaLog(toLog);
		}

		// Hace un intento a la actividad
		startActivity(intent);
	}

	public void abrirDetalleVisita2(View view) {
		Intent intent = new Intent(this, ActividadDetalleVisita.class);

		// Verifica si la visita activa ya tiene un formulario
		/*if (ordenServicio.getVisitaActiva().getFormulario() != null) {
			intent.putExtra("MODO_EDICION", true);
		}*/

		// Hace un intento a la actividad
		startActivity(intent);
	}
	@Override
	public void onBackPressed() {
		String mensaje;
		final String datosAdicionales = aplicacion.getOrdenServicioActiva().getDatosAdicionales();
		final LinkedHashMap <String, String> datosEspecificos = aplicacion.getOrdenServicioActiva().getDatosEspecCliente();

		if (ordenServicio.getVisitaActiva() != null) {
			mensaje = "¿Está seguro de CANCELAR la visita?";
		} else {
			mensaje = "¿Está seguro de SALIR de la información de la orden?";
		}
		toolsApp.alertDialogYESNO(this, mensaje,
				null, null, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Deshabilita el botón
						((AlertDialog) dialog).getButton(which).setEnabled(false);

						// Dependiendo del botón presionado, se ejecuta la acción
						switch (which){
							case DialogInterface.BUTTON_POSITIVE:

								int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
								//Calendario para la fecha de los hitos
								Calendar cal = Calendar.getInstance();
								SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
								String fecha = sdf.format(cal.getTime());
								String toLog = "Hundí en Cancelar Visita a las " + fecha + " con el idOrden " + idOrdenServicioLog;
								toolsApp.generaLog(toLog);

								// Limpia objetos
								aplicacion.limpiarObjetosAplicacion();

								// Salva el contexto de los objetos
								aplicacion.salvarObjetosContexto(true);

								finish();
								break;
						}
					}
				});
	}

	protected void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			case ControladorFormularios.REQUEST_AUTOCOMPLETE:
				// Analiza el resultado
				if (resultCode == RESULT_OK) {
					// Obtiene el valor retornado
					if (data != null) {
						int idCampo = data.getIntExtra("idCampo", -1);
						HashMap<String, String> objetoDatos = (HashMap<String, String>) data.getSerializableExtra("valor");

						// Si es un valor válido, lo setea
						if (objetoDatos != null && idCampo > 0) {
							EditText editText = (EditText) layoutView.findViewById(idCampo);
							Campo campoEvento = camposHash.get(String.valueOf(idCampo));
							String nombreCampo = campoEvento.getNombre();
							String valor = objetoDatos.get(nombreCampo);
							editText.setText(valor);

							// Genera evento autocomplete
							auditorFormulario.eventoAutoComplete(ActividadOrdenServicio.this, layoutView, campoEvento, editText, objetoDatos);
						}
					}
				}
				break;
			case ControladorFormularios.REQUEST_SUBFORMULARIO:
				// Analiza el resultado
				if (resultCode == RESULT_OK) {
					// Obtiene el valor retornado
					if (data != null) {
						// Listener para el campo tipo "Detalle"
						int idCampo = data.getIntExtra("idCampo", -1);
						Campo campoEvento = camposHash.get(String.valueOf(idCampo));
						auditorFormulario.validarEventoCampo(ActividadOrdenServicio.this, layoutView, campoEvento,
								null, contenedoresCamposHash, camposHash);
					}
				}
				break;
			case ControladorFormularios.REQUEST_SCANCODE:
				// Analiza el resultado
				if (resultCode == RESULT_OK) {
					// Obtiene el valor retornado
					if (data != null) {
						String valor = data.getStringExtra("SCAN_RESULT");
						//String format = data.getStringExtra("SCAN_RESULT_FORMAT");
						int idCampo = data.getIntExtra("idCampo", -1);
						// Si es un valor válido, lo setea
						if (valor != null && idCampo > 0) {
							EditText editText = (EditText) layoutView.findViewById(idCampo);
							editText.setText(valor);
						}
					}
				}
				break;
			case ControladorFormularios.REQUEST_LISTACAMPO:
				// Analiza el resultado
				if (resultCode == RESULT_OK) {
					// Obtiene el valor retornado
					if (data != null) {
						int idCampo = data.getIntExtra("idCampo", -1);
						String valor = data.getStringExtra("valor");

						// Si es un valor válido, lo setea
						if (valor != null && idCampo > 0) {
							EditText editText = (EditText) layoutView.findViewById(idCampo);
							editText.setText(valor);
						}
					}
				}
				break;
			default:
				break;
		}
	}

	@Override
	public void onTaskCompleted(String accion, Object objto, String... prmtros_callback) {
		// Verifica callback
		if (accion.equals("callback_solicitarNuevoTalonario")){
			if (objto != null) {
				JSONObject jsonObject = (JSONObject) objto;
				try {
					JSONManager jsonManager = new JSONManager(jsonObject);

					// Almacena el talonario
					controladorTalonarios.guardarTalonarioNuevo(jsonManager);
					toolsApp.showShortMessage("Talonario Nuevo!", this);

					// Muestra el formulario
					mostrarFormulario();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				// Muestra el formulario
				mostrarFormulario();
			}
		}
	}

	// Menu para que inicia historial visitas
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.actividad_historial_visitas, menu);
		menuItemVerHistorial = (MenuItem) menu.findItem(R.id.ver_historial);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO: Determinar si se muestra u oculta el historial
		if (ordenServicio.getVisitaActiva() == null) {
			menuItemVerHistorial.setVisible(true);
		} else {
			menuItemVerHistorial.setVisible(false);
		}
		return super.onPrepareOptionsMenu(menu);
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
			case R.id.ver_historial:
				// Revisar si hay visitas asociadas a la orden
				int numeroVisitas = ((SQLiteRoutinesOS) aplicacion.getConfiguracionApp().getSqliteRoutines())
						.routine_consultarNumeroVisitasCreadas(
								ordenServicio.getId(),
								(CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines());
				if (numeroVisitas > 0) {
					// Si hay visitas, crea intent de Historial de visitas
					Intent intent = new Intent(this, ActividadHistorialVisitas.class);
					startActivity(intent);
				} else {
					// No hay visitas
					toolsApp.showLongMessage("No hay visitas asociadas a esta Orden!", this);
				}
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
