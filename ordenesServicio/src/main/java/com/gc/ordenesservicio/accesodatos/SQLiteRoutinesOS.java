package com.gc.ordenesservicio.accesodatos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.gc.coregestionclientes.Configuracion;
import com.gc.coregestionclientes.accesodatos.SQLiteRoutines;
import com.gc.coregestionclientes.objetos.Campo;
import com.gc.coregestionclientes.objetos.Cliente;
import com.gc.coregestionclientes.objetos.Formulario;
import com.gc.ordenesservicio.AplicacionOrdenesServicio;
import com.gc.ordenesservicio.objetos.ActividadAdministrativa;
import com.gc.ordenesservicio.objetos.AuditorFormularioOS;
import com.gc.ordenesservicio.objetos.OrdenServicio;
import com.gc.ordenesservicio.objetos.VisitaOS;

public class SQLiteRoutinesOS extends SQLiteRoutines{

	public SQLiteRoutinesOS(Context contexto) {
		super(contexto, AplicacionOrdenesServicio.nombreBasePrincipal);
	}

	/*************************************************/
	// RUTINAS PARA EL MANEJO GENERAL DE LA APP
	/*************************************************/

	// Permite actualizar los objetos globales en base de datos
	public void routine_actualizarObjetosContexto(
			boolean recuperarContexto, String usuarioGson, String clienteGson, String ordenServicioGson
	) {
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		String sql_update;
		
		if(recuperarContexto){
			sql_update = 
				"UPDATE cntxto_aplccion " +
				"SET " +
				"	recuperarContexto = 1, " +
				"	objetoUsuario = '" + usuarioGson + "', " +
				"	objetoCliente = '" + clienteGson + "', " +
				"	objetoOrdenServicio = '" + ordenServicioGson + "' " +
				"WHERE " +
				"	id = 1";
		}else{
			sql_update = 
				"UPDATE cntxto_aplccion " +
				"SET " +
				"	recuperarContexto = 0, " +
				"	objetoUsuario = NULL, " +
				"	objetoCliente = NULL, " +
				"	objetoOrdenServicio = NULL " +
				"WHERE " +
				"	id = 1";
		}
		ejecutarSQL(sql_update);
		//sqliteManager.cerrarDB();	
	}
	
	// Permite recuperar los objetos globales de la aplicación 
	public Vector<String> routine_recuperarContexto() {
		Vector<String> objetosString = null;
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Crea el query
		String sql = 
			"SELECT recuperarContexto, objetoUsuario, objetoCliente, objetoOrdenServicio " +
			"FROM " +
			"	cntxto_aplccion " +
			"WHERE " +
			"	id = 1";
		Cursor cursor = ejecutarConsultaSQL(sql, null);
		
		// Evalua si hay datos y los extrae
		if (cursor != null){
			objetosString = new Vector<String>(3);
			if(cursor.moveToFirst()) {	
				boolean recuperarContexto 	= toolsApp.bitToBoolean(cursor.getInt(0));
				String objetoUsuario 		= cursor.getString(1);
				String objetoCliente 		= cursor.getString(2);
				String objetoOrdenServicio 		= cursor.getString(3);
				
				// Verifica si es necesario recuperar el contexto
				if(recuperarContexto){
					objetosString.add(0, objetoUsuario);
					objetosString.add(1, objetoCliente);
					objetosString.add(2, objetoOrdenServicio);
				}
			}
			cursor.close();
		}
		
		// Cierra la base de datos
		//sqliteManager.cerrarDB();		
		return objetosString;
	}	
	
	// Método para consultar estados de un cliente 
	public ArrayList<String> consultarEstado(String estado){
		ArrayList<String> estadoSalida = new ArrayList<String>();
		// Crea el query
		String sql = 
				"SELECT estadoSecundario FROM mapeoEstadosOrden " +
				"WHERE estadoPrincipal='"+estado+"'";
		Cursor cursor = ejecutarConsultaSQL(sql, null);
		
		if (cursor != null){
			if(cursor.moveToFirst()) {	
				estadoSalida.add(cursor.getString(0));
			}
			cursor.close();
		}
		
		return estadoSalida;
	}
	
	/*************************************************/
	// RUTINAS PARA EL MANEJO DE ORDENES DE SERVICIO
	/*************************************************/
	// Consulta las ordenes de servicio
	public List<OrdenServicio> routine_consultarOrdenesServicio(CustomSQLiteRoutinesOS customSQLiteRoutines,Configuracion configApp) {
		List<OrdenServicio> ordenesServicio = null;
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Consulta el formulario de refencia para crear Objetos OrdenServicio
		Formulario formularioReferencia = ((SQLiteRoutines)configApp.getSqliteRoutines())
				.routine_consultarFormularioLocal("ORDEN DE SERVICIO ACTIVA", 
						Formulario.APLICACION, true, -1);
		
		// Crea y ejecuta el query
		String sql = 
			"SELECT " +
			"	id, idCliente, unixFechaCreacion, estado, idUsuarioCreacion " +
			"FROM ordenesServicio WHERE estado IN ('PENDIENTE','EN CURSO') ORDER BY unixFechaCreacion desc";
		Cursor cursor = ejecutarConsultaSQL(sql, null);
		
		// Evalua si hay datos y los extrae
		if (cursor != null){
			int numeroOrdenes = cursor.getCount(); 
			if(cursor.moveToFirst()) {
				ordenesServicio = new ArrayList<OrdenServicio>(numeroOrdenes);
				do {
					int id 						= cursor.getInt(0);
					int idCliente 				= cursor.getInt(1);
					long unixFechaCreacion 		= cursor.getLong(2);
					String estado 				= cursor.getString(3);
					int idUsuarioCreacion 		= cursor.getInt(4);
											
					// Consulta los datos adicionales
					String datosAdicionales = customSQLiteRoutines.routine_consultarDatosOrdenServicio(id);
					
					// Se crea un nuevo formulario para la orden a partir del formulario referencia
					Formulario formularioOS = new Formulario(formularioReferencia.getId(),
							formularioReferencia.getNombre(),
							formularioReferencia.getIcono(),
							formularioReferencia.getCamposPlantilla(),
							formularioReferencia.getTipo(),
							formularioReferencia.isMultiple(),
							formularioReferencia.getTablaAsociadas()
					);
					
					// Asigna los valores a los campos del formulario asociado a la orden
					asignarValoresCamposFormulario(configApp, formularioOS, "idOrdenServicio", id);
					
					// Crea un objeto cliente y lo adiciona a la lista
					OrdenServicio ordenServicio = new OrdenServicio(id, idCliente, unixFechaCreacion, estado, datosAdicionales, idUsuarioCreacion);
					ordenServicio.setFormulario(formularioOS);
					
					// Adiciona el cliente a la lista
					ordenesServicio.add(ordenServicio);
				} while(cursor.moveToNext());
			}
			cursor.close();
		}
		//sqliteManager.cerrarDB();
		
		// Retorno de resultado
		return ordenesServicio;
	}
	
	// TODO: Rutina para consultar Visitas de una orden	
	public List<VisitaOS> routine_consultarVisitasOrden(int idOrdenServicio,
			CustomSQLiteRoutinesOS customSqlRoutine, Formulario formularioReferencia, Configuracion configApp, boolean pendientes) {
		Cursor cursorVisitas = null;
		List<VisitaOS> listaVisitaOS = null;
		String adicionSql = "";
		if(pendientes) adicionSql = "AND sincronizado = 0";
		
		try {
			// Crea cursor a partir de consulta de visitas en base de datos general
			cursorVisitas = this.getWritableDatabase().rawQuery(
					"SELECT id, idUsuario, unixFechaInicio, unixFechaFin FROM visitas "+
					"WHERE idOrdenServicio = " + idOrdenServicio + " " + adicionSql + " " +
					"ORDER BY unixFechaInicio DESC", null);
			if(cursorVisitas.moveToFirst()){
				listaVisitaOS = new ArrayList<VisitaOS>();

				// Recorrer listado de visitas de la orden
				do {
					// Se crea objeto visitaOS
					VisitaOS visitaOS = new VisitaOS(cursorVisitas.getInt(0),
							cursorVisitas.getInt(1), cursorVisitas.getLong(2),
							idOrdenServicio, null);
					visitaOS.setUnixFechaHoraFin(cursorVisitas.getLong(3));
					
					// Se crea un nuevo formulario para la visita a partir del formulario referencia
					Formulario formularioVisitaOs = new Formulario(formularioReferencia.getId(),
							formularioReferencia.getNombre(),
							formularioReferencia.getIcono(),
							formularioReferencia.getCamposPlantilla(),
							formularioReferencia.getTipo(),
							formularioReferencia.isMultiple(),
							formularioReferencia.getTablaAsociadas()
					);
					
					// Asigna los valores a los campos del formulario asociado a la orden
					asignarValoresCamposFormulario(configApp, formularioVisitaOs, "idVisita", visitaOS.getId());
					
					/*List<String[]> tablasAsociadas = formularioVisitaOs.getTablaAsociadas();
					Vector<Campo> campos = formularioVisitaOs.getCampos();
					
					// Recorre las tablas asociadas al formulario
					 * 
					 * 
					for(int i= 0; i < tablasAsociadas.size(); i++){
						String[] tablaAsociada = tablasAsociadas.get(i);
						
						// Consultar datos de la tabla asociada en la base secundaria 
						// para setear los campos del formulario
						HashMap<String,String> hashDatosAdicionales = ((CustomSQLiteRoutinesOS)configApp.
								getCustomSqliteRoutines()).consultarDatosTablaAdicional(
										visitaOS.getId(),
										tablaAsociada[1],"idVisita");
						
						// Recorre los campos del formulario para asignar los valores
						for(int j = 0; j < campos.size(); j++){
							Campo campo = campos.get(j);
							List<String[]> camposAsociados = campo.getCamposAsociados();
							
							// Recorremos los campos asociados al campo del formulario
							if (camposAsociados != null) {
								for(int k = 0; k < camposAsociados.size(); k++){
									String[] campoAsociado = camposAsociados.get(k);
									if(!campo.getModoEdicion().equals("Detalle")){
										// Verificar si hay datos asociados al campo del formulario
										if(hashDatosAdicionales.get(campoAsociado[1]) != null){
											// Setea el valor del campo
											campo.setValor(hashDatosAdicionales.get(campoAsociado[1]));
										}
									}else{
										int idFormulario = Integer.parseInt(campo.getDatosAdicionales());
										Formulario formularioReferenciaDetalle = ((SQLiteRoutines)configApp.getSqliteRoutines())
												.routine_consultarFormularioLocal(null, null, true, idFormulario);
										
										String detalle = ((CustomSQLiteRoutinesOS)configApp.getCustomSqliteRoutines())
												.consultarFormularioDetalle(visitaOS.getId(),
												configApp,
												"detalleActividadesVisita", 
												"idVisita",
												formularioReferenciaDetalle);
										campo.setValor(detalle);
									}
								}
							}
						}
					}*/

					// Si es la primer visita (La más reciente), habilita el modo edición
					if(cursorVisitas.isFirst()){
						visitaOS.setModoEdicion(true);
					}
					
					visitaOS.setFormulario(formularioVisitaOs);
					
					// Se añade visita a lista
					listaVisitaOS.add(visitaOS);
				} while(cursorVisitas.moveToNext());
			}
		} catch (Exception e){
			Log.e(AplicacionOrdenesServicio.tag_app, e.toString());
		} finally {
			if (cursorVisitas != null && !cursorVisitas.isClosed()) cursorVisitas.close();
		}

		// Retorno...
		return listaVisitaOS;
	}
	
	// Rutina consultar si hay visitas creadas
	public int routine_consultarNumeroVisitasCreadas(int idOrdenServicio,CustomSQLiteRoutinesOS customSqlRoutines){
		int numeroVisitas = 0;
		Cursor cursor = null;
		try {
			cursor = this.getWritableDatabase().rawQuery(
					"SELECT COUNT(id) FROM visitas " +
					"WHERE idOrdenServicio = " + idOrdenServicio, null);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					numeroVisitas = cursor.getInt(0);
				}
			}
		} catch(Exception e){
			Log.e(AplicacionOrdenesServicio.tag_app, e.toString());
		} finally {
			if (cursor != null && !cursor.isClosed()) cursor.close();
		}

		// Retorno...
		return numeroVisitas;
	}
	
	// TODO: Revisar comentarios y bloques de código 
	// Permite consultar la orden de servicio que no se haya sincronizado
	public synchronized OrdenServicio consultarOrdenServicioPendiente(Configuracion configApp){
		OrdenServicio ordenServicio = null;
		
		// Consulta el formulario de refencia para orden de servicio
		Formulario formulario = ((SQLiteRoutines)configApp.getSqliteRoutines())
				.routine_consultarFormularioLocal("ORDEN DE SERVICIO ACTIVA", 
						Formulario.APLICACION, true, -1);
		
		// Crea query de consulta de orden pendiente
		String sql = 
				"SELECT id,idUsuarioCreacion, idCliente,unixFechaCreacion,unixFechaFinalizacion, observacion, justificacion, estado "
				+ "FROM ordenesServicio WHERE sincronizado = 0 LIMIT 1";
		Cursor cursor = ejecutarConsultaSQL(sql, null);
		if (cursor != null){
			if (cursor.moveToFirst()){
				// Extrae los datos de la orden
				int idOrdenServicio = cursor.getInt(0);
				int idUsuarioCreacion = cursor.getInt(1);
				int idCliente = cursor.getInt(2); 
				long unixFechaCreacion = cursor.getLong(3);
				long unixFechaFinalizacion = cursor.getLong(4);
				String observacion = cursor.getString(5);
				String justificacion = cursor.getString(6);
				String estado = cursor.getString(7);
				
				// Asigna los valores a los campos del formulario asociado a la orden
				asignarValoresCamposFormulario(configApp, formulario, "idOrdenServicio", idOrdenServicio);
				
				/*List<String[]> tablasAsociadas = formulario.getTablaAsociadas();
				Vector<Campo> campos = formulario.getCampos();
				
				// Recorre las tablas asociadas al formulario
				for(int i= 0; i < tablasAsociadas.size(); i++){
					String[] tablaAsociada = tablasAsociadas.get(i);
					
					// Consultar datos de la tabla asociada en la base secundaria 
					// para setear los campos del formulario
					HashMap<String,String> hashDatosAdicionales = ((CustomSQLiteRoutinesOS)configApp.
							getCustomSqliteRoutines()).consultarDatosTablaAdicional(
									idOrdenServicio,
									tablaAsociada[1],"idOrdenServicio");
					
					// Recorre los campos del formulario para asignar los valores
					for(int j = 0; j < campos.size(); j++){
						Campo campo = campos.get(j);
						List<String[]> camposAsociados = campo.getCamposAsociados();
						
						// Recorremos los campos asociados al campo del formulario
						if (camposAsociados != null) {
							for(int k = 0; k < camposAsociados.size(); k++){
								String[] campoAsociado = camposAsociados.get(k);
								
								// Verificar si hay datos asociados al campo del formulario
								if(hashDatosAdicionales.get(campoAsociado[1]) != null){
									// Setea el valor del campo
									campo.setValor(hashDatosAdicionales.get(campoAsociado[1]));
								}
							}
						}
					}
											
				}*/
				
	
				String nombreFormulario = obtenerTipoOrden(idOrdenServicio, configApp);
				
				// Consulta formulario referencia para visitas
				Formulario formularioReferencia = ((SQLiteRoutines)configApp.getSqliteRoutines())
						.routine_consultarFormularioLocal(nombreFormulario, 
								Formulario.APLICACION, true, -1);
				
				// Consulta las visitas pendientes asociadas a la orden
				CustomSQLiteRoutinesOS customSqlRoutine =(CustomSQLiteRoutinesOS)configApp.getCustomSqliteRoutines();
				List<VisitaOS> visitas = routine_consultarVisitasOrden(idOrdenServicio, 
						customSqlRoutine, formularioReferencia, configApp, true);
				
				// Crea el objeto de OrdenServicio
				ordenServicio = new OrdenServicio(idOrdenServicio, idCliente, unixFechaCreacion, estado, "", idUsuarioCreacion);
				ordenServicio.setUnixFechaFinalizacion(unixFechaFinalizacion);
				ordenServicio.setJustificacion(justificacion);
				ordenServicio.setObservacion(observacion);
				ordenServicio.setVisitas(visitas);
				ordenServicio.setFormulario(formulario);
			}
			cursor.close();
		}

		// Retorno...
		return ordenServicio;
	}
	
	public String obtenerTipoOrden(int  idOrden, Configuracion configApp){
		String nombreFormulario = "TAREAS / VISITAS";
		//Obtengo el idCLienteGlobal pues no todos los formularios son dinámicos.
		int idClienteGlobal = configApp.getUsuario().getIdClienteGlobal();
		
		if (idClienteGlobal == 14) {
			// Consulta el tipo de orden por el id
			String tipoServicio = ((CustomSQLiteRoutinesOS)configApp.
					getCustomSqliteRoutines()).routine_obtenerTipoOrden(idOrden);
			if (tipoServicio.equals("PREVENTIVO")) {
				nombreFormulario = "TAREAS / VISITAS PREVENTIVO";
			}else if (tipoServicio.equals("ALISTAMIENTO")) {
				nombreFormulario = "TAREAS / VISITAS ALISTAMIENTO";
			}
		} else if(idClienteGlobal == 16){
			nombreFormulario = "TAREAS / VISITA ENERGITECA"; //"TAREAS / VISITAS CAPACITACIÓN INTERNA";
		}

		return nombreFormulario;
	}
	
	
	// Permite consultar el número de órdenes no sincronizadas.
	public synchronized int consultarNumeroOrdenesPendientes(Configuracion configApp){
		int numeroOrdenes = 0;
		
		// Crea query de consulta de numero de órdenes pendientes
		String sql = 
				"SELECT count(*) FROM ordenesServicio WHERE sincronizado = 0";
		Cursor cursor = ejecutarConsultaSQL(sql, null);
		if (cursor != null){
			if (cursor.moveToFirst()){
				numeroOrdenes = cursor.getInt(0);
			}
			cursor.close();
		}

		// Retorno...
		return numeroOrdenes;
	}
	
	// Permite asignar los valores a los campos de un formulario consultando la información en la base adicional
	private void asignarValoresCamposFormulario(Configuracion configApp, Formulario formulario, String campoLlave, int valorLlave) {
		List<String[]> tablasAsociadas = formulario.getTablaAsociadas();
		Vector<Campo> campos = formulario.getCampos();
		
		// Recorre las tablas asociadas al formulario
		for(int i= 0; i < tablasAsociadas.size(); i++){
			String[] tablaAsociada = tablasAsociadas.get(i);
			
			// Consultar datos de la tabla asociada en la base secundaria 
			// para setear los campos del formulario
			HashMap<String,String> hashDatosAdicionales = ((CustomSQLiteRoutinesOS)configApp.
					getCustomSqliteRoutines()).consultarDatosTablaAdicional(
							tablaAsociada[1], campoLlave, valorLlave);
			
			// Recorre los campos del formulario para asignar los valores
			for(int j = 0; j < campos.size(); j++){
				Campo campo = campos.get(j);
				List<String[]> camposAsociados = campo.getCamposAsociados();
				
				// Verificar si si el campo es modo detalle o no				
				if(!campo.getModoEdicion().equals("Detalle")){
					// No es detalle, verifica sus campos asociados
					if (camposAsociados != null) {
						// Recorremos los campos asociados al campo del formulario
						for(int k = 0; k < camposAsociados.size(); k++){
							String[] campoAsociado = camposAsociados.get(k);
							
							// Verificar si hay datos asociados al campo del formulario
							if(hashDatosAdicionales.get(campoAsociado[1]) != null){
								// Setea el valor del campo
								campo.setValor(hashDatosAdicionales.get(campoAsociado[1]));
							}
						}
					}
				} else {
					// Es un campo detalle, extrae la información asociada
					int idSubformulario = Integer.parseInt(campo.getDatosAdicionales());
					Formulario formularioReferenciaDetalle = ((SQLiteRoutines)configApp.getSqliteRoutines())
							.routine_consultarFormularioLocal(null, null, true, idSubformulario);
					
					String detalle = ((CustomSQLiteRoutinesOS)configApp.getCustomSqliteRoutines())
							.consultarFormularioDetalle(configApp, formularioReferenciaDetalle, campoLlave, valorLlave);
					campo.setValor(detalle);
				}					
			}
		}
	}
	
	/*************************************************/
	// RUTINAS PARA EL MANEJO DE CLIENTES
	/*************************************************/
	// Consulta los clientes
	public List<Cliente> routine_consultarClientesLocales() {
		List<Cliente> clntes = null;
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);

		// Crea y ejecuta el query
		String sql = 
			"SELECT " +
			"	id, cdgo, nmbres, lttud, lngtud, actvo, dtos_adcnles " +
			"FROM clntes WHERE actvo = 1 ORDER BY nmbres";
		Cursor cursor = ejecutarConsultaSQL(sql, null);
		
		// Evalua si hay datos y los extrae
		if (cursor != null){
			int nmro_clntes = cursor.getCount(); 
			if(cursor.moveToFirst()) {
				clntes = new ArrayList<Cliente>(nmro_clntes);
				do{
					int id 			= cursor.getInt(0);
					String cdgo 	= cursor.getString(1);
					String nmbres 	= cursor.getString(2);
					float lttud 	= cursor.getFloat(3);
					float lngtud 	= cursor.getFloat(4);
					boolean actvo 	= toolsApp.bitToBoolean(cursor.getInt(5));
					String dtos_adcnles = cursor.getString(6);
										
					// Crea un objeto cliente y lo adiciona a la lista
					Cliente cliente = new Cliente(id, cdgo, nmbres, lttud, lngtud, actvo, dtos_adcnles);
					
					// Adiciona el cliente a la lista
					clntes.add(cliente);
				}while(cursor.moveToNext());
			}
			cursor.close();
		}
		//sqliteManager.cerrarDB();
		
		// Retorno de resultado
		return clntes;
	}
		
	// Consulta el cliente
	public Cliente routine_consultarCliente(int idCliente, String codigoCliente) {
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		Cliente cliente = null;
		String sql = null;
		
		// Crea y ejecuta el query
		if (idCliente != -1) {
			// Consulta cliente por id
			sql = 
				"SELECT id, cdgo, nmbres, lttud, lngtud, actvo, dtos_adcnles " +
				"FROM clntes WHERE actvo = 1 AND id = " + idCliente + " ORDER BY nmbres";
		} else if (codigoCliente != null){
			// Consulta cliente por id
			sql = 
				"SELECT id, cdgo, nmbres, lttud, lngtud, actvo, dtos_adcnles " +
				"FROM clntes WHERE actvo = 1 AND cdgo = '" + codigoCliente + "' ORDER BY nmbres";
		}

		// Si hay un query válido, lo ejecuta
		if (sql != null) {
			Cursor cursor = ejecutarConsultaSQL(sql, null);

			// Evalua si hay datos y los extrae
			if (cursor != null){
				if(cursor.moveToFirst()) {
					int id 			= cursor.getInt(0);
					String cdgo 	= cursor.getString(1);
					String nmbres 	= cursor.getString(2);
					float lttud 	= cursor.getFloat(3);
					float lngtud 	= cursor.getFloat(4);
					boolean actvo 	= toolsApp.bitToBoolean(cursor.getInt(5));
					String dtos_adcnles = cursor.getString(6);
										
					// Crea un objeto cliente y lo adiciona a la lista
					cliente = new Cliente(id, cdgo, nmbres, lttud, lngtud, actvo, dtos_adcnles);
				}
				cursor.close();
			}
		}
		// TODO: 
		//sqliteManager.cerrarDB();
		
		// Retorno de resultado
		return cliente;
	}

	// Busqueda dinamica del cliente
	public int consultarTotalClientePatron(String patron) {
		patron = patron.toUpperCase();
		int numeroItems = 0;

		// Crea y ejecuta el query
		String sql =
				"SELECT count(*) " +
						"FROM clntes " +
						"WHERE " +
						"(cdgo LIKE '%" + patron + "%' " +
						"OR nmbres LIKE '%" + patron + "%' )";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				// Extrae los datos
				numeroItems = cursor.getInt(0);
			}
			cursor.close();
		}

		// Retorno de resultado
		return numeroItems;
	}

	// Retorna el vector de cliente de respueta
	public Vector<LinkedHashMap<String, String>> consultaClienteVector(String patron) {
		patron = patron.toUpperCase();
		Vector<LinkedHashMap<String, String>> items = null;
		LinkedHashMap<String, String> item = null;

		// Crea y ejecuta el query
		String sql =
				"SELECT * " +
						"FROM clntes " +
						"WHERE " +
						"(cdgo LIKE '%" + patron + "%' " +
						"OR nmbres LIKE '%" + patron + "%' )";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			int numeroProductos = cursor.getCount();
			if (cursor.moveToFirst()) {
				items = new Vector<LinkedHashMap<String, String>>(numeroProductos);
				do {
					// Extrae los datos del producto
					String codigo 				= cursor.getString(1);
					String descripcion 		= cursor.getString(2);

					// Inicia el objeto
					item = new LinkedHashMap<String, String>();
					item.put("Código Cliente", codigo);
					item.put("Nombre", descripcion);

					// Adiciona al vector
					items.addElement(item);
				} while (cursor.moveToNext());
			}
			cursor.close();
		}
		//sqliteManager.cerrarDB();

		// Retorno de resultado
		return items;
	}

	/*************************************************/
	// RUTINAS PARA EL MANEJO DE ACTIVIDADES ADMINISTRATIVAS
	/*************************************************/
	
	// Guarda informacion de la actividad administrativa
	public synchronized boolean guardarActividadAdministrativa(AplicacionOrdenesServicio aplicacion, Configuracion configuracionApp, 
			ActividadAdministrativa actividadAdministrativa){
		try {
			long fechaFinalizacion = toolsApp.getUnixDateNOW(true, 0, 0, 0);
			String sql = 
					"INSERT INTO actividadesAdministrativas (id, idUsuario, unixFechaInicio, unixFechaFin)" +
					" VALUES (" +
						actividadAdministrativa.getId() + "," +
						configuracionApp.getUsuario().getId() + "," +
						actividadAdministrativa.getUnixFechaInicio() + "," +
						fechaFinalizacion +
					")";
			
			// Valida si se guardo e tabla principal
			if(ejecutarSQL(sql)){
				// Asigna el campo Llave al formulario de actividades administrativas
				String[] campoLlave = {"idActividad",String.valueOf(actividadAdministrativa.getId())};
				actividadAdministrativa.getFormulario().setCampoLlave(campoLlave);
				
				// Almacena los datos adicionales de la actividad administrativa
				((CustomSQLiteRoutinesOS) configuracionApp.getCustomSqliteRoutines())
				.guardarDatosAdicionales(aplicacion, actividadAdministrativa.getFormulario(), configuracionApp, true);
			}
			
			return true;
		} catch (Exception e) {
			Log.i(AplicacionOrdenesServicio.tag_app, e.getMessage());
		}
		return false;
	}
	
	// Consulta actividad aministrativa pendiente por enviar a la nube
	public synchronized ActividadAdministrativa consultarActividadAdministrativaPendiente(Configuracion configApp){
		ActividadAdministrativa actividadAdministrativa = null;
		try {
			// Crea query de consulta
			String sql = 
					"SELECT id,idUsuario,unixFechaInicio,unixFechaFin,sincronizado "
					+ "FROM actividadesAdministrativas WHERE sincronizado = 0 LIMIT 1";
			Cursor cursor = ejecutarConsultaSQL(sql, null);
			
			// Consulta el formulario de refencia para actividades administrativas
			Formulario formulario = ((SQLiteRoutines)configApp.getSqliteRoutines())
					.routine_consultarFormularioLocal("ACTIVIDADES ADMINISTRATIVAS", 
							Formulario.APLICACION, true, -1);
			if (cursor != null){
				if (cursor.moveToFirst()){
					int idActividadAdministrativa = cursor.getInt(0);
					int idUsuario = cursor.getInt(1);
					long unixFechaInicio = cursor.getLong(2);
					long unixFechaFin = cursor.getLong(3);
					
					List<String[]> tablasAsociadas = formulario.getTablaAsociadas();
					Vector<Campo> campos = formulario.getCampos();
					
					// Recorre las tablas asociadas al formulario
					for(int i= 0; i < tablasAsociadas.size(); i++){
						String[] tablaAsociada = tablasAsociadas.get(i);
						
						// Consultar datos de la tabla asociada en la base secundaria 
						// para setear los campos del formulario
						HashMap<String,String> hashDatosAdicionales = ((CustomSQLiteRoutinesOS)configApp.
								getCustomSqliteRoutines()).consultarDatosTablaAdicional(
										tablaAsociada[1], "idActividad", idActividadAdministrativa);
						
						// Recorre los campos del formulario para asignar los valores
						for(int j = 0; j < campos.size(); j++){
							Campo campo = campos.get(j);
							List<String[]> camposAsociados = campo.getCamposAsociados();
							
							// Recorremos los campos asociados al campo del formulario
							if (camposAsociados != null) {
								for(int k = 0; k < camposAsociados.size(); k++){
									String[] campoAsociado = camposAsociados.get(k);
									
									// Verificar si hay datos asociados al campo del formulario
									if(hashDatosAdicionales.get(campoAsociado[1]) != null){
										// Setea el valor del campo
										campo.setValor(hashDatosAdicionales.get(campoAsociado[1]));
									}
								}
							}
						}	
					}
					
					// Crea el objeto de actividad administrativa
					actividadAdministrativa = new ActividadAdministrativa(idActividadAdministrativa, 
							idUsuario, unixFechaInicio, unixFechaFin, formulario);
				}
				cursor.close();
			}
		} catch(Exception e){
			Log.e(AplicacionOrdenesServicio.tag_app, e.getMessage());
		}
		
		// Retorno...
		return actividadAdministrativa;
	}
	
	// Actualizar estado actividad administrativa
	public void routine_actualizarBanderaSincronizadoActividadAdministartiva(long idActividadAdministrativa){
		// Crea y ejecuta el query
		String sql = "UPDATE actividadesAdministrativas SET sincronizado = 1 WHERE id = " + idActividadAdministrativa;
		ejecutarSQL(sql);
	}
	
	// Actualizar estado orden de servicio
	public void routine_actualizarBanderaSincronizadoOrdenServicio(long idOrdenServicio){
		// Crea y ejecuta el query
		String sql = "UPDATE ordenesServicio SET sincronizado = 1 WHERE id = " + idOrdenServicio;
		ejecutarSQL(sql);
		sql = "UPDATE visitas SET sincronizado = 1 WHERE idOrdenServicio = " + idOrdenServicio;
		ejecutarSQL(sql);
		
	}
	
	// Guarda informacion de la orden de servicio
	public synchronized boolean guardarOrdenServicio(AplicacionOrdenesServicio aplicacion, Configuracion configuracionApp, OrdenServicio ordenServicio){
		// Evalúa la fecha de finalización de la orden y dependiendo de eso, la actualiza o no
		long fechaFinalizacion = (ordenServicio.getUnixFechaFinalizacion() <= 0 ? 
				toolsApp.getUnixDateNOW(true, 0, 0, 0) : ordenServicio.getUnixFechaFinalizacion());
		
		// Crea y ejecuta el query para guardar o actualizar la orden
		Log.i(AplicacionOrdenesServicio.tag_app, "Guardando Orden");
		String sql = 
				"REPLACE INTO ordenesServicio " +
				"(id, idCliente, unixFechaCreacion, unixFechaFinalizacion, estado, idUsuarioCreacion, "
				+ "observacion, justificacion, sincronizado)" +
				" VALUES (" +
					ordenServicio.getId() + "," +
					ordenServicio.getIdCliente() + "," +
					ordenServicio.getUnixFechaCreacion() + "," +
					fechaFinalizacion + "," +
					"'" + ordenServicio.getEstado() + "'," +
					ordenServicio.getIdUsuarioCreacion() + "," +
					"'" + ordenServicio.getObservacion() + "'," +
					"'" + ordenServicio.getJustificacion() + "'," +
					"0" +
				")";
		
		// Valida si se guardo en la tabla principal
		if(ejecutarSQL(sql)) {
			// Asigna campo llave al formulario de la orden
			String[] campoLlave = {"idOrdenServicio", String.valueOf(ordenServicio.getId())};
			ordenServicio.getFormulario().setCampoLlave(campoLlave);
							
			// Guardar datos adicionales de Ordenes
			((CustomSQLiteRoutinesOS) configuracionApp.getCustomSqliteRoutines())
			.guardarDatosAdicionales(aplicacion, ordenServicio.getFormulario(), configuracionApp, true);
			
			// Extrae las visitas y las recorre
			List<VisitaOS> visitas = ordenServicio.getVisitas();
			if (visitas != null) {
				for(int i = 0; i < visitas.size(); i++){
					guardarVisita(aplicacion, configuracionApp, visitas.get(i));
				}
			}
			
			// Retorno...
			return true;
		} else {
			
			// Retorno...
			return false;	
		}
	}
	
	// Permite guardar una visita y sus datos adicionales
	public synchronized boolean guardarVisita(AplicacionOrdenesServicio aplicacion, Configuracion configuracionApp, VisitaOS visita){
		try {
			long fechaFinalizacion = (visita.getUnixFechaHoraFin() <= 0 ? toolsApp.getUnixDateNOW(true, 0, 0, 0) : visita.getUnixFechaHoraFin());
			String sql = 
					"REPLACE INTO visitas (id, idUsuario, unixFechaInicio, unixFechaFin, idOrdenServicio, sincronizado) " +
					" VALUES (" +
						visita.getId() + "," +
						visita.getIdUsuario() + "," +
						visita.getUnixFechaHoraIncio() + "," +
						fechaFinalizacion + "," +
						visita.getIdOrdenServicio() + "," +
						"0" +
					")";
			
			// Valida si se guardo e tabla principal
			if(ejecutarSQL(sql)){
				// Asigna el campo llave al formulario de visita
				String[] campoLlave = {"idVisita", String.valueOf(visita.getId())};
				visita.getFormulario().setCampoLlave(campoLlave);
				
				// Guardar datos adicionales de Ordenes
				((CustomSQLiteRoutinesOS) configuracionApp.getCustomSqliteRoutines())
				.guardarDatosAdicionales(aplicacion, visita.getFormulario(), configuracionApp, true);
			}
			
			return true;
		} catch (Exception e) {
			Log.i(AplicacionOrdenesServicio.tag_app, e.getMessage());
		}
		return false;
	}
}
