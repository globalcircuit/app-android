package com.gc.ordenesservicio.accesodatos;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.gc.coregestionclientes.Configuracion;
import com.gc.coregestionclientes.novedades.librerias.GC_JSON;
import com.gc.coregestionclientes.novedades.librerias.VectorGC;
import com.gc.ordenesservicio.AplicacionOrdenesServicio;


public class ComandosAplicacion {
	// Variables
	private static ProgressDialog progressDialog;
	public static boolean showProgress = false;
	
	// Función que maneja los callbacks cuando no existe interfaz gráfica que reciba la respuesta.
	public static boolean manejadorCallbacks(Context contexto, String nombreIntento, Configuracion configApp, byte[] respuesta) {		
		// Dependiendo del nombre del intent ejecuta la accion
		if (nombreIntento.startsWith("aplicacion.reportarOrdenServicioNuevo")) {
			return callbackReportarOrdenesServicio(contexto, configApp, respuesta);
			
		} else if(nombreIntento.startsWith("aplicacion.reportarActividadAdministrativa")){
			return callbackActividadAdministrativa(contexto, configApp, respuesta);
		}
		
		// Retorno default
		return false;
	}
	
	// Callback del reporte de visita
	public static boolean callbackReportarOrdenesServicio(Context contexto, Configuracion configApp, byte[] respuesta){
		// Si esta habilitada la opcion para mostrar el progresDialog,
		// Lo remueve de la pantalla una vez se ha completado la tarea en background
		if(showProgress) {
			progressDialog.dismiss();
			showProgress = false;
		}
		
		// Actualiza bandera de datos enviados en SQLITE
		try {
			// Verifica la respuesta
			if (respuesta != null) {
				// Hay datos
				@SuppressWarnings("unchecked")
				VectorGC<Object> datosRespuesta = (VectorGC<Object>) GC_JSON.parseJSON(respuesta, false, false);
				if (datosRespuesta.getString(0).equals("OK")) {
					long idOrdenServicio = datosRespuesta.getObject(2).getLong("idOrdenServicio", -1);
					((SQLiteRoutinesOS) configApp.getSqliteRoutines()).routine_actualizarBanderaSincronizadoOrdenServicio(idOrdenServicio);
					return true;
				} else {
					Log.i(AplicacionOrdenesServicio.tag_app, "Error: " + datosRespuesta.getString(1));
					return false;
				}
			} else {
				// No hay respuesta
				Log.i(AplicacionOrdenesServicio.tag_app, "Datos guardados offline!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	// Callback de guardado de actividad administrativa
	public static boolean callbackActividadAdministrativa(Context contexto, Configuracion configApp, byte[] respuesta){
		// Si esta habilitada la opcion para mostrar el progresDialog,
		// Lo remueve de la pantalla una vez se ha completado la tarea en background
		if(showProgress) {
			progressDialog.dismiss();
			showProgress = false;
		}
		
		// Actualiza bandera de datos enviados en SQLITE
		try {
			// Verifica la respuesta
			if (respuesta != null) {
				// Hay datos
				@SuppressWarnings("unchecked")
				VectorGC<Object> datosRespuesta = (VectorGC<Object>) GC_JSON.parseJSON(respuesta, false, false);
				if (datosRespuesta.getString(0).equals("OK")) {
					long idActividadAdministrativa = datosRespuesta.getObject(2).getLong("IdActividad", -1);
					((SQLiteRoutinesOS) configApp.getSqliteRoutines())
						.routine_actualizarBanderaSincronizadoActividadAdministartiva(idActividadAdministrativa);
					return true;
				} else {
					Log.i(AplicacionOrdenesServicio.tag_app, "Error: " + datosRespuesta.getString(1));
					return false;
				}
			} else {
				// No hay respuesta
				Log.i(AplicacionOrdenesServicio.tag_app, "Datos guardados offline!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
