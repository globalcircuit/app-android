package com.gc.ordenesservicio.accesodatos;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.util.SparseArray;

import com.gc.coregestionclientes.Configuracion;
import com.gc.coregestionclientes.accesodatos.CustomSQLiteRoutines;
import com.gc.coregestionclientes.accesodatos.SQLiteRoutines;
import com.gc.coregestionclientes.objetos.Campo;
import com.gc.coregestionclientes.objetos.ControladorFormularios;
import com.gc.coregestionclientes.objetos.Formulario;
import com.gc.ordenesservicio.AplicacionOrdenesServicio;
import com.gc.ordenesservicio.novedades.ServicioNovedades;
import com.gc.ordenesservicio.objetos.OrdenServicio;

public class CustomSQLiteRoutinesOS extends CustomSQLiteRoutines {

	public CustomSQLiteRoutinesOS(Context contexto) {
		super(contexto, AplicacionOrdenesServicio.nombreBaseSecundaria);
	}

	// Método que permite consu
	//
	//
	//
	// ltar los datos adicionales de una orden de servicio
	public String routine_consultarDatosOrdenServicio(int id) {
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		StringBuilder datosAdicionales = new StringBuilder();
		
		// Crea y ejecuta el query
		String sql = 
			"SELECT * FROM datosOrdenesServicio WHERE idOrdenServicio = " + id;
		Cursor cursor = ejecutarConsultaSQL(sql, null);
		
		// Evalua si hay datos y los extrae
		if (cursor != null){
			int numeroColumnas = cursor.getColumnCount();
			if(cursor.moveToFirst()) {
				// Recorre las columnas y construye el String JSON con los campos
				for (int i = 0; i < numeroColumnas; i++) {
					// Extrae datos
					String nombreColumna = cursor.getColumnName(i);
					String valor = cursor.getString(i);
					if (datosAdicionales.length() > 0) datosAdicionales.append(", ");
					datosAdicionales.append("\"").append(nombreColumna).append("\":");
					if (valor != null){
						datosAdicionales.append("\"").append(valor).append("\"");
					} else {
						datosAdicionales.append(valor);
					}
				}
				
				// Adiciona las llaves al String
				datosAdicionales.insert(0, "{").append("}");
			}
			cursor.close();
		}

		// Retorno...
		return datosAdicionales.toString();
	}

	
	// Método que permite consultar las herramientas y repuestos de una visita
	public String[]   routine_obtenerHerramientas() {
		String[] herramientas = null;
	
		// Crea y ejecuta el query
		String sql = 
			"SELECT * FROM datosHerramientasRepuestos"
			+ " WHERE tipo='Herramienta'";
		Cursor cursor = ejecutarConsultaSQL(sql, null);
		
		// Evalua si hay datos y los extrae
		if (cursor != null){
			int numeroItems = cursor.getCount(); 
			if(cursor.moveToFirst()) {
				herramientas = new String[numeroItems+1];
				herramientas[0] = "";
				int indice = 1;
				do {
					// Extrae los datos del query
					String id =   String.valueOf( cursor.getString(0) );
					String descripcion = cursor.getString(1);
					String tipo = cursor.getString(2);
					
					herramientas[indice] = descripcion;
					indice++;
					
				} while(cursor.moveToNext());
			}
			cursor.close();
		}

		return herramientas;
	}
	
	// Método que permite consultar las herramientas y repuestos de una visita
	public String[]   routine_obtenerRepuestos() {
		String[] repuestos = null;
	
		// Crea y ejecuta el query
		String sql = 
			"SELECT * FROM datosHerramientasRepuestos"
			+ " WHERE tipo='Repuesto'";
		Cursor cursor = ejecutarConsultaSQL(sql, null);
		
		// Evalua si hay datos y los extrae
		if (cursor != null){
			int numeroItems = cursor.getCount(); 
			if(cursor.moveToFirst()) {
				repuestos = new String[numeroItems+1];
				repuestos[0] = "";
				int indice = 1;
				do {
					// Extrae los datos del query
					String id =   String.valueOf( cursor.getString(0) );
					String descripcion = cursor.getString(1);
					String tipo = cursor.getString(2);
					
					repuestos[indice] = descripcion;
					indice++;
					
				} while(cursor.moveToNext());
			}
			cursor.close();
		}

		return repuestos;
	}
	
	
	// Consulta un String JSON con los datos adicionales de la visita
	public String routine_consultarDatosVisita(int idVisita) {
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		StringBuilder datosAdicionales = new StringBuilder();
		
		// Crea y ejecuta el query
		String sql = 
			"SELECT * FROM datosVisitas WHERE idVisita = " + idVisita;
		Cursor cursor = ejecutarConsultaSQL(sql, null);
		
		// Evalua si hay datos y los extrae
		if (cursor != null){
			int numeroColumnas = cursor.getColumnCount();
			if(cursor.moveToFirst()) {
				// Recorre las columnas y construye el String JSON con los campos
				for (int i = 0; i < numeroColumnas; i++) {
					// Extrae datos
					String nombreColumna = cursor.getColumnName(i);
					String valor = cursor.getString(i);
					if (datosAdicionales.length() > 0) datosAdicionales.append(", ");
					datosAdicionales.append("\"").append(nombreColumna).append("\":");
					if (valor != null){
						datosAdicionales.append("\"").append(valor).append("\"");
					} else {
						datosAdicionales.append(valor);
					}
				}
				
				// Adiciona las llaves al String
				datosAdicionales.insert(0, "{").append("}");
			}
			cursor.close();
		}

		// Retorno...
		return datosAdicionales.toString();
	}

	// TODO: 
	public String consultarFormularioDetalle(
			Configuracion configApp, Formulario formularioReferencia, String campoLlave, int valorLlave){
		StringBuilder detalleStringJson = new StringBuilder();
		
		// Construye el query
		String sql = "SELECT * FROM " + formularioReferencia.getTablaAsociadas().get(0)[1] + 
				" WHERE " + campoLlave + " = " + valorLlave;
		Cursor cursor = ejecutarConsultaSQL(sql, null);
		if (cursor != null){
			if(cursor.moveToFirst()) {
				int consecutivo = 0;
				do {
					// Crea un nuevo formulario a partir de la referencia
					Formulario formularioItemDetalle = new Formulario(formularioReferencia.getId(),
							formularioReferencia.getNombre(),
							formularioReferencia.getIcono(),
							formularioReferencia.getCamposPlantilla(),
							formularioReferencia.getTipo(),
							formularioReferencia.isMultiple(),
							formularioReferencia.getTablaAsociadas());
					
					// Extrae los campos y los recorre
					SparseArray<Campo> campos = formularioItemDetalle.getCamposHash();
					for(int i=0;i<campos.size();i++){
						// Extrae la llave y el campo
						int key = campos.keyAt(i);
						Campo campo = campos.get(key);
						
						// Evalua campos asociados
						List<String[]> camposAsociados = campo.getCamposAsociados();
						if(camposAsociados != null){
							for(int j=0;j<camposAsociados.size();j++){
								String[] campoAsociado = camposAsociados.get(j);
								
								// Asigna el valor al campo si coincide
								if(campoAsociado[0].equals(formularioReferencia.getTablaAsociadas().get(0)[1])){
									int indiceCursor = cursor.getColumnIndex(campoAsociado[1]);
									campo.setValor(cursor.getString(indiceCursor));
								}
							}
						}
					}
					
					// Adiciona separador
					if(consecutivo > 0){
						detalleStringJson.append(",");
					}
					
					// Adiciona el formulario como Json String
					detalleStringJson.append("{");
					detalleStringJson.append(formularioItemDetalle.generarJson(configApp));
					detalleStringJson.append("}");
					consecutivo++;
				} while(cursor.moveToNext());
			}
			cursor.close();
		}
		detalleStringJson.insert(0, "[").append("]");
		
		// Retorno...
		return detalleStringJson.toString();
	}
	
	// Actualiza el estado de la orden
	public void almatec_actualizarEstadoOrdenServicio(OrdenServicio ordenServicio) {			
		// Extrae el formulario de la visita
		Formulario formularioVisita = ordenServicio.getVisitaActiva().getFormulario();
		SparseArray<Campo> camposVisita = formularioVisita.getCamposHash();
		
		// Actualiza el estado de la orden en los datos adicionales
		String datosAdicionales = ordenServicio.getDatosAdicionales();
		if(datosAdicionales != null){
			try {
				JSONObject jsonObject = new JSONObject(datosAdicionales);
				//jsonObject.put("estado", camposVisita.get(401).getValor());
				jsonObject.put("estado", camposVisita.get(1109).getValor());
				ordenServicio.setDatosAdicionales(jsonObject.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		//almatec_ejecutarActualizacionEstadoOrdenServicio(camposVisita.get(401).getValor(), ordenServicio.getId());
		almatec_ejecutarActualizacionEstadoOrdenServicio(camposVisita.get(1109).getValor(), ordenServicio.getId());
	}

	// Actualiza el estado de la orden
	public void coexito_actualizarEstadoOrdenServicio(OrdenServicio ordenServicio) {
		// Extrae el formulario de la visita
		Formulario formularioVisita = ordenServicio.getVisitaActiva().getFormulario();
		SparseArray<Campo> camposVisita = formularioVisita.getCamposHash();

		// Actualiza el estado de la orden en los datos adicionales
		String datosAdicionales = ordenServicio.getDatosAdicionales();
		if(datosAdicionales != null){
			try {
				JSONObject jsonObject = new JSONObject(datosAdicionales);
				//jsonObject.put("estado", camposVisita.get(401).getValor());
				jsonObject.put("estado", "CERRADO");
				ordenServicio.setDatosAdicionales(jsonObject.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		//almatec_ejecutarActualizacionEstadoOrdenServicio(camposVisita.get(401).getValor(), ordenServicio.getId());
		almatec_ejecutarActualizacionEstadoOrdenServicio("CERRADO", ordenServicio.getId());
	}
	public void almatec_ejecutarActualizacionEstadoOrdenServicio(String estado,int idOrden)
	{
		// Crea y ejecuta el query de actualización
		String sql = 
			"UPDATE datosOrdenesServicio " + 
			"SET estado = '" + estado + "', sincronizado = 0 " +
			"WHERE idOrdenServicio = " + idOrden;
		ejecutarSQL(sql);
	}
	
	// Permite consultar todas las montacargas asociadas a un cliente
	public Vector<LinkedHashMap<String, String>> almatec_consultarMontacargas(String nombreCampo) {
		Vector<LinkedHashMap<String, String>> montacargas = null;
		LinkedHashMap<String, String> montacarga = null;
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Crea y ejecuta el query
		String sql = 
				"SELECT "
					+ "m.marca AS marca, "
					+ "m.codigo AS codigo, "
					+ "m.descripcion AS descripcion, "
					+ "m.serial AS serial, "
					+ "m.tipo AS tipo, "
					+ "m.tipoEquipo AS tipoEquipo, "
					+ "m.modelo AS modelo, "
					+ "m.fechaActivacion AS fechaActivacion, "
					+ "m.fechaInstalacion AS fechaInstalacion, "
					+ "m.codigoCliente AS codigoCliente, "
					+ "m.direccionInstalacion AS direccionInstalacion, "
					+ "m.ciudad AS ciudad, "
					+ "m.departamento AS departamento, "
					+ "m.distrito AS distrito, "
					+ "m.numeroContrato AS numeroContrato, "
					+ "m.inicioContrato AS inicioContrato, "
					+ "m.finContrato "
				+ "FROM montacargas AS m";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			int numeroOrdenes = cursor.getCount();
			if (cursor.moveToFirst()) {	
				montacargas = new Vector<LinkedHashMap<String, String>>(numeroOrdenes);
				do {
					// Extrae los datos del montacarga
					String serial = cursor.getString(3);
					String descripcion = cursor.getString(2);
					String marca = cursor.getString(0);
					String modelo = cursor.getString(6);
					
					// Inicia el objeto
					montacarga = new LinkedHashMap<String, String>();
					montacarga.put(nombreCampo, serial);
					montacarga.put("Descripción", descripcion);
					montacarga.put("Marca", marca);
					montacarga.put("Modelo", modelo);
					
					// Adiciona al vector
					montacargas.addElement(montacarga);
				} while(cursor.moveToNext());
			}
			cursor.close();
		}
		//sqliteManager.cerrarDB();
		
		// Retorno...
		return montacargas;
	}

	// Permite consultar el montacarga dependiendo de su serial
	// TODO: 
	public HashMap<String, String> almatec_consultarMontacarga(String serialMontacarga) {
		HashMap<String, String> montacarga = null;
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Crea y ejecuta el query
		String sql = 
				"SELECT "
					+ "m.marca AS marca, "
					+ "m.codigo AS codigo, "
					+ "m.descripcion AS descripcion, "
					+ "m.serial AS serial, "
					+ "m.tipo AS tipo, "
					+ "m.tipoEquipo AS tipoEquipo, "
					+ "m.modelo AS modelo, "
					+ "m.fechaActivacion AS fechaActivacion, "
					+ "m.fechaInstalacion AS fechaInstalacion, "
					+ "m.codigoCliente AS codigoCliente, "
					+ "m.direccionInstalacion AS direccionInstalacion, "
					+ "m.ciudad AS ciudad, "
					+ "m.departamento AS departamento, "
					+ "m.distrito AS distrito, "
					+ "m.numeroContrato AS numeroContrato, "
					+ "m.inicioContrato AS inicioContrato, "
					+ "m.finContrato "
				+ "FROM montacargas AS m "
				+ "WHERE "
					+ "m.serial = '" + serialMontacarga + "'";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			if (cursor.moveToFirst()) {	
					// Extrae los datos del montacarga
					String codigoCliente = cursor.getString(9);
					String descripcion = cursor.getString(2);
					String marca = cursor.getString(0);
					String modelo = cursor.getString(6);
					String tipo = cursor.getString(4);
					String direccionInstalacion = cursor.getString(10);
					
					// Inicia el objeto
					montacarga = new HashMap<String, String>();
					montacarga.put("codigoCliente", codigoCliente);
					montacarga.put("descripcion", descripcion);
					montacarga.put("marca", marca);
					montacarga.put("modelo", modelo);
					montacarga.put("tipo", tipo);
					montacarga.put("direccionInstalacion", direccionInstalacion);
			}
			cursor.close();
		}
		//sqliteManager.cerrarDB();
		
		// Retorno...
		return montacarga;
	}
	
	// Permite consultar la información de un repuesto por código
	public HashMap<String, String> agenciaAlemana_consultarRepuestoXCodigoBarras(String codigo) {
		HashMap<String, String> item = null;
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		// Crea y ejecuta el query
		String sql = 
				"SELECT codigoSAP,codigoBarras,referenciaFabricante,descripcion,grupo " + 
				"FROM listaArticulos " + 
				"WHERE " +
				"codigoBarras = '" + codigo + "' " +
				"limit 1";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			if (cursor.moveToFirst()) {			
				// Extrae los datos del producto
				String codigoSAP = cursor.getString(0);
				String codigoBarras = cursor.getString(1);
				String referenciaFabricante = cursor.getString(2);
				String descripcion = cursor.getString(3);
				String grupo = cursor.getString(4);
				
				// Inicia el objeto
				item = new HashMap<String, String>();
				item.put("codigoSAP", codigoSAP);
				item.put("codigoBarras", codigoBarras);
				item.put("referenciaFabricante", referenciaFabricante);
				item.put("descripcion", descripcion);
				item.put("grupo", grupo);
			}
			cursor.close();
		}
		//sqliteManager.cerrarDB();

		// Retorno de resultado
		return item;
	}
	
	
	/*
	 *  MÉTODOS PARA ROYAL - INICIO
	 *  */
	
	// Método que permite consultar la cantidad de productos crown basado en el código/descripción que se escribe
	public int royal_consultarCantidadCodigo(String patron) {
		patron = patron.toUpperCase();
		int numeroItems = 0;
		
		// Crea y ejecuta el query
		String sql = 
			"SELECT count(*)" +
				"FROM referenciaRepuestosCrown AS rpc " +
			"WHERE " +
				"rpc.codigo LIKE '%" + patron + "%' " +
				"OR UPPER(rpc.descripcion) LIKE '%" + patron + "%' ";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				// Extrae los datos
				numeroItems = cursor.getInt(0);
			}
			cursor.close();
		}

		// Retorno de resultado
		return numeroItems;
	}
	
	// Consulta los repuestos que coinciden con el patron
	public Vector<LinkedHashMap<String, String>> royal_consultarRepuestosCrown(String patron) {
		patron = patron.toUpperCase();
		Vector<LinkedHashMap<String, String>> items = null;
		LinkedHashMap<String, String> item = null;
		
		// Crea y ejecuta el query
		String sql = 
				"SELECT *" +
				"FROM referenciaRepuestosCrown " + 
				"WHERE " +
					"codigo LIKE '%" + patron + "%' " +
					"OR UPPER(descripcion) LIKE '%" + patron + "%' ";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			int numeroProductos = cursor.getCount();
			if (cursor.moveToFirst()) {
				items = new Vector<LinkedHashMap<String, String>>(numeroProductos);
				do {
					// Extrae los datos del producto
					String codigo 				= cursor.getString(0);
					String descripcion 		= cursor.getString(1);

					// Inicia el objeto
					item = new LinkedHashMap<String, String>();
					item.put("Código", codigo);
					item.put("Descripción", descripcion);

					// Adiciona al vector
					items.addElement(item);
				} while (cursor.moveToNext());
			}
			cursor.close();
		}
		//sqliteManager.cerrarDB();

		// Retorno de resultado
		return items;
	}
	
	//Método que devuelve los valores de un repuesto crown por su código
	public HashMap<String, String> royal_consultarRepuestoCrownCodigo(String codigo) {
		HashMap<String, String> repuesto = null;

		// Crea y ejecuta el query
		String sql = 
				"SELECT *"
				+ "FROM referenciaRepuestosCrown AS rpc "
				+ "WHERE "
					+ "rpc.codigo = '" + codigo + "'";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			if (cursor.moveToFirst()) {	
					// Extrae los datos del montacarga
					String codigoRepuesto = cursor.getString(0);
					String descripcion = cursor.getString(1);
					// Inicia el objeto
					repuesto = new HashMap<String, String>();
					repuesto.put("codigo", codigoRepuesto);
					repuesto.put("descripcion", descripcion);
			}
			cursor.close();
		}
		return repuesto;
	}
	
	/*
	 *  MÉTODOS PARA ROYAL - FIN
	 *  */
	
	
	// Permite consultar la información de un repuesto por código
	public HashMap<String, String> agenciaAlemana_consultarRepuesto(String codigo, HashMap<String, String> objetoDatos) {
		HashMap<String, String> item = null;

		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		// Crea y ejecuta el query

		String sql = 
				"SELECT codigoSAP,codigoBarras,referenciaFabricante,descripcion,grupo " + 
				"FROM listaArticulos " + 
				"WHERE " +
				"referenciaFabricante = '" + codigo + "' " +
				"AND codigoBarras = '" + objetoDatos.get("Código Barras") + "' " +
				"limit 1";

		Log.d("sebas", "query: " + sql);
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			if (cursor.moveToFirst()) {			
				// Extrae los datos del producto
				String codigoSAP = cursor.getString(0);
				String codigoBarras = cursor.getString(1);
				String referenciaFabricante = cursor.getString(2);
				String descripcion = cursor.getString(3)==null?"":cursor.getString(3);
				String grupo = cursor.getString(4);
				
				// Inicia el objeto
				item = new HashMap<String, String>();
				item.put("codigoSAP", codigoSAP);
				item.put("codigoBarras", codigoBarras);
				item.put("referenciaFabricante", referenciaFabricante);
				item.put("descripcion", descripcion);
				item.put("grupo", grupo);
			}
			cursor.close();
		}
		//sqliteManager.cerrarDB();

		// Retorno de resultado
		return item;
	}
		
	// Permite consultar la información de un repuesto por código
	public HashMap<String, String> almatec_consultarRepuesto(String codigo, HashMap<String, String> objetoDatos) {
		HashMap<String, String> item = null;
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		// Crea y ejecuta el query
		String sql = 
				"SELECT codigoArticulo, descripcionArticulo, subinventario, localizador, uomProducto, uen, bodega " + 
				"FROM listaRepuestos " + 
				"WHERE " +
				"codigoArticulo = '" + codigo + "' " +
				"AND bodega = '" + objetoDatos.get("Bodega") + "' " +
				"AND subinventario = '" + objetoDatos.get("Subinventario") + "'";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			if (cursor.moveToFirst()) {			
				// Extrae los datos del producto
				String codigoArticulo = cursor.getString(0);
				String descripcionArticulo = cursor.getString(1);
				String subinventario = cursor.getString(2);
				String localizador = cursor.getString(3);
				String uomProducto = cursor.getString(4);
				String uen = cursor.getString(5);
				String bodega = cursor.getString(6);
				
				// Inicia el objeto
				item = new HashMap<String, String>();
				item.put("codigoArticulo", codigoArticulo);
				item.put("descripcionArticulo", descripcionArticulo);
				item.put("subinventario", subinventario);
				item.put("localizador", localizador);
				item.put("tipoUnidad", uomProducto);
				item.put("uen", uen);
				item.put("bodega", bodega);
			}
			cursor.close();
		}
		//sqliteManager.cerrarDB();

		// Retorno de resultado
		return item;
	}
	
	// Consulta el total de coincidencias de repuestos
	// TODO: 
	public int agenciaAlemana_consultarTotalRepuestosPatron(String patron) {
		patron = patron.toUpperCase();
		int numeroItems = 0;
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Crea y ejecuta el query
		String sql = 
			"SELECT count(*) FROM listaArticulos " + 
			"WHERE " +
				"UPPER(referenciaFabricante) LIKE '%" + patron + "%' " +
				"OR codigoBarras LIKE '%" + patron + "%' " +
				"OR UPPER(descripcion) LIKE '%" + patron + "%' ";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				// Extrae los datos
				numeroItems = cursor.getInt(0);
			}
			cursor.close();
		}
		//sqliteManager.cerrarDB();

		// Retorno de resultado
		return numeroItems;
	}
	
	// Consulta el total de coincidencias de repuestos
	// TODO: 
	public int almatec_consultarTotalRepuestosPatron(String patron) {
		patron = patron.toUpperCase();
		int numeroItems = 0;
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Crea y ejecuta el query
		String sql = 
			"SELECT count(*) FROM listaRepuestos " + 
			"WHERE " +
				"codigoArticulo LIKE '%" + patron + "%' " +
				"OR UPPER(descripcionArticulo) LIKE '%" + patron + "%' ";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				// Extrae los datos
				numeroItems = cursor.getInt(0);
			}
			cursor.close();
		}
		//sqliteManager.cerrarDB();

		// Retorno de resultado
		return numeroItems;
	}
	
	// Consulta los repuestos que coinciden con el patron
	// TODO: 
	public Vector<LinkedHashMap<String, String>> agenciaAlemana_consultarRepuestosPatron(String patron) {
		patron = patron.toUpperCase();
		Vector<LinkedHashMap<String, String>> items = null;
		LinkedHashMap<String, String> item = null;
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Crea y ejecuta el query
		String sql = 
				"SELECT "
				+ "codigoBarras, "
				+ "referenciaFabricante, "
				+ "descripcion , " 
				+ "grupo " +
				"FROM listaArticulos " + 
				"WHERE " +
					"UPPER(referenciaFabricante) LIKE '%" + patron + "%' " +
					"OR codigoBarras LIKE '%" + patron + "%' " +
					"OR UPPER(descripcion) LIKE '%" + patron + "%' ";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			int numeroProductos = cursor.getCount();
			if (cursor.moveToFirst()) {
				items = new Vector<LinkedHashMap<String, String>>(numeroProductos);
				do {
					// Extrae los datos del producto
					String codigoBarras 				= cursor.getString(0);
					String referenciaFabricante 		= cursor.getString(1);
					String descripcion 		= cursor.getString(2);
					String grupo 	= cursor.getString(3);

					// Inicia el objeto
					item = new LinkedHashMap<String, String>();
					item.put("Referencia", referenciaFabricante);
					item.put("Código Barras", codigoBarras);
					item.put("Descripción", descripcion);
					item.put("Grupo", grupo);
					
					// Adiciona al vector
					items.addElement(item);
				} while (cursor.moveToNext());
			}
			cursor.close();
		}
		//sqliteManager.cerrarDB();

		// Retorno de resultado
		return items;
	}
	
	// Consulta los repuestos que coinciden con el patron
	// TODO: 
	public Vector<LinkedHashMap<String, String>> almatec_consultarRepuestosPatron(String patron) {
		patron = patron.toUpperCase();
		Vector<LinkedHashMap<String, String>> items = null;
		LinkedHashMap<String, String> item = null;
		//sqliteManager.abrirDB(SQLiteManager.LECTURA_ESCRITURA);
		
		// Crea y ejecuta el query
		String sql = 
				"SELECT "
				+ "bodega, subinventario, "
				+ "localizador, codigoArticulo, descripcionArticulo, "
				+ "cantidadInventario, cantidadReservada, disponibleReservar " + 
				"FROM listaRepuestos " + 
				"WHERE " +
					"codigoArticulo LIKE '%" + patron + "%' " +
					"OR UPPER(descripcionArticulo) LIKE '%" + patron + "%' " +
				"ORDER BY descripcionArticulo";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			int numeroProductos = cursor.getCount();
			if (cursor.moveToFirst()) {
				items = new Vector<LinkedHashMap<String, String>>(numeroProductos);
				do {
					// Extrae los datos del producto
					String bodega 				= cursor.getString(0);
					String subinventario 		= cursor.getString(1);
					//String localizador 			= cursor.getString(3);
					String codigoArticulo 		= cursor.getString(3);
					String descripcionArticulo 	= cursor.getString(4);
					int cantidadInventario 		= cursor.getInt(5);
					int cantidadReservada 		= cursor.getInt(6);
					int disponibleReservar 		= cursor.getInt(7);

					// Inicia el objeto
					item = new LinkedHashMap<String, String>();
					item.put("Descripción", descripcionArticulo);
					item.put("Código", codigoArticulo);
					item.put("Bodega", bodega);
					item.put("Subinventario", subinventario);
					item.put("Cant. Inventario",  String.valueOf(cantidadInventario));
					item.put("Cant. Reservada",  String.valueOf(cantidadReservada));
					item.put("Disp. Reservar",  String.valueOf(disponibleReservar));
					
					// Adiciona al vector
					items.addElement(item);
				} while (cursor.moveToNext());
			}
			cursor.close();
		}
		//sqliteManager.cerrarDB();

		// Retorno de resultado
		return items;
	}
	
	// TODO: 
	// Método que permite almacenar los datos adicionales 
	public void guardarDatosAdicionales (AplicacionOrdenesServicio aplicacion, Formulario formulario, Configuracion configApp, boolean borrarDatos) {
		// Crea variables auxiliares para crear cadena sql
		StringBuilder sql = new StringBuilder();
		StringBuilder camposConsulta = new StringBuilder();
		StringBuilder values = new StringBuilder();
		List<String[]> tablasAsociadas = formulario.getTablaAsociadas();
		Vector<Campo> campos = formulario.getCampos();
		String[] campoLlave = formulario.getCampoLlave();
		
		// Se recorre las tablas asociadas al formulario
		for(int indiceTabla = 0; indiceTabla < tablasAsociadas.size(); indiceTabla++){
			
			// Elimina los registros en la tabla asociada teniendo como condicion la bandera borrarDatos
			if (borrarDatos) {
				StringBuilder sqlDelete = new StringBuilder();
				sqlDelete.append("DELETE FROM " + tablasAsociadas.get(indiceTabla)[1] + " WHERE ");
				if(campoLlave != null) sqlDelete.append(campoLlave[0]).append(" = ").append(campoLlave[1]);
				ejecutarSQL(sqlDelete.toString());
			}
			
			// Crea queries y concatena nombre de la tabla destino
			sql.append("INSERT INTO " + tablasAsociadas.get(indiceTabla)[1] + " (");
			
			// Recorre los campos del formulario
			for(int indiceCampo = 0; indiceCampo < campos.size(); indiceCampo++){
				Campo campo = campos.get(indiceCampo);
				String valorCampo = campo.getValor();

				// Evalua si NO ES campo Detalle
				if(!campo.getModoEdicion().equals("Detalle")){
					// Analiza Campo "Típico"
					List<String[]> camposAsociados = campo.getCamposAsociados();
					
					// Valida si tiene campos asociados
					if(camposAsociados != null){
						// Recorre los campos asociados a cada formulario
						for(int indiceCampoAsociado = 0; indiceCampoAsociado < camposAsociados.size();indiceCampoAsociado++){
							String[] campoAsociado = camposAsociados.get(indiceCampoAsociado);
							
							// Valida si el id de la tabla asociada es igual al id tabla del campos asociado
							if(tablasAsociadas.get(indiceTabla)[1].equals(campoAsociado[0])){
								
								// Adiciona separadores si existen datos
								if(values.length() > 0) values.append(",");
								if(camposConsulta.length() > 0) camposConsulta.append(",");
								
								// Extrae y adiciona al query el campo llave
								if(indiceCampo==0 && campoLlave != null){
									camposConsulta.append(campoLlave[0] + ",");
									values.append("'" + campoLlave[1] + "',");
								}
								
								// Adiciona campo y valor
								camposConsulta.append(campoAsociado[1]);
								String valor = (valorCampo != null ? valorCampo.replace("'", "") : valorCampo);
								values.append("'" + valor + "'");
							}
						}
					}
					
					// TODO: Evalúa si el campo es AdjuntaFotos para colocar comando wamp de envío de imagenes
					if (campo.getModoEdicion().equals("AdjuntaFotos")) {
		    			int idOrdenServicio = aplicacion.getOrdenServicioActiva().getId();
		    			int idVisita = aplicacion.getOrdenServicioActiva().getVisitaActiva().getId();
						Intent intentoServicioNovedades = new Intent(contexto, ServicioNovedades.class);
				    	intentoServicioNovedades.putExtra("tipoIntent", "FOTO");
				    	intentoServicioNovedades.putExtra("idOrdenServicio", idOrdenServicio);
				    	intentoServicioNovedades.putExtra("idVisita", idVisita);
				    	intentoServicioNovedades.putExtra("datosFotos", valorCampo);
				    	contexto.startService(intentoServicioNovedades);
					}
				} else {
					// Analiza Campo "Detalle"
					int idSubformulario = Integer.parseInt(campo.getDatosAdicionales());
					ControladorFormularios controladorFormularios = new ControladorFormularios();
					
					// Parsea el campo detalle de String a Json
					JSONArray formulariosDetalleJson;
					int numeroItems = 0;
					try {
						if (valorCampo != null) {
							formulariosDetalleJson = new JSONArray(valorCampo);
							numeroItems = formulariosDetalleJson.length();
						} else {
							formulariosDetalleJson = new JSONArray();
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					
					// Carga desde base de datos un formulario de referencia para el detalle
					Formulario formularioReferencia = controladorFormularios.cargarFormularioLocal(
							configApp.getSqliteRoutines(), "", 
							Formulario.APLICACION, idSubformulario);
					
					// Convierte el Json a un vector de vector de campos
					Vector<Vector<Campo>> vectorCampos = controladorFormularios.jsonToCampoDetalle(
							campo.getValor(), formularioReferencia); 
					 
					// Recorre los items del detalle y por cada uno construye un objeto formulario y guarda los datos
					for(int indiceItemDetalle = 0; indiceItemDetalle < numeroItems; indiceItemDetalle++){
						Vector<Campo> camposDetalle = vectorCampos.get(indiceItemDetalle);
						Formulario formularioDetalle = new Formulario(formularioReferencia.getId(), 
								formularioReferencia.getNombre(), formularioReferencia.getIcono(), 
								camposDetalle, formularioReferencia.getTipo(), 
								formularioReferencia.isMultiple(), formularioReferencia.getTablaAsociadas());
						
						// Propaga el campo llave original
						formularioDetalle.setCampoLlave(campoLlave);
						
						// Vuelve a llamar la función para almacenar el detalle (Recursiva)
						guardarDatosAdicionales(aplicacion, formularioDetalle, configApp, (indiceItemDetalle == 0 ? true : false));
					}
				}
			}
			
			// Termina sql y concatena los values
			sql.append(camposConsulta.toString()).append(") VALUES(").append(values.toString()).append(")");
			
			// Ejecuta el query
			ejecutarSQL(sql.toString());
		}
	}
	
	// TODO: Método que permite consultar la informacion de una tabla adicional
	public HashMap<String,String> consultarDatosTablaAdicional(String nombreTabla, String campoLlave, int valorLlave){
		HashMap<String,String> datosAdicionales = null;
		try{
			String sql = "SELECT * FROM " + nombreTabla + " WHERE " + campoLlave + " = " + valorLlave;
			Cursor cursor = ejecutarConsultaSQL(sql, null);
			datosAdicionales = new HashMap<String, String>();
			if (cursor != null){
				if(cursor.moveToFirst()){
					for(int i=0;i<cursor.getColumnCount();i++){
						datosAdicionales.put(cursor.getColumnName(i), cursor.getString(i));
					}
				}
				cursor.close();
			}
		}catch(Exception e){
			Log.e("Error", e.getMessage());
		}

		// Retorno...
		return datosAdicionales;
	}	

	
	/*****************************************************************/
	// MÉTODOS DE MEPAL
	/*****************************************************************/
	// Actualiza el estado de la orden
	public void mepal_actualizarEstadoOrdenServicio(OrdenServicio ordenServicio) {			
		// Extrae el formulario de la visita
		Formulario formularioVisita = ordenServicio.getVisitaActiva().getFormulario();
		SparseArray<Campo> camposVisita = formularioVisita.getCamposHash();
		
		// Actualiza el estado de la orden en los datos adicionales
		String datosAdicionales = ordenServicio.getDatosAdicionales();
		if(datosAdicionales != null){
			try {
				JSONObject jsonObject = new JSONObject(datosAdicionales);
				jsonObject.put("estado", camposVisita.get(783).getValor());
				ordenServicio.setDatosAdicionales(jsonObject.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		mepal_ejecutarActualizacionEstadoOrdenServicio(camposVisita.get(783).getValor(), ordenServicio.getId());
	}
	
	public void mepal_ejecutarActualizacionEstadoOrdenServicio(String estado, int idOrden) {
		// Crea y ejecuta el query de actualización
		String sql = 
			"UPDATE datosOrdenesServicio " + 
			"SET estado = '" + estado + "', sincronizado = 0 " +
			"WHERE idOrdenServicio = " + idOrden;
		ejecutarSQL(sql);
	}
	
	// Método que permite consultar el total de proyectos
	public int mepal_consultarTotalProyectosPatron(String patron) {
		patron = patron.toUpperCase();
		int numeroItems = 0;
		
		// Crea y ejecuta el query
		String sql = 
			"SELECT count(*) FROM " +
				"(SELECT codigoProyecto, descripcionProyecto " +
				"FROM datosProyectos GROUP BY codigoProyecto) AS proyectos " +
			"WHERE " +
				"proyectos.codigoProyecto LIKE '%" + patron + "%' " +
				"OR UPPER(proyectos.descripcionProyecto) LIKE '%" + patron + "%' ";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				// Extrae los datos
				numeroItems = cursor.getInt(0);
			}
			cursor.close();
		}

		// Retorno de resultado
		return numeroItems;
	}
	
	// Método que permite traer los proyectos
	public Vector<LinkedHashMap<String, String>> mepal_consultarProyectosPatron(String patron) {
		patron = patron.toUpperCase();
		Vector<LinkedHashMap<String, String>> items = null;
		LinkedHashMap<String, String> item = null;
		
		// Crea y ejecuta el query
		String sql = 
			"SELECT * FROM " +
				"(SELECT " +
					"codigoProyecto, descripcionProyecto, codigoCliente, nombreCliente, uen, distritoVenta, " +
					"distritoInstalacion, fechaEntrega, fechaCompromisoInsta, fechaFinInsta " + 
				"FROM datosProyectos " + 
				"GROUP BY codigoProyecto) AS proyectos " +
			"WHERE " +
				"proyectos.codigoProyecto LIKE '%" + patron + "%' " +
				"OR UPPER(proyectos.descripcionProyecto) LIKE '%" + patron + "%' ";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			int numeroProductos = cursor.getCount();
			if (cursor.moveToFirst()) {
				items = new Vector<LinkedHashMap<String, String>>(numeroProductos);
				do {
					// Extrae los datos del producto
					String codigoProyecto		= cursor.getString(0);
					String descripcionProyecto 	= cursor.getString(1);
					String codigoCliente 		= cursor.getString(2);
					String nombreCliente 		= cursor.getString(3);
					String uen 					= cursor.getString(4);
					String distritoVenta 		= cursor.getString(5);
					String distritoInstalacion 	= cursor.getString(6);
					String fechaEntrega 		= cursor.getString(7);
					String fechaCompromisoInsta = cursor.getString(8);
					String fechaFinInsta 		= cursor.getString(9);

					// Inicia el objeto
					item = new LinkedHashMap<String, String>();
					item.put("Descripción", descripcionProyecto);
					item.put("Proyecto No.", codigoProyecto);
					item.put("Código Cliente", codigoCliente);
					item.put("Cliente", nombreCliente);
					item.put("Uen",  uen);
					item.put("Distrito Venta",  distritoVenta);
					item.put("Distrito Instalación",  distritoInstalacion);
					item.put("Fecha Entrega",  fechaEntrega);
					//item.put("Fecha Compromiso",  fechaCompromisoInsta);
					//item.put("Fecha Fin Instalación",  fechaFinInsta);
					
					// Adiciona al vector
					items.addElement(item);
				} while (cursor.moveToNext());
			}
			cursor.close();
		}

		// Retorno de resultado
		return items;
	}
	
	// Método que permite consultar el total de pedidos
	public int mepal_consultarTotalPedidosPatron(String patron, HashMap<String, String> datosAdicionales) {
		patron = patron.toUpperCase();
		int numeroItems = 0;
		
		// Extrae datos adicionales
		String codigoProyecto = datosAdicionales.get("codigoProyecto");
		
		// Crea y ejecuta el query
		String sql = 
			"SELECT count(*) FROM " +
				"(SELECT codigoProyecto, codigoPedido " +
				"FROM datosProyectos GROUP BY codigoProyecto, codigoPedido) AS pedidos " +
			"WHERE " +
				"pedidos.codigoProyecto = '" + codigoProyecto + "' " +
				"AND pedidos.codigoPedido LIKE '%" + patron + "%'";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				// Extrae los datos
				numeroItems = cursor.getInt(0);
			}
			cursor.close();
		}

		// Retorno de resultado
		return numeroItems;
	}
	
	// Método que permite traer los pedidos
	public Vector<LinkedHashMap<String, String>> mepal_consultarPedidosPatron(String patron, HashMap<String, String> datosAdicionales) {
		patron = patron.toUpperCase();
		Vector<LinkedHashMap<String, String>> items = null;
		LinkedHashMap<String, String> item = null;
		
		// Extrae datos adicionales
		String codigoProyecto = datosAdicionales.get("codigoProyecto");
		
		// Crea y ejecuta el query
		String sql = 
			"SELECT * FROM " +
				"(SELECT " +
					"codigoProyecto, codigoPedido, tipoPedido, estadoPedido, ciudadDestino, vendedor, fechaPrometida " + 
				"FROM datosProyectos " + 
				"GROUP BY codigoProyecto, codigoPedido) AS pedidos " +
			"WHERE " +
				"pedidos.codigoProyecto = '" + codigoProyecto + "' " +
				"AND pedidos.codigoPedido LIKE '%" + patron + "%'";

		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			int numeroProductos = cursor.getCount();
			if (cursor.moveToFirst()) {
				items = new Vector<LinkedHashMap<String, String>>(numeroProductos);
				do {
					// Extrae los datos del producto
					String codigoProyectoArticulo	= cursor.getString(0);
					String codigoPedido 	= cursor.getString(1);
					String tipoPedido 		= cursor.getString(2);
					String estadoPedido 	= cursor.getString(3);
					String ciudadDestino 	= cursor.getString(4);
					String vendedor 		= cursor.getString(5);
					String fechaPrometida 	= cursor.getString(6);

					// Inicia el objeto
					item = new LinkedHashMap<String, String>();
					item.put("Proyecto No.", codigoProyectoArticulo);
					item.put("Pedido No.", codigoPedido);
					item.put("Tipo", tipoPedido);
					item.put("Estado",  estadoPedido);
					item.put("Ciudad destino",  ciudadDestino);
					item.put("Vendedor",  vendedor);
					item.put("Fecha prometida",  fechaPrometida);
					
					// Adiciona al vector
					items.addElement(item);
				} while (cursor.moveToNext());
			}
			cursor.close();
		}

		// Retorno de resultado
		return items;
	}
	
	// Método que permite consultar el total de articulos
	public int mepal_consultarTotalArticulosPatron(String patron, HashMap<String, String> datosAdicionales) {
		patron = patron.toUpperCase();
		int numeroItems = 0;
		
		// Extrae datos adicionales
		String codigoProyecto = datosAdicionales.get("codigoProyecto");
		String codigoPedido = datosAdicionales.get("codigoPedido");
		
		// Crea y ejecuta el query
		String sql = 
			"SELECT count(*) FROM " +
				"(SELECT codigoProyecto, codigoPedido, codigoArticulo, descripcionArticulo " +
				"FROM datosProyectos GROUP BY codigoProyecto, codigoPedido, codigoArticulo) AS articulos " +
			"WHERE " +
				"articulos.codigoProyecto = '" + codigoProyecto + "' " +
				"AND articulos.codigoPedido = '" + codigoPedido + "' " +
				"AND (articulos.codigoArticulo LIKE '%" + patron + "%' " +
				"OR UPPER(articulos.descripcionArticulo) LIKE '%" + patron + "%') ";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				// Extrae los datos
				numeroItems = cursor.getInt(0);
			}
			cursor.close();
		}

		// Retorno de resultado
		return numeroItems;
	}
	
	// Método que permite traer los articulos
	public Vector<LinkedHashMap<String, String>> mepal_consultarArticulosPatron(String patron, HashMap<String, String> datosAdicionales) {
		patron = patron.toUpperCase();
		Vector<LinkedHashMap<String, String>> items = null;
		LinkedHashMap<String, String> item = null;
		
		// Extrae datos adicionales
		String codigoProyecto = datosAdicionales.get("codigoProyecto");
		String codigoPedido = datosAdicionales.get("codigoPedido");
		
		// Crea y ejecuta el query
		String sql = 
			"SELECT * FROM " +
				"(SELECT " +
					"codigoProyecto, codigoPedido, codigoArticulo, descripcionArticulo, " +
					"cantidadSolicitada, cantidadEnviada, proveedor, ordenCompraEnvio, billTo, shipTo, direccionEnvio " + 
				"FROM datosProyectos " + 
				"GROUP BY codigoProyecto, codigoPedido, codigoArticulo) AS articulos " +
			"WHERE " +
				"articulos.codigoProyecto = '" + codigoProyecto + "' " +
				"AND articulos.codigoPedido = '" + codigoPedido + "' " +
				"AND (articulos.codigoArticulo LIKE '%" + patron + "%' " +
				"OR UPPER(articulos.descripcionArticulo) LIKE '%" + patron + "%') ";

		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			int numeroProductos = cursor.getCount();
			if (cursor.moveToFirst()) {
				items = new Vector<LinkedHashMap<String, String>>(numeroProductos);
				do {
					// Extrae los datos del producto
					String codigoProyectoArticulo	= cursor.getString(0);
					String codigoPedidoArticulo 	= cursor.getString(1);
					String codigoArticulo 		= cursor.getString(2);
					String descripcionArticulo 	= cursor.getString(3);
					String cantidadSolicitada 	= cursor.getString(4);
					String cantidadEnviada 		= cursor.getString(5);
					String proveedor 			= cursor.getString(6);
					String ordenCompraEnvio 	= cursor.getString(7);
					String billTo 	 			= cursor.getString(8);
					String shipTo 				= cursor.getString(9); 
					
					// Inicia el objeto
					item = new LinkedHashMap<String, String>();
					item.put("Proyecto No.", codigoProyectoArticulo);
					item.put("Pedido No.", codigoPedidoArticulo);
					item.put("Código articulo", codigoArticulo);
					item.put("Descripción",  descripcionArticulo);
					item.put("Cantidad Solicitada",  cantidadSolicitada);
					item.put("Cantidad Enviada",  cantidadEnviada);
					item.put("Proveedor",  proveedor);
					item.put("Orden de compra",  ordenCompraEnvio);
					item.put("BillTo",  billTo);
					item.put("ShipTo",  shipTo);

					// Adiciona al vector
					items.addElement(item);
				} while (cursor.moveToNext());
			}
			cursor.close();
		}

		// Retorno de resultado
		return items;
	}

	// Método que consulta la información de un item
	public HashMap<String, String> mepal_consultarArticulo(String codigo) {
		HashMap<String, String> item = null;

		// Crea y ejecuta el query
		String sql = 
			"SELECT * FROM " +
				"(SELECT " +
					"codigoProyecto, codigoPedido, codigoArticulo, descripcionArticulo, " +
					"cantidadSolicitada, cantidadEnviada, proveedor, ordenCompraEnvio  , billTo, shipTo, direccionEnvio " + 
				"FROM datosProyectos " + 
				"GROUP BY codigoProyecto, codigoPedido, codigoArticulo) AS articulos " +
			"WHERE " +
				"articulos.codigoArticulo = '" + codigo + "' ";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			if (cursor.moveToFirst()) {			
				// Extrae los datos del producto
				String codigoProyecto = cursor.getString(0);
				String codigoPedido = cursor.getString(1);
				String codigoArticulo = cursor.getString(2);
				String descripcionArticulo = cursor.getString(3);
				String cantidadSolicitada = cursor.getString(4);
				String cantidadEnviada 		= cursor.getString(5);
				String proveedor = cursor.getString(6);
				String ordenCompraEnvio 	= cursor.getString(7);
				String billTo 	 			= cursor.getString(8);
				String shipTo 				= cursor.getString(9); 
				
				// Inicia el objeto
				item = new HashMap<String, String>();
				item.put("codigoProyecto", codigoProyecto);
				item.put("codigoPedido", codigoPedido);
				item.put("codigoArticulo", codigoArticulo);
				item.put("descripcionArticulo", descripcionArticulo);
				item.put("cantidadSolicitada", cantidadSolicitada);
				item.put("cantidadEnviada", cantidadEnviada);
				item.put("proveedor", proveedor);
				item.put("ordenCompraEnvio", ordenCompraEnvio);
				item.put("billTo",  billTo);
				item.put("shipTo",  shipTo);

			}
			cursor.close();
		}
		
		// Retorno
		return item;
	}

	// Consulta las fallas a nivel de sistema
	public String[] almatec_consultarFallasSistema() {
		String[] items = null;
		
		// Crea y ejecuta el query
		String sql = 
			"SELECT "
			+ "codigo AS codigo, "
			+ "descripcion AS descripcion "
			+ "FROM datosSistemaFallas;";

		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			int numeroProductos = cursor.getCount();
			if (cursor.moveToFirst()) {
				items = new String[numeroProductos+1];
				items[0] = "";
				int indice = 1;
				do {
					// Extrae los datos de la falla
					String descripcion 	= cursor.getString(1);

					// Adiciona al arreglo
					items[indice] = descripcion;
					indice++;
				} while (cursor.moveToNext());
			}
			cursor.close();
		}

		// Retorno de resultado
		return items;
	}
	
	// Consulta las fallas
	public String[] almatec_consultarFallas(String fallaSistema) {
		String[] items = {};
		
		// Crea y ejecuta el query
		String sql = 
			"SELECT "
				+ "df.codigo AS codigo, "
				+ "df.descripcion AS descripcion, "
				+ "dfs.descripcion AS descripcionFallaSistema "
			+ "FROM "
				+ "datosFallas AS df "
			+ "INNER JOIN "
				+ "datosSistemaFallas AS dfs "
				+ "ON df.codigoFallaSistema = dfs.codigo "
				+ "AND dfs.descripcion = '" + fallaSistema + "';";

		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			int numeroProductos = cursor.getCount();
			if (cursor.moveToFirst()) {
				items = new String[numeroProductos+1];
				items[0] = "";
				int indice = 1;
				do {
					// Extrae los datos de la falla
					String descripcion 	= cursor.getString(1);

					// Adiciona al arreglo
					items[indice] = descripcion;
					indice++;
				} while (cursor.moveToNext());
			}
			cursor.close();
		}

		// Retorno de resultado
		return items;
	}

	public Vector<LinkedHashMap<String, String>> mepal_consultarListaActividades() {
		Vector<LinkedHashMap<String, String>> items = null;
		LinkedHashMap<String, String> item = null;
		
		// Crea y ejecuta el query
		String sql = "SELECT descripcion, unidadMedida FROM listaActividades ORDER BY descripcion";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			int numeroProductos = cursor.getCount();
			if (cursor.moveToFirst()) {
				items = new Vector<LinkedHashMap<String, String>>(numeroProductos);
				do {
					// Extrae los datos del producto
					String descripcion	= cursor.getString(0);
					String unidadMedida = cursor.getString(1);

					// Inicia el objeto
					item = new LinkedHashMap<String, String>();
					item.put("Actividad", descripcion);
					item.put("Medida", unidadMedida);

					// Adiciona al vector
					items.addElement(item);
				} while (cursor.moveToNext());
			}
			cursor.close();
		}

		// Retorno de resultado
		return items;
	}
	
	
	// Método que permite consultar los datos de la programcion de una visita
	// basados en el código de OT de una orden de servicio
	public  LinkedHashMap <String, String>  routine_obtenerProgramacionVisita(String codigoOTOrden, String codigoVisita) {
		LinkedHashMap<String, String> datosVisitas = null;
		
		//Falta el código del técnico que también es primary key
		// Crea y ejecuta el query
		String sql = 
			"SELECT * FROM programacionVisita"
			+ " WHERE codigoOT = '" + codigoOTOrden + "'"
			+ " AND codigoVisita = '" + codigoVisita + "'";
		Cursor cursor = ejecutarConsultaSQL(sql, null);
		
		// Evalua si hay datos y los extrae
		if (cursor != null){
			if(cursor.moveToFirst()) {

				// Extrae los datos del query
				datosVisitas = new LinkedHashMap<String, String>();
				datosVisitas.put("CódigoOT", cursor.getString(0));
				datosVisitas.put("Código Visita", cursor.getString(1));
				datosVisitas.put("Código Técnico", cursor.getString(2));
				datosVisitas.put("Fecha Programada Inicio", cursor.getString(3));
				datosVisitas.put("Fecha Programada Fin", cursor.getString(4));
				datosVisitas.put("Estado", cursor.getString(5));
					
				cursor.close();
				}
			}
		return datosVisitas;
	}
	
	// Consulta las ordenes de servicio con los datos de visitas
	public  Vector < LinkedHashMap <String, String> > routine_consultarOrdenesXVisitas() {
		Vector<LinkedHashMap<String, String>> datos = null;
		LinkedHashMap<String, String> datosVisitas = null;

		// Crea y ejecuta el query
		String sql = 
				"SELECT dos.idOrdenServicio, dos.codigoOT, pv.codigoVisita, dos.tipoOrden, dos.codigoEquipo,"
				+ " dos.nombreCliente, dos.direccionOT, pv.fechaProgramadaInicio , pv.fechaProgramadaFin,"
				+ " pv.estado FROM datosOrdenesServicio AS dos "
				+ " INNER JOIN programacionVisita AS pv"
				+ " on dos.codigoOT = pv.codigoOT"
				+ " WHERE pv.estado = 'PROGRAMADA'"
				+ " OR ( pv.estado = 'FINALIZADA' AND ( julianday() - julianday(pv.fechaProgramadaFin) <= 2) )"
				+ " ORDER BY pv.fechaProgramadaInicio DESC";
		
		Cursor cursor = ejecutarConsultaSQL(sql, null);
		
		// Evalua si hay datos y los extrae
		if (cursor != null){
			int numeroOrdenes = cursor.getCount(); 
			if(cursor.moveToFirst()) {
				datos = new Vector<LinkedHashMap<String, String>>(numeroOrdenes);
				do {
					// Extrae los datos del query
					datosVisitas = new LinkedHashMap<String, String>();
					datosVisitas.put("Orden #", cursor.getString(0));
					datosVisitas.put("CódigoOT", cursor.getString(1));
					datosVisitas.put("Código Visita", cursor.getString(2));
					datosVisitas.put("Tipo", cursor.getString(3));
					datosVisitas.put("Equipo", cursor.getString(4));
					datosVisitas.put("Cliente", cursor.getString(5));
					datosVisitas.put("Dirección", cursor.getString(6));
					
					//Se modifica el string fecha inicio con fecha fin para cuestiones gráficas, falta validar si las fechas pueden estar vacías.
					String fechaInicio =  cursor.getString(7);
					String fechaFin = cursor.getString(8);
					String sub = fechaFin.substring(11, fechaFin.length());
					
					fechaInicio += " - " + sub;
					
					datosVisitas.put("Fecha", fechaInicio);					
					datosVisitas.put("Estado", cursor.getString(9));
					
					// Adiciona al vector
					datos.addElement(datosVisitas);
					
				} while(cursor.moveToNext());
			}
			cursor.close();
		}
		
		//Se retornan los datos
		return datos;
	}

	 // Rutina que permite actualizar el estado de la visita para Agencia Alemana
	public void agenciaAlemana_actualizarEstadoVisita(String estado, String codigoOT, String codigoVisita, String codigoTecnico) {
		//Query actualiza el estado de una visita mediante su primary key
		String queryProgramacion = 
				"UPDATE programacionVisita " +
				"SET estado ='" + estado + "' " +
				"WHERE codigoTecnico= '" + codigoTecnico + "' " +
				"AND codigoOT='"+ codigoOT + "' " +
				"AND codigoVisita='"+ codigoVisita +"'";
				
		// Ejecuta el query
		ejecutarSQL(queryProgramacion);
		
	}
	
	 // Rutina que permite actualizar el estado de la visita para Agencia Alemana
	public String agenciaAlemana_obtenerEstadoVisita(String codigoOT, String codigoVisita, String codigoTecnico) {
		String estado = "";
		// Crea y ejecuta el query
		String query = "SELECT estado FROM programacionVisita " +
				"WHERE codigoTecnico= '" + codigoTecnico + "' " +
				"AND codigoOT='"+ codigoOT + "' " +
				"AND codigoVisita='"+ codigoVisita +"'";
		
		Cursor cursor = ejecutarConsultaSQL(query, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			if(cursor.moveToFirst()) {
				estado = cursor.getString(0); 
			}
		}
		
		return estado;
	}
	
	 // Rutina que permite actualizar el estado de la visita para Agencia Alemana
	public String routine_obtenerTipoOrden(int idOrden) {
		String tipo = "";
		// Crea y ejecuta el query
		String query = "SELECT tipoOrden FROM datosOrdenesServicio " +
				"WHERE idOrdenServicio= " + idOrden;
		
		Cursor cursor = ejecutarConsultaSQL(query, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			if(cursor.moveToFirst()) {
				tipo = cursor.getString(0); 
			}
		}
		
		return tipo;
	}


	/****************************************************************************************************
	 * ****************** Metodo que trae el array de info de orden personalizada ***********************
	 ***************************************************************************************************/
	// Dependiendo del cliente
	public  Vector < LinkedHashMap <String, String> > routine_consultarOrdenesPersonalizado(int idClienteGlobal){
		Vector<LinkedHashMap<String, String>> datos = null;

		// Dependiendo del cliente se carga una listahasmap
		if(idClienteGlobal == 14){
			datos = routine_consultarOrdenesXVisitas();
		}else if(idClienteGlobal == 16){
			datos = routine_consultaCoexitoOrdenes();
		}

		//Se retornan los datos
		return datos;
	}


	// Consulta las ordenes de servicio con los datos de visitas
	public  Vector < LinkedHashMap <String, String> > routine_consultaCoexitoOrdenes() {
		Vector<LinkedHashMap<String, String>> datos = null;
		LinkedHashMap<String, String> datosOrden = null;

		// Crea y ejecuta el query
		String sql =
				"SELECT idOrdenServicio,fechaCreacion,codigoCliente,nombreCliente,actividad,estado "
						+ "FROM datosOrdenesServicio ";
						//+ "WHERE estado = 'NO ATENDIDA'";

		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null){
			int numeroOrdenes = cursor.getCount();
			if(cursor.moveToFirst()) {
				datos = new Vector<LinkedHashMap<String, String>>(numeroOrdenes);
				do {
					// Extrae los datos del query
					datosOrden = new LinkedHashMap<String, String>();
					datosOrden.put("Orden #", cursor.getString(0));
					datosOrden.put("Fecha Creación", cursor.getString(1));
					//datosOrden.put("Código Cliente", cursor.getString(2));
					datosOrden.put("Cliente", cursor.getString(3));
					datosOrden.put("Actividad", cursor.getString(4));
					//datosOrden.put("Estado", cursor.getString(5));
					/*
					//Se modifica el string fecha inicio con fecha fin para cuestiones gráficas, falta validar si las fechas pueden estar vacías.
					String fechaInicio =  cursor.getString(7);
					String fechaFin = cursor.getString(8);
					String sub = fechaFin.substring(11, fechaFin.length());

					fechaInicio += " - " + sub;

					datosOrden.put("Fecha", fechaInicio);
					datosOrden.put("Estado", cursor.getString(9));
					*/
					// Adiciona al vector
					datos.addElement(datosOrden);

				} while(cursor.moveToNext());
			}
			cursor.close();
		}

		//Se retornan los datos
		return datos;
	}

	//Método que devuelve los valores de una energiteca
	public HashMap<String, String> coexito_consultarEnergiteca(String codigo) {
		HashMap<String, String> datosEnergiteca = null;

		// Crea y ejecuta el query
		String sql =
				"SELECT  energiteca, correo1, correo2, ciudad "
						+ "FROM datosEnergiteca  "
						+ "WHERE "
						+ "energiteca = '" + codigo + "'";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				// Extrae los datos del montacarga
				String energiteca = cursor.getString(0);
				String correo1 = cursor.getString(1);
				String correo2 = cursor.getString(2);
				String ciudad = cursor.getString(3);

				// Inicia el objeto
				datosEnergiteca = new HashMap<String, String>();
				datosEnergiteca.put("energiteca", energiteca);
				datosEnergiteca.put("ciudad", ciudad);
				datosEnergiteca.put("correo1", correo1);
				datosEnergiteca.put("correo2", correo2);
			}
			cursor.close();
		}
		return datosEnergiteca;
	}

	// Busqueda dinamica del cliente
	public int consultarTotalEnergitecaPatron(String patron) {
		patron = patron.toUpperCase();
		int numeroItems = 0;

		// Crea y ejecuta el query
		String sql =
				"SELECT count(*) " +
						"FROM datosEnergiteca " +
						"WHERE " +
						"energiteca LIKE '%" + patron + "%' ";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				// Extrae los datos
				numeroItems = cursor.getInt(0);
			}
			cursor.close();
		}

		// Retorno de resultado
		return numeroItems;
	}

	// Retorna el vector de cliente de respueta
	public Vector<LinkedHashMap<String, String>> consultaEnergitecaVector(String patron) {
		patron = patron.toUpperCase();
		Vector<LinkedHashMap<String, String>> items = null;
		LinkedHashMap<String, String> item = null;

		// Crea y ejecuta el query
		String sql =
				"SELECT energiteca, correo1, correo2 " +
						"FROM datosEnergiteca " +
						"WHERE " +
						"energiteca LIKE '%" + patron + "%' ";
		Cursor cursor = ejecutarConsultaSQL(sql, null);

		// Evalua si hay datos y los extrae
		if (cursor != null) {
			int numeroProductos = cursor.getCount();
			if (cursor.moveToFirst()) {
				items = new Vector<LinkedHashMap<String, String>>(numeroProductos);
				do {
					// Extrae los datos del producto
					String energiteca 	= cursor.getString(0);
					String correo1 		= cursor.getString(1);
					String correo2 		= cursor.getString(2);

					// Inicia el objeto
					item = new LinkedHashMap<String, String>();
					item.put("Energiteca", energiteca);
					item.put("Correo1", correo1);
					item.put("Correo2", correo2);

					// Adiciona al vector
					items.addElement(item);
				} while (cursor.moveToNext());
			}
			cursor.close();
		}
		//sqliteManager.cerrarDB();

		// Retorno de resultado
		return items;
	}
}
