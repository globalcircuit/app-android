package com.gc.ordenesservicio;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gc.coregestionclientes.accesodatos.ConsumeWebService;
import com.gc.coregestionclientes.accesodatos.DownloadsManager;
import com.gc.coregestionclientes.interfaces.OnTaskCompleted;
import com.gc.coregestionclientes.librerias.JSONManager;
import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.objetos.Dispositivo;
import com.gc.ordenesservicio.accesodatos.SQLiteRoutinesOS;
import com.gc.ordenesservicio.novedades.ServicioNovedades;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ActividadLogin extends Activity implements OnTaskCompleted {
	public Dispositivo dispositivo;
	// Objetos
	private AplicacionOrdenesServicio aplicacion;
	private ToolsApp toolsApp;
	private ConsumeWebService consumeWS;
	private final int MY_PERMISSIONS = 100;
	// Elementos GrÃ¡ficos
	private TextView lbel_nmbre_clnte_global;
	private TextView lbel_vrfcndo_dspstvo;
	private Context context;
	// Parametros auxiliares
	private String mnsje_alrta = null;
	private int numeroOrdenesNoSincronizadas = 0;
	private int contadorCallbacksOrdenes = 0;
	private boolean vstas_sncrnzdas = true;
	public static final int MY_DEFAULT_TIMEOUT = 15000;
	// Almacena el estado de verificacion de datos No sincronizados,
	// ademas determina si ya es posible cambiar de actividad
	// 3. SincronizaciÃ³n de BD
	// 4. Cambio de actividad
	private int ESTADO_VERIFICACION = 2;
	String token;
	String email;
	private String m_Text = "";




	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.actividad_login);

		storagePermission();

		dispositivo = new Dispositivo();
		//	notificacionFirebase();

		// Inicializa el Objeto con la configuracion de la App para ser utilizado
		aplicacion = ((AplicacionOrdenesServicio)getApplicationContext());

		// Inicializa el objeto de ToolsApp
		toolsApp = new ToolsApp();

		// Inicializa el label lbel_nmbre_clnte_global
		lbel_nmbre_clnte_global = (TextView)findViewById(R.id.lbel_nmbre_clnte_global);
		lbel_vrfcndo_dspstvo = (TextView)findViewById(R.id.lbel_vrfcndo_dspstvo);



		/*
		// Verificar si tiene conexiÃ³n
		if(toolsApp.isConnected(this)){
			// Tiene ConexiÃ³n
			// Trae informaciÃ³n de la nube y actualiza, Consumir servicio de login
			Hashtable<String, String> prmtros_adcnles = new Hashtable<String, String>();
			prmtros_adcnles.put("IdDispositivo", aplicacion.getConfiguracionApp().getDispositivo().getId());
			prmtros_adcnles.put("Version", aplicacion.getConfiguracionApp().getDispositivo().getVersion());
			consumeWS = new ConsumeWebService(this);
			consumeWS.iniciarProceso("verificarDispositivo", prmtros_adcnles, false, null, "callback_InicializarParametros");
		} else {
			// No Tiene ConexiÃ³n
			iniciarSesionOffline();
		}

		 */
	}

	private void notificacionFirebase() {
		FirebaseInstanceId.getInstance().getInstanceId()
				.addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
					@Override
					public void onComplete(@NonNull Task<InstanceIdResult> task) {
						if (!task.isSuccessful()) {
							Log.v("Token", "getInstanceId failed", task.getException());
							return;
						}

						// Get new Instance ID token
						String token = task.getResult().getToken();

						// Log and toast
						//String msg = getString(R.string.msg_token_fmt, token);
						Log.v("Mensaje token", token);
						Toast.makeText(ActividadLogin.this, token, Toast.LENGTH_SHORT).show();
					}
				});

		notificaciones();
	}

	/*
	private void notificaciones() {

		RequestQueue queue = Volley.newRequestQueue(this);
		String url = "https://fcm.googleapis.com/fcm/send";
		JSONObject notification = new JSONObject();
		try {
			notification.put("title", "Java");
			notification.put("body", "Notificacion do Java");

		} catch (JSONException e) {
			e.printStackTrace();
		}

		JSONObject message = new JSONObject();
		try {
			message.put("to", "fFvjTq-nAIo:APA91bE0gMJIRtBDUjDkmpcXcO-fWpcrYWL8lz4O2-bjRvS6PlIdwOT1uVJ9NhZKDZnrAO6QF3En6ChP6Otl3ORPH7Yh7xAtA3EvjpLXAlTIudCYK0LYy76a1UbAYfefrjry3vttPmMD");
			message.put("priority", "high");
			message.put("notification", notification);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, url, message,
				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject jsonObject) {
						// response
						Log.d("Response", String.valueOf(jsonObject));
					}
				},
				new Response.ErrorListener()
				{
					@Override
					public void onErrorResponse(VolleyError error) {
						// error
						Log.d("Error.Response", String.valueOf(error));
					}
				}
		)
		{
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("Content-Type", "application/json");
				params.put("Authorization", "key=AAAAXvR41fY:APA91bG35m9dpHNAnHEEkW6Iub3ASDwOTIbsm0z0P2KB4shFG-SVlPaQsQrMVumJsGP7hUjqBpFTlCiJ4ao5wfwA6m7dapnGTM4WVUjmVB_0gItibPny0pwLwbUxvKaMKq8G5v1kZl6w");
				return params;

			}
		};

		queue.add(postRequest);
	}

	 */

	private void notificaciones() {

		RequestQueue queue = Volley.newRequestQueue(this);
		//	String url = "https://127.0.0.1:8080/garden_develop/notificacion";
		String url = "http://23.253.57.17:8081/ordenes_develop/notificacion";
		JSONObject notification = new JSONObject();
		try {
			notification.put("id", 807);
			notification.put("title", "Java");
			notification.put("body", "Javas");
		} catch (JSONException e) {
			e.printStackTrace();
		}



		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, url, notification,
				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject jsonObject) {
						// response
						Log.v("Response", String.valueOf(jsonObject));
					}
				},
				new Response.ErrorListener()
				{
					@Override
					public void onErrorResponse(VolleyError error) {
						// error
						if (error instanceof NetworkError) {
						} else if (error instanceof ServerError) {
						} else if (error instanceof AuthFailureError) {
						} else if (error instanceof ParseError) {
						} else if (error instanceof NoConnectionError) {
						} else if (error instanceof TimeoutError) {
							Toast.makeText(ActividadLogin.this,
									"Oops. Timeout error!",
									Toast.LENGTH_LONG).show();
						}
						Log.v("Error.Response", String.valueOf(error));
					}
				}

		)	{
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("Content-Type", "application/json");
				params.put("Authorization", "key=AAAAXvR41fY:APA91bG35m9dpHNAnHEEkW6Iub3ASDwOTIbsm0z0P2KB4shFG-SVlPaQsQrMVumJsGP7hUjqBpFTlCiJ4ao5wfwA6m7dapnGTM4WVUjmVB_0gItibPny0pwLwbUxvKaMKq8G5v1kZl6w");
				return params;

			}
		};

		queue.add(postRequest);
	}

	@Override
	protected void onResume() {
		super.onResume();

		// Registrar al LocalBroadcastManager
		//storagePermission();

		if(storagePermission() == true){
			aplicacion.inicializarObjetos();
			if(toolsApp.isConnected(this)){
				// Tiene ConexiÃ³n
				// Trae informaciÃ³n de la nube y actualiza, Consumir servicio de login
				if (aplicacion.getConfiguracionApp().getDispositivo().getToken() == null){
					FirebaseInstanceId.getInstance().getInstanceId()
							.addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
								@Override
								public void onComplete(@NonNull Task<InstanceIdResult> task) {
									if (!task.isSuccessful()) {
										Log.v("Token", "getInstanceId failed", task.getException());
										return;
									}

									// Get new Instance ID token
									token = task.getResult().getToken();
									email = (aplicacion.getConfiguracionApp().getDispositivo().getEmail() == null)?"":aplicacion.getConfiguracionApp().getDispositivo().getEmail();
									dispositivo.setToken(token);
									dispositivo.setEmail(email);
									Hashtable<String, String> prmtros_adcnles = new Hashtable<String, String>();
									prmtros_adcnles.put("IdDispositivo", aplicacion.getConfiguracionApp().getDispositivo().getId());
									prmtros_adcnles.put("Version", aplicacion.getConfiguracionApp().getDispositivo().getVersion());
									prmtros_adcnles.put("token", dispositivo.getToken());
									prmtros_adcnles.put("email", email );
									consumeWS = new ConsumeWebService(ActividadLogin.this);
									consumeWS.iniciarProceso("verificarDispositivo", prmtros_adcnles, false, null, "callback_InicializarParametros");

									// Log and toast
									//String msg = getString(R.string.msg_token_fmt, token);

									//Toast.makeText(ActividadLogin.this, token, Toast.LENGTH_SHORT).show();
								}
							});

				}else {
					Hashtable<String, String> prmtros_adcnles = new Hashtable<String, String>();
					prmtros_adcnles.put("IdDispositivo", aplicacion.getConfiguracionApp().getDispositivo().getId());
					prmtros_adcnles.put("Version", aplicacion.getConfiguracionApp().getDispositivo().getVersion());
					prmtros_adcnles.put("token", aplicacion.getConfiguracionApp().getDispositivo().getToken());
					prmtros_adcnles.put("Email", aplicacion.getConfiguracionApp().getDispositivo().getEmail());

					consumeWS = new ConsumeWebService(this);
					consumeWS.iniciarProceso("verificarDispositivo", prmtros_adcnles, false, null, "callback_InicializarParametros");

				}
			} else {
				// No Tiene ConexiÃ³n
				iniciarSesionOffline();
			}
		}else {
			storagePermission();
		}



		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("aplicacion.reportarOrdenServicioNuevo");
		LocalBroadcastManager.getInstance(this).registerReceiver(respuestaComandoWAMP, intentFilter);
	}

	@Override
	protected void onPause() {
		// Desregistrar al LocalBroadcastManager
		LocalBroadcastManager.getInstance(this).unregisterReceiver(respuestaComandoWAMP);
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.actividad_login, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		if(consumeWS != null && consumeWS.obtenerTareasActivas().contains("verificarDispositivo")){
			consumeWS.obtenerTareasActivas().get("verificarDispositivo").cancel(true);
		}
		super.onBackPressed();
	}

	@Override
	public void onTaskCompleted(String accion, Object objto, String... prmtros_callback) {
		// Verifica callback
		if (accion.equals("callback_InicializarParametros")){
			// Verifica que exista un objeto de respuesta
			if(objto != null) {
				// Existe,
				@SuppressWarnings("unchecked")
				Vector<Object> dtos_rspsta = (Vector<Object>) objto;

				// Extrae el objeto con los datos
				String rsltdo = (String) dtos_rspsta.elementAt(0);

				// Verifica que el rsltdo sea un ok
				if(rsltdo.equals("OK")){
					// Extrae los datos
					Object json_object = dtos_rspsta.elementAt(1);


					// Inicializa parametros de Usuario
					inicializarParametrosUsuario("WEB", json_object);

					// Verifica si existe base de datos principal y adicional,
					boolean utlza_bd_adcnal = aplicacion.getConfiguracionApp().getUsuario().isBDAdicional();
					boolean exste_bd = aplicacion.getConfiguracionApp().getSqliteRoutines().verificarArchivoDB();
					boolean exste_bd_adcnal = aplicacion.getConfiguracionApp().getCustomSqliteRoutines().verificarArchivoDB();
					if(exste_bd && (!utlza_bd_adcnal || (utlza_bd_adcnal &&  exste_bd_adcnal))){
						// Existe BD
						// Actualiza Datos de usuario locales
						aplicacion.getConfiguracionApp().actualizarDatosUsuario(1);

						// Inicia la verificacion de datos pendientes

						PendingCheckList();


					}else {
						// No existe BD,
						// Prepara la base de datos que serÃ¡ descargada

						prepararBaseDatos();
					}

				} else {
					// Resultado es KO,
					// Verifica si existe base de datos,
					boolean exste_bd = aplicacion.getConfiguracionApp().getSqliteRoutines().verificarArchivoDB();
					if(exste_bd){
						// Existe BD
						// Des-registra el usuario local
						aplicacion.getConfiguracionApp().actualizarDatosUsuario(2);
					}

					// Muestra el mensaje de alerta
					String mnsje = (String) dtos_rspsta.elementAt(1);
					//toolsApp.alertDialogYES(this, null, mnsje + '\n'  +" Id dispositivo: "+ aplicacion.getConfiguracionApp().getDispositivo().getId(), listenerAlertDialogFinish);

					if(mnsje.equals("1")){


					aplicacion.getConfiguracionApp().getSqliteRoutines().baseOcupada(true);
					aplicacion.getConfiguracionApp().getCustomSqliteRoutines().baseOcupada(true);
					boolean dblEliminada = aplicacion.getConfiguracionApp().getSqliteRoutines().renombrarBaseDatos();
					boolean dbAdicionalEliminada = aplicacion.getConfiguracionApp().getCustomSqliteRoutines().renombrarBaseDatos();

					Log.i(AplicacionOrdenesServicio.tag_app, "Base adicional modificada de nombre: " + dblEliminada);
					Log.i(AplicacionOrdenesServicio.tag_app, "Base adicional modificada de nombre: " + dbAdicionalEliminada);



						AlertDialog.Builder builder = new AlertDialog.Builder(this);
						builder.setTitle("Solicitud de ingreso");
						builder.setMessage("Por favor ingrese el correo electrónico asociado para poder ingresar a la aplicación.");
						// Set up the input
						final EditText input = new EditText(this);
//                      Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
						input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
						builder.setView(input);

//                      Set up the buttons
						builder.setPositiveButton("Ingresar", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {

								m_Text = input.getText().toString();
								dispositivo.setEmail(m_Text);


								// Tiene ConexiÃ³n
								// Trae informaciÃ³n de la nube y actualiza, Consumir servicio de login
								Hashtable<String, String> prmtros_adcnles = new Hashtable<String, String>();
								prmtros_adcnles.put("IdDispositivo", aplicacion.getConfiguracionApp().getDispositivo().getId());
								prmtros_adcnles.put("Version", aplicacion.getConfiguracionApp().getDispositivo().getVersion());
								prmtros_adcnles.put("token", dispositivo.getToken());
								prmtros_adcnles.put("email", dispositivo.getEmail());
								consumeWS = new ConsumeWebService(ActividadLogin.this);
								consumeWS.iniciarProceso("verificarDispositivo", prmtros_adcnles, false, null, "callback_InicializarParametros");

							}
						});
						builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
								finish();
							}
						});

						builder.show();


					}else if (mnsje.equals("2")){

						//se comenta para probar el alert pero esto renombra la bd
						 aplicacion.getConfiguracionApp().getSqliteRoutines().baseOcupada(true);
						aplicacion.getConfiguracionApp().getCustomSqliteRoutines().baseOcupada(true);
						boolean dblEliminada = aplicacion.getConfiguracionApp().getSqliteRoutines().renombrarBaseDatos();
						boolean dbAdicionalEliminada = aplicacion.getConfiguracionApp().getCustomSqliteRoutines().renombrarBaseDatos();

						Log.i(AplicacionOrdenesServicio.tag_app, "Base adicional modificada de nombre: " + dblEliminada);
						Log.i(AplicacionOrdenesServicio.tag_app, "Base adicional modificada de nombre: " + dbAdicionalEliminada);

						// Tiene ConexiÃ³n
						// Trae informaciÃ³n de la nube y actualiza, Consumir servicio de login
						Hashtable<String, String> prmtros_adcnles = new Hashtable<String, String>();
						prmtros_adcnles.put("IdDispositivo", aplicacion.getConfiguracionApp().getDispositivo().getId());
						prmtros_adcnles.put("Version", aplicacion.getConfiguracionApp().getDispositivo().getVersion());
						prmtros_adcnles.put("token", dispositivo.getToken());
						prmtros_adcnles.put("Email", dispositivo.getEmail());
						consumeWS = new ConsumeWebService(ActividadLogin.this);
						consumeWS.iniciarProceso("verificarDispositivo", prmtros_adcnles, false, null, "callback_InicializarParametros");

					}else if (mnsje.equals("3")){
                        //se comenta para probar el alert pero esto renombra la bd
                        aplicacion.getConfiguracionApp().getSqliteRoutines().baseOcupada(true);
                        aplicacion.getConfiguracionApp().getCustomSqliteRoutines().baseOcupada(true);
                        boolean dblEliminada = aplicacion.getConfiguracionApp().getSqliteRoutines().renombrarBaseDatos();
                        boolean dbAdicionalEliminada = aplicacion.getConfiguracionApp().getCustomSqliteRoutines().renombrarBaseDatos();

                        Log.i(AplicacionOrdenesServicio.tag_app, "Base adicional modificada de nombre: " + dblEliminada);
                        Log.i(AplicacionOrdenesServicio.tag_app, "Base adicional modificada de nombre: " + dbAdicionalEliminada);

                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setTitle("Acualización disponible.");
                        builder.setMessage("Es necesario actualizar a la nueva versión para poder continuar usando la aplicación.");

                        // Set up the input
 						//Specify the type of input expected; this, for example, sets the input as a password, and will mask the text

						//Set up the buttons
                        builder.setPositiveButton("Actualizar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(
                                        "https://play.google.com/apps/internaltest/4699614921131230536"));
                                startActivity(intent);


                            }
                        });
                        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                finish();
                            }
                        });

                        builder.show();



						//toolsApp.alertDialogYES(this, null, "Actualizacion diosponible" + '\n'  +" Por favor ingrese a la play Store y actualize la aplicacion: " , listenerAlertDialogFinish);

					}
					//aqui se deberia validar el manejo de la carpeta gc y los casos de uso de la aplicacion
					// cambia el nombre de la base de datos y crea un backup

					// finish();
				}
			}else{
				// No llego respuesta,
				// SesiÃ³n offline
				iniciarSesionOffline();
			}

		} else if(accion.equals("callback_PreparacionBD")){
			if(objto != null) {
				JSONObject jsonObject = (JSONObject) objto;
				try {
					// Extrae los datos de la preparaciÃ³n de las bases de datos.
					aplicacion.getConfiguracionApp().getUsuario().setDimensionDBPrincipal(jsonObject.getInt("DimensionDBPrincipal"));
					aplicacion.getConfiguracionApp().getUsuario().setDimensionDBAdicional(jsonObject.getInt("DimensionDBAdicional"));

					// Descarga la base de datos principal
					descargarBDPrincipal();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				// No se pudo preparar base de datos.
				// Verifica si hay datos locales para trabajar
				boolean exste_bd = aplicacion.getConfiguracionApp().getSqliteRoutines().verificarArchivoDB();
				if(exste_bd){
					// Si hay datos locales
					// Cambia el estado de la variable auxiliar y llama el mÃ©todo de verificacion
					ESTADO_VERIFICACION = 4;
					PendingCheckList();
				}else{
					// No hay datos locales
					toolsApp.showLongMessage("No hay datos locales!", this);
				}
			}
		} else if(accion.equals("callback_DescargaBD")){
			if(objto != null) {
				boolean dscrga_extsa = (Boolean) objto;
				if(dscrga_extsa){
					// Descarga Exitosa,
					aplicacion.getConfiguracionApp().getSqliteRoutines().baseOcupada(false);

					// Inserta datos de Sesion en la Base de Datos
					aplicacion.getConfiguracionApp().guardarDatosUsuario();

					// Si la aplicaciÃ³n utiliza una base de datos adicional, la descarga,
					// en caso contrario termina el proceso
					boolean utlza_bd_adcnal = aplicacion.getConfiguracionApp().getUsuario().isBDAdicional();
					boolean exste_bd_adcnal = aplicacion.getConfiguracionApp().getCustomSqliteRoutines().verificarArchivoDB();
					if(utlza_bd_adcnal && (!exste_bd_adcnal || aplicacion.getConfiguracionApp().getUsuario().isSincronizarBDAdicional())){
						// Requiere una BD adicional
						descargarBDAdicional();
					} else {
						// No requiere actualizar BD adicional,
						// Si no requiere BD Adicional la borra si existe
						if(!utlza_bd_adcnal){
							aplicacion.getConfiguracionApp().getCustomSqliteRoutines().eliminarArchivoDB();
						}

						// Consume servicio para indicar que la sincronizacion de base de datos fue exitosa
						Hashtable<String, String> prmtros_adcnles = new Hashtable<String, String>();
						prmtros_adcnles.put("IdDispositivo", aplicacion.getConfiguracionApp().getDispositivo().getId());
						consumeWS = new ConsumeWebService(this);
						consumeWS.iniciarProceso(
								"actualizarBanderaSincronizacion", prmtros_adcnles, false, null,
								"callback_actualizarBanderaSincronizacion");
					}
				} else {
					lbel_vrfcndo_dspstvo.setText("NO se sincronizaron los datos, ingrese nuevamente.");
				}
			} else {
				lbel_vrfcndo_dspstvo.setText("NO se sincronizaron los datos, ingrese nuevamente.");
			}
		} else if(accion.equals("callback_DescargaBDAdicional")){
			if(objto != null) {
				boolean dscrga_extsa = (Boolean) objto;
				if(dscrga_extsa){
					// Descarga de BD Adicional Exitosa,
					aplicacion.getConfiguracionApp().getCustomSqliteRoutines().baseOcupada(false);

					// Consume servicio para indicar que la sincronizacion de base de datos fue exitosa
					Hashtable<String, String> prmtros_adcnles = new Hashtable<String, String>();
					prmtros_adcnles.put("IdDispositivo", aplicacion.getConfiguracionApp().getDispositivo().getId());
					consumeWS = new ConsumeWebService(this);
					consumeWS.iniciarProceso(
							"actualizarBanderaSincronizacion", prmtros_adcnles, false, null,
							"callback_actualizarBanderaSincronizacion");
				} else {
					lbel_vrfcndo_dspstvo.setText("NO se sincronizaron los datos, ingrese nuevamente.");
				}
			} else {
				lbel_vrfcndo_dspstvo.setText("NO se sincronizaron los datos, ingrese nuevamente.");
			}
		} else if(accion.equals("callback_actualizarBanderaSincronizacion")){
			if(objto != null) {
				// ActualizaciÃ³n de bandera de sincronizaciÃ³n exitosa
				toolsApp.showLongMessage("Datos Actualizados Correctamente!", this);
			}

			// Cambia el estado de la variable auxiliar y llama el mÃ©todo de verificacion
			ESTADO_VERIFICACION = 4;
			PendingCheckList();

		}
	}

	// Broadcast receiver para recibir respuesta del comando WAMP
	private BroadcastReceiver respuestaComandoWAMP = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String nombreIntento = intent.getStringExtra("nombreIntento");
			boolean sincronizado = intent.getBooleanExtra("sincronizado", false);

			// Dependiendo del intent, ejecuta la funciÃ³n Callback
			if (nombreIntento.startsWith("aplicacion.reportarOrdenServicioNuevo")) {

				// Incrementa contador
				contadorCallbacksOrdenes++;
				Log.i(AplicacionOrdenesServicio.tag_app,
						"Ã“rdenes No Sincronizadas: " + numeroOrdenesNoSincronizadas +
								" Orden actual: "+contadorCallbacksOrdenes
				);

				// Verifica si todos los callback fueron llamados
				if(numeroOrdenesNoSincronizadas == contadorCallbacksOrdenes){
					// Limpia variables auxiliares
					numeroOrdenesNoSincronizadas = 0;
					contadorCallbacksOrdenes = 0;

					// Cambia el estado de la variable auxiliar y llama el mÃ©todo de verificaciÃ³n
					if(sincronizado){
						ESTADO_VERIFICACION = 3;
						PendingCheckList();
					} else {
						ESTADO_VERIFICACION = 4;
						PendingCheckList();
					}
				}
			}
		}
	};

	// Consumir servicio para preparar la base de datos que serÃ¡ descargada
	private void prepararBaseDatos( ) {


		Hashtable<String, String> prmtros_adcnles = new Hashtable<String, String>();
		prmtros_adcnles.put("Seudonimo", aplicacion.getConfiguracionApp().getUsuario().getSeudonimo());
		prmtros_adcnles.put("IdClienteGlobal", String.valueOf(aplicacion.getConfiguracionApp().getUsuario().getIdClienteGlobal()));
		prmtros_adcnles.put("IdUsuario", String.valueOf(aplicacion.getConfiguracionApp().getUsuario().getId()));
		prmtros_adcnles.put("IdDispositivo", aplicacion.getConfiguracionApp().getDispositivo().getId());
		prmtros_adcnles.put("BDAdicional", String.valueOf(toolsApp.booleanToBit(aplicacion.getConfiguracionApp().getUsuario().isBDAdicional())));

		consumeWS.iniciarProceso(
				"prepararBaseDatosOS", prmtros_adcnles, true, "Preparando Base de Datos", "callback_PreparacionBD");
	}

	public boolean storagePermission() {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
			return true;
		if ((checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) &&
				(checkSelfPermission(CAMERA) == PackageManager.PERMISSION_GRANTED) &&
				(checkSelfPermission(ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) &&
				(checkSelfPermission(ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) &&
				(checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {

			return true;
		} else {
			requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, CAMERA, READ_EXTERNAL_STORAGE,ACCESS_COARSE_LOCATION,ACCESS_FINE_LOCATION}, MY_PERMISSIONS);

		}
		return false;

	}

	// Descarga la base de datos principal
	private void descargarBDPrincipal() {
		// Inicializa las variables del objeto de descargas para traer el archivo principal de bd
		String url =
				"http://23.253.57.17/GestionClientes/Databases/"+ aplicacion.getConfiguracionApp().getUsuario().getSeudonimo() + "/"+
						aplicacion.getConfiguracionApp().getUsuario().getSeudonimo() + "_" + aplicacion.getConfiguracionApp().getDispositivo().getId() + ".db";
		String drccion_dstno = AplicacionOrdenesServicio.pathSD + AplicacionOrdenesServicio.pathDB;
		String nmbre_archvo = AplicacionOrdenesServicio.nombreBasePrincipal;

		// Borra la base de datos principal actual
		aplicacion.getConfiguracionApp().getSqliteRoutines().baseOcupada(true);
		boolean dbPrincipalEliminada = aplicacion.getConfiguracionApp().getSqliteRoutines().eliminarArchivoDB();
		Log.i(AplicacionOrdenesServicio.tag_app, "Base principal eliminada: " + dbPrincipalEliminada);

		// Inicia la descarga la base de datos principal de la nube
		DownloadsManager downloaderManager = new DownloadsManager(
				this, "Descargando Base de Datos", "callback_DescargaBD",
				url, drccion_dstno, nmbre_archvo, 3, 15, 30, false);
		downloaderManager.iniciarDescarga(aplicacion.getConfiguracionApp().getUsuario().getDimensionDBPrincipal());
		lbel_vrfcndo_dspstvo.setText("Descargando datos principales...");
	}

	// Descarga la base de datos adicional
	private void  descargarBDAdicional(){
		// Inicializa las variables del objeto de descargas para traer el archivo adicional de bd
		String url =
				"http://23.253.57.17/GestionClientes/Databases/"+ aplicacion.getConfiguracionApp().getUsuario().getSeudonimo() + "/"+
						aplicacion.getConfiguracionApp().getUsuario().getSeudonimo() + "Adicional_" +
						aplicacion.getConfiguracionApp().getDispositivo().getId() + ".db";
		String drccion_dstno = AplicacionOrdenesServicio.pathSD + AplicacionOrdenesServicio.pathDB;
		String nmbre_archvo = AplicacionOrdenesServicio.nombreBaseSecundaria;

		// Borra la base de datos adicional actual
		aplicacion.getConfiguracionApp().getCustomSqliteRoutines().baseOcupada(true);
		boolean dbAdicionalEliminada = aplicacion.getConfiguracionApp().getCustomSqliteRoutines().eliminarArchivoDB();
		Log.i(AplicacionOrdenesServicio.tag_app, "Base adicional eliminada: " + dbAdicionalEliminada);

		// Inicia la descarga de la base de datos adicional de la nube
		DownloadsManager downloaderManager = new DownloadsManager(
				this, "Descargando Base de Datos Adicional", "callback_DescargaBDAdicional",
				url, drccion_dstno, nmbre_archvo, 3, 15, 30, false);
		downloaderManager.iniciarDescarga(aplicacion.getConfiguracionApp().getUsuario().getDimensionDBAdicional());
		lbel_vrfcndo_dspstvo.setText("Descargando datos adicionales...");
	}

	// Inicia sesiÃ³n offline
	private void iniciarSesionOffline() {
		toolsApp.showShortMessage("SesiÃ³n Offline!", this);

		// Verifica si hay datos locales para trabajar
		boolean exste_bd = aplicacion.getConfiguracionApp().getSqliteRoutines().verificarArchivoDB();
		boolean exste_bd_adcnal = aplicacion.getConfiguracionApp().getCustomSqliteRoutines().verificarArchivoDB();
		if(exste_bd){
			// Si hay datos locales
			// Inicializa parametros de Usuario
			inicializarParametrosUsuario("LOCAL", null);

			// Verifica la base de datos adicional
			if(aplicacion.getConfiguracionApp().getUsuario().isBDAdicional() == exste_bd_adcnal){
				mostrarSiguienteActividad();
			} else {
				// No hay datos locales
				toolsApp.showLongMessage("No hay datos locales adicionales!", this);
			}
		} else {
			// No hay datos locales
			toolsApp.showLongMessage("No hay datos locales!", this);
		}
	}

	// Inicializa los parametros de usuario
	private void inicializarParametrosUsuario(String MODO, Object objto) {
		if(MODO.equals("WEB")){
			JSONObject jsonObject = (JSONObject) objto;
			try {
				// Realizar clase para parsear JSON dependiendo de un tipo
				JSONManager jsonManager = new JSONManager(jsonObject);


				// Asignar Usuario WEB
				mnsje_alrta = aplicacion.getConfiguracionApp().asignarUsuarioWeb(jsonManager, this);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}else if (MODO.equals("LOCAL")) {
			// Asignar Usuario Local
			aplicacion.getConfiguracionApp().asignarUsuarioLocal();
		}

		// Verifica si el usuario estÃ¡ registrado
		if(aplicacion.getConfiguracionApp().isUsuarioRegistrado()){
			// Setea el nombre del cliente global en la interfaz
			Log.i(AplicacionOrdenesServicio.tag_app, "Usuario Logeado con Ã©xito!");
			lbel_nmbre_clnte_global.setText("Cliente Registrado: "+aplicacion.getConfiguracionApp().getUsuario().getNombresClienteGlobal());
		}
	}

	// Listener para el alert para continuar a la siguiente actividad
	DialogInterface.OnClickListener listenerAlertDialogFinish = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// Deshabilita el botÃ³n
			((AlertDialog) dialog).getButton(which).setEnabled(false);

			// Dependiendo del botÃ³n presionado, se ejecuta la acciÃ³n
			switch (which){
				case DialogInterface.BUTTON_POSITIVE:
					finish();
					break;
			}
		}
	};

	// Listener para el alert para continuar a la siguiente actividad
	DialogInterface.OnClickListener listenerAlertDialogActividad = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// Deshabilita el botÃ³n
			((AlertDialog) dialog).getButton(which).setEnabled(false);

			// Dependiendo del botÃ³n presionado, se ejecuta la acciÃ³n
			switch (which){
				case DialogInterface.BUTTON_POSITIVE:
					mostrarSiguienteActividad();
					break;
			}
		}
	};

	// MÃ©todo que permite direccionar a la lista de clientes
	private void mostrarSiguienteActividad(){
		// Verifica si existe un mensaje
		if(mnsje_alrta != null){
			// Existe mensaje,
			// Muestra el mensaje de alerta
			toolsApp.alertDialogYES(this, null, mnsje_alrta + '\n'  +"Id dispositivo:"+ aplicacion.getConfiguracionApp().getDispositivo().getId(), listenerAlertDialogActividad);

			// Limpia el mensaje
			mnsje_alrta = null;
		}else {
			// No existe mensaje,
			// Verifica si estÃ¡ registrado
			if(aplicacion.getConfiguracionApp().isUsuarioRegistrado()){
				// Una vez se ha realizado el Login, se dirige a la lista de clientes
				//Intent intent = new Intent(ActividadLogin.this, ActividadListaClientes.class);

				//AquÃ­ se llama la siguiente activity, aquÃ­ pondrÃ­a la actividad del login

				boolean bandera_login = aplicacion.getConfiguracionApp().getUsuario().getBanderaLogin();
				//boolean bandera_login = false;
				//Intent intent = new Intent(ActividadLogin.this, ActividadListaOrdenesServicio.class);

				//Se verifica si el usuario tiene userLogin a travÃ©s de su bandera.
				if (bandera_login) {
					Intent intent = new Intent(ActividadLogin.this, ActividadLoginUsuario.class);
					startActivity(intent);
				}else{
					Intent intent = new Intent(ActividadLogin.this, ActividadListaOrdenesServicio.class);
					startActivity(intent);
				}
				finish();
			}else {
				toolsApp.alertDialogYES(this, null,
						"El Usuario no se encuentra registrado! ComunÃ­quese con el administrador" + '\n'  +"Id dispositivo:"+ aplicacion.getConfiguracionApp().getDispositivo().getId(),
						listenerAlertDialogFinish
				);
			}
		}
	}

	// Dependiendo del estado en el que se encuentre ejecuta la acciÃ³n
	private void PendingCheckList() {
		int ESTADO_ACTUAL = ESTADO_VERIFICACION;
		boolean cntnuar = true;
		do {
			switch (ESTADO_ACTUAL) {
				case 2:
					// Verifica ordenes No sincronizadas
					if(verificarOrdenesServicioNoSincronizadas()){
						cntnuar = false;
					} else {
						// No hay datos,
						ESTADO_ACTUAL = 3;
					}
					break;
				case 3:
					// Verifica si estÃ¡ pendiente sincronizar BD
					if(verificarSincronizacionBD()){
						cntnuar = false;
					} else {
						// No hay datos,
						ESTADO_ACTUAL = 4;
					}
					break;
				case 4:
					// Cambia de actividad
					cntnuar = false;
					mostrarSiguienteActividad();
					break;
			}
		} while (cntnuar);
	}

	// Verifica visitas pendientes por ser enviadas.
	private boolean verificarOrdenesServicioNoSincronizadas(){
		boolean existenOrdenesNoSincronizadas = false;
		numeroOrdenesNoSincronizadas = ((SQLiteRoutinesOS)aplicacion.getConfiguracionApp().getSqliteRoutines())
				.consultarNumeroOrdenesPendientes(aplicacion.getConfiguracionApp());
		if(numeroOrdenesNoSincronizadas > 0){
			// Setea la bandera
			existenOrdenesNoSincronizadas = true;

			// Cambia mensaje a sincronizando datos
			lbel_vrfcndo_dspstvo.setText("Enviando datos pendientes...");

			// Si hay conexiÃ³n, Llama servicio para enviar datos pendientes
			Intent intent = new Intent(this, ServicioNovedades.class);
			intent.putExtra("tipoIntent", "WAMP");
			startService(intent);
		}
		return existenOrdenesNoSincronizadas;
	}

	// Verificar si es necesario actualizar alguna de las bases de datos.
	private boolean verificarSincronizacionBD(){
		boolean sncrnzcion_pndnte = false;

		// Verifica si es necesario sincronizar base de datos
		if((aplicacion.getConfiguracionApp().getUsuario().isSincronizarBD())
				&& !aplicacion.getConfiguracionApp().isRecuperacionContexto()){
		/*if((aplicacion.getConfiguracionApp().getUsuario().isSincronizarBD() || aplicacion.getConfiguracionApp().getUsuario().isSincronizarBDAdicional())
				&& !aplicacion.getConfiguracionApp().isRecuperacionContexto()){*/
			// Cambia mensaje a sincronizando visitas
			lbel_vrfcndo_dspstvo.setText("Preparando y sincronizando datos...");
			sncrnzcion_pndnte = true;

			// Prepara la base de datos ya que debe ser descargada nuevamente para sincronizar datos
			prepararBaseDatos();
		}
		return sncrnzcion_pndnte;
	}
}
