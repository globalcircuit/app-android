package com.gc.ordenesservicio;

import java.util.Vector;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import androidx.annotation.NonNull;

import android.telephony.TelephonyManager;
import android.util.Log;

import com.gc.coregestionclientes.Configuracion;
import com.gc.coregestionclientes.objetos.Cliente;
import com.gc.coregestionclientes.objetos.Dispositivo;
import com.gc.coregestionclientes.objetos.Usuario;
import com.gc.ordenesservicio.accesodatos.CustomSQLiteRoutinesOS;
import com.gc.ordenesservicio.accesodatos.SQLiteRoutinesOS;
import com.gc.ordenesservicio.objetos.ActividadAdministrativa;
import com.gc.ordenesservicio.objetos.OrdenServicio;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AplicacionOrdenesServicio extends Application {

	// Parametros Estaticos
	public final static String tag_app = "ORDENES_SERVICIO";
	public static String pathSD = Environment.getExternalStorageDirectory()
			.getAbsolutePath() + "/GC/";
	public static String pathDB = "databases/";
	public static String nombreBasePrincipal = "os.db";
	public static String nombreBaseSecundaria = "osAdicional.db";
	public static String pathMedia = "MEDIA/";
	public static int sdk = android.os.Build.VERSION.SDK_INT;
	public SQLiteRoutinesOS sqliteRoutines;
	public Dispositivo dispositivo;
	private String token;
	private String imei, mac ,vrsion;
	private String firma;
	private String rutaFirma;

	// Objetos

	private Configuracion configuracionApp;
	private OrdenServicio ordenServicioActiva;
	private ActividadAdministrativa actividadAdministrativaActiva;

	@Override
	public void onCreate() {
		super.onCreate();


		imei = null;
		TelephonyManager tm =
				(TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
		String imaiCompleto = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);



		//imei = token;
		imei = imaiCompleto.substring(0,15);
		Log.v("Imei", imei);

		// Inicializa el wifimanager y extrae el wifi
		WifiManager wifimanager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		 mac = wifimanager.getConnectionInfo().getMacAddress();
		if (mac != null) {
			mac = mac.replace(":", "");
		}

		// Obtiene la version de la aplicación
		PackageInfo pinfo = null;
		try {
			pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		 vrsion = pinfo.versionName;

		// Inicializa el Objeto Dispositivo
		// dispositivo = new Dispositivo("012345678901234", mac, vrsion);


		FirebaseInstanceId.getInstance().getInstanceId()
				.addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
					@Override
					public void onComplete(@NonNull Task<InstanceIdResult> task) {
						if (!task.isSuccessful()) {
							Log.v("Token", "getInstanceId failed", task.getException());
							return;
						}

						// Get new Instance ID token
						token = task.getResult().getToken();

						// Log and toast
						//String msg = getString(R.string.msg_token_fmt, token);
						Log.v("Mensaje token 1", token);
						//Toast.makeText(AplicacionOrdenesServicio.this, token, Toast.LENGTH_SHORT).show();
					}
				});

		dispositivo = new Dispositivo(token, imei, mac, vrsion);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){

				inicializarObjetos();
			}

		}

	}

	public void inicializarObjetos() {
		// Incializa el objeto que tiene precargada algunas rutinas de acceso a DB principal
		SQLiteRoutinesOS sqliteRoutines = new SQLiteRoutinesOS(this);

		// Incializa el objeto que tiene precargada algunas rutinas de acceso a DB adicional
		CustomSQLiteRoutinesOS customSqliteRoutines = new CustomSQLiteRoutinesOS(this);

		// Inicializa el objeto de Congifuracion
		configuracionApp = new Configuracion(this,
				Configuracion.ORDENES_SERVICIO_APP, dispositivo, sqliteRoutines, customSqliteRoutines,
				nombreBasePrincipal, nombreBaseSecundaria);

		// Inicializa bandera que indica si es necesario recuperar contexto
		// permitiendo recrear la ultima actividad principal abierta
		configuracionApp.setRecuperacionContexto(false);

		// Verifica la bandera de base de datos para recuperar el contexto de la
		// app Si es necesario



		boolean exste_bd = sqliteRoutines.verificarArchivoDB();
		if (exste_bd) {
			Log.i(tag_app, "Recreando Application!");
			recuperarObjetosContexto();
		}
	}



	/*
	 * @Override public void onTrimMemory(int level) {
	 * super.onTrimMemory(level); Log.i(tag_app,
	 * "onTrimMemory -> Posible cierre level:"+level); }
	 */

	public void salvarObjetosContexto(boolean recuperarContexto) {
		// Convierte los objetos de contexto a JSON
		Gson gson = new Gson();
		String usuarioGson = gson.toJson(configuracionApp.getUsuario());
		String clienteGson = gson.toJson(configuracionApp.getClienteActivo());
		
		//Log.d("Formato Json cadena", usuarioGson);
		
		// Objetos asociados a la orden de servicio
		String ordenServicioGson = gson.toJson(ordenServicioActiva);

		// Almacena los objetos en BD
		/*
		 * Log.i(tag_app, "Objetos: " + rcprar_cntxto + "\nUsuario: " +
		 * usrio_gson + "\nCliente: " + clnte_gson + "\nVisita: " + vsta_gson );
		 */
		((SQLiteRoutinesOS) configuracionApp.getSqliteRoutines())
				.routine_actualizarObjetosContexto(recuperarContexto,
						usuarioGson, clienteGson, ordenServicioGson);

		// Si rcprar_cntxto es false, el de actividades también
		if (!recuperarContexto) {
			configuracionApp.setRecuperacionContexto(false);
		}
	}

	// Método que permite recuperar los objetos del contexto de la aplicacion
	private void recuperarObjetosContexto() {
		Gson gson = new Gson();
		Vector<String> objetosString = ((SQLiteRoutinesOS) configuracionApp
				.getSqliteRoutines()).routine_recuperarContexto();

		// Verifica que el vector contenga objetos por recuperar
		if (objetosString != null && !objetosString.isEmpty()) {
			// Obtiene el objeto usuario
			String usuarioGson = objetosString.elementAt(0);
			//Log.d("sebas", "hola  "+usuarioGson);
			if (usuarioGson != null) {
				configuracionApp.setUsuario(gson.fromJson(usuarioGson,
						Usuario.class));
			}

			// Obtiene el objeto cliente
			String clienteGson = objetosString.elementAt(1);
			if (clienteGson != null) {
				configuracionApp.setClienteActivo(gson.fromJson(clienteGson,
						Cliente.class));
				configuracionApp.setRecuperacionContexto(true);
			}

			// Obtiene el objeto de orden de servicio
			String ordenServicioGson = objetosString.elementAt(2);
			if (ordenServicioGson != null) {
				ordenServicioActiva = gson.fromJson(ordenServicioGson,
						OrdenServicio.class);
			}

			// Log
			/*
			 * Log.i(tag_app, "Recuperando objetos!"); Log.i(tag_app,
			 * "Objetos: " + "\nUsuario: " + usrio_gson + "\nCliente: " +
			 * clnte_gson + "\nVisita: " + vsta_gson );
			 */
		}
	}

	/**
	 * @return the configuracionApp
	 */
	public Configuracion getConfiguracionApp() {
		return configuracionApp;
	}

	/**
	 * @param configuracionApp
	 *            the configuracionApp to set
	 */
	public void setConfiguracionApp(Configuracion configuracionApp) {
		this.configuracionApp = configuracionApp;
	}

	/**
	 * @return the ordenServicioActiva
	 */
	public OrdenServicio getOrdenServicioActiva() {
		return ordenServicioActiva;
	}

	/**
	 * @param ordenServicioActiva
	 *            the ordenServicioActiva to set
	 */
	public void setOrdenServicioActiva(OrdenServicio ordenServicioActiva) {
		this.ordenServicioActiva = ordenServicioActiva;
	}
	
	// Método que permite limpiar los objetos de la aplicacion
	// Cliente activo, orden y memoria de dados
	public void limpiarObjetosAplicacion() {
		this.ordenServicioActiva = null;
		this.actividadAdministrativaActiva = null;
		this.configuracionApp.setClienteActivo(null);
		this.configuracionApp.limpiarMemoria();	
	}
	
	// Métdo para setear Actividad Administrativa activa	
	public void setActividadAdministrativaActiva(ActividadAdministrativa actividadAdministrativaActiva) {
		this.actividadAdministrativaActiva = actividadAdministrativaActiva;
	}
	
	// Método para obtener Actividad Administrativa activa
	public ActividadAdministrativa getActividadAdministrativaActiva() {
		return actividadAdministrativaActiva;
	}

	public String getFirma() {
		return firma;
	}

	public void setFirma(String firma) {
		this.firma = firma;
	}


	public String getRutaFirma() {
		return rutaFirma;
	}

	public void setRutaFirma(String rutaFirma) {
		this.rutaFirma = rutaFirma;
	}
}
