package com.gc.ordenesservicio;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.gc.coregestionclientes.interfaces.OnTaskCompleted;
import com.gc.coregestionclientes.librerias.JSONManager;
import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.objetos.Campo;
import com.gc.coregestionclientes.objetos.ControladorFormularios;
import com.gc.coregestionclientes.objetos.ControladorTalonarios;
import com.gc.coregestionclientes.objetos.Formulario;
import com.gc.ordenesservicio.accesodatos.SQLiteRoutinesOS;
import com.gc.ordenesservicio.novedades.ServicioNovedades;
import com.gc.ordenesservicio.objetos.AuditorFormularioOS;
import com.gc.ordenesservicio.objetos.AutocompletarCampoOS;
import com.gc.ordenesservicio.objetos.OrdenServicio;

public class ActividadNuevaOrdenServicio extends FragmentActivity implements OnTaskCompleted {

	// Objetos
	private AplicacionOrdenesServicio aplicacion;
	private ControladorFormularios controladorFormularios = new ControladorFormularios();
	private ControladorTalonarios controladorTalonarios;
	private ToolsApp toolsApp = new ToolsApp();
	private AuditorFormularioOS auditorFormulario;
	private Formulario formulario;
	private OrdenServicio ordenServicio = null;
	
	//private TextView lbel_nmbre_clnte;
	private LinearLayout contenedorFormulario;
	private View layoutView = null;
	
	// Objetos auxiliares 
	private HashMap<String, Campo> camposHash = new HashMap<String, Campo>();
	private HashMap<String, View> contenedoresCamposHash = new HashMap<String, View>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.actividad_nueva_orden_servicio);
		layoutView = getWindow().getDecorView().findViewById(android.R.id.content);
		
		// Inicializa elementos gráficos
		contenedorFormulario = (LinearLayout)findViewById(R.id.cntndor_frmlrio);
			
		// Inicializa el objeto de la aplicación y controlador de ordenes de servicio
		aplicacion = ((AplicacionOrdenesServicio) getApplicationContext());
		
		// Inicializa el nombre
		/*lbel_nmbre_clnte = (TextView)findViewById(R.id.lbel_nmbre_clnte);
		lbel_nmbre_clnte.setText(aplicacion.getConfiguracionApp().getClienteActivo().getNombres());*/
		
		// Incializa el controlador de talonarios
		controladorTalonarios = new ControladorTalonarios(aplicacion.getConfiguracionApp().getSqliteRoutines());
		controladorTalonarios.cargarTalonario("ordenesServicio", "id");
		boolean solicitarTalonario = controladorTalonarios.solicitarNuevoTalonario(
				this, aplicacion.getConfiguracionApp(), "ordenesServicio", "id");
		if(!solicitarTalonario) {
			// Muestra Formulario
			mostrarFormulario();
		}
	}

	private void mostrarFormulario() {
		int consecutivo = controladorTalonarios.obtenerConsecutivo("ordenesServicio", "id");
		if(consecutivo > 0) {
			// Crea una nueva Orden de Servicio
			ordenServicio = new OrdenServicio(consecutivo, -1, 
					toolsApp.getUnixDateNOW(true, 0, 0, 0), OrdenServicio.PENDIENTE, null,aplicacion.getConfiguracionApp().getUsuario().getId());
			aplicacion.setOrdenServicioActiva(ordenServicio);
			
			// Inicializa el controlador del Formulario activo
			formulario = controladorFormularios.cargarFormularioLocal(
					aplicacion.getConfiguracionApp().getSqliteRoutines(), 
					"NUEVA ORDEN DE SERVICIO", Formulario.APLICACION, -1
			);
			ordenServicio.setFormulario(formulario);
			controladorFormularios.cargarFormulariosLocales(aplicacion.getConfiguracionApp().getSqliteRoutines(), 
					Formulario.SUBFORMULARIO);
			
			// Inicializa el auditor
			auditorFormulario = new AuditorFormularioOS(formulario, null, controladorFormularios);
			
			// Crea dinámicamente el formulario
			controladorFormularios.visualizarFormulario(
					this, layoutView, contenedorFormulario, camposHash, contenedoresCamposHash, 
					auditorFormulario, new AutocompletarCampoOS(), 
					onItemSelectedListener, onCheckedChangeListener, true);
			
			// Inicializa campos
			auditorFormulario.inicializarCamposFormulario(this,contenedoresCamposHash);
		} else {
			finish();
			toolsApp.showLongMessage(
					"No hay talonarios para trabajar Offline, asegúrese de tener internet!", this);
		}
	}
	
	// Listener para los Spinner
	AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {		
			// Extrae el campo segun el view (Spinner)
			Campo cmpo_evnto = camposHash.get(String.valueOf(arg0.getId()));
			auditorFormulario.validarEventoCampo(ActividadNuevaOrdenServicio.this, layoutView, cmpo_evnto, 
					arg0, contenedoresCamposHash, camposHash);	
		}
		
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {}
	};
	
	// Listener para los Checkbox
	CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// Extrae el campo segun el view (Checkbox)
			Campo cmpo_evnto = camposHash.get(String.valueOf(buttonView.getId()));
			auditorFormulario.validarEventoCampo(ActividadNuevaOrdenServicio.this, layoutView, cmpo_evnto, 
					buttonView, contenedoresCamposHash, camposHash);	
		}
	};
	
	// OnClick de los botones de la actividad
	public void ejecutarAccionBoton(View view) {
		// Dependiendo del view, ejecuta la acción
		switch (view.getId()) {
		case R.id.boton_guardar_orden:
			toolsApp.alertDialogYESNO(this, "¿Está seguro de GUARDAR ésta orden?", null, null, 
					new DialogInterface.OnClickListener() {
					    @Override
					    public void onClick(DialogInterface dialog, int which) {
					    	// Deshabilita el botón
				        	((AlertDialog) dialog).getButton(which).setEnabled(false);
		
				        	// Dependiendo del botón presionado, se ejecuta la acción
					        switch (which){
						        case DialogInterface.BUTTON_POSITIVE:
						        	guardarNuevaOrdenServicio();
						        	break;
					        }
					    }
					});
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		toolsApp.alertDialogYESNO(this, "¿Está seguro de CANCELAR este formulario?", 
				null, null, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
				    	// Deshabilita el botón
			        	((AlertDialog) dialog).getButton(which).setEnabled(false);

			        	// Dependiendo del botón presionado, se ejecuta la acción
				        switch (which){
					        case DialogInterface.BUTTON_POSITIVE:
								aplicacion.limpiarObjetosAplicacion();		
								finish();
					        break;
				        }
					}
				});
	}
	
	private void guardarNuevaOrdenServicio() {
		// Asigna los valores del formulario
		boolean asignacionExitosa = controladorFormularios.interfazAFormulario(
				this, aplicacion.getConfiguracionApp(), true, auditorFormulario, 
				layoutView, contenedoresCamposHash);

		// Verifica si la asignación fue exitosa
		if(asignacionExitosa) {
			// Exitosa,	Crea la orden
			((SQLiteRoutinesOS) aplicacion.getConfiguracionApp().getSqliteRoutines())
				.guardarOrdenServicio(aplicacion, aplicacion.getConfiguracionApp(), ordenServicio);
			
			// Procesa el evento de finalización del formulario
			auditorFormulario.validarEventoFinFormulario(this, layoutView);
			
			// Verifica si hay conexión
			if(toolsApp.isConnected(this)){
				// Si hay conexión, Llama servicio para enviar datos pendientes
				Intent intent = new Intent(this, ServicioNovedades.class);
				intent.putExtra("tipoIntent", "WAMP");
				startService(intent);
			}
			
			// Limpia objetos
			aplicacion.limpiarObjetosAplicacion();	
			toolsApp.showLongMessage("Datos guardados!", this);
			finish();	
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
		switch (requestCode) {
			case ControladorFormularios.REQUEST_AUTOCOMPLETE:
				// Analiza el resultado
				if(resultCode == RESULT_OK){   			
					// Obtiene el valor retornado
					if(data != null){
						int idCampo = data.getIntExtra("idCampo", -1);
						HashMap<String, String> objetoDatos = (HashMap<String, String>) data.getSerializableExtra("valor");
						
						// Si es un valor válido, lo setea
						if(objetoDatos != null && idCampo > 0){
							EditText editText = (EditText)layoutView.findViewById(idCampo);
							Campo campoEvento = camposHash.get(String.valueOf(idCampo));
							String nombreCampo = campoEvento.getNombre();
							String valor = objetoDatos.get(nombreCampo);
							editText.setText(valor);
							
							// Genera evento autocomplete
							auditorFormulario.eventoAutoComplete(ActividadNuevaOrdenServicio.this, layoutView, campoEvento, editText, objetoDatos);
						}
					} 
				}
				break;
			case ControladorFormularios.REQUEST_SUBFORMULARIO:
				// Analiza el resultado
				if(resultCode == RESULT_OK){   			
					// Obtiene el valor retornado
					if(data != null) {
						// Listener para el campo tipo "Detalle"
						int idCampo = data.getIntExtra("idCampo", -1);
						Campo campoEvento = camposHash.get(String.valueOf(idCampo));
						auditorFormulario.validarEventoCampo(ActividadNuevaOrdenServicio.this, layoutView, campoEvento, 
								null, contenedoresCamposHash, camposHash);	
					} 
				}				
				break;
			case ControladorFormularios.REQUEST_SCANCODE:
				// Analiza el resultado
				if(resultCode == RESULT_OK){   			
					// Obtiene el valor retornado
					if(data != null){
						String valor = data.getStringExtra("SCAN_RESULT");
				        //String format = data.getStringExtra("SCAN_RESULT_FORMAT");
						int idCampo = data.getIntExtra("idCampo", -1);
						// Si es un valor válido, lo setea
						if(valor != null && idCampo > 0){
							EditText editText = (EditText)layoutView.findViewById(idCampo);
							editText.setText(valor);
						}
				    } 
				}				
				break;
			case ControladorFormularios.REQUEST_LISTACAMPO:
				// Analiza el resultado
				if(resultCode == RESULT_OK){   			
					// Obtiene el valor retornado
					if(data != null){
						int idCampo = data.getIntExtra("idCampo", -1);
						String valor = data.getStringExtra("valor");
						
						// Si es un valor válido, lo setea
						if(valor != null && idCampo > 0){
							EditText editText = (EditText)layoutView.findViewById(idCampo);
							editText.setText(valor);
						}
					} 
				}
				break;
			default:
				break;
		}
	}
	
	@Override
	public void onTaskCompleted(String accion, Object objto, String... prmtros_callback) {
		// Verifica callback
		if (accion.equals("callback_solicitarNuevoTalonario")){
			if (objto != null) {
				JSONObject jsonObject = (JSONObject) objto;
				try {
					JSONManager jsonManager = new JSONManager(jsonObject);

					// Almacena el talonario
					controladorTalonarios.guardarTalonarioNuevo(jsonManager);
					toolsApp.showShortMessage("Talonario Nuevo!", this);
					
					// Muestra el formulario
					mostrarFormulario();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				// Muestra el formulario
				mostrarFormulario();
			}
		}
	}
}
