package com.gc.ordenesservicio;

import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.gc.coregestionclientes.fragmentos.TimePickerFragment.OnTimePickerEvent;
import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.objetos.AuditorFormulario;
import com.gc.coregestionclientes.objetos.Campo;
import com.gc.coregestionclientes.objetos.ControladorFormularios;
import com.gc.coregestionclientes.objetos.Formulario;
import com.gc.ordenesservicio.FragmentoListaVisitas.OnVisitaSeleccionada;
import com.gc.ordenesservicio.accesodatos.SQLiteRoutinesOS;
import com.gc.ordenesservicio.novedades.ServicioNovedades;
import com.gc.ordenesservicio.objetos.AuditorFormularioOS;
import com.gc.ordenesservicio.objetos.AutocompletarCampoOS;
import com.gc.ordenesservicio.objetos.OrdenServicio;
import com.gc.ordenesservicio.objetos.VisitaOS;

public class ActividadHistorialVisitas extends FragmentActivity implements OnTimePickerEvent,OnVisitaSeleccionada {
	
	// Objetos
	private AplicacionOrdenesServicio aplicacion;
	private ControladorFormularios controladorFormularios = new ControladorFormularios();
	private AuditorFormulario auditorFormulario;
	private Formulario formularioReferencia;
	private OrdenServicio ordenServicio;
	private ToolsApp toolsApp = new ToolsApp();
	
	// Elementos Gráficos	
	private LinearLayout contenedorFormulario;
	private View layoutView = null;
	
	// Objetos auxiliares 
	private HashMap<String, Campo> camposHash = new HashMap<String, Campo>();
	private HashMap<String, View> contenedoresCamposHash = new HashMap<String, View>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.actividad_historial_visitas);
		layoutView = getWindow().getDecorView().findViewById(android.R.id.content);
		
		// Inicializa elemntos graficos.
		contenedorFormulario = (LinearLayout)findViewById(R.id.contenedor_formulario);
		
		// Obtiene el objeto con la configuracion de la app
		aplicacion = ((AplicacionOrdenesServicio)getApplicationContext());

		// Obtiene la orden activa
		ordenServicio = aplicacion.getOrdenServicioActiva();
		
		// Crea el formulario		
		crearFormulario();
		
		// Consulta si existen visitas creadas
		consultarVisitasExistentes();
	}
	
	// Método que inicializa objetos relacionados con Formulario
	private void crearFormulario() {
		// Consulta un formulario de referencia
		formularioReferencia = controladorFormularios.cargarFormularioLocal(
				aplicacion.getConfiguracionApp().getSqliteRoutines(), 
				"TAREAS / VISITAS", Formulario.APLICACION, -1);
		
		// Dependiendo del modo de edición, carga el formulario
		controladorFormularios.setFormularioActivo(formularioReferencia);
				
		// Carga formularios adicionales
		controladorFormularios.cargarFormulariosLocales(aplicacion.getConfiguracionApp().getSqliteRoutines(), 
				Formulario.SUBFORMULARIO);
		
		// Inicializa el auditor
		auditorFormulario = new AuditorFormularioOS(formularioReferencia, null, controladorFormularios);
		
		// Crea dinámicamente el formulario
		controladorFormularios.visualizarFormulario(
				this, layoutView, contenedorFormulario, camposHash, contenedoresCamposHash, 
				auditorFormulario, new AutocompletarCampoOS(), 
				onItemSelectedListener, onCheckedChangeListener, true);
	}
	
	// Consulta visitas creadas
	public void consultarVisitasExistentes(){
		if (getSupportFragmentManager().findFragmentById(android.R.id.content) == null) {
	    	FragmentoListaVisitas fragmentoListaTransacciones = new FragmentoListaVisitas();
			Bundle argumentos = new Bundle();
			argumentos.putParcelable("formulario", formularioReferencia);
			fragmentoListaTransacciones.setArguments(argumentos);
	        getSupportFragmentManager().beginTransaction().add(android.R.id.content, fragmentoListaTransacciones).commit();
	    }
	}

	// Listener para los Spinner
	AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			// Extrae el campo segun el view (Spinner)
			Campo cmpo_evnto = camposHash.get(String.valueOf(arg0.getId()));
			auditorFormulario.validarEventoCampo(ActividadHistorialVisitas.this, layoutView, cmpo_evnto, 
					arg0, contenedoresCamposHash, camposHash);	
		}
		
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {}
	};
	
	// Listener para los Checkbox
	CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// Extrae el campo segun el view (Checkbox)
			Campo cmpo_evnto = camposHash.get(String.valueOf(buttonView.getId()));
			auditorFormulario.validarEventoCampo(ActividadHistorialVisitas.this, layoutView, cmpo_evnto, 
					buttonView, contenedoresCamposHash, camposHash);
		}
	};
	
	public void onBackPressed() {
		// Verificar si se ha seleccionado una visita o no
		if(ordenServicio.getVisitaActiva() != null){
			// Se ha seleccionado
			// Verifica si la visita se edita
			if(ordenServicio.getVisitaActiva().getModoEdicion()) {
				// Si se edita, Guarda los datos
				guardarDatosVisita();
			}
			
			// Muestra nuevamente la lista de visitas
			consultarVisitasExistentes();
		} /*else {
			super.onBackPressed();
		}*/
		
		// Limpia visita activa
		ordenServicio.setIdVisitaActiva(-1);
		
		// Limpia objeto temporal de memoria
		aplicacion.getConfiguracionApp().limpiarMemoria();
		
		super.onBackPressed();

	};
	
	// Guarda los datos en el objeto
	private boolean guardarDatosVisita() {
		// Asigna los valores de la interfaz al formulario
		boolean asignacionExitosa = controladorFormularios.interfazAFormulario(
				this, aplicacion.getConfiguracionApp(), true, auditorFormulario, 
				layoutView, contenedoresCamposHash);

		// Evaluar si la asignación fue exitosa para poder guardar en BD
		if (asignacionExitosa) {
			// Actualiza nuevamente la información de la Orden y la visita
			boolean guardado = ((SQLiteRoutinesOS) aplicacion.getConfiguracionApp().getSqliteRoutines())
			.guardarOrdenServicio(aplicacion, aplicacion.getConfiguracionApp(), ordenServicio);
			
			// Verifica si se guardó en base de datos
			if(guardado) {
				// Procesa el evento de finalización del formulario
				auditorFormulario.validarEventoFinFormulario(this, layoutView);
				toolsApp.showLongMessage("Datos actualizados con éxito!", this);
				
				// Verifica si hay conexión
				if(toolsApp.isConnected(this)){
					// Si hay conexión, Llama servicio para enviar datos pendientes
					Intent intent = new Intent(this, ServicioNovedades.class);
					intent.putExtra("tipoIntent", "WAMP");
					startService(intent);
				}
				
				return true;
			} else toolsApp.showLongMessage("No fue posible actualizar la visita", this);
		}
		return false;
	}

	@Override
	public void onTimePickerEvent(int idCampo, long unixTime) {
		((EditText)layoutView.findViewById(idCampo)).setText(toolsApp.unixTimeToStringHourMinutes(unixTime));
	}

	public void onVisitaSeleccionada(VisitaOS visita) {		
		// Remueve el fragment
		removerFragmento();

		// Adiciona la visita a la orden y la pone activa
		ordenServicio.adicionarVisita(visita);
		ordenServicio.setIdVisitaActiva(visita.getId());
				
		// Setea el nuevo formulario activo
		controladorFormularios.setFormularioActivo(ordenServicio.getVisitaActiva().getFormulario());
		auditorFormulario.setFormulario(ordenServicio.getVisitaActiva().getFormulario());
		
		// Muestra el formulario
		controladorFormularios.formularioAInterfaz(this, layoutView, camposHash, aplicacion.getConfiguracionApp());
	}	
	
	// Elimina el fragmento del stack
	private void removerFragmento() {
		if (getSupportFragmentManager().findFragmentById(android.R.id.content) != null){
			getSupportFragmentManager().beginTransaction().remove(
					getSupportFragmentManager().findFragmentById(android.R.id.content)).commit();
		}
	}
}
