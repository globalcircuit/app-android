package com.gc.ordenesservicio;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gc.coregestionclientes.fragmentos.TimePickerFragment.OnTimePickerEvent;
import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.objetos.AuditorFormulario;
import com.gc.coregestionclientes.objetos.Campo;
import com.gc.coregestionclientes.objetos.ControladorFormularios;
import com.gc.coregestionclientes.objetos.Formulario;
import com.gc.ordenesservicio.accesodatos.CustomSQLiteRoutinesOS;
import com.gc.ordenesservicio.objetos.AuditorFormularioOS;
import com.gc.ordenesservicio.objetos.AutocompletarCampoOS;

import org.json.JSONException;
import org.json.JSONObject;

public class ActividadVisita extends FragmentActivity implements OnTimePickerEvent {

	// Objetos
	private AplicacionOrdenesServicio aplicacion;
	private ControladorFormularios controladorFormularios = new ControladorFormularios();
	private AuditorFormulario auditorFormulario;
	private Formulario formulario;
	private ToolsApp toolsApp = new ToolsApp();
	private boolean modoEdicion;

	// Elementos GrÃ¡ficos
	private LinearLayout contenedorFormulario;
	//private LayoutInflater inflater = null;
	private View layoutView = null;

	// Objetos auxiliares
	private HashMap<String, Campo> camposHash = new HashMap<String, Campo>();
	private HashMap<String, View> contenedoresCamposHash = new HashMap<String, View>();

	private TextView labelDatosOrden;




	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/*inflater = (LayoutInflater) (ActividadVisita.this).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutView = inflater.inflate(R.layout.actividad_visita, null);*/
		setContentView(R.layout.actividad_visita);
		layoutView = getWindow().getDecorView().findViewById(android.R.id.content);
		// lbel_datosOT
		// Inicializa elemntos graficos.
		contenedorFormulario = (LinearLayout)findViewById(R.id.contenedor_formulario);

		//setTitle(getResources().getString(R.string.ttlo_frmlrio_clnte));

		// Obtiene el objeto con la configuracion de la app
		aplicacion = ((AplicacionOrdenesServicio)getApplicationContext());


		try{
			LinkedHashMap <String, String> datosEspecificos = aplicacion.getOrdenServicioActiva().getDatosEspecCliente();
			String datosAdicionales = aplicacion.getOrdenServicioActiva().getDatosAdicionales();

			if( (datosEspecificos != null && datosEspecificos.size() > 0)
					&& (datosAdicionales != null && datosAdicionales.length() > 0)){
				//Convierto los datos adicionales de una orden de servicio a un objeto tipo JSON
				JSONObject jsonObject = new JSONObject(datosAdicionales);
				String paraMostrar = "CÃ³digo Visita: "+ datosEspecificos.get("CÃ³digo Visita") + " / CÃ³digoOT: "
						+ jsonObject.getString("codigoOT");
				// Inicializa el nombre del cliente
				labelDatosOrden = (TextView)findViewById(R.id.lbel_datosOT);
				labelDatosOrden.setText(paraMostrar);
			}else{
				labelDatosOrden = (TextView)findViewById(R.id.lbel_datosOT);
				labelDatosOrden.setVisibility(View.GONE);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (savedInstanceState != null) {
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String fecha = sdf.format(cal.getTime());
			String toLog = "RecuperÃ© el contexto de un formulario matado por Android a las " + fecha;
			toolsApp.generaLog(toLog);
			// AquÃ­ miramos cuando la aplicaciÃ³n se reinicia
			modoEdicion = savedInstanceState.getBoolean("MODO_EDICION");
		}else{
			// Extrae los datos del intent
			modoEdicion = getIntent().getBooleanExtra("MODO_EDICION", false);
		}

		// Mostrar
		mostrarFormulario();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(aplicacion.getOrdenServicioActiva() != null) {
			if(aplicacion.getOrdenServicioActiva().getVisitaActiva() != null) {
				aplicacion.getOrdenServicioActiva().getVisitaActiva().getFormulario().banderaGuardadoTemporal = false;
			}
		}
	};

	@Override
	public void onSaveInstanceState(Bundle bundle) {
		modoEdicion = true;
		bundle.putBoolean("MODO_EDICION", modoEdicion);
		super.onSaveInstanceState(bundle);
	}

	// Inicializa y muestra el formulario
	private void mostrarFormulario() {
		// Dependiendo del modo de ediciÃ³n, carga el formulario
		if (modoEdicion) {
			formulario = aplicacion.getOrdenServicioActiva().getVisitaActiva().getFormulario();
			controladorFormularios = new ControladorFormularios(formulario);

		} else {
			/*
			int idCliennteGlobal = aplicacion.getConfiguracionApp().getUsuario().getIdClienteGlobal();

			// Dependiendo del cliente global se realiza un cambio
			if(idCliennteGlobal == 14){
				//aplicacion
				String nombreFormulario = AuditorFormularioOS.cargaFormularioDinamico(this);;

				// Carga el formulario de visitas o tareas realizadas.
				formulario = controladorFormularios.cargarFormularioLocal(
						aplicacion.getConfiguracionApp().getSqliteRoutines(),
						nombreFormulario, Formulario.APLICACION, -1);
			} else{

				// Carga el formulario de visitas o tareas realizadas.
				formulario = controladorFormularios.cargarFormularioLocal(
						aplicacion.getConfiguracionApp().getSqliteRoutines(),
						"TAREAS / VISITAS", Formulario.APLICACION, -1);

			}*/
			//Obtengo el nombre del formulario dependiendo del cliente global en el auditor
			String nombreFormulario = AuditorFormularioOS.cargaFormularioDinamico(this);

			// Carga el formulario de visitas o tareas realizadas.
			formulario = controladorFormularios.cargarFormularioLocal(
					aplicacion.getConfiguracionApp().getSqliteRoutines(),
					nombreFormulario, Formulario.APLICACION, -1);

			// Setea el formulario de la visita
			aplicacion.getOrdenServicioActiva().getVisitaActiva().setFormulario(formulario);
		}

		// Inicializa el auditor
		controladorFormularios.cargarFormulariosLocales(aplicacion.getConfiguracionApp().getSqliteRoutines(),
				Formulario.SUBFORMULARIO);

		// Inicializa el auditor
		auditorFormulario = new AuditorFormularioOS(formulario, null, controladorFormularios);

		// Crea dinÃ¡micamente el formulario
		controladorFormularios.visualizarFormulario(
				this, layoutView, contenedorFormulario, camposHash, contenedoresCamposHash,
				auditorFormulario, new AutocompletarCampoOS(),
				onItemSelectedListener, onCheckedChangeListener, true);

		// Dependiendo del modo de ediciÃ³n, muestra los datos.
		if (modoEdicion) {
			// Muestra los datos diligenciados previamente.
			controladorFormularios.formularioAInterfaz(this, layoutView, camposHash, aplicacion.getConfiguracionApp());
		} else {
			// Inicializa campos
			auditorFormulario.inicializarCamposFormulario(this,contenedoresCamposHash);
		}
	}

	// Listener para los Spinner
	AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			// Extrae el campo segun el view (Spinner)
			Campo cmpo_evnto = camposHash.get(String.valueOf(arg0.getId()));
			auditorFormulario.validarEventoCampo(ActividadVisita.this, layoutView, cmpo_evnto,
					arg0, contenedoresCamposHash, camposHash);
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {}
	};

	// Listener para los Checkbox
	CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// Extrae el campo segun el view (Checkbox)
			Campo cmpo_evnto = camposHash.get(String.valueOf(buttonView.getId()));
			auditorFormulario.validarEventoCampo(ActividadVisita.this, layoutView, cmpo_evnto,
					buttonView, contenedoresCamposHash, camposHash);
		}
	};

	public void onBackPressed() {
		// Se deshabilita el funcionamiento del back
	};

	// Guarda los datos en el objeto
	private boolean guardarDatosVisita() {
		// Asigna los valores del formulario
		boolean asignacionExitosa = controladorFormularios.interfazAFormulario(
				this, aplicacion.getConfiguracionApp(), true, auditorFormulario,
				layoutView, contenedoresCamposHash);

		// Verifica si la asignaciÃ³n fue exitosa
		if(asignacionExitosa){

			int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
			//Calendario para la fecha de los hitos
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String fecha = sdf.format(cal.getTime());
			String toLog = "guardÃ© el formulario a las " + fecha + " con el idOrden " + idOrdenServicioLog;
			toolsApp.generaLog(toLog);

			// Exitosa,
			aplicacion.getOrdenServicioActiva().getVisitaActiva().setDatos(true);

			if(aplicacion.getOrdenServicioActiva().getVisitaActiva().getFormulario().getId() == 102){
				aplicacion.getOrdenServicioActiva().getVisitaActiva().getFormulario().getCampos().get(12).setObligatorio(true);
			}

			return true;
		}else{
			//Manejo de string para pintar en el log
			int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
			//Calendario para la fecha de los hitos
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String fecha = sdf.format(cal.getTime());
			String toLog = "No pude guardar el formulario a las " + fecha + " con el idOrden " + idOrdenServicioLog;
			toolsApp.generaLog(toLog);
			return false;

		}
	}


	@Override protected void onStop() {
		// Se valida que formulario no este vacio


		if(aplicacion.getOrdenServicioActiva().getVisitaActiva().getFormulario() != null) {
			aplicacion.getOrdenServicioActiva().getVisitaActiva().getFormulario().banderaGuardadoTemporal = true;
			aplicacion.getOrdenServicioActiva().getVisitaActiva().getFormulario().getCampos().get(12).setObligatorio(false);

			if (guardarDatosVisita()) {
				// Salva el contexto de los objetos
				aplicacion.salvarObjetosContexto(true);

				//Manejo de string para pintar en el log
				int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
				//Calendario para la fecha de los hitos
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				String fecha = sdf.format(cal.getTime());
				String toLog = "EntrÃ© a onStop de una visita a las " + fecha + " con el idOrden " + idOrdenServicioLog;
				toolsApp.generaLog(toLog);


			}


		}

		super.onStop();

	};



	@Override
	public void onTimePickerEvent(int idCampo, long unixTime) {
		((EditText)layoutView.findViewById(idCampo)).setText(toolsApp.unixTimeToStringHourMinutes(unixTime));
	}

	// MÃ©todo que maneja las acciones de los botones de layout
	public void ejecutarAccionBoton(View view) {
		switch(view.getId()){

			case R.id.boton_guardarFormulario:
				// Variable formulario banera de guardado temporal
				aplicacion.getOrdenServicioActiva().getVisitaActiva().getFormulario().banderaGuardadoTemporal=false;

				if(guardarDatosVisita()) {
					// Salva el contexto de los objetos
					aplicacion.salvarObjetosContexto(true);

					//Manejo de string para pintar en el log
					int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
					//Calendario para la fecha de los hitos
					Calendar cal = Calendar.getInstance();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					String fecha = sdf.format(cal.getTime());
					String toLog = "HundÃ­ en guardar el formulario a las " + fecha + " con el idOrden " + idOrdenServicioLog;
					toolsApp.generaLog(toLog);

					aplicacion.getOrdenServicioActiva().getVisitaActiva().setEstadoVisita("TERMINADA");
					// GuardÃ³ datos, finaliza la actividad
					finish();
				}

				break;

			case R.id.boton_cancelarFormulario:
				final String datosAdicionales = aplicacion.getOrdenServicioActiva().getDatosAdicionales();
				final LinkedHashMap<String, String> datosEspecificos = aplicacion.getOrdenServicioActiva().getDatosEspecCliente();

				// Mensaje de verificaciÃ³n
				toolsApp.alertDialogYESNO(this, "Â¿EstÃ¡ seguro de CANCELAR este formulario?",
						null, null, new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// Deshabilita el botÃ³n
								((AlertDialog) dialog).getButton(which).setEnabled(false);

								// Dependiendo del botÃ³n presionado, se ejecuta la acciÃ³n
								switch (which){
									case DialogInterface.BUTTON_POSITIVE:
										//Manejo de string para pintar en el log
										int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
										//Calendario para la fecha de los hitos
										Calendar cal = Calendar.getInstance();
										SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
										String fecha = sdf.format(cal.getTime());
										String toLog = "CancelÃ© el formulario de Visitas a las " + fecha + " con el idOrden " + idOrdenServicioLog;
										toolsApp.generaLog(toLog);

										aplicacion.getOrdenServicioActiva().getVisitaActiva().setFormulario(null);
										aplicacion.getOrdenServicioActiva().getVisitaActiva().setEstadoVisita("CANCELADA");

										finish();
										break;
								}
							}
						});
				break;
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			case ControladorFormularios.REQUEST_AUTOCOMPLETE:
				// Analiza el resultado
				if (resultCode == RESULT_OK) {
					// Obtiene el valor retornado
					if (data != null) {
						int idCampo = data.getIntExtra("idCampo", -1);
						HashMap<String, String> objetoDatos = (HashMap<String, String>) data.getSerializableExtra("valor");

						// Si es un valor vÃ¡lido, lo setea
						if (objetoDatos != null && idCampo > 0) {
							EditText editText = (EditText) layoutView.findViewById(idCampo);
							Campo campoEvento = camposHash.get(String.valueOf(idCampo));
							String nombreCampo = campoEvento.getNombre();
							String valor = objetoDatos.get(nombreCampo);
							editText.setText(valor);

							// Genera evento autocomplete
							auditorFormulario.eventoAutoComplete(ActividadVisita.this, layoutView, campoEvento, editText, objetoDatos);
						}
					}
				}
				break;
			case ControladorFormularios.REQUEST_SUBFORMULARIO:
				// Analiza el resultado
				if (resultCode == RESULT_OK) {
					// Obtiene el valor retornado
					if (data != null) {
						// Listener para el campo tipo "Detalle"
						int idCampo = data.getIntExtra("idCampo", -1);
						Campo campoEvento = camposHash.get(String.valueOf(idCampo));
						auditorFormulario.validarEventoCampo(ActividadVisita.this, layoutView, campoEvento,
								null, contenedoresCamposHash, camposHash);
					}
				}
				break;
			case ControladorFormularios.REQUEST_SCANCODE:
				// Analiza el resultado
				if (resultCode == RESULT_OK) {
					// Obtiene el valor retornado
					if (data != null) {
						String valor = data.getStringExtra("SCAN_RESULT");
						//String format = data.getStringExtra("SCAN_RESULT_FORMAT");
						int idCampo = data.getIntExtra("idCampo", -1);
						// Si es un valor vÃ¡lido, lo setea
						if (valor != null && idCampo > 0) {
							EditText editText = (EditText) layoutView.findViewById(idCampo);
							editText.setText(valor);
						}
					}
				}
				break;
			case ControladorFormularios.REQUEST_LISTACAMPO:
				// Analiza el resultado
				if (resultCode == RESULT_OK) {
					// Obtiene el valor retornado
					if (data != null) {
						int idCampo = data.getIntExtra("idCampo", -1);
						String valor = data.getStringExtra("valor");

						// Si es un valor vÃ¡lido, lo setea
						if (valor != null && idCampo > 0) {
							EditText editText = (EditText) layoutView.findViewById(idCampo);
							editText.setText(valor);
						}
					}
				}
				break;
			case ControladorFormularios.REQUEST_ADJUNTAFOTOS:
				// Analiza el resultado
				if (resultCode == RESULT_OK) {
					// Obtiene el valor retornado
					if (data != null) {
						int idCampo = data.getIntExtra("idCampo", -1);
						String valor = data.getStringExtra("valor");

						// TODO: Si es un valor vÃ¡lido, lo setea
						if (valor != null && idCampo > 0) {
							toolsApp.showLongMessage(valor, this);
						}
					}
				}
				break;
			default:
				break;
		}
	}

}
