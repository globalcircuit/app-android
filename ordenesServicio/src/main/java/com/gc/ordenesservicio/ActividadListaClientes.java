package com.gc.ordenesservicio;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import com.gc.coregestionclientes.librerias.JSONManager;
import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.objetos.Cliente;
import com.gc.ordenesservicio.accesodatos.SQLiteRoutinesOS;
import com.gc.ordenesservicio.objetos.ControladorClientes;

public class ActividadListaClientes extends ListActivity {

	// Objetos
	private AplicacionOrdenesServicio aplicacion;
	private ToolsApp toolsApp = new ToolsApp();
	private ControladorClientes controladorClientes = new ControladorClientes();
	private CustomAdapterClientes adapter;
    private Cliente clnte_tmpral;

	// Elementos Gráficos y Menu
	private EditText edt_fltro_clntes;
	private MenuItem item_clntes_tdos; 
	private MenuItem item_clntes_no_atntcdos;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.actividad_lista_clientes);
		
		edt_fltro_clntes = (EditText) findViewById(R.id.edt_fltro_clntes);
		edt_fltro_clntes.addTextChangedListener(filterTextWatcher);
	
		// Inicializa el Objeto con la configuracion de la App para ser
		// utilizado
		aplicacion = ((AplicacionOrdenesServicio) getApplicationContext());
		
		// Inicializa el controlador de clientes con acceso a DB
		controladorClientes = new ControladorClientes(
				(SQLiteRoutinesOS) aplicacion.getConfiguracionApp().getSqliteRoutines());
		
		// Registra el menu contextual
	    registerForContextMenu(getListView());
	}

	@Override
	protected void onResume() {
		super.onResume();

		// Lista los clintes locales
		inicializarListaClientes("LOCAL", null);	
	}

	@Override
	public void finish() {
		super.finish();
		
		// Salva el contexto de los objetos
		aplicacion.salvarObjetosContexto(false);
	}
	
	@Override
	protected void onDestroy() {
		edt_fltro_clntes.removeTextChangedListener(filterTextWatcher);
		super.onDestroy();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	@Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
    	super.onCreateContextMenu(menu, v, menuInfo);   		
    	// Obtiene la información de la lista
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
        
        // Extrae el objeto seleccionado
        clnte_tmpral = (Cliente)this.getListAdapter().getItem(info.position);
        
        // Dependiendo del cliente habilita el menu
        if(clnte_tmpral.getCodigo() == null){
        	// Clientes sin codigo
	    	// Permite crear el menu teniendo en cuenta la plantilla creada.
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.menu_contextual_lista_clientes, menu);
        } else {
        	// Cliente registrado, no se puede eliminar
			clnte_tmpral = null;
        }
    }
	
	@Override
    public boolean onContextItemSelected(MenuItem item) {			
        switch (item.getItemId()) {
        	case R.id.opcion_elmnar_clnte:
				// Pregunta por la cancelación de la visita programada
				toolsApp.alertDialogYESNO(this, 
					"¿Está seguro de eliminar el cliente: " + clnte_tmpral.getNombres(),
					"SI", 
					"NO",
					new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
					    	// Deshabilita el botón
				        	((AlertDialog) dialog).getButton(which).setEnabled(false);
				        	
				        	// Dependiendo del botón presionado, se ejecuta la acción
							switch (which){
								case DialogInterface.BUTTON_POSITIVE:							    	
							    	// Desactiva el cliente
									clnte_tmpral.setActivo(false);
									controladorClientes.desactivarCliente(clnte_tmpral.getId());
									
									// Verifica si hay conexión
									if(toolsApp.isConnected(ActividadListaClientes.this)){
										// Si hay conexión,
										// TODO:
										/*controladorClientes.enviarDatosCliente(
												ActividadListaClientes.this, aplicacion, clnte_tmpral, true);*/
									}else{
										//No hay conexión
										toolsApp.showLongMessage("Cliente eliminado offline!", ActividadListaClientes.this);
									}
								break;
								case DialogInterface.BUTTON_NEGATIVE:
									// No hace nada.
								break;
							}
						}
					}
				);
        		return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.actividad_lista_clientes, menu);
		item_clntes_tdos = (MenuItem) menu.findItem(R.id.opcion_clntes_tdos); 
		item_clntes_no_atntcdos = (MenuItem) menu.findItem(R.id.opcion_clntes_no_atntcdos);  
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {	
		// Handle item selection
		switch (item.getItemId()) {
			case R.id.opcion_clntes_tdos:
				// Filtrar Clientes
				if(adapter != null){
					adapter.getFilter().filter("GC_TODOS");
					item_clntes_tdos.setVisible(false);
					item_clntes_no_atntcdos.setVisible(true);
				}
				return true;
			case R.id.opcion_clntes_no_atntcdos:
				// Filtrar Clientes
				if(adapter != null){
					adapter.getFilter().filter("GC_NO_AUTENTICADOS");
					item_clntes_tdos.setVisible(true);
					item_clntes_no_atntcdos.setVisible(false);
				}
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	// TextWatcher para filtrar las sucursales por nombre
	private TextWatcher filterTextWatcher = new TextWatcher() {
		@Override
		public void afterTextChanged(Editable s) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		// Permite filtrar el TextView a medida que va cambiando
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if(adapter != null){
				adapter.getFilter().filter(s);
			}
		}
	};

	// Inicializa los clientes dependiendo del MODO
	private void inicializarListaClientes(String MODO, Object objto) {
		if(MODO.equals("WEB")){
			JSONObject jsonObject = (JSONObject) objto;
			try {
				JSONManager jsonManager = new JSONManager(jsonObject);

				// Inicializa Clientes
				controladorClientes.asignarClientesWeb(jsonManager);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}else if (MODO.equals("LOCAL")) {
			controladorClientes.asignarClientesLocales(aplicacion.getConfiguracionApp().getCustomSqliteRoutines());
		}

		// Visualiza los clientes
		if(controladorClientes.getListaClientes() != null){
			adapter = new CustomAdapterClientes(this,
					controladorClientes.getListaClientes());
			setListAdapter(adapter);
			Log.i(AplicacionOrdenesServicio.tag_app, "Clientes cargados con éxito!");
		} else {
			toolsApp.showLongMessage("No hay clientes asociados!!", this);
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		// Extrae el objeto seleccionado
		Cliente clnte = (Cliente) this.getListAdapter().getItem(position);
		
		// Coloca el cliente como activo
		aplicacion.getConfiguracionApp().setClienteActivo(clnte);

		// Salva el contexto de los objetos
		aplicacion.salvarObjetosContexto(true);
		
		// Se inicia sesión con el cliente seleccionado.
		Intent intent = new Intent(this, ActividadNuevaOrdenServicio.class);
		startActivity(intent);
		finish();
	}

	// Clase customizda para la visualizacion de los clientes
	private class CustomAdapterClientes extends BaseAdapter implements
			Filterable {
		private Activity activity;
		private List<Cliente> list;
		private Cliente clnte;
		private LayoutInflater inflater = null;
		protected List<Cliente> orig;

		// Constructor
		public CustomAdapterClientes(Activity activity, List<Cliente> list) {
			this.activity = activity;
			this.list = list;
			this.inflater = (LayoutInflater) this.activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		// Retorna el número de items en la lista
		public int getCount() {
			return list.size();
		}

		// Permite obtener el objeto en la posición
		public Object getItem(int position) {
			clnte = list.get(position);
			return clnte;
		}

		// Permite obtener el estilo de layout previamente definido y coloca los
		// valores de la fila
		// que serán mostrados
		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;
			if (convertView == null)
				vi = inflater.inflate(R.layout.row_cliente, parent, false);

			// Obtiene los view donde será colocada la infromación
			TextView row_cdgo_clnte = (TextView) vi.findViewById(R.id.row_cdgo_clnte);
			TextView row_nmbre_clnte = (TextView) vi.findViewById(R.id.row_nmbre_clnte);
			TextView row_vstas_prgrmdas = (TextView) vi.findViewById(R.id.row_vstas_prgrmdas);

			// Obtiene los datos en la posición del arreglo
			Cliente clnte = list.get(position);

			// Coloca el nombre
			row_nmbre_clnte.setText(clnte.getNombres());

			// Coloca el id
			String cdgo_clnte = clnte.getCodigo();
			if(cdgo_clnte == null){
				// Adiciona vacío en la sección de código
				row_cdgo_clnte.setText("");
				
				// Coloca el nombre del cliente con el color rojo
				row_nmbre_clnte.setTextColor(Color.CYAN);
			}else{
				// Adiciona el código respectivo.
				row_cdgo_clnte.setText(cdgo_clnte);
				
				// Coloca el nombre del cliente con el color default
				row_nmbre_clnte.setTextColor(Color.WHITE);
			}

			row_vstas_prgrmdas.setText("");
			row_vstas_prgrmdas.setVisibility(TextView.GONE);

			return vi;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public Filter getFilter() {
			return new Filter() {

				@Override
				protected FilterResults performFiltering(CharSequence constraint) {
					String prmtro = constraint.toString();
					final FilterResults oReturn = new FilterResults();
					final ArrayList<Cliente> results = new ArrayList<Cliente>();
					if (orig == null){
						orig = list;
					}
					
					// Recorre la lista original y dependiendo del filtro aplica el criterio
					if (orig != null && orig.size() > 0) {
						for (Cliente clnte : orig) {
							if (prmtro.equals("GC_NO_AUTENTICADOS")) {
								if (clnte.getCodigo() == null) {
									results.add(clnte);
								}
							} else if (prmtro.equals("GC_TODOS")) {
								results.add(clnte);
							} else if (clnte
									.getNombres()
									.toLowerCase(Locale.getDefault())
									.contains(constraint.toString().toLowerCase(Locale.getDefault()))
							) {
								results.add(clnte);
							}
						}
					}
				
					oReturn.values = results;
					return oReturn;
				}

				@SuppressWarnings("unchecked")
				@Override
				protected void publishResults(CharSequence constraint,
						FilterResults results) {
					list = (ArrayList<Cliente>) results.values;
					notifyDataSetChanged();
				}
			};
		}

		public void notifyDataSetChanged() {
			super.notifyDataSetChanged();
		}
	}
}
