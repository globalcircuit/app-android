package com.gc.ordenesservicio;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.gc.coregestionclientes.fragmentos.DatePickerFragment.OnDatePickerEvent;
import com.gc.coregestionclientes.fragmentos.TimePickerFragment.OnTimePickerEvent;
import com.gc.coregestionclientes.interfaces.OnTaskCompleted;
import com.gc.coregestionclientes.librerias.JSONManager;
import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.objetos.AuditorFormulario;
import com.gc.coregestionclientes.objetos.Campo;
import com.gc.coregestionclientes.objetos.ControladorFormularios;
import com.gc.coregestionclientes.objetos.ControladorTalonarios;
import com.gc.coregestionclientes.objetos.Formulario;
import com.gc.ordenesservicio.accesodatos.SQLiteRoutinesOS;
import com.gc.ordenesservicio.novedades.ServicioNovedades;
import com.gc.ordenesservicio.objetos.ActividadAdministrativa;
import com.gc.ordenesservicio.objetos.AuditorFormularioOS;
import com.gc.ordenesservicio.objetos.AutocompletarCampoOS;

public class ActividadActividadesAdministrativas extends FragmentActivity implements OnTimePickerEvent, OnDatePickerEvent, OnTaskCompleted {

	private AplicacionOrdenesServicio aplicacion;
	private ToolsApp toolsApp;
	private Formulario formulario;
	private ControladorFormularios controladorFormularios = new ControladorFormularios();
	private AuditorFormulario auditorFormulario;
	ActividadAdministrativa actividadAdministrativa;
	private ControladorTalonarios controladorTalonarios;
	
	// Elementos Gráficos	
	private LinearLayout contenedorFormulario;
	private View layoutView = null;
	
	// Objetos auxiliares 
	private HashMap<String, Campo> camposHash = new HashMap<String, Campo>();
	private HashMap<String, View> contenedoresCamposHash = new HashMap<String, View>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_actividad_actividades_administrativas);
		
		layoutView = getWindow().getDecorView().findViewById(android.R.id.content);
		
		// Inicializa elemntos graficos.
		contenedorFormulario = (LinearLayout)findViewById(R.id.contenedor_formulario);
		
		// Obtiene el objeto con la configuracion de la app
		aplicacion = ((AplicacionOrdenesServicio)getApplicationContext());
		
		// Inicializa toolsapp
		toolsApp = new ToolsApp();
		
		controladorTalonarios = new ControladorTalonarios(aplicacion.getConfiguracionApp().getSqliteRoutines());
		controladorTalonarios.cargarTalonario("actividades", "id");
		boolean solicitarTalonario = controladorTalonarios.solicitarNuevoTalonario(
		this, aplicacion.getConfiguracionApp(), "actividades", "id");
		if(!solicitarTalonario) {
			crearFormulario();
		}
	}
	
	private void crearFormulario() {
		int consecutivo = controladorTalonarios.obtenerConsecutivo("actividades", "id");
		if(consecutivo > 0) {
			String fechaInicioString = toolsApp.getDateTimeNOW();
	
			long fechaInicio = toolsApp.stringToUnixDate(fechaInicioString,true);
			
			formulario = controladorFormularios.cargarFormularioLocal(
					aplicacion.getConfiguracionApp().getSqliteRoutines(), 
					"ACTIVIDADES ADMINISTRATIVAS", Formulario.APLICACION, -1);
			
			actividadAdministrativa = new ActividadAdministrativa(consecutivo,
					aplicacion.getConfiguracionApp().getUsuario().getId(),
					fechaInicio, -1, formulario);
			
			aplicacion.setActividadAdministrativaActiva(actividadAdministrativa);
			
			// Dependiendo del modo de ediciÃ³n, carga el formulario
			controladorFormularios = new ControladorFormularios(formulario);
					
			// Inicializa el auditor
			controladorFormularios.cargarFormulariosLocales(aplicacion.getConfiguracionApp().getSqliteRoutines(), 
					Formulario.SUBFORMULARIO);
			
			// Inicializa el auditor
			auditorFormulario = new AuditorFormularioOS(formulario, null, controladorFormularios);
			
			// Crea dinámicamente el formulario
			controladorFormularios.visualizarFormulario(
					this, layoutView, contenedorFormulario, camposHash, contenedoresCamposHash, 
					auditorFormulario, new AutocompletarCampoOS(), 
					onItemSelectedListener, onCheckedChangeListener, true);
			
			mostrarFormulario();
		}else {
			finish();
			toolsApp.showLongMessage(
					"No hay talonarios para trabajar Offline, asegúrese de tener internet!", this);
		}
	}
	
	private void mostrarFormulario() {	
		auditorFormulario.inicializarCamposFormulario(this,contenedoresCamposHash);
		// Muestra los datos diligenciados previamente.
		controladorFormularios.formularioAInterfaz(this, layoutView, camposHash, aplicacion.getConfiguracionApp());
		
		// Salvamos contexto
		aplicacion.salvarObjetosContexto(false);
	}
	
	// Listener para los Spinner
	AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			// Extrae el campo segun el view (Spinner)
			Campo cmpo_evnto = camposHash.get(String.valueOf(arg0.getId()));
			auditorFormulario.validarEventoCampo(ActividadActividadesAdministrativas.this, layoutView, cmpo_evnto, 
					arg0, contenedoresCamposHash, camposHash);	
		}
		
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {}
	};
	
	// Listener para los Checkbox
	CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// Extrae el campo segun el view (Checkbox)
			Campo cmpo_evnto = camposHash.get(String.valueOf(buttonView.getId()));
			auditorFormulario.validarEventoCampo(ActividadActividadesAdministrativas.this, layoutView, cmpo_evnto, 
					buttonView, contenedoresCamposHash, camposHash);
		}
	};

	@Override
	public void onTimePickerEvent(int idCampo, long unixTime) {
		((EditText)layoutView.findViewById(idCampo)).setText(toolsApp.unixTimeToStringHourMinutes(unixTime));
	}
	
	@Override
	public void onBackPressed() {
		// TODO: Se quitó la funcionalidad del back, motrar mensaje ?
	}
	
	// Método se utiliza para los botones del layout
	public void ejecutarAccionBoton(View view){
		
		switch(view.getId()){
		case R.id.boton_guardar_actividad_administrativa:
			toolsApp.alertDialogYESNO(this, "Está seguro de GUARDAR esta actividad?", 
					null, null, new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
					    	// Deshabilita el botón
				        	((AlertDialog) dialog).getButton(which).setEnabled(false);

				        	// Dependiendo del botón presionado, se ejecuta la acción
					        switch (which){
						        case DialogInterface.BUTTON_POSITIVE:	
									guardarActividad();
						        break;
					        }
						}
					});
			break;
		case R.id.boton_cancelar_actividad_administrativa:
			toolsApp.alertDialogYESNO(this, "Está seguro de CANCELAR este formulario?", 
				null, null, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
				    	// Deshabilita el botón
			        	((AlertDialog) dialog).getButton(which).setEnabled(false);

			        	// Dependiendo del botón presionado, se ejecuta la acción
				        switch (which){
					        case DialogInterface.BUTTON_POSITIVE:
								aplicacion.limpiarObjetosAplicacion();		
								finish();
					        break;
				        }
					}
				});
			break;
		}
		
	}
	
	private void guardarActividad(){
		
		// Asigna los valores del formulario
		boolean asignacionExitosa = controladorFormularios.interfazAFormulario(
				this, aplicacion.getConfiguracionApp(), true, auditorFormulario, 
				layoutView, contenedoresCamposHash);
		
		if(asignacionExitosa) {
			
			// Guarda la actividad administrativa
			((SQLiteRoutinesOS) aplicacion.getConfiguracionApp().getSqliteRoutines())
			.guardarActividadAdministrativa(aplicacion, aplicacion.getConfiguracionApp(), actividadAdministrativa);
			
			// Inicia Servicio de novedades para sincronizar
			Intent intent = new Intent(this, ServicioNovedades.class);
	        intent.putExtra("tipoIntent", "WAMP");
	        this.startService(intent);
			
			// Limpia objetos
			aplicacion.limpiarObjetosAplicacion();		
			toolsApp.showLongMessage("Datos guardados!", this);
			finish();	
		}
	}
	
	@Override
	public void onTaskCompleted(String accion, Object objto, String... prmtros_callback) {
		// Verifica callback
		if (accion.equals("callback_solicitarNuevoTalonario")){
			if (objto != null) {
				JSONObject jsonObject = (JSONObject) objto;
				try {
					JSONManager jsonManager = new JSONManager(jsonObject);

					// Almacena el talonario
					controladorTalonarios.guardarTalonarioNuevo(jsonManager);
					toolsApp.showShortMessage("Talonario Nuevo!", this);
					
					// Muestra el formulario
					crearFormulario();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				// Muestra el formulario
				crearFormulario();
			}
		}
	}

	@Override
	public void onDatePickerEvent(long unixDate,int idCampo) {
		((EditText)layoutView.findViewById(idCampo)).setText(toolsApp.unixDateToString(unixDate, 1, false));
	}
}
