package com.gc.ordenesservicio.novedades;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

public class NovedadesBR extends BroadcastReceiver {
    public static final int NOTIF_ALERTA_ID = 1;

    @Override
    public void onReceive(Context context, Intent intent) {
        // Crear objeto para mantener el "móvil despierto" e iniciarlo
        if(ServicioNovedades.wakeLock == null) {
            PowerManager powerManager = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
            ServicioNovedades.wakeLock = powerManager.newWakeLock(
                    PowerManager.PARTIAL_WAKE_LOCK, NovedadesBR.class.getName());
        }
        ServicioNovedades.wakeLock.acquire();

        // Intentar cancelar una alarma pendiente
        Intent intentoAlarma = new Intent(context, NovedadesBR.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intentoAlarma, 0);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        try {
            alarmManager.cancel(pendingIntent);
        } catch(Exception e) {
            // Error...
            Log.e(NovedadesBR.class.getName(), Log.getStackTraceString(e));
        }

        // Intent al servicio
        Intent intentoServicioNovedades = new Intent(context, ServicioNovedades.class);
        intentoServicioNovedades.putExtra("tipoIntent", "GPS");
        intentoServicioNovedades.putExtra("quienSolicita", ModuloSeguimiento.RASTREO);
        context.startService(intentoServicioNovedades);

		
        // Programar la alarma segun el intervalo.
        alarmManager.set(AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis() + (60000 * 5), pendingIntent);


    }
}
