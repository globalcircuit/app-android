package com.gc.ordenesservicio.novedades;

import java.util.Iterator;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.gc.coregestionclientes.Configuracion;
import com.gc.coregestionclientes.R;
import com.gc.coregestionclientes.novedades.BaseDatosTemporal;
import com.gc.coregestionclientes.novedades.tracking.EstadoTelefono;
import com.gc.ordenesservicio.AplicacionOrdenesServicio;

public class ModuloSeguimiento implements LocationListener {
    private AplicacionOrdenesServicio aplicacion;

    // Constantes
	public final static int RASTREO = 1;
	public final static int INICIO_VISITA = 2;
	public final static int FIN_VISITA = 4;
	public final static int POSICION_CLIENTE = 8;
	
    // Timer y Handler utilizado en el posicionamiento GPS
    private Handler thGPS = new Handler();
    private Runnable trGPS = new Runnable() {
        @Override
        public void run() {
            obtenerPosicion();
        }
    };
    
    // Estados de posicionamiento
    private int estadoPosicionamiento = -1;
    public static final int SOLICITAR_POSICION_GPS = 1;
    public static final int ESPERANDO_POSICION_GPS = 2;
    public static final int ESPERANDO_POSICION_GPS_2 = 3;
    public static final int SOLICITAR_POSICION_NETWORK = 4;
    public static final int ESPERANDO_POSICION_NETWORK = 5;
    public static final int FIN_POSICIONAMIENTO = 6;
    
    // Variables para calcular posición geográfica
    private Configuracion configApp;
    private Context contexto;
    //private Cola colaComandosWAMP;
	private BaseDatosTemporal baseDatosTemporal = null;
    private TelephonyManager servicioTelefonia;
    private EstadoTelefono monitorTelefono;
    protected LocationManager locationManager;
    private int numeroIntentos;
    private Location posicion;
    private long tiempoUTC;
    private int maximoIntentos;
    private boolean localizacionValida;
    private String imei;
    private String mcc_mnc;
    private int quienSolicita = 0;
	//private boolean guardarTrama;
    
    public ModuloSeguimiento(Context contexto, Configuracion configApp) {
        aplicacion = ((AplicacionOrdenesServicio)contexto);
        this.contexto = contexto;
    	this.configApp = configApp;
    	//TODO: this.colaComandosWAMP = colaComandosWAMP;
        baseDatosTemporal = BaseDatosTemporal.getInstance(contexto);
    	
        // Declarar objeto para obtener información del teléfono
        servicioTelefonia = (TelephonyManager)contexto.getSystemService(Context.TELEPHONY_SERVICE);

        // Obtener imei, MCC + MNC
        imei = aplicacion.getConfiguracionApp().getDispositivo().getId();
        mcc_mnc = servicioTelefonia.getNetworkOperator();
        
        // Crear clase para obtener estado del teléfono y registrarla para que
        // reciba actualizaciones
        monitorTelefono = new EstadoTelefono(contexto);
        servicioTelefonia.listen(monitorTelefono,
                PhoneStateListener.LISTEN_SIGNAL_STRENGTHS |
                        PhoneStateListener.LISTEN_CELL_LOCATION |
                        PhoneStateListener.LISTEN_DATA_CONNECTION_STATE);
    	
        locationManager = null;
        if (contexto != null)
            // Obtener manejador de localización
            locationManager = (LocationManager)contexto.getSystemService(Context.LOCATION_SERVICE);
    }
    
    public void iniciarProceso() {
    	// Consulta los minutos acumulados y su timestamp desde el ultimo posicionamiento.
    	int minutosAcumulados = baseDatosTemporal.consultarMinutosAcumulados();
    	long timestampAnterior = baseDatosTemporal.consultarTimeStamp();
    	
    	// Verifica si el timestamp es mayor a 0
    	if (timestampAnterior > 0){
    		// Haya la diferencia en minutos entre el timestamp actual y el anterior
    		 Log.i(ModuloSeguimiento.class.getName(), 
    				 "Division: " + (System.currentTimeMillis() - timestampAnterior) / 60000f);
    		int diferencia = Math.round(((System.currentTimeMillis() - timestampAnterior) / 60000f));
    		
    		// Aumenta la diferencia a los minutos acumulados.
    		minutosAcumulados += diferencia;
    	}
        Log.i(ModuloSeguimiento.class.getName(), 
        		"Minutos Acumulados: " + minutosAcumulados + " horaValida: " + configApp.getSqliteRoutines().horaValida());
        
        // TODO: Borrar o comentar
        /*enviarNotificacion("Sol: " + quienSolicita,
                "Min. Acu: " + minutosAcumulados + " Hr: " + configApp.getSqliteRoutines().horaValida() +
                " Int: " + configApp.getSqliteRoutines().leerIntervaloSeguimiento(),
                true, true, true);*/
        
    	// Verifica si esta disponible para tomar posición.
		if (estadoPosicionamiento == -1 || estadoPosicionamiento == FIN_POSICIONAMIENTO) {
	    	// Verifica quien solicita el posicionamiento (por demanda o rastreo periodico)
	    	if (quienSolicita > 1) {
				// Inicio de visita, Fin de visita o Geoposicion de cliente, 
                estadoPosicionamiento = SOLICITAR_POSICION_GPS;
                obtenerPosicion();
			} else if (configApp.getSqliteRoutines().horaValida() > 0){
				// Hora valida para rastreo,				
				// Verifica si los minutos acumulados son mayores o iguales al intervalo de Posicionamiento
				if (minutosAcumulados >= configApp.getSqliteRoutines().leerIntervaloSeguimiento()){
					
					// limpia los minutos acumulados
					minutosAcumulados = 0;
					
					// Solicita posición
	                estadoPosicionamiento = SOLICITAR_POSICION_GPS;
	                obtenerPosicion();
				}
			}
    	} else {
    		// ya está tomando posición...
    	}
		
		// Actualiza los minutos acumulados y timeStamp
		baseDatosTemporal.actualizarParametros(minutosAcumulados);
	}
    
	public void deshabilitarListeners() {
        servicioTelefonia.listen(monitorTelefono, PhoneStateListener.LISTEN_NONE);
        monitorTelefono = null;		
	}
    
    public int obtenerEstadoActual() {
		return estadoPosicionamiento;
	}
    
    @Override
    public void onLocationChanged(Location location) {
        if (location != null && location.hasAccuracy()) {
            if (numeroIntentos == 0 || location.getAccuracy() < posicion.getAccuracy()) {
                posicion = location;

                // Verificar el número de intentos
                if (++numeroIntentos < maximoIntentos && posicion.getAccuracy() > 20)
                    // Continua iterando...
                    return;

                // Detener las actualizaciones de este proveedor
                localizacionValida = true;
                locationManager.removeUpdates(this);

                // Llamar la máquina de estados
                thGPS.removeCallbacks(trGPS);
                thGPS.postDelayed(trGPS, 0);
            }
        }
    }
    
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onProviderDisabled(String provider) {}

    // Hallar el número de satélites que están a la "vista"
    public int numeroSatelitesVista() {
        // Recorrer la lista de satélites y contarlos
        final GpsStatus gpsStatus = locationManager.getGpsStatus(null);
        Iterator<GpsSatellite> listaSatelites = gpsStatus.getSatellites().iterator();
        int i = 0;
        for (; listaSatelites.hasNext(); ++i) listaSatelites.next();

        return i;
    }
    
    private void enviarNotificacion(String titulo, String contenido,
                                    boolean sonido, boolean continua, boolean seCancela) {
        // TODO: íconos
        //Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher_globalcircuit);
        int smalIcon = R.drawable.ic_launcher;

        // Tarea que se ejecuta cuando se pulsa la notificación (en este caso Dummy)
        PendingIntent pendingIntent = PendingIntent.getActivity(
                contexto, 0, new Intent(), Intent.FLAG_ACTIVITY_NEW_TASK);

        // Obtener instancia del servicios administrador de notificaciones
        NotificationManager notificationManager =
                (NotificationManager) contexto.getSystemService(Context.NOTIFICATION_SERVICE);

        // Configurar y crear la notificación
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(contexto)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(titulo)
                .setContentText(contenido)
                .setSmallIcon(smalIcon)
                .setAutoCancel(true)
                .setTicker(titulo)
                //.setLargeIcon(largeIcon)
                .setDefaults(sonido ? Notification.DEFAULT_SOUND : 0)
                .setOngoing(continua)
                .setAutoCancel(seCancela)
                .setContentIntent(pendingIntent);
        Notification notification = notificationBuilder.build();

        // Enviar notificacion
        notificationManager.notify(1428, notification);
    }

    private boolean estaHabilitadoGPS() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private boolean estaHabilitadoNETWORK() {
        return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void enviarTrack(int estadoMovil) {
        StringBuilder datos = new StringBuilder("{");
        //datos.append("\"idDispositivo\":\"").append(imei).append("\",");
        datos.append("\"quienSolicita\":").append(quienSolicita).append(",");
        datos.append("\"utcMovil\":").append(tiempoUTC).append(",");
        datos.append("\"estado\":").append(estadoMovil).append(",");
        if (localizacionValida) {
            datos.append("\"posicion\":[");
            datos.append("\"").append(posicion.getProvider()).append("\",");
            datos.append(posicion.getTime()).append(",");
            datos.append(posicion.getLatitude()).append(",");
            datos.append(posicion.getLongitude()).append(",");
            datos.append(posicion.getAltitude()).append(",");
            datos.append(posicion.getAccuracy()).append(",");
            datos.append(posicion.getSpeed()).append(",");
            datos.append(posicion.getBearing()).append("],");
        }
        if (monitorTelefono.locationAreaCode() != -1 && monitorTelefono.cellIdentifier() != -1) {
            datos.append("\"celda\":[");
            datos.append("\"").append(mcc_mnc).append("\",");
            datos.append(monitorTelefono.locationAreaCode()).append(",");
            datos.append(monitorTelefono.cellIdentifier()).append("],");
            datos.append("\"potenciaAntena\":").append(monitorTelefono.potenciaAntena()).append("}");
        } else {
            datos.replace(datos.length() - 1, datos.length(), "}");
        }

        // TODO: Verifica si debe guardar la trama.
        //if (guardarTrama) {
        	// Guardar Trama
        	baseDatosTemporal.guardarTrama(ServicioNovedades.siguienteIdConsulta(), datos.toString());
        /*} else {
            // Enviar comando para reportar track
            colaComandosWAMP.deposit(new ComandoWAMP(
                    2, "coregc.rastreo", datos.toString(), null, null, "rastreo", -1, -1, -1));
    		Log.i(BaseDatosTemporal.class.getName(), "Enviando trama normal: " + ServicioNovedades.consecutivoIdConsulta);
        }*/
    }

    private void obtenerPosicion() {
        try {
            switch (estadoPosicionamiento) {
                case SOLICITAR_POSICION_GPS:
                    tiempoUTC = System.currentTimeMillis();

                    // Está habilitado el GPS?
                    if (estaHabilitadoGPS()) {
                        numeroIntentos = 0;
                        maximoIntentos = 5;
                        posicion = null;
                        localizacionValida = false;
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                        estadoPosicionamiento = ESPERANDO_POSICION_GPS;
                        thGPS.postDelayed(trGPS, 60 * 1000);
                    } else {
                        estadoPosicionamiento = SOLICITAR_POSICION_NETWORK;
                        thGPS.postDelayed(trGPS, 0);
                    }
                    break;
                case ESPERANDO_POSICION_GPS:
                case ESPERANDO_POSICION_GPS_2:
                    if (localizacionValida || posicion != null) {
                        localizacionValida = true;
                        estadoPosicionamiento = FIN_POSICIONAMIENTO;
                        thGPS.postDelayed(trGPS, 0);
                    } else if (estadoPosicionamiento == ESPERANDO_POSICION_GPS && numeroSatelitesVista() > 2) {
                        // Esperar un tiempo más
                        estadoPosicionamiento = ESPERANDO_POSICION_GPS_2;
                        thGPS.postDelayed(trGPS, 30 * 1000);
                    } else {
                        // Detener GPS e intentar con NETWORK
                        locationManager.removeUpdates(this);
                        estadoPosicionamiento = SOLICITAR_POSICION_NETWORK;
                        thGPS.postDelayed(trGPS, 0);
                    }
                    break;
                case SOLICITAR_POSICION_NETWORK:
                    // Está habilitado el NETWORK y hay acceso a datos?
                    if (estaHabilitadoNETWORK() && monitorTelefono.hayConexionDatos(contexto)) {
                        numeroIntentos = 0;
                        maximoIntentos = 1;
                        posicion = null;
                        localizacionValida = false;
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
                        thGPS.postDelayed(trGPS, 15 * 60 * 1000);
                        estadoPosicionamiento = ESPERANDO_POSICION_NETWORK;
                    } else {
                        thGPS.postDelayed(trGPS, 0);
                        estadoPosicionamiento = FIN_POSICIONAMIENTO;
                    }
                    break;
                case ESPERANDO_POSICION_NETWORK:
                    if (!localizacionValida) {
                        // Detener NETWORK
                        locationManager.removeUpdates(this);
                    }
                    estadoPosicionamiento = FIN_POSICIONAMIENTO;
                    thGPS.postDelayed(trGPS, 0);
                    break;
                case FIN_POSICIONAMIENTO:                    
                    // Obtiene el estado del dispositivo
                    int estadoMovil = monitorTelefono.obtenerEstado(estaHabilitadoNETWORK(), estaHabilitadoGPS());
                    
                    // Envía el track
                    enviarTrack(estadoMovil);

                    // Muestra notificación
                    if (quienSolicita >= POSICION_CLIENTE) {
	                    if (localizacionValida) {
	                        enviarNotificacion("Posición geográfica calculada",
	                                "R: " + posicion.getAccuracy() + " mts., " + "Estado: " + estadoMovil,
	                                true, true, true);
	                    } else {
	                        enviarNotificacion("No se pudo calcular posición geográfica",
	                                "Estado: " + estadoMovil,
	                                true, true, true);
	                    }
                    }
                    
                    // Limpia el vector quienSolicita
                    quienSolicita = 0;
            }
        } catch (Exception e) {
            Log.e(ServicioNovedades.class.getName(), Log.getStackTraceString(e));
        }
    }

	public int getQuienSolicita() {
		return quienSolicita;
	}

	public void setQuienSolicita(int quienSolicita) {
		this.quienSolicita = quienSolicita;		
	}

	public String getImei() {
		return imei;
	}

	// TODO:
	/*public boolean getGuardarTrama() {
		return guardarTrama;
	}

	public void setGuardarTrama(boolean guardarTrama) {
		this.guardarTrama = guardarTrama;		
	}*/	
}
