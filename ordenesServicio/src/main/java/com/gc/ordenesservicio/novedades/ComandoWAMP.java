package com.gc.ordenesservicio.novedades;

import java.io.UnsupportedEncodingException;

import android.util.Log;

public class ComandoWAMP {
    private int idconsulta;
    private int accion;
    private String uri;
    private String datos;
    private String excluidos, elegidos;
    private String nombreIntento;
    
    // En segundos
    private int timeoutEnvio = 20;
    private int timeoutRecepcion = 50;

    public ComandoWAMP(int accion, String uri, String datos, String excluidos,
                       String elegidos, String nombreIntento, int timeoutEnvio, int timeoutRecepcion,
                       int idConsulta) {
        this.accion = accion;
        this.uri = uri;
        this.datos = datos;
        this.excluidos = excluidos;
        this.elegidos = elegidos;
        this.nombreIntento = nombreIntento;

        // Asigna timeouts
        if (timeoutEnvio != -1)
        	this.timeoutEnvio = timeoutEnvio;
        if (timeoutRecepcion != -1)
        	this.timeoutRecepcion = timeoutRecepcion;
        
        // Si la acción es una consulta asignarle un id
        if (accion == 2 && idConsulta == -1)
            this.idconsulta = ServicioNovedades.siguienteIdConsulta();
        else
        	this.idconsulta = idConsulta;
    }

    public byte[] datosTX() {
        StringBuilder comandoWAMP = new StringBuilder("[");

        // Construir comando WAMP
        comandoWAMP.append(accion);
        if (accion == 2) {
            // Adicionar el id de consulta
            comandoWAMP.append(",\"").append(idconsulta).append("\"");
        }

        // Adicionar uri y datos (si los hay)
        comandoWAMP.append(",\"").append(uri).append("\"");
        if (datos != null) comandoWAMP.append(",").append(datos);

        // Si la acción es una publicación verificar lista de excluidos y elegidos
        if (accion == 7) {
            if (excluidos != null) {
                // Hay lista de excluidos
                comandoWAMP.append(",").append(excluidos);
            } else if (elegidos != null) {
                // Hay lista de elegidos
                comandoWAMP.append(",[],").append(elegidos);
            }
        }
        comandoWAMP.append("]");

        // Retornar el arreglo de bytes correspondiente
        try {
            return(comandoWAMP.toString().getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            Log.e(ServicioNovedades.class.getName(), Log.getStackTraceString(e));
            return(null);
        }
    }

    public int obtenerAccion() {
    	return accion;
	}
    
    public String obtenerUri() {
    	return uri;
	}
    
    public String obtenerDatos() {
    	return datos;
	}
    
    public int obtenerIdConsulta() {
    	return idconsulta;
	}
    
    public String obtenerNombreIntento() {
    	return nombreIntento;
	}
    
    public int obtenerTimeoutEnvio() {
    	return timeoutEnvio;
	}
    
    public int obtenerTimeoutRecepcion() {
    	return timeoutRecepcion;
	}
}
