package com.gc.ordenesservicio.novedades;

import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Base64;
import android.util.Log;
import android.util.SparseArray;

import com.gc.coregestionclientes.accesodatos.CustomSQLiteRoutines;
import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.novedades.BaseDatosTemporal;
import com.gc.coregestionclientes.novedades.Cola;
import com.gc.coregestionclientes.novedades.DynamicByteBuffer;
import com.gc.coregestionclientes.novedades.librerias.GC_JSON;
import com.gc.coregestionclientes.novedades.librerias.HashMapGC;
import com.gc.coregestionclientes.novedades.librerias.VectorGC;
import com.gc.ordenesservicio.AplicacionOrdenesServicio;
import com.gc.ordenesservicio.accesodatos.ComandosAplicacion;
import com.gc.ordenesservicio.accesodatos.SQLiteRoutinesOS;
import com.gc.ordenesservicio.objetos.ActividadAdministrativa;
import com.gc.ordenesservicio.objetos.ControladorActividadAdministrativa;
import com.gc.ordenesservicio.objetos.ControladorOrdenServicio;
import com.gc.ordenesservicio.objetos.Notificaciones;
import com.gc.ordenesservicio.objetos.OrdenServicio;

public class ServicioNovedades extends Service {
	public static volatile PowerManager.WakeLock wakeLock = null;

	private AplicacionOrdenesServicio aplicacion;
	private BaseDatosTemporal baseDatosTemporal = null;
    
    // Modulos
    private ModuloSeguimiento moduloSeguimiento = null; 
    
    // Atributos relacionados con los comandos WAMP
    private Cola colaComandosNovedades = null;
    private ComandoWAMP comandoWAMP = null;
    boolean comandoEnCurso = false;
    boolean interpretandoComando = false;
    private AtenderComandos threadComandos = null;
    private long tiempoEnvio;
    private long tiempoRecepcion;
    private int timeoutRecepcionSuscripcion = 50; // Seg
    public static int consecutivoIdConsulta = 0;
    public static int consecutivoIdFoto = 0;

    // Auxiliares
    int manejoRespuestaApp = 0;
    private boolean primeraVezServicio = true;
    private ToolsApp toolsApp = new ToolsApp();
    
    // Timer y Handler utilizado cuando el Thread que atiende los comandos finaliza.
    private Handler thComandos = new Handler();
    private Runnable trComandos = new Runnable() {
        @Override
        public void run() {
        	finalizaAtencionComandos();
        }
    };

    // Función que maneja la finalización del thread de comandosWamp
    private void finalizaAtencionComandos() {
        // Avisa de la finalización anormal del thread

    	// Revisar si estaba interpretando un comando
    	if (interpretandoComando) {
            Log.i(AplicacionOrdenesServicio.tag_app, "TODO: Interpretando comando de: " + comandoWAMP.obtenerNombreIntento());

        	// TODO: Se pierden datos recibidos.
            // Limpia bandera
        	interpretandoComando = false;
    	}
    	
		// Revisar si había un comando en curso
    	if (comandoEnCurso && comandoWAMP != null) { 
    		// Existe un comando en curso. Finaliza el comando
            finalizaComandoEnCurso();
    	} else {
    		// No existe un comando en curso.
    		// Verifica si el estado de posicionamiento se encuentra en FIN_POSICIONAMIENTO
        	if (moduloSeguimiento.obtenerEstadoActual() != -1 
        			&& moduloSeguimiento.obtenerEstadoActual() != ModuloSeguimiento.FIN_POSICIONAMIENTO) {
        		
        		// TODO: Le informa al modulo de posicionamiento que 
        		// debe guardar la trama cuando termine.
        		/*if(!moduloSeguimiento.getGuardarTrama()){
        			moduloSeguimiento.setGuardarTrama(true);
                    Log.i(ServicioNovedades.class.getName(), "Espera por posicionamiento");
        		}*/
                // Vuelve a revisar en 100 ms
                thComandos.postDelayed(trComandos, 100);
                return;
        	}
    	}
    	
    	// TODO: Recorre los comandos pendientes
    	/*while ((comandoWAMP = (ComandoWAMP) colaComandosWAMP.fetch()) != null) {   
    		// Maneja la respuesta de un comando abortado       
            if (comandoWAMP.obtenerNombreIntento() != null) {
            	manejarRespuestaComando(comandoWAMP.obtenerNombreIntento(), null, true);
            }
		}*/
    	
        // Terminar el servicio..
        Log.i(AplicacionOrdenesServicio.tag_app, "Termina el servicio");
        stopSelf();
	}
    
    // Función que maneja la finalización del comando en curso
    private void finalizaComandoEnCurso() {	    
		// Maneja la respuesta de un comando abortado    
        Log.i(AplicacionOrdenesServicio.tag_app, "Finalizando comando en curso!");
        if (comandoWAMP.obtenerNombreIntento() != null) {
        	manejarRespuestaComando(comandoWAMP.obtenerNombreIntento(), null, true);
        }
		
		// Limpia bandera
		comandoEnCurso = false;
		comandoWAMP = null;
	}
    
    // Funcion que maneja la respuesta de un comando
    private void manejarRespuestaComando(String nombreIntento, VectorGC<?> comandoJSON, boolean comandoAbortado) {
    	if (comandoAbortado) {
    		// Comando o respuesta de comando Abortada
            if (nombreIntento.equals("novedades")) {
                // TODO: Guardar comando en curso en base de datos
            } else if (nombreIntento.equals("rastreo")) {              
        		// Guardar trama obtenida en base de datos
            	// TODO: baseDatosTemporal.guardarTrama(comandoWAMP.obtenerIdConsulta(), comandoWAMP.obtenerDatos()); 
            	
            } else if (nombreIntento.startsWith("aplicacion")) {
                Log.i(AplicacionOrdenesServicio.tag_app, "Comando de aplicacion no procesado!");
                
            } else {
                // Enviar respuesta al interesado
                Intent intento = new Intent(nombreIntento);
                intento.putExtra("nombreIntento", nombreIntento);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intento);
            }
    	} else {
    		// Manejo normal de la respuesta de un comando.
            // A qué tipo de comando corresponde la respuesta ?
            if (nombreIntento.equals("novedades")) {
                if (comandoJSON.get(2) instanceof String &&
                        comandoJSON.getString(2).equals("OK")) {
                    // Confirmación confirmada...
                    Log.i(AplicacionOrdenesServicio.tag_app, "Confirmada la confirmación...");
                } else {
                    // Consignar novedades
                	threadComandos.actualizarNovedades(comandoJSON.getRawJSON(2));
                }
            } else if (nombreIntento.equals("rastreo")) {
            	Log.i(AplicacionOrdenesServicio.tag_app, "Respuesta Rastreo: " + comandoJSON.toString());
    			try {
					HashMapGC<?, ?> datosRespuesta = (HashMapGC<?,?>) GC_JSON.parseJSON(comandoJSON.getRawJSON(2), false, false);
					Log.i(AplicacionOrdenesServicio.tag_app, datosRespuesta.toString());
				} catch (Exception e) {
					e.printStackTrace();
				}
                
        		// Eliminar trama de base de datos si existe
                baseDatosTemporal.eliminarTramas(comandoWAMP.obtenerIdConsulta());
            } else if (nombreIntento.equals("fotoAdjunto")) {
            	Log.i(AplicacionOrdenesServicio.tag_app, "Respuesta FotoAdjunto: " + comandoJSON.toString());
        		// Decodifica los datos para extraer datos del registro de la imagen que será eliminada
            	if (comandoJSON.getRawJSON(2) != null) {
	        		try {
	        			HashMapGC<?, ?> datosRespuesta = (HashMapGC<?,?>) GC_JSON.parseJSON(comandoJSON.getRawJSON(2), false, false);
	        			if(datosRespuesta != null && datosRespuesta.size() > 0) {
	        				// Imprime log
	        				Log.i(AplicacionOrdenesServicio.tag_app, "Foto procesada: " + datosRespuesta.toString());
	        				if (datosRespuesta.getString("resultado").equals("OK")) {
	        					// Borra el registro de fotos adjuntos
	        	                baseDatosTemporal.eliminarFotoAdjunto(comandoWAMP.obtenerIdConsulta());
	        				}
	        			}
	        			datosRespuesta.getString("resultado");
	        		} catch (Exception e1) {
	        			e1.printStackTrace();
	        		}
        		}
            }
            else if (nombreIntento.equals("firma")) {
                Log.i(AplicacionOrdenesServicio.tag_app, "Respuesta firma: " + comandoJSON.toString());
                // Decodifica los datos para extraer datos del registro de la firma que será eliminada
                if (comandoJSON.getRawJSON(2) != null) {
                    try {
                        HashMapGC<?, ?> datosRespuesta = (HashMapGC<?,?>) GC_JSON.parseJSON(comandoJSON.getRawJSON(2), false, false);
                        if(datosRespuesta != null && datosRespuesta.size() > 0) {
                            // Imprime log
                            Log.i(AplicacionOrdenesServicio.tag_app, "Firma procesada: " + datosRespuesta.toString());
                            if (datosRespuesta.getString("resultado").equals("OK")) {
                                // Borra el registro de fotos adjuntos
                                baseDatosTemporal.eliminarFirma(comandoWAMP.obtenerIdConsulta());
                            }
                        }
                        datosRespuesta.getString("resultado");
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }
            else if (nombreIntento.startsWith("aplicacion")) {
            	// Maneja la respuesta del comando de aplicacion
                boolean sincronizado = ComandosAplicacion.manejadorCallbacks(
                		aplicacion, nombreIntento, 
                		aplicacion.getConfiguracionApp(), 
                		comandoJSON.getRawJSON(2));
                
                // Enviar respuesta al interesado
                Intent intento = new Intent(nombreIntento);
                intento.putExtra("nombreIntento", nombreIntento);
                intento.putExtra("sincronizado", sincronizado);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intento);
                
            } else {
                // Enviar respuesta al interesado
                Intent intento = new Intent(nombreIntento);
                intento.putExtra("nombreIntento", nombreIntento);
                intento.putExtra("respuesta", comandoJSON.getRawJSON(2));
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intento);
            }
    	}
	}
    
    public static int siguienteIdConsulta() {
        return(++consecutivoIdConsulta);
    }
    
    public static int siguienteIdFoto() {
        return(++consecutivoIdFoto);
    }

    private class Parser extends Thread {
        VectorGC<?> comandoJSON = null;
        DynamicByteBuffer datosRX;
        boolean datosComprimidos;
        boolean primerNivel;

        public Parser(DynamicByteBuffer datosRX, boolean datosComprimidos, boolean primerNivel) {
            this.datosRX = datosRX;
            this.datosComprimidos = datosComprimidos;
            this.primerNivel = primerNivel;
        }

        public VectorGC<?> getJSON() {
            return comandoJSON;
        }

        public void run() {
            try {
                comandoJSON = (VectorGC<?>)GC_JSON.parseJSON(datosRX, datosComprimidos, primerNivel);
            } catch (EOFException eof) {
            	// Posible error generado por timeout o perdida de bytes en la transmisión
                Log.e(AplicacionOrdenesServicio.tag_app, "Error 'Parser' por timeout...");
            } catch (Exception e) {
            	// Posible error generado por datos corruptos o compresion erronea
                Log.e(AplicacionOrdenesServicio.tag_app, Log.getStackTraceString(e));
            }
        }
    }

    // Thread que atiende comandos WAMP
    private class AtenderComandos extends Thread {
        private SocketChannel cliente = null;
        private DynamicByteBuffer datosRX = new DynamicByteBuffer(5);

        private int cuenta = 0;
        private boolean finalizar;
        private SparseArray<String> comandosPendientes;
        private HashMap<String, String> servicios;

        private static final int PUERTO_SERVIDOR = 4773;
        private static final String IP_SERVIDOR = "23.253.57.17";
        //private static final String conexionwss = "wss://gc.globalcircuit.co:443/websocket";

        //private static final String IP_SERVIDOR = "50.57.132.133";
        //private static final String IP_SERVIDOR = "50.57.132.133";
       //private static final String IP_SERVIDOR = "192.168.0.21";

        public AtenderComandos() {
            // Inicialización
            finalizar = true;
            comandosPendientes = new SparseArray<String>();
            servicios = new HashMap<String, String>();
        }

        public void terminarThread() {
            finalizar = true;
        }

        private boolean enviarComando(byte[] datosTX) {
            try {
                // Separar espacio para los datos
                int longitud = datosTX.length;
                ByteBuffer bufferEscritura = ByteBuffer.allocateDirect(6 + longitud);

                // Adicionar primer byte (aún no definido)
                bufferEscritura.put((byte)0x80);

                // Verificar el tamaño del mensaje y según eso preparar el encabezado
                if (longitud <= 125) {
                    bufferEscritura.put((byte)longitud);
                } else if ((longitud > 125) && (longitud < 65536)) {
                    bufferEscritura.put((byte)126);
                    bufferEscritura.put((byte)((longitud >> 8) & 0xff));
                    bufferEscritura.put((byte)(longitud & 0xff));
                } else if (longitud >= 65536) {
                    // La longitud se llena en 4 bytes (longitud máxima de ~ 4 GBytes)
                    byte[] longitudBytes = new byte[5];
                    Arrays.fill(longitudBytes, (byte)0);
                    longitudBytes[0] = 127;
                    for (int i = 1; i < 5; i++) {
                        longitudBytes[i] = (byte)((longitud >> (48 - 8*(i + 2))) & 0xff);
                    }
                    bufferEscritura.put(longitudBytes);
                }

                // Adicionar datos
                bufferEscritura.put(datosTX);

                // Enviar
                bufferEscritura.flip();
                while (bufferEscritura.hasRemaining()) {
                    cliente.write(bufferEscritura);
                }
                return(true);
            } catch (IOException e) {
                Log.e(AplicacionOrdenesServicio.tag_app, Log.getStackTraceString(e));
                return(false);
            }
        }

        // Actualiza novedades (dump o cambios)
        private void actualizarNovedades(byte[] rawJSON) {
            HashMapGC<?,?> objeto;
            String accion, baseDatos, datos, idUsuario, idDispositivo;
            VectorGC<?> imagenes;
            StringBuilder lista = new StringBuilder();

            // Parsear este stream
            try {
            	//(new String(rawJSON)).toString()
                VectorGC<?> comandoJSON = (VectorGC<?>)GC_JSON.parseJSON(rawJSON, false, false);
                for (int i = 0, n = comandoJSON.size(); i < n; i++) {
                    objeto = comandoJSON.getObject(i);
                    
                    // Valida el tipo de objeto de acuerdo a los paramtros que tiene
                    accion = objeto.getString("accion");
                    baseDatos = objeto.getString("baseDatos");
                    imagenes = objeto.getArray("imagenes");
                    idUsuario = objeto.getString("idUsuario");
                    idDispositivo = objeto.getString("idDispositivo");


                    HashMapGC datosUsuario = objeto.getObject("datosUsuario");

                    //Falta por comentar bien pues hay que cambiar los nombres
                    VectorGC<?> datosOrdenNotificacion = objeto.getArray("datosOrdenServicio");

                    if( datosOrdenNotificacion != null && datosUsuario != null ){
                        // Recorre los datos y muestra las notificaciones.
                        int numeroOrdenesServicio = datosOrdenNotificacion.size();
                        int idCliente = Integer.parseInt( datosUsuario.getString("idClienteGlobal") );
                        String codigoTecnico = aplicacion.getConfiguracionApp().getUsuario().getCodigo();
                        for(int indice = 0; indice < numeroOrdenesServicio; indice++) {
                            int idOrdenServicio2 = Integer.parseInt(datosOrdenNotificacion.getObject(indice).getString("idOrden") );
                            String nombreCliente2 = "AGENCIA ALEMANA";
                            String serialMontacarga2 = "";
                            String codigoOT = datosOrdenNotificacion.getObject(indice).getString("codigoOT");
                            String codigoVisita = datosOrdenNotificacion.getObject(indice).getString("codigoVisita");
                            String fecha = datosOrdenNotificacion.getObject(indice).getString("fecha");
                            Notificaciones.generarNotificacion(aplicacion, indice,
                                    "¡TIENE UNA NUEVA VISITA!"
                                    ,"# "+ idOrdenServicio2 + " Codigo: " + codigoOT + " CodigoVisita: " + codigoVisita, nombreCliente2, fecha);

                            String datosConfirmacion = "{" +
                                    "\"codigoOT\":\"" + codigoOT + "\"" +
                                    ", \"codigoVisita\":\"" + codigoVisita + "\"" +
                                    ", \"codigoTecnico\":\"" + codigoTecnico + "\"" + "}";
                            colaComandosNovedades.deposit(new ComandoWAMP(
                                    2, "coregc.notificaciones.version2", datosConfirmacion, null, null,
                                    "notificaciones", -1, -1, -1));
                        }


                    }else if(accion != null && baseDatos != null && imagenes != null) {
                    	// Es un objeto con novedades
	                    if (accion.equals("nuevo")) {
	                        // Los datos (base de datos) están codificados base64
	                    	if (baseDatos.equals("bddDA")) {
	                    		((CustomSQLiteRoutines) aplicacion.getConfiguracionApp().getCustomSqliteRoutines()).actualizarArchivo(objeto.getString("datos"));
	                    	} else if (baseDatos.equals("bddGC")){
	                    		//TODO: configApp.getSqliteRoutines().actualizarArchivo(objeto.getString("datos"));
	                    	}
	                    } else {
	                        // Actualizar...
	                    	if (baseDatos.equals("bddDA")) {
	                    		aplicacion.getConfiguracionApp().getCustomSqliteRoutines().actualizarBaseDatos(objeto.getString("datos"));
	                    	} else if (baseDatos.equals("bddGC")){
	                    		//TODO: configApp.getSqliteRoutines().actualizarBaseDatos(objeto.getString("datos"));
	                    	}
	                    }
	
	                    // Crear lista de imágenes
	                    lista.setLength(0);
	                    for (int j = 0, m = imagenes.size(); j < m; j++) {
	                        lista.append(",").append(imagenes.getArray(j).toString());
	                    }
	
	                    // Crear objeto JSON con datos de confirmación
	                    datos = "{\"idDispositivo\":\"" + aplicacion.getConfiguracionApp().getDispositivo().getId() + "\",\"datosConfirmacion\":[\"" +
	                            accion + "\",\"" + baseDatos + "\"" + lista.toString() + "]}";
	                    Log.i(AplicacionOrdenesServicio.tag_app, datos);
	
	                    // Enviar comando de confirmación
	                    colaComandosNovedades.deposit(new ComandoWAMP(
	                            2, "coregc.novedades.confirmar", datos, null, null, "novedades", -1, -1, -1));
                    } else if(idUsuario != null && idDispositivo != null){
                    	// Es un objeto con datos para notificacion

                    	VectorGC<?> datosOrden = objeto.getArray("datosOrsden");
                    	if (datosOrden != null) {
                    		// Recorre los datos y muestra las notificaciones.
                    		int numeroOrdenes = datosOrden.size();
                    		for(int indice = 0; indice < numeroOrdenes; indice++){
                    			HashMapGC<?, ?> datosOrdenServicio = datosOrden.getObject(indice);
                    			int idOrdenServicio = Integer.parseInt(datosOrdenServicio.getString("idOrdenServicio"));
                    			String nombreCliente = datosOrdenServicio.getString("nombreCliente");
                    			String fechaCreacion = datosOrdenServicio.getString("FechaCreacion");
                    			String serialMontacarga = datosOrdenServicio.getString("serialMontacarga");
                    			Notificaciones.generarNotificacion(aplicacion, idOrdenServicio,
                    					"O.S.#" + idOrdenServicio, nombreCliente, fechaCreacion, serialMontacarga);
                    			
                    			String datosConfirmacion = "{"
                    					+ "\"idUsuario\":" + aplicacion.getConfiguracionApp().getUsuario().getId()  + 
                    					", \"idOrdenServicio\":" + idOrdenServicio + "}";
        	                    colaComandosNovedades.deposit(new ComandoWAMP(
        	                            2, "coregc.notificaciones.version1", datosConfirmacion, null, null,
        	                            "notificaciones", -1, -1, -1));
                    		}
                    	}
                    }
                }
            } catch (Exception e) {
                Log.e(AplicacionOrdenesServicio.tag_app, Log.getStackTraceString(e));
            }
        }

        public void run() {
            String nombreIntento;
            boolean estaConectado;

            try {
                // Intentar conexión con el servidor
                cliente = SocketChannel.open();
                cliente.configureBlocking(true);
                InetSocketAddress remoto = new InetSocketAddress(
                        InetAddress.getByName(IP_SERVIDOR), PUERTO_SERVIDOR);
                cliente.connect(remoto);



                // Conexión exitosa, poner el canal en modo de no bloqueo
                cliente.configureBlocking(false);
                finalizar = false;
            } catch (UnknownHostException e) {
                Log.e(AplicacionOrdenesServicio.tag_app, 
                		"Error UnknownHost: " + e.toString() + " StackTrace: " + Log.getStackTraceString(e));
            } catch (IOException e) {
                Log.e(AplicacionOrdenesServicio.tag_app, 
                		"Error IO: " + e.toString() + " StackTrace: " + Log.getStackTraceString(e));
            } catch (Exception e) {

                //Calendario para la fecha de los hitos
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                String fecha = sdf.format(cal.getTime());
                String toLog = "Tuve un error, el cual fue: " +  e.toString() + " y tiene esta ruta: "+
                        Log.getStackTraceString(e) + " a las: " + fecha;
                toolsApp.generaLog(toLog);
                Log.e(AplicacionOrdenesServicio.tag_app,
                        "Error General Exception: " + e.toString() + " StackTrace: " + Log.getStackTraceString(e));
            }

            // Inicia la fiesta de los bytes!!!
            Parser parser = null;
            VectorGC<?> comandoJSON;
            comandoEnCurso = false;
            interpretandoComando = false;
            while (!finalizar && cliente.isConnected()) {
                // Intentar leer datos
                estaConectado = datosRX.getBytes(cliente);
                if (!estaConectado) {
                    // Error, el socket se desconectó
                    finalizar = true;
                    continue;
                }

                // Se está interpretando comando ?
                if (interpretandoComando) {
                	// Está interpretando datos.
                    // Terminó el thread ?
                    if (parser != null && !parser.isAlive()) {
                        // El thread 'Parser' terminó,
                    	// Ejecuta o despacha comando
                        comandoEnCurso = false;
                        interpretandoComando = false;
                        comandoJSON = parser.getJSON();
                        parser = null;
                        if (comandoJSON == null) {
                            Log.e(AplicacionOrdenesServicio.tag_app, "Comando inválido...");
                            
                            // Limpiar buffer dinámico
                            datosRX.inicializarBuffer();
                            continue;
                        }

                        // Verificar el tipo de comando
                        Log.i(AplicacionOrdenesServicio.tag_app, "Comando interpreado correctamente");
                        switch ((int) comandoJSON.getLong(0, -1)) {
                            case 3:
                                // Respuesta de un procedimiento (RPC)
                                String idConsulta = comandoJSON.getString(1);
                                if (idConsulta == null) continue;
                                nombreIntento = comandosPendientes.get(Integer.parseInt(idConsulta));
                                if (nombreIntento != null) {
                                    // Quitar comando de la lista de pendientes
                                    comandosPendientes.remove(Integer.parseInt(idConsulta));

                                    // Maneja la respuesta del comando     
                                    if (comandoWAMP != null) {
                                    	manejarRespuestaComando(comandoWAMP.obtenerNombreIntento(), comandoJSON, false);
                                    }
                                }
                                break;
                            case 4:
                                // Error de un procedimiento (RPC)
                                break;
                            case 8:
                                // Evento (publish and subscribe)
                                String uri = comandoJSON.getString(1);
                                nombreIntento = servicios.get(uri);
                                if (nombreIntento != null) {
                                    // Es un comando de novedades?
                                    if (nombreIntento.equals("novedades")) {
                                        if (comandoJSON.get(2) instanceof String) {
                                            if (comandoJSON.getString(2).equals("nuevos datos...")) {
                                                // Enviar comando de actualización
                                                colaComandosNovedades.deposit(new ComandoWAMP(2, "coregc.novedades.actualizar",
                                                        "{\"idDispositivo\":\"" + aplicacion.getConfiguracionApp().getDispositivo().getId() + "\"}", 
                                                        null, null, "novedades", -1, -1, -1));
                                            } else if (comandoJSON.getString(2).equals("OK")) {
                                                // Suscripción confirmada
                                                Log.i(AplicacionOrdenesServicio.tag_app, "Suscripcion OK");
                                            }
                                        } else {
                                            // Consignar novedades
                                            actualizarNovedades(comandoJSON.getRawJSON(2));
                                        }
                                    } else {
                                        // Enviar evento al interesado (GUI)
                                        Intent intento = new Intent(nombreIntento);
                                        intento.putExtra("evento", comandoJSON.getRawJSON(2));
                                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intento);
                                    }
                                }
                                break;
                            default:
                        }
                    } else {
                    	// El thread 'Parser' no ha terminado.
                        // Valida el tiempo máximo sin recibir bytes (T3)
                    	if (comandoEnCurso && comandoWAMP != null 
                    			&& (System.currentTimeMillis() - tiempoRecepcion) > comandoWAMP.obtenerTimeoutRecepcion() * 1000) {
                    		// Existe comando en curso, aborta la lectura de datos y finaliza el comando
                    		datosRX.abortarLectura();
                			finalizaComandoEnCurso();
                			
                    	} else if (!comandoEnCurso 
                    			&& (System.currentTimeMillis() - tiempoRecepcion) > timeoutRecepcionSuscripcion * 1000){
                    		// No existe comando en curso, aborta sólo la lectura.
                    		datosRX.abortarLectura();
                    	}
                    }
                } else {
                	// No está interpretando datos.
                    // Han llegado datos?
                    try {
                        if (datosRX.hayDatos()) {
                        	// Timestamp (T2) del inicio del proceso de recepción de datos
                        	tiempoRecepcion = System.currentTimeMillis();

                            // Iniciar thread para interpretar comando (parse JSON y/o unzip)
                            comandoEnCurso = true;
                            interpretandoComando = true;
                            parser = new Parser(datosRX, datosRX.estaComprimido(), true);
                            parser.start();
                        }
                    } catch (Exception e) {
                        Log.e(AplicacionOrdenesServicio.tag_app, e.toString());
                    }

                    // No hay comando en curso?
                    if (!comandoEnCurso) {
                    	// No hay comando en curso.
                        // TODO: Revisar si hay un comando en cola
                        // comandoWAMP = (ComandoWAMP) colaComandosWAMP.fetch();
                    	comandoWAMP = obtenerComandoPendiente();
                        if (comandoWAMP != null) {
                        	// Hay un comando en cola.
                        	// Timestamp (T0) del inicio del proceso de envío del comando
                        	tiempoEnvio = System.currentTimeMillis();
                            cuenta = 0; 

                            // Ejecutar comando
                            comandoEnCurso = true;
                            Log.i(AplicacionOrdenesServicio.tag_app, "Inicio envío de comando");
                            switch (comandoWAMP.obtenerAccion()) {
                                case 0:
                                    // Cliente inicia sesión
                                case 2:
                                    // Llamado a un procedimiento (RPC)
                                    comandosPendientes.put(comandoWAMP.obtenerIdConsulta(), comandoWAMP.obtenerNombreIntento());
                                    enviarComando(comandoWAMP.datosTX());
                                    break;
                                case 5:
                                    // Suscripción a un servicio
                                    servicios.put(comandoWAMP.obtenerUri(), comandoWAMP.obtenerNombreIntento());
                                    enviarComando(comandoWAMP.datosTX());
                                    break;
                                case 6:
                                    // Terminar suscripción
                                    servicios.remove(comandoWAMP.obtenerUri());
                                    enviarComando(comandoWAMP.datosTX());
                                    break;
                                case 7:
                                    // Publicar datos para suscritos a un servicio
                                    enviarComando(comandoWAMP.datosTX());
                                    break;
                            }
                        } else {
                        	// No hay un comando en cola.
                        	// Verifica si el posicionamiento (Si existe) ha finalizado
                        	if (++cuenta >= 100 
                        			&& moduloSeguimiento.obtenerEstadoActual() == ModuloSeguimiento.FIN_POSICIONAMIENTO) {
                                // Terminar el thread y por ende el servicio
                                if (cliente.isConnected()) {
                                    try {
                                        cliente.close();
                                    } catch (IOException e) {
                                    }
                                    break;
                                }

                                // TODO: enviar comando WAMP de desconexión
                            }
                        }
                    } else {
                    	// Hay un comando en curso.
                        // Valida el tiempo máximo esperando el primer byte... (T1) - timeout
                    	if (comandoWAMP != null &&
                    			(System.currentTimeMillis() - tiempoEnvio) > comandoWAMP.obtenerTimeoutEnvio() * 1000) {                   		
                    		// Descartar comando en curso
                    		finalizaComandoEnCurso();
                    	}
                    }
                }

                // Esperar un momento e intentar nuevamente
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    Log.e(AplicacionOrdenesServicio.tag_app, Log.getStackTraceString(e));
                }
            }
            
            // Handler para manejar la finalización de éste thread (1 seg)
            Log.i(ServicioNovedades.class.getName(), "Finalizando thread");
            thComandos.postDelayed(trComandos, 1000);
        }
    }
    
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();       
		// Obtiene el objeto con la configuracion de la app (contexto)
		aplicacion = ((AplicacionOrdenesServicio)this.getApplicationContext());
		Log.i(AplicacionOrdenesServicio.tag_app, "onCreate Service, Contexto app: " + 
				(aplicacion != null ? aplicacion.toString() : "Null"));
        
		// Instancia el objeto de base de datos temporal
        baseDatosTemporal = BaseDatosTemporal.getInstance(this);
        
        // Crear cola de comandos WAMP
        colaComandosNovedades = new Cola();
        
        // Verifica datos básicos
        if (verificaDatos()){
	        // Carga el modulo de posicionamiento
	        moduloSeguimiento = new ModuloSeguimiento(aplicacion, aplicacion.getConfiguracionApp());
	
	        // Ejecuta la lista de chequeo para revisar datos pendientes por ser enviados
	        //ejecutarListaChequeo();
        
        	if(verificaUsuario()){
	        	// Datos válidos, iniciar thread que atiende la cola de comandos
	        	threadComandos = new AtenderComandos();
	        	threadComandos.start();
        	}
        }
        
        // TODO: Suscribirse al servicio de novedades
        colaComandosNovedades.deposit(new ComandoWAMP(5, "coregc.novedades",
                "{\"idDispositivo\":\"" + aplicacion.getConfiguracionApp().getDispositivo().getId() + "\"}", 
                null, null, "novedades",  -1, -1, -1));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
		// Obtiene el objeto con la configuracion de la app (contexto)
		aplicacion = ((AplicacionOrdenesServicio)this.getApplicationContext());
		Log.i(AplicacionOrdenesServicio.tag_app, "onStartCommand Service, Contexto app: " + 
				(aplicacion != null ? aplicacion.toString() : "Null"));
    	
    	String tipoIntent = (intent == null ? "GPS" : intent.getStringExtra("tipoIntent"));
		Log.i(AplicacionOrdenesServicio.tag_app,"Comando tipo: " + tipoIntent);
		if (tipoIntent.equals("GPS") && moduloSeguimiento != null) {
			// Extrae el valor que indica desde donde se solicitó posición
			moduloSeguimiento.setQuienSolicita(
					moduloSeguimiento.getQuienSolicita() | 
					(intent == null ? ModuloSeguimiento.RASTREO : 
						intent.getIntExtra("quienSolicita", ModuloSeguimiento.RASTREO)));
			
			// Inicial el proceso de posicionamiento
			moduloSeguimiento.iniciarProceso();
    	} else if (tipoIntent.equals("FOTO")) {
    		// Intent proveniente de la app
    		// Extrae datos y guarda el registro
    		if(intent != null){
    			String datosFotos = intent.getStringExtra("datosFotos");
    			int idOrdenServicio = intent.getIntExtra("idOrdenServicio", -1);
    			int idVisita = intent.getIntExtra("idVisita", -1);
    			if(idOrdenServicio > 0 && idVisita > 0){
    				// Recorrer las imagenes del JSON y generar un registro en BD
    				JSONArray datosImagenJson = new JSONArray();
    				try {
    					datosImagenJson = new JSONArray(datosFotos);
    				} catch (JSONException e) {
    					e.printStackTrace();
    				}
    				if (datosImagenJson.length() > 0){
    					for (int i = 0; i < datosImagenJson.length(); i++) {
    						String datosFoto = "";
    						try {
								JSONArray datosFotoJson = datosImagenJson.getJSONArray(i);
								datosFoto = datosFotoJson.toString();
							} catch (JSONException e) {
								e.printStackTrace();
							}
        					baseDatosTemporal.guardarDatosImagen(siguienteIdFoto(), idOrdenServicio, idVisita, datosFoto);
						}
    				}
        		}
    		}
    	}else if (tipoIntent.equals("FIRMA")) {
            // Intent proveniente de la app
            // Extrae datos y guarda el registro
            if (intent != null) {
                String nombre = intent.getStringExtra("nombre");
                String ruta = intent.getStringExtra("ruta");
                baseDatosTemporal.guardarDatosFirma(siguienteIdFoto(), nombre, ruta);
            }

        } else if (tipoIntent.equals("WAMP")) {
    		// Intent proveniente de la app
    		// Realiza el proceso normal de chequeo de comandos pendientes...
    	}
		
		// La primera vez no corre comando de consulta, solo se corre si previamente ya se habia creado el servicio
		if (primeraVezServicio) {
			// La primera vez no hace nada, pues ya realizó suscripción en el oncreate.
			primeraVezServicio = false;
			Log.i(AplicacionOrdenesServicio.tag_app,"Primera vez");
		} else {
			// Mientras el servicio exista, y sea llamado, corre comando de consulta si la cola esta vacía.
			Log.i(AplicacionOrdenesServicio.tag_app,"Servicio ya existe");
			if (colaComandosNovedades.numeroElementos() == 0) {
	            colaComandosNovedades.deposit(new ComandoWAMP(2, "coregc.novedades.actualizar",
	                    "{\"idDispositivo\":\"" + aplicacion.getConfiguracionApp().getDispositivo().getId() + "\"}", 
	                    null, null, "novedades", -1, -1, -1));
			}
		}

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            // Deshabilitar el listener del estado del teléfono
        	moduloSeguimiento.deshabilitarListeners();
            
            // Cerrar thread de atención de comandos
            if ((threadComandos != null) && (threadComandos.isAlive())) {
                threadComandos.terminarThread();
                threadComandos = null;
            }
        } catch(Exception e) {
            // Error...
            Log.e(AplicacionOrdenesServicio.tag_app, "Servicio TxRxTCP, Exception: " + e.toString());
        } finally {
            // La CPU ya se puede "liberar"
            if(wakeLock != null) {
                if(wakeLock.isHeld()) {
                    wakeLock.release();
                    Log.i(AplicacionOrdenesServicio.tag_app, "wakelock liberado...");
                }
                wakeLock = null;
            }
        }
    }

    // Método que revisa que si existe la base de datos principal
    private boolean verificaDatos() {
		// Verifica si hay datos locales para trabajar
		boolean exste_bd = aplicacion.getConfiguracionApp().getSqliteRoutines().verificarArchivoDB();
		//boolean exste_bd_adcnal = configApp.getCustomSqliteManager().verificarArchivoDB();
		if(exste_bd){
			// Si hay datos locales
			// Inicializa parametros de Usuario
			if (aplicacion.getConfiguracionApp().getUsuario() == null) {
				aplicacion.getConfiguracionApp().asignarUsuarioLocal();
			}
			
			// Datos correctos
			Log.i(AplicacionOrdenesServicio.tag_app, "Service - Datos válidos!");
			return true;
		} else {
			// No hay datos locales
			Log.i(AplicacionOrdenesServicio.tag_app, "Service - No hay datos locales!");
		}
		
		// Retorno
		return false;
	}
    
    // Método que revisa que si existe el usuario
    private boolean verificaUsuario() {
		// Verifica el usuario
		if(aplicacion.getConfiguracionApp().getUsuario() != null){
			// Datos correctos
			Log.i(AplicacionOrdenesServicio.tag_app, "Service - Usuario válido!");
			return true;
		} else {
			// Usuario Nulo
			Log.i(AplicacionOrdenesServicio.tag_app, "Service - Usuario Nulo!");
			return false;
		}
	}
    
    // Premite obtener un comando wamp pendiente teniendo en cuenta la prioridad
    private ComandoWAMP obtenerComandoPendiente() { 
    	//Log.i(AplicacionOrdenesServicio.tag_app, "Obteniedo COMANDO PENDIENTE");
    	ComandoWAMP comandoWAMP = null;
    	
    	// (1) Revisa tramas de seguimiento pendientes por enviar.
    	comandoWAMP = obtenerComandoGPS();
    	if (comandoWAMP != null) {
        	Log.i(AplicacionOrdenesServicio.tag_app, comandoWAMP.obtenerDatos());
    		return comandoWAMP;
    	}

    	// Verifica si la base NO está ocupada
    	/*Log.i(AplicacionOrdenesServicio.tag_app, 
    			"BD Ocupada: " + aplicacion.getConfiguracionApp().getSqliteRoutines().estaBaseOcupada() + " " +
    			"BDA Ocupada: " + aplicacion.getConfiguracionApp().getCustomSqliteRoutines().estaBaseOcupada());
    	*/
    	if (!aplicacion.getConfiguracionApp().getSqliteRoutines().estaBaseOcupada() 
    			&& !aplicacion.getConfiguracionApp().getCustomSqliteRoutines().estaBaseOcupada()) {	
			// (2) Verifica ordenes pendientes
	    	comandoWAMP = obtenerComandoOrdenServicio();
	    	if (comandoWAMP != null) {
	    		return comandoWAMP;
	    	}
	    		
	    	// (3) Actividades Administrativas
	    	comandoWAMP = obtenerComandoActividadAdministrativa();
	    	if (comandoWAMP != null) {
	    		return comandoWAMP;
	    	}
	    	
			// (4) Verifica clientes con visitas programadas pendientes
	
			// (5) Verifica si está pendiente sincronizar BD
            comandoWAMP = (ComandoWAMP) colaComandosNovedades.fetch();
            if (comandoWAMP != null) {
            	Log.i(AplicacionOrdenesServicio.tag_app, comandoWAMP.obtenerDatos());
            	return comandoWAMP;
            }
            
            // (6) Verifica si está pendiente enviar alguna foto
	    	comandoWAMP = obtenerComandoFoto();
	    	if (comandoWAMP != null) {
	    		Log.i(AplicacionOrdenesServicio.tag_app, "Comando de foto obtenido: " + comandoWAMP.obtenerNombreIntento() + " "
	    				+ "" + comandoWAMP.obtenerUri());
	    		return comandoWAMP;
	    	}

             //(7) verificar si esta pendiente alguna firma

            comandoWAMP = obtenerComandoFirma();
            if(comandoWAMP != null){
                Log.i(AplicacionOrdenesServicio.tag_app, "Comando de firma obtenido: " + comandoWAMP.obtenerNombreIntento() + " "
                        + "" + comandoWAMP.obtenerUri());
                return comandoWAMP;
            }
    	}
    	
    	// Retorno...
    	return comandoWAMP;		
	}
    
    private ComandoWAMP obtenerComandoFoto() {
    	ComandoWAMP comandoWAMP = null;
    	HashMap<String, String> datosFotoAdjunto = baseDatosTemporal.consultarImagenPendiente();
    	
		// Verifica si hay datos
    	if (datosFotoAdjunto != null) {
			// Hay datos,
	    	int id = Integer.parseInt(datosFotoAdjunto.get("id"));
	    	int idOrdenServicio = Integer.parseInt(datosFotoAdjunto.get("idOrdenServicio"));
	    	int idVisita = Integer.parseInt(datosFotoAdjunto.get("idVisita"));
	    	String clienteGlobal = "";
	    	try {
				clienteGlobal = aplicacion.getConfiguracionApp().getUsuario().getSeudonimo();
			} catch (Exception e) {
			}
    		String datosImagen = datosFotoAdjunto.get("datosImagen");
    		JSONArray datosImagenJson;
    		String rutaImagen = null, nombreImagen = null;
			try {
				datosImagenJson = new JSONArray(datosImagen);
				rutaImagen = datosImagenJson.getString(0);
				nombreImagen = datosImagenJson.getString(1);
			} catch (JSONException e) {
				e.printStackTrace();
			}
    		
    		// Lee la imagen y la codifica en base 64
			// String imagen = leerArchivoBase64(rutaImagen);
			Bitmap bitmap = BitmapFactory.decodeFile(rutaImagen);
			Bitmap resized = Bitmap.createScaledBitmap(bitmap,(int)(bitmap.getWidth()*0.5), (int)(bitmap.getHeight()*0.5), true);
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();  
			resized.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
			byte[] byteArray = byteArrayOutputStream.toByteArray();
			String imagen = Base64.encodeToString(byteArray, Base64.DEFAULT);
	    	
    		// Verifica si hay imagen válida
			if(rutaImagen != null && nombreImagen != null && imagen != null && !clienteGlobal.equals("")){
				// Extrae datos y crea comando
	    		StringBuilder datos = new StringBuilder();
	    		datos.append("{\"idOrdenServicio\":").append(idOrdenServicio).append(",");
	    		datos.append("\"idVisita\":").append(idVisita).append(",");
	    		datos.append("\"clienteGlobal\":\"").append(clienteGlobal).append("\",");
	    		datos.append("\"nombreImagen\":\"").append(nombreImagen).append("\",");
	    		datos.append("\"imagen\":\"").append(imagen).append("\"}");
	    		comandoWAMP = new ComandoWAMP(2, "os.aplicacion.guardarImagenVisita", datos.toString(), null, null, 
	    				"fotoAdjunto", 100, 100, id);  
			}
    	}
		
		// Retorno
		return comandoWAMP;
	}


    private ComandoWAMP obtenerComandoFirma() {
        ComandoWAMP comandoWAMP = null;
        HashMap<String, String> datosFirma = baseDatosTemporal.consultarFirma();

        // Verifica si hay datos
        if (datosFirma != null) {
            // Hay datos,
            int id = Integer.parseInt(datosFirma.get("id"));
            String nombre = datosFirma.get("nombre");
            String ruta = datosFirma.get("ruta");
            String clienteGlobal = "";
            try {
                clienteGlobal = aplicacion.getConfiguracionApp().getUsuario().getSeudonimo();
            } catch (Exception e) {
            }

            // Lee la imagen y la codifica en base 64
            // String imagen = leerArchivoBase64(rutaImagen);
            Bitmap bitmap = BitmapFactory.decodeFile(ruta + "/" + nombre);
            Bitmap resized = Bitmap.createScaledBitmap(bitmap,(int)(bitmap.getWidth()*0.5), (int)(bitmap.getHeight()*0.5), true);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            resized.compress(Bitmap.CompressFormat.PNG, 50, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            String imagen = Base64.encodeToString(byteArray, Base64.DEFAULT);

            // Verifica si hay imagen válida
            if(ruta != null && nombre != null && imagen != null ){
                // Extrae datos y crea comando
                StringBuilder datos = new StringBuilder();
                datos.append("{\"nombre\":\"").append(nombre).append("\",");
                //  datos.append("\"ruta\":").append(ruta).append(",");
                datos.append("\"clienteGlobal\":\"").append(clienteGlobal).append("\",");
                datos.append("\"imagen\":\"").append(imagen).append("\"}");
                comandoWAMP = new ComandoWAMP(2, "os.aplicacion.guardarFirmaVisita", datos.toString(), null, null,
                        "firma", 100, 100, id);
            }
        }

        // Retorno
        return comandoWAMP;
    }
    
    // Permite leer un archivo y codificarlo en Base64
    public static String leerArchivoBase64(String nombreArchivo) {
    	// Generar cadena base64 que representa el archivo dado
		try {
			File archivo = new File(nombreArchivo);
			FileInputStream streamArchivo = new FileInputStream(archivo);
			byte bytesArchivo[] = new byte[(int) archivo.length()];
			streamArchivo.read(bytesArchivo);
			streamArchivo.close();
			return Base64.encodeToString(bytesArchivo, Base64.DEFAULT);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// Permite obtener un comando WAMP de GPS
    private ComandoWAMP obtenerComandoGPS(){
    	ComandoWAMP comandoWAMP = null;
    	HashMap<String, String> tramas = baseDatosTemporal.consultarTramasPendientes();
    	
		// Verifica tramas
    	if (tramas != null) {
			// Hay tramas,
	    	int numeroTramas = Integer.parseInt(tramas.get("numeroTramas"));
	    	int ultimoId = Integer.parseInt(tramas.get("ultimoId"));
            Log.i(AplicacionOrdenesServicio.tag_app, "Tramas pendientes: " + numeroTramas + " ultimo Id: " + ultimoId);
    		
    		// Extrae datos y crea comando
    		StringBuilder datos = new StringBuilder();
    		datos.append("{\"idDispositivo\":\"").append(
    				aplicacion.getConfiguracionApp().getDispositivo().getId()).append("\", ");
    		datos.append("\"tramas\":").append(tramas.get("tramas")).append("}");
    		comandoWAMP = new ComandoWAMP(2, "coregc.aplicacion.rastreo", datos.toString(), null, null, "rastreo", -1, -1, ultimoId);  

            // Cambia el valor del consecutivo
            consecutivoIdConsulta = ultimoId;
    	}
		
		// Retorno
		return comandoWAMP;
    }
    
    // Permite obtener un comando WAMP de Ordenes de Servicio
	private ComandoWAMP obtenerComandoOrdenServicio(){
		ComandoWAMP comandoWAMP = null;
		
		// Inicializa controlador y consulta el JSON de las ordenes no sincronizadas.
		OrdenServicio ordenServicio = 
				((SQLiteRoutinesOS) aplicacion.getConfiguracionApp().getSqliteRoutines())
					.consultarOrdenServicioPendiente(aplicacion.getConfiguracionApp());
		
		
		String datosPendientes = ControladorOrdenServicio.generarJson(ordenServicio, aplicacion);
		
		// Verifica el JSON
		if(datosPendientes != null && !datosPendientes.equals("")){													
			// Genera los datos de las ordenes en JSON
			String datosOrdenesServicio = "{" +
					"\"datosSesion\": {"
					+ "\"idClienteGlobal\": " + aplicacion.getConfiguracionApp().getUsuario().getIdClienteGlobal() + ", "
					+ "\"idCliente\": " + ordenServicio.getIdCliente() + ", "
					+ "\"seudonimo\": \"" + aplicacion.getConfiguracionApp().getUsuario().getSeudonimo() + "\", "
					+ "\"idDispositivo\": \"" + aplicacion.getConfiguracionApp().getDispositivo().getId() + "\", "
					+ "\"idUsuario\":" + aplicacion.getConfiguracionApp().getUsuario().getId()
					+ "}, "
					+ "\"ordenServicio\":" + datosPendientes + "}";
			//Log.i(AplicacionOrdenesServicio.tag_app, datosOrdenesServicio);
			
			// Crea el comando.
			comandoWAMP = new ComandoWAMP(
					2, "os.aplicacion.reportarOrdenServicioNuevo", datosOrdenesServicio, null, null, 
					"aplicacion.reportarOrdenServicioNuevo", 15, 15, -1);
		}
		
		// Retorno
		return comandoWAMP;
	}
	
	 // Permite obtener un comando WAMP de Actividades Administrativas
	private ComandoWAMP obtenerComandoActividadAdministrativa(){
		ComandoWAMP comandoWAMP = null;
		
		// Consulta una actividad administrativa pendiente por sincronizar
		ActividadAdministrativa actividadAdministrativa = 
				((SQLiteRoutinesOS) aplicacion.getConfiguracionApp().getSqliteRoutines())
					.consultarActividadAdministrativaPendiente(aplicacion.getConfiguracionApp());
		
		// Genera el Json de las actividades pendientes por enviar
		String datosPendientes = ControladorActividadAdministrativa.generaraJson(actividadAdministrativa, aplicacion);
		
		// Verifica el JSON
		if(datosPendientes != null && !datosPendientes.equals("")){													
			// Genera los datos de las ordenes en JSON
			String datosActividadAdministrativa = "{" +
					"\"datosSesion\": {"
					+ "\"idClienteGlobal\": " + aplicacion.getConfiguracionApp().getUsuario().getIdClienteGlobal() + ", "
					+ "\"seudonimo\": \"" + aplicacion.getConfiguracionApp().getUsuario().getSeudonimo() + "\", "
					+ "\"idDispositivo\": \"" + aplicacion.getConfiguracionApp().getDispositivo().getId() + "\", "
					+ "\"idUsuario\":" + aplicacion.getConfiguracionApp().getUsuario().getId()
					+ "}, "
					+ "\"datosActividad\":" + datosPendientes + "}";
			//Log.i(AplicacionOrdenesServicio.tag_app, datosActividadAdministrativa);
			
			// Crea el comando.
			comandoWAMP = new ComandoWAMP(
					2, "os.aplicacion.reportarActividadAdministrativaNuevo", datosActividadAdministrativa, null, null, 
					"aplicacion.reportarActividadAdministrativa", 15, 15, -1);
		}
		
		// Retorno
		return comandoWAMP;
	}
}
