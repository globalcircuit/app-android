package com.gc.ordenesservicio;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.objetos.Cliente;
import com.gc.ordenesservicio.FragmentoCancelacionOrdenServicio.ListenerCancelacionOrdenServicio;
import com.gc.ordenesservicio.accesodatos.CustomSQLiteRoutinesOS;
import com.gc.ordenesservicio.accesodatos.SQLiteRoutinesOS;
import com.gc.ordenesservicio.objetos.OrdenServicio;

public class ActividadListaOrdenesServicio extends FragmentActivity implements ListenerCancelacionOrdenServicio {

	// Objetos
	private AplicacionOrdenesServicio aplicacion;
	private ToolsApp toolsApp = new ToolsApp();
	private CustomAdapterOrdenes adapter;
	private OrdenServicio ordenSeleccionada;
	

	// Elementos Gráficos y Menu
	private EditText campo_filtro_ordenes;
	private MenuItem item_opcion_cnclar_prgrmcion;
	private ListView lista_ordenesServicio;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.actividad_lista_ordenes_servicio);
		
		// Inicializa elementos gráficos
		campo_filtro_ordenes = (EditText) findViewById(R.id.campo_filtro_ordenes);
		campo_filtro_ordenes.addTextChangedListener(filterTextWatcher);
	
		// Inicializa el objeto de la aplicación
		aplicacion = ((AplicacionOrdenesServicio) getApplicationContext());
		
		lista_ordenesServicio = (ListView)findViewById(R.id.lista_ordenes_servicio);
		lista_ordenesServicio.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// Extrae el objeto seleccionado
				ordenSeleccionada = (OrdenServicio) lista_ordenesServicio.getAdapter().getItem(position);
				
				// Coloca la orden activa a nivel de aplicacion
				aplicacion.setOrdenServicioActiva(ordenSeleccionada);
				
				// Abre la orden de servicio
				abrirActividadOrdenServicio(ordenSeleccionada);				
			}			
		});
		registerForContextMenu(lista_ordenesServicio);
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		// Lista las ordenes de servicio
		inicializarListaOrdenes();

		// Verifica la existencia de una orden de servicio activa en memoria.
		OrdenServicio ordenServicio = aplicacion.getOrdenServicioActiva();
		if (ordenServicio != null) {
			// Existe la orden, redirecciona a la actividad de orden
			abrirActividadOrdenServicio(ordenServicio);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.actividad_lista_ordenes_servicio, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {	
		switch (item.getItemId()) {
			case R.id.actividades_administrativas:
				Intent intent = new Intent(aplicacion, ActividadActividadesAdministrativas.class);
				startActivity(intent);
				return true;
			default: 
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void finish() {
		super.finish();
		
		// Salva el contexto de los objetos
		aplicacion.salvarObjetosContexto(false);
	}
	
	@Override
	protected void onDestroy() {
		campo_filtro_ordenes.removeTextChangedListener(filterTextWatcher);
		super.onDestroy();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	// TextWatcher para filtrar las ordenes
	private TextWatcher filterTextWatcher = new TextWatcher() {
		@Override
		public void afterTextChanged(Editable s) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		// Permite filtrar el TextView a medida que va cambiando
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if(adapter != null){
				adapter.getFilter().filter(s);
			}
		}
	};

	// Método que permite iniciar la actividad para crear una orden.
	public void crearNuevaOrden(View view) {
		// TODO: 
		Intent intent = new Intent(this, ActividadNuevaOrdenServicio.class);
		startActivity(intent);
		/*Intent intent = new Intent(this, ActividadListaClientes.class);
		startActivity(intent);*/
	}
	
	// Inicializa los clientes dependiendo del MODO
	private void inicializarListaOrdenes() {
		// Consultar ordenes de servicio
		List<OrdenServicio> ordenesServicio = 
				((SQLiteRoutinesOS)aplicacion.getConfiguracionApp().getSqliteRoutines())
				.routine_consultarOrdenesServicio(
						(CustomSQLiteRoutinesOS) aplicacion.getConfiguracionApp().getCustomSqliteRoutines(),
						aplicacion.getConfiguracionApp());

		// Visualiza los clientes
		if(ordenesServicio != null && ordenesServicio.size() > 0){
			adapter = new CustomAdapterOrdenes(this, ordenesServicio);
			lista_ordenesServicio.setAdapter(adapter);
			Log.i(AplicacionOrdenesServicio.tag_app, "Órdenes cargadas con éxito!");
		} else {
			lista_ordenesServicio.setAdapter(null);
			toolsApp.showLongMessage("No hay órdenes de servicio!", this);
		}
	}
	
	// Crea menu contextual
	@Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		
		// Obtiene la información de la lista
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
        
        ordenSeleccionada = (OrdenServicio)adapter.getItem(info.position);
        
        if(ordenSeleccionada.getEstado().equals(OrdenServicio.PENDIENTE))
        {

        	MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_contextual_ordenes_servicio_cancelar, menu);
	        // Inicializa las opciones del menu contextual
	    	item_opcion_cnclar_prgrmcion = (MenuItem) menu.findItem(R.id.opcion_cnclar_prgrmcion); 
	    	// Se hace visible la opciónn
	    	item_opcion_cnclar_prgrmcion.setVisible(false);
        }
	}
	
	@Override
    public boolean onContextItemSelected(MenuItem item) {		
        switch (item.getItemId()) {
        	case R.id.opcion_cnclar_prgrmcion:
				// Pregunta por la cancelación de la visita programada
				/*
        		if(ordenSeleccionada.getIdUsuarioCreacion() == aplicacion.getConfiguracionApp().getUsuario().getId()){
	        		DialogFragment fragmentoCancelacion = new FragmentoCancelacionOrdenServicio();
	        		Bundle argumentos = new Bundle();
	        		argumentos.putString(
	        				"Mensaje", "¿Está seguro de CANCELAR la orden del " + 
	    					toolsApp.unixDateToString(ordenSeleccionada.getUnixFechaCreacion(), 2, true) + "?"); 
	        		fragmentoCancelacion.setArguments(argumentos);
	        		fragmentoCancelacion.show(getSupportFragmentManager(), "fragmentoCancelacion");
        		}else{
        			toolsApp.showShortMessage("Esta orden solo puede ser cancelada por un administrador", this);
        		}
        		*/


                return true;
            default:
                return super.onContextItemSelected(item);
        }
	}

	// Método que permite abrir la actividad con la información de la orden de servicio
	private void abrirActividadOrdenServicio(OrdenServicio ordenSeleccionada) {		
		// Consulta la informacion del cliente y lo setea a nivel de aplicacion
		Cliente cliente = ((SQLiteRoutinesOS)aplicacion.getConfiguracionApp().getSqliteRoutines())
				.routine_consultarCliente(ordenSeleccionada.getIdCliente(), null);
		if (cliente != null) {
			aplicacion.getConfiguracionApp().setClienteActivo(cliente);
			
			// Abre la Orden
			Intent intent = new Intent(this, ActividadOrdenServicio.class);
			startActivity(intent);
		} else {
			// Cliente no asignado
			toolsApp.alertDialogYES(this, null, 
				"El cliente asociado a la orden # " + ordenSeleccionada.getId() + " no está asignado.", null);
		}
	}
	
	// Clase customizda para la visualizacion de las órdenes
	private class CustomAdapterOrdenes extends BaseAdapter implements
			Filterable {
		private Activity activity;
		private List<OrdenServicio> list;
		private OrdenServicio ordenServicio;
		private LayoutInflater inflater = null;
		protected List<OrdenServicio> listaOrigen;

		// Constructor
		public CustomAdapterOrdenes(Activity activity, List<OrdenServicio> list) {
			this.activity = activity;
			this.list = list;
			this.inflater = (LayoutInflater) this.activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		// Retorna el número de items en la lista
		public int getCount() {
			return list.size();
		}

		// Permite obtener el objeto en la posición
		public Object getItem(int position) {
			ordenServicio = list.get(position);
			return ordenServicio;
		}

		// Permite obtener el estilo de layout previamente definido y coloca los
		// valores de la fila que serán mostrados
		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;
			if (convertView == null)
				vi = inflater.inflate(R.layout.fila_orden_servicio, parent, false);

			// Obtiene los view donde será colocada la información
			TextView fila_valor_codigo_orden = (TextView) vi.findViewById(R.id.fila_valor_codigo_orden);
			TextView fila_valor_fecha_orden = (TextView) vi.findViewById(R.id.fila_valor_fecha_orden);
			TextView fila_valor_nombre_cliente = (TextView) vi.findViewById(R.id.fila_valor_nombre_cliente);
			TextView fila_estado_orden = (TextView) vi.findViewById(R.id.fila_estado_orden);
			
			// Obtiene los datos en la posición del arreglo
			OrdenServicio ordenServicio = list.get(position);
			
			// Consulta la informacion del cliente
			Cliente cliente = ((SQLiteRoutinesOS)aplicacion.getConfiguracionApp().getSqliteRoutines())
					.routine_consultarCliente(ordenServicio.getIdCliente(), null);
			
			// Setea los valores
			fila_valor_codigo_orden.setText(String.valueOf(ordenServicio.getId()));
			fila_valor_fecha_orden.setText(toolsApp.unixDateToString(
					ordenServicio.getUnixFechaCreacion(), 2, true));
			fila_valor_nombre_cliente.setText((cliente!=null ? cliente.getNombres() : "CLIENTE NO ASIGNADO"));
			
			// Consulta el estado del cliente en los datos adicionales.
			try {
				String datosAdicionales = ordenServicio.getDatosAdicionales();
				if(datosAdicionales != null) {
					JSONObject jsonObject = new JSONObject(datosAdicionales);
					String estado = jsonObject.getString("estado");
					fila_estado_orden.setText(estado);
				} else {
					fila_estado_orden.setText("SIN ESTADO");
				}
			} catch(JSONException e) {
				e.printStackTrace();
				fila_estado_orden.setText("-");
			}
			
			// Retorno...
			return vi;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public Filter getFilter() {
			return new Filter() {

				@Override
				protected FilterResults performFiltering(CharSequence constraint) {
					final FilterResults oReturn = new FilterResults();
					final ArrayList<OrdenServicio> listaFiltrada = new ArrayList<OrdenServicio>();
					if (listaOrigen == null){
						listaOrigen = list;
					}
					
					// Recorre la lista original y dependiendo del filtro aplica el criterio
					if (listaOrigen != null && listaOrigen.size() > 0) {
						for (OrdenServicio clnte : listaOrigen) {
							if (String.valueOf(clnte.getId())
									.contains(constraint.toString().toLowerCase(Locale.getDefault()))
							) {
								listaFiltrada.add(clnte);
							}
						}
					}

					oReturn.values = listaFiltrada;
					return oReturn;
				}

				@SuppressWarnings("unchecked")
				@Override
				protected void publishResults(CharSequence constraint,
						FilterResults results) {
					list = (ArrayList<OrdenServicio>) results.values;
					notifyDataSetChanged();
				}
			};
		}

		public void notifyDataSetChanged() {
			super.notifyDataSetChanged();
		}
		
		
	}

	@Override
	public void eventoCancelacionOrdenServicio(String justificacion) {
		// Setea estado de la orden
		ordenSeleccionada.setEstado(OrdenServicio.CANCELADA);
		ordenSeleccionada.setJustificacion(justificacion);
		ordenSeleccionada.setIdUsuarioCreacion(aplicacion.getConfiguracionApp().getUsuario().getId());
		
		// TODO:
		ArrayList<String> estado = ((SQLiteRoutinesOS)aplicacion.getConfiguracionApp().getSqliteRoutines())
				.consultarEstado(OrdenServicio.CANCELADA);
		try{
			JSONObject jsonObject = new JSONObject(ordenSeleccionada.getDatosAdicionales());
			jsonObject.put("estado",estado.get(0));
			ordenSeleccionada.setDatosAdicionales(jsonObject.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		((SQLiteRoutinesOS) aplicacion.getConfiguracionApp().getSqliteRoutines())
			.guardarOrdenServicio(aplicacion, aplicacion.getConfiguracionApp(), ordenSeleccionada);
		((CustomSQLiteRoutinesOS)aplicacion.getConfiguracionApp().getCustomSqliteRoutines())
			.almatec_ejecutarActualizacionEstadoOrdenServicio(estado.get(0),ordenSeleccionada.getId());
		
		inicializarListaOrdenes();
		
	}
}
