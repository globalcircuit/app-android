package com.gc.ordenesservicio;

import java.util.HashMap;
import java.util.Vector;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;

import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.gc.coregestionclientes.fragmentos.TimePickerFragment.OnTimePickerEvent;
import com.gc.coregestionclientes.librerias.ToolsApp;
import com.gc.coregestionclientes.objetos.AuditorFormulario;
import com.gc.coregestionclientes.objetos.Campo;
import com.gc.coregestionclientes.objetos.ControladorFormularios;
import com.gc.coregestionclientes.objetos.Formulario;
import com.gc.ordenesservicio.objetos.AuditorFormularioOS;
import com.gc.ordenesservicio.objetos.AutocompletarCampoOS;

public class ActividadSubformulario extends FragmentActivity implements OnTimePickerEvent {


	// Variables
	private int indiceDatos = -1;

	// Librerias
	private ToolsApp toolsApp;

	// Objetos
	private AplicacionOrdenesServicio aplicacion;
	private ControladorFormularios controladorFormularios;
	private AuditorFormulario auditorFormulario;
	private int idCampo = -1;
	private String nombreCampo = null;
	private Formulario formulario;
	private Vector<Vector<Campo>> datos = null;
	private GestureDetector gesturedetector = null;

	// Elementos Gráficos	
	private TextView etiqueta_titulo_detalle_item = null;
	private TextView etiqueta_valor_detalle_item = null;
	private ScrollView body = null;	
	private LinearLayout contenedorFormulario;
	//private LayoutInflater inflater = null;
	private View layoutView = null;

	// Objetos auxiliares 
	private HashMap<String, Campo> camposHash = new HashMap<String, Campo>();
	private HashMap<String, View> contenedoresCamposHash = new HashMap<String, View>();

	// Parametros auxiliares
	private final int REQUEST_GOTO = 1000;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*inflater = (LayoutInflater) (ActividadSubformulario.this).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutView = inflater.inflate(R.layout.actividad_subformulario, null);*/
		setContentView(R.layout.actividad_subformulario);
		layoutView = getWindow().getDecorView().findViewById(android.R.id.content);

		// Inicializa elemntos graficos.
		etiqueta_titulo_detalle_item = (TextView)findViewById(R.id.etiqueta_titulo_detalle_item);
		etiqueta_valor_detalle_item = (TextView)findViewById(R.id.etiqueta_valor_detalle_item);
		body = (ScrollView)findViewById(R.id.body);
		contenedorFormulario = (LinearLayout)findViewById(R.id.cntndor_subfrmlrio);

		// Inicializa el objeto Gestos
		gesturedetector = new GestureDetector(this, new MyGestureListener());

		// Obtiene el objeto con la configuracion de la app
		aplicacion = ((AplicacionOrdenesServicio)getApplicationContext());

		// Inicializa toolsApp
		toolsApp = new ToolsApp();

		// Extrae los datos del intent
		idCampo = getIntent().getIntExtra("idCampo", -1);
		nombreCampo = getIntent().getStringExtra("nombreCampo");
		formulario = getIntent().getParcelableExtra("Formulario");
		etiqueta_titulo_detalle_item.setText("(" + nombreCampo + ")");

		// Inicializa el controlador del formulario activo y el auditor
		controladorFormularios = new ControladorFormularios(formulario);
		controladorFormularios.cargarFormulariosLocales(aplicacion.getConfiguracionApp().getSqliteRoutines(), 
				Formulario.SUBFORMULARIO);

		// Inicializa el auditor
		auditorFormulario = new AuditorFormularioOS(formulario, null, controladorFormularios);

		// Crea dinámicamente el formulario
		controladorFormularios.visualizarFormulario(
				this, layoutView, contenedorFormulario, camposHash, contenedoresCamposHash, 
				auditorFormulario, new AutocompletarCampoOS(), 
				onItemSelectedListener, onCheckedChangeListener, true);

		// Verifica si existen datos 
		datos = (Vector<Vector<Campo>>) aplicacion.getConfiguracionApp().getMemoria(idCampo);
		if(datos == null || datos.size() == 0){
			// No hay datos
			datos = new Vector<Vector<Campo>>();
			aplicacion.getConfiguracionApp().setMemoria(idCampo, datos);
			nuevaListaCampos();
		} else {
			// Si hay datos
			indiceDatos = 0;
			actualizarDatosFormulario();
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
		switch (requestCode) {
		case REQUEST_GOTO:
			// Analiza el resultado
			if(resultCode == RESULT_OK){   			
				// Obtiene el indice del item.
				if(data != null){
					int indiceItem = data.getIntExtra("indiceItem", -1);
					if(indiceItem >= 0){
						indiceDatos = indiceItem;
						actualizarDatosFormulario();
					}
				} 
			}
			break;
		case ControladorFormularios.REQUEST_AUTOCOMPLETE:
			// Analiza el resultado
			if(resultCode == RESULT_OK){   			
				// Obtiene el valor retornado
				if(data != null){
					int idCampo = data.getIntExtra("idCampo", -1);
					final Campo campoEvento = camposHash.get(String.valueOf(idCampo));
					HashMap<String, String> objetoDatos = (HashMap<String, String>) data.getSerializableExtra("valor");

					if(objetoDatos != null && idCampo > 0){

						// Dependiendo del campo valida la existencia de todo el item en la lista (Vector de items)
						EditText editText = (EditText)layoutView.findViewById(idCampo);
						String nombreCampo = campoEvento.getNombre();
						String valor = objetoDatos.get(nombreCampo);

						final int indiceItemExistente = auditorFormulario.validarExistenciaItem(
								this, campoEvento, valor, datos);

						// Setea el valor si el producto es valido
						if (indiceItemExistente < 0) {
							editText.setText(valor);
						} else {
							toolsApp.alertDialogYES(this, "Advertencia!", 
									"El item ya existe en la posición " + (indiceItemExistente + 1), 
									new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// Si el valor del campo es null o vacio puede eliminar el item
									if (campoEvento.getValor() == null || campoEvento.getValor().equals("")){
										// Elimina el item del vector y actualiza la pantalla
										datos.remove(indiceDatos);

										// Elimina el rastro dejado por el item si es necesario
										auditorFormulario.eliminarRastroItem(
												ActividadSubformulario.this, layoutView);
									}

									// Actualiza datos
									indiceDatos = indiceItemExistente;
									actualizarDatosFormulario();												
								}
							}
									);
						}

						// Genera evento autocomplete
						auditorFormulario.eventoAutoComplete(ActividadSubformulario.this, layoutView, campoEvento, editText, objetoDatos);
					}
				} 
			}
			break;
		case ControladorFormularios.REQUEST_SUBFORMULARIO:
			// Analiza el resultado
			if(resultCode == RESULT_OK){   			
				// Obtiene el valor retornado
				if(data != null){
					// Listener para el campo tipo "Detalle"
					int idCampo = data.getIntExtra("idCampo", -1);
					Campo campoEvento = camposHash.get(String.valueOf(idCampo));
					auditorFormulario.validarEventoCampo(ActividadSubformulario.this, layoutView, campoEvento, 
							null, contenedoresCamposHash, camposHash);	
				} 
			}				
			break;
		case ControladorFormularios.REQUEST_LISTACAMPO:
			// Analiza el resultado
			if(resultCode == RESULT_OK){   			
				// Obtiene el valor retornado
				if(data != null){
					int idCampo = data.getIntExtra("idCampo", -1);
					String valor = data.getStringExtra("valor");

					// Si es un valor válido, lo setea
					if(valor != null && idCampo > 0){
						EditText editText = (EditText)layoutView.findViewById(idCampo);
						editText.setText(valor);
					}
				} 
			}
			break;
		case ControladorFormularios.REQUEST_SCANCODE:
			// Analiza el resultado
			if(resultCode == RESULT_OK){   			
				// Obtiene el valor retornado
				if(data != null){
					String valor = data.getStringExtra("SCAN_RESULT");
					//String format = data.getStringExtra("SCAN_RESULT_FORMAT");
					int idCampo = data.getIntExtra("idCampo", -1);
					if(valor != null && idCampo > 0){
						((EditText)layoutView.findViewById(idCampo)).setText(valor);
					}
				} 
			}				
			break;
		default:
			break;
		}
	};

	@Override
	public void finish() {
		aplicacion.getConfiguracionApp().setMemoria(idCampo, datos);
		Intent resultIntent = new Intent();
		resultIntent.putExtra("idCampo", idCampo);
		setResult(RESULT_OK, resultIntent);
		super.finish();
	}

	// Adiciona una nueva lista al vector
	private void nuevaListaCampos() {
		datos.add(formulario.getCamposPlantilla());
		String modoEdicion = "";
		indiceDatos = datos.size() - 1;

		//Se ejecuta la condición solo cuando se va a crear desde el segundo formulario en adelante
		if( indiceDatos > 0 ){

			//Se hace un ciclo recorriendo todos los datos de un formulario
			for (int j = 0; j < datos.get(indiceDatos).size(); j++) {  
				//Se asignan a variables termporales los datos del vector para sacar valores específicos
				//y trabajar con ellos
				modoEdicion = datos.get(indiceDatos).get(j).getModoEdicion();
				int idCampo = datos.get(indiceDatos).get(j).getId();
				String valorCampo =  datos.get(indiceDatos).get(j).getValor();

				//Se hace un switchcase entre los diferentes modos de edición(tipo de dato)
				//de los campos del formulario y se setean en vacío según su tipo
				switch (modoEdicion) {
					case "EditText":
						((EditText)layoutView.findViewById(idCampo)).setText("");
						break;
					case "Spinner":
						((Spinner)layoutView.findViewById(idCampo)).setSelection(0);
						break;
					case "CheckBox":
						((CheckBox)layoutView.findViewById(idCampo)).setChecked(false);
						break;
					case "AutoComplete":
						((EditText)layoutView.findViewById(idCampo)).setText("");
						break;
					case "TextView":
						((TextView)layoutView.findViewById(idCampo)).setText("");
						break;
					case "ScanCode":
						((EditText)layoutView.findViewById(idCampo)).setText("");
						break;
				}
			}
		} 
		actualizarDatosFormulario();
		auditorFormulario.inicializarCamposFormulario(this,contenedoresCamposHash);
		body.fullScroll(ScrollView.FOCUS_UP);
	}

	@Override
	public void onBackPressed() {
		// Pasa los valores de la Interfaz Gráfica al vector de datos
		if(datos != null){
			controladorFormularios.interfazAFormulario(this, aplicacion.getConfiguracionApp(), 
					false, auditorFormulario, layoutView, contenedoresCamposHash);
			datos.setElementAt(formulario.getCampos(), indiceDatos);
		}
		super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.actividad_subformulario, menu);
		return true;
	}

	// // Método que se llama cuando una de las opciones del menú contextual es seleccionada.
	public boolean onOptionsItemSelected(MenuItem item) {	
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.opcion_nvo_dtlle:				
			// Pasa los valores de la Interfaz Gráfica al vector de datos
			controladorFormularios.interfazAFormulario(this, aplicacion.getConfiguracionApp(), false, 
					auditorFormulario, layoutView, contenedoresCamposHash);
			datos.setElementAt(formulario.getCampos(), indiceDatos);

			// Crea un nuevo Item.
			nuevaListaCampos();

			// Retorno
			return true;

		case R.id.opcion_elmnar_dtlle:	
			toolsApp.alertDialogYESNO(this, 
					"¿Está seguro de eliminar el item " + (indiceDatos + 1) + "?", null, null, 
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// Deshabilita el botón
					((AlertDialog) dialog).getButton(which).setEnabled(false);

					// Dependiendo del botón presionado, se ejecuta la acción
					switch (which){
					case DialogInterface.BUTTON_POSITIVE:
						// Elimina el item del vector y actualiza la pantalla
						datos.remove(indiceDatos);
						indiceDatos--;
						if(indiceDatos<0){
							indiceDatos = 0;
						}

						// Elimina el rastro dejado por el item si es necesario
						auditorFormulario.eliminarRastroItem(ActividadSubformulario.this, layoutView);

						// Actualiza datos
						actualizarDatosFormulario();
						break;
					}
				}
			});

			// Retorno
			return true;

		case R.id.opcion_slir_dtlle:				
			// Pasa los valores de la Interfaz Gráfica al vector de datos
			controladorFormularios.interfazAFormulario(this, aplicacion.getConfiguracionApp(), false, 
					auditorFormulario, layoutView, contenedoresCamposHash);
			datos.setElementAt(formulario.getCampos(), indiceDatos);

			// Finaliza la actividad: 
			finish();
			return true;
		case R.id.opcion_rsmen_dtlle:
			// Pasa los valores de la Interfaz Gráfica al vector de datos
			controladorFormularios.interfazAFormulario(this, aplicacion.getConfiguracionApp(), false, 
					auditorFormulario, layoutView, contenedoresCamposHash);
			datos.setElementAt(formulario.getCampos(), indiceDatos);

			// Genera el resumen del detalle
			// TODO: Mostrar en un Fragment la lista con los datos
			aplicacion.getConfiguracionApp().setMemoria(idCampo, datos);
			Intent intent = new Intent(this, ActividadResumenSubformulario.class);
			intent.putExtra("idCampo", idCampo);
			startActivityForResult(intent, REQUEST_GOTO);

			// TEMPORAL:
			// Muestra el total acumulado
			//generarResumen();

			// Retorno
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	// Listener para los Spinner
	AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			// Extrae el campo segun el view (Spinner)
			Campo cmpo_evnto = camposHash.get(String.valueOf(arg0.getId()));
			auditorFormulario.validarEventoCampo(ActividadSubformulario.this, layoutView, cmpo_evnto, 
					arg0, contenedoresCamposHash, camposHash);	
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {}
	};

	// Listener para los Checkbox
	CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// Extrae el campo segun el view (Checkbox)
			Campo cmpo_evnto = camposHash.get(String.valueOf(buttonView.getId()));
			auditorFormulario.validarEventoCampo(ActividadSubformulario.this, layoutView, cmpo_evnto, 
					buttonView, contenedoresCamposHash, camposHash);
		}
	};

	// Manejador de todos los eventos touch de la actividad
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if(!gesturedetector.onTouchEvent(ev)) {
			//Log.d(ConfiguracionAplicacion.tag_app, ev.toString());
			return super.dispatchTouchEvent(ev);
		} else {
			//Log.d(ConfiguracionAplicacion.tag_app, "M: " + ev.toString());
			return true;
		}
	}

	// Clase que maneja los gestos simples como el swipe 
	// con el fin de viajar entre los items.
	private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
		private static final int SWIPE_THRESHOLD_VELOCITY = 20;

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			float dX = e2.getX()-e1.getX();
			float dY = e1.getY()-e2.getY();
			//Log.d(ConfiguracionAplicacion.tag_app, "dX: " + Math.abs(dX) + " dY: " + Math.abs(dY) + " Div: " + (Math.abs(dX)/Math.abs(dY)));

			if (Math.abs(velocityX) >= SWIPE_THRESHOLD_VELOCITY && (Math.abs(dX)/Math.abs(dY) >= 3 || Math.abs(dY) == 0)) {
				// Pasa los valores de la Interfaz Gráfica al vector de datos
				controladorFormularios.interfazAFormulario(
						ActividadSubformulario.this, aplicacion.getConfiguracionApp(), false, 
						auditorFormulario, layoutView, contenedoresCamposHash);
				datos.setElementAt(formulario.getCampos(), indiceDatos);

				// Dependiendo del desplazamiento, actualiza el indice de datos.
				if (dX>0) {
					indiceDatos--;
					if(indiceDatos<0){
						indiceDatos = 0;
					}
				} else {
					indiceDatos++;
					if(indiceDatos > (datos.size()-1)){
						indiceDatos = datos.size()-1;
					}
				}
				actualizarDatosFormulario();
				//dtlle_vlor_item.requestFocus();
				return true;
			}
			return false;
		}
	}

	// Método que permite refrescar los datos del formulario, 
	// teniendo en cuenta el indice o puntero actual
	private void actualizarDatosFormulario(){
		// Se deben actualizar los datos del formulario siempre y cuando existan datos.
		if(datos.size()>0){
			Vector<Campo> cmpos = datos.get(indiceDatos);
			formulario.setCampos(cmpos);

			// Actualiza el número del item
			etiqueta_valor_detalle_item.setText((String.valueOf(indiceDatos+1)) + "/" + datos.size());

			// Actualiza dinámicamente el formulario
			controladorFormularios.formularioAInterfaz(this, layoutView, camposHash, aplicacion.getConfiguracionApp());
		} else {
			// Se han eliminado todos los items!
			datos = null;
			toolsApp.showLongMessage("Se han eliminado todos los items!", ActividadSubformulario.this);

			// Cierra la actividad
			finish();
		}
	}

	@Override
	public void onTimePickerEvent(int idCampo, long unixTime) {
		((EditText)layoutView.findViewById(idCampo)).setText(toolsApp.unixTimeToStringHourMinutes(unixTime));
	}
}
