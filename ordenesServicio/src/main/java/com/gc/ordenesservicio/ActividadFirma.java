package com.gc.ordenesservicio;

import com.gc.coregestionclientes.librerias.ToolsApp;
import com.kyanogen.signatureview.SignatureView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import androidx.fragment.app.FragmentActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.graphics.Bitmap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class ActividadFirma extends FragmentActivity {

    private AplicacionOrdenesServicio aplicacion;
    private ToolsApp toolsApp = new ToolsApp();
    private SignatureView signatureView;
    private String nombre;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Inicializa el Objeto con la configuracion de la App para ser utilizado
        aplicacion = ((AplicacionOrdenesServicio)getApplicationContext());
        setContentView(R.layout.activity_actividad_firma);//see xml layout
        signatureView = (SignatureView) findViewById(R.id.signature_view);
        Intent resultado= getIntent();
        Bundle b = resultado.getExtras();

        if(b!=null)
        {
            nombre = String.valueOf((int) b.get("id_Orden"));
            nombre = nombre + "_";
            nombre = nombre + String.valueOf((int) b.get("id_Visita"));
            nombre = nombre + ".png";
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actividad_menu_firma, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public void onBackPressed() {
        // Se deshabilita el funcionamiento del back
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int idOrdenServicioLog = aplicacion.getOrdenServicioActiva().getId();
        //Calendario para la fecha de los hitos
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String fecha = sdf.format(cal.getTime());
        String toLog = "";

        switch (item.getItemId())
        {
            case R.id.action_clear:


                toLog = "Limpié el canvas de la firma a las " + fecha + " con el idOrden " + idOrdenServicioLog;
                toolsApp.generaLog(toLog);

                signatureView.clearCanvas();//Clear SignatureView
                return true;
            case R.id.action_download:

                //String directorio =File.separator + "GC" + File.separator + "databases" + File.separator;
                String pathSD = Environment.getExternalStorageDirectory()
                        .getAbsolutePath() + "/GC/" + "/Firma/" ;   // ruta

                File drctrio = new File(pathSD);
                drctrio.mkdirs();  // crear el fichero

                toLog = "Hundí en guardar la firma a las " + fecha + " con el idOrden " + idOrdenServicioLog;
                toolsApp.generaLog(toLog);


                //File directory = Environment.getExternalStoragePublicDirectory(pathSD);
                //String variable = directory.getPath();

                File file = new File(pathSD, nombre);

                FileOutputStream out = null;
                Bitmap bitmap = signatureView.getSignatureBitmap();
                try {
                    out = new FileOutputStream(file);
                    if(bitmap!=null){
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                    }else{
                        throw new FileNotFoundException();
                    }


                    aplicacion.setFirma(nombre);
                    aplicacion.setRutaFirma(pathSD);

                    /*Intent intentoServicioNovedades = new Intent(this, ServicioNovedades.class);
                    intentoServicioNovedades.putExtra("tipoIntent", "FIRMA");
                    intentoServicioNovedades.putExtra("nombre",nombre); // ya esta
                    intentoServicioNovedades.putExtra("ruta",pathSD);  // ya esta
                    startService(intentoServicioNovedades);*/
                } catch (Exception e) {
                    e.printStackTrace();
                }


                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}


