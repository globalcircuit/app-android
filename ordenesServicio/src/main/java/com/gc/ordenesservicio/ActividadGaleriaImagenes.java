package com.gc.ordenesservicio;

import java.io.File;
import java.util.Locale;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.gc.coregestionclientes.objetos.ControladorFormularios;

public class ActividadGaleriaImagenes extends Activity {
	private int numeroTotalImagenes;
	private Bitmap[] imagenesMiniatura;
	private boolean[] arregloImagenesSeleccionadas;
	private String[] arregloDirecciones;
	private String[] arregloNombres;
	private ImageAdapter imageAdapter;
	private Vector<String[]> datos = null;
	private AplicacionOrdenesServicio aplicacion;
	private String pathImagen;
	private String nombreImagen;
	private final int numeroMaximoImagenes = 5;


	// Objetos
	private int idCampo = -1;
	private int tipoPeticion = -1;
	//private String nombreCampo = null;

	// clase definida y estructurada para que la persona pueda seleccionar de la galeria un numero maximo de fotos,
	// definido anteriormente con la nueva posibilidad de tomar en tiempo de ejecucion fotos con su camara sin salir de la app
	// esta posibilidad sigue restringiendo al usuario para solo adjuntar el numero fotos

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

		// Obtiene el objeto con la configuracion de la app
		aplicacion = ((AplicacionOrdenesServicio)getApplicationContext());

		// Obtiene los datos del intent
		Bundle extras = getIntent().getExtras();
		idCampo = extras.getInt("idCampo", -1);
		// tiene dos peticiones posibles seleccionar de la galeria o tomar foto
		tipoPeticion = extras.getInt("tipoPeticion",-1);

		//nombreCampo = extras.getString("nombreCampo");

		// Traer de memoria un objeto asociado a el id campo en cuestion
		datos = (Vector<String[]>) aplicacion.getConfiguracionApp().getMemoria(idCampo);

		// evaluar si con anterioridad se han seleccionado imagenes.
		if(datos == null || datos.size() == 0){
			// No hay imagenes
			datos = new Vector<String[]>();
			aplicacion.getConfiguracionApp().setMemoria(idCampo, datos);
		}
		// variable para determinar si se ha alcanzado o no el maximo de fotos
		boolean maximoFotos = true;

		if(datos.size() >= numeroMaximoImagenes){

			// caso donde se han alzancaso el maximo de fotos, no puede tomar mas fotos
			// pero si puede en galeria editar la seleccion de esas las fotos
			maximoFotos = false;
			// finish();

			// se muestra mensaje informando que se alcanzo el tope de fotos
			lanzarToast("Ya se han adjuntado "+ numeroMaximoImagenes +" imagenes.");

		}

		// Evaluar el tipo de peticion recibida en el primer caso es tipo Foto se debe abrir la camara
		// de ser posible.
		if (tipoPeticion == ControladorFormularios.REQUEST_FOTO){

			// ruta de la carpeta donde se almacenaran las imagenes tomadas.
			pathImagen = ControladorFormularios.pathSD + ControladorFormularios.pathMedia + ControladorFormularios.pathImagenes;
			File mFile = new File(pathImagen);

			// revisar si la el directorio existe.
			boolean esCreada = mFile.exists();

			// si se ha alcanzado el numero de fotos maxima se debe terminar la actividad.

			if(!maximoFotos)
			{
				finish();
			}

			if(!esCreada){
				// crear el directorio si este no esta creado
				esCreada =mFile.mkdirs();

			}
			// si el directorio es creado y cuenta menos de las fotos maximas
			if(esCreada && maximoFotos){
				// metodo encargado de lanzar la camara
				lanzarActividadFoto();

				// dado que no se presentan  las condiciones se finaliza la actividad
			} else {
				finish();
			}

			// peticion es para la galeria

		} else if(tipoPeticion == ControladorFormularios.GALERIA){

			// se asigna layout correspondiente para la actividad
			setContentView(R.layout.actividad_galeria_imagenes);
			// Inicializar imagenes

			inicializarImagenes();

			// Inicializa el gridview y el adaptador
			// organizar elementos visibles del layout para los elementos logicos que dan soporte funcional
			GridView gridView = (GridView) findViewById(R.id.PhoneImageGrid);
			imageAdapter = new ImageAdapter();
			gridView.setAdapter(imageAdapter);

			// Inicializa el botón seleccionar
			final Button botonSeleccionar = (Button) findViewById(R.id.selectBtn);

			// se agrega el evento listener para el boton seleccionar
			botonSeleccionar.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// tomar el valor elementos seleccionados
					final int len = arregloImagenesSeleccionadas.length;
					int numeroImagenesSeleccionadas = 0;
					datos.clear(); // limpiar el arreglo donde se encuentran las imagenes seleccionadas
					for (int i = 0; i < len; i++) {

						// agregar las imagenes seleccionadas siguiendo el siguiente esquema
						// arreglo de dos dimensiones : path completo de la imagen incluido nombre
						// y el nombre de la imagen
						if (arregloImagenesSeleccionadas[i]) {
							numeroImagenesSeleccionadas++;
							String[] datosImagen = {arregloDirecciones[i], arregloNombres[i]};
							datos.add(datosImagen);
						}
					}

					// casos donde se evaluan eventualidades donde no se encuentren seleccionadas imagenes
					// donde se adjuntan mas de el numero permitido de  imagenes
					if (numeroImagenesSeleccionadas == 0) {
						lanzarToast("Por favor seleccione al menos una imagen");

					} else if (numeroImagenesSeleccionadas + datos.size() > numeroMaximoImagenes) {
						lanzarToast("Se pueden adjuntar máximo "+ numeroMaximoImagenes +" imágenes");

					} else {
						// caso donde hay almenos una imagen seleccionada y menos de 6
						// la finalidad es informar cuantas imagenes se seleccionaron
						// y terminar la actividad de forma correcta con un resultado OK
						lanzarToast("Ha seleccionado " + numeroImagenesSeleccionadas + " imagen(es).");
						// Retorna los datos
						aplicacion.getConfiguracionApp().setMemoria(idCampo, datos);
						Intent resultIntent = new Intent();
						resultIntent.putExtra("idCampo", idCampo);
						setResult(RESULT_OK, resultIntent);
						finish();
					}
				}
			});

		}

	}

	/* lanazar actividad foto es un metodo el cual se encarga de llamar a la camara mediante un intent
	// del sistema para que se puedan tomar fotos, ademas se le da la funciionalidad de guardar la foto en un
	lugar determinado
	ademas se asigna un nombre a la imagen basandose en la hora en milisegundos
	Esta actividad espera resultados para esto se implementa onActivityResults en la misma clase
	Se llama la actividad con el codigo de peticion de foto unico de este tipo para poder diferenciar
	de forma correcta los resultados de esta actividad
	*/
	public void lanzarActividadFoto(){
		Long consecutivo= System.currentTimeMillis()/1000;
		nombreImagen = consecutivo.toString()+".jpg";
		String pathCompelto = pathImagen + nombreImagen;
		File fileImagen;
		fileImagen=new File(pathCompelto);
		Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileImagen));
		startActivityForResult(intent,ControladorFormularios.REQUEST_FOTO);


	}

	/*metodo encargado de revisar los resultados de las actividades invocadas desde esta actividad
	se evalua el codigo de peticion de fotos donde se evaluan los casos de respuesta
	*/

	protected void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
		switch (requestCode) {
			// caso de tipo foto
			case ControladorFormularios.REQUEST_FOTO:
				// caso de resultado es satisfactorio
				if(resultCode == RESULT_OK){
					// se crea un archivo para guardar el contenido de la imgen que se ha capturado
					File mFile = new File(pathImagen + nombreImagen);
					// verificar si la imagen existe si ha sido creada
					boolean esCreada = mFile.exists();
					if (esCreada){
						// si existe se procede a realizar dos eventos importantes
						// suscribir la fotos a los indices del sistema para que sea visible por la galeria
						// por ende ser visible a seleccionar imagenes.
						ContentValues values = new ContentValues();
						values.put(MediaStore.Images.Media.TITLE, "Titulo");
						values.put(MediaStore.Images.Media.DESCRIPTION, "Descripción");
						values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis ());
						values.put(MediaStore.Images.ImageColumns.BUCKET_ID, mFile.toString().toLowerCase(Locale.getDefault()).hashCode());
						values.put(MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME, mFile.getName().toLowerCase(Locale.getDefault()));
						values.put("_data", mFile.getAbsolutePath());
						ContentResolver cr = getContentResolver();
						cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
						// evento dos es agregar la imagen al vector de imagenes seleccionadas
						String[] datosImagen = {pathImagen + nombreImagen, nombreImagen};
						datos.add(datosImagen);
						lanzarToast("La imagen ha sido guardada.");

						// posiblemente el usuario quiera guardar mas imagenes, se brinda la posibilidad de hacerlo
						// mediante un cuadro de dialogo siempre y cuando no tenga el maximo numero de imagenes seleccionadas

						// se pueden tomar mas fotos se le brinda la opcion de escoger si desea hacerlo o no
						if(datos.size() <= numeroMaximoImagenes -1 ){
							mostrarDialogOpciones();

						} else{
							// no se pueden guardar mas fotos se muestra mensaje y se cierra la actividad
							lanzarToast("Se han guardado " + numeroMaximoImagenes + " imagenes.");

							finish();
						}

					}else {
						// la imagen no pudo ser guardada se lanza la actividad d enuevo
						// cabe resaltar que la posibilidad que un resultado sea result_ok
						// y no se guarde la imagen es muy baja
						// pero se puede asumir casos extraños donde es bueno manejarlos
						lanzarToast("Problemas al guardar la imagen en el dispositivo");

						lanzarActividadFoto();

					}

					// caso de reultado no satisfactorio
				} else{
					lanzarToast("No se ha tomado foto.");
					finish();
				}
				break;
		}

	}

	// metodo de lanzar un toast
	public void lanzarToast (String texto ){
		Toast.makeText(getApplicationContext(),
				texto,
				Toast.LENGTH_LONG).show();
	}

	// metodo encargado de mostrar un cuadro de dialogo con opiones para escoger
	// sobre si desea tomar o no, mas imagenes
	private void mostrarDialogOpciones(){
		final CharSequence[] opciones={"Tomar otra foto","Salir"};
		final AlertDialog.Builder builder= new AlertDialog.Builder(this);
		builder.setTitle("Elige una Opción");
		builder.setItems(opciones, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				if (opciones[i].equals("Tomar otra foto")){

					// siempre y cuando sea posible
					if(datos.size() <= numeroMaximoImagenes){
						lanzarActividadFoto();
					}

				}else if (opciones[i].equals("Salir")){
					// cerrar la actividad
					dialogInterface.dismiss();
					finish();

				}
			}

		});
		builder.show();
	}

	@Override
	public void finish() {

		super.finish();
	}
	/*
	Metodo encargado de listar las imagenes y dar soporte para que puedan ser seleccionadas
	asi mismo listarlas d forma grafica para que el usuario pueda seleccionarlas
	 */
	private void inicializarImagenes() {
		// Consulta las imágenes que se encuentran disponibles en el dispositivo
		final String[] columnas = { MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID, MediaStore.Images.Media.DISPLAY_NAME};
		//final String ordenadoPor = MediaStore.Images.Media._ID;
		String sortOrder = String.format("%s limit 200 ", MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");

		Cursor cursorImagen = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
				columnas, null, null, sortOrder);

		// Se obtiene los datos del total de imagenes y se inicializan las variables
		int indiceColumnaIdImagen = cursorImagen.getColumnIndex(MediaStore.Images.Media._ID);
		this.numeroTotalImagenes = cursorImagen.getCount();
		this.imagenesMiniatura = new Bitmap[this.numeroTotalImagenes];
		this.arregloDirecciones = new String[this.numeroTotalImagenes];
		this.arregloNombres = new String[this.numeroTotalImagenes];
		this.arregloImagenesSeleccionadas = new boolean[this.numeroTotalImagenes];

		// Recorre las imagenes consultadas para inicializar los arreglos de las miniaturas y su path
		for (int i = 0; i < this.numeroTotalImagenes; i++) {
			cursorImagen.moveToPosition(i);
			int id = cursorImagen.getInt(indiceColumnaIdImagen);
			int indiceColumnaRuta = cursorImagen.getColumnIndex(MediaStore.Images.Media.DATA);
			int indiceColumnaNombre = cursorImagen.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME);
			imagenesMiniatura[i] = MediaStore.Images.Thumbnails.getThumbnail(
					getApplicationContext().getContentResolver(), id,
					MediaStore.Images.Thumbnails.MICRO_KIND, null);
			arregloDirecciones[i] = cursorImagen.getString(indiceColumnaRuta);
			arregloNombres[i] = cursorImagen.getString(indiceColumnaNombre);
		}

		// Finaliza el cursor
		if(cursorImagen != null) cursorImagen.close();
	}
	// clase para el adaptador de imagenes y poder seleccionrlas una a una generando la grilla

	public class ImageAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public ImageAdapter() {
			mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public int getCount() {
			return numeroTotalImagenes;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = mInflater.inflate(R.layout.item_galeria, null);
				holder.imageview = (ImageView) convertView
						.findViewById(R.id.thumbImage);
				holder.checkbox = (CheckBox) convertView
						.findViewById(R.id.itemCheckBox);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.checkbox.setId(position);
			holder.imageview.setId(position);
			holder.checkbox.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					CheckBox cb = (CheckBox) v;
					int id = cb.getId();
					if (arregloImagenesSeleccionadas[id]) {
						cb.setChecked(false);
						arregloImagenesSeleccionadas[id] = false;
					} else {
						cb.setChecked(true);
						arregloImagenesSeleccionadas[id] = true;
					}
				}
			});
			holder.imageview.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					int id = v.getId();
					Intent intent = new Intent();
					intent.setAction(Intent.ACTION_VIEW);
					intent.setDataAndType(
							Uri.parse("file://" + arregloDirecciones[id]),
							"image/*");
					startActivity(intent);
				}
			});
			holder.imageview.setImageBitmap(imagenesMiniatura[position]);

			// TODO: Si el vector de datos tiene información, lo compara para preseleccionar las imágenes
			// holder.checkbox.setChecked(arregloImagenesSeleccionadas[position]);
			String[] datosImagen = {arregloDirecciones[position], arregloNombres[position]};
			if (datos.size() > 0 && imagenSeleccionada(datosImagen)){
				holder.checkbox.setChecked(true);
				arregloImagenesSeleccionadas[position] = true;
			} else {
				holder.checkbox.setChecked(false);
				arregloImagenesSeleccionadas[position] = false;
			}

			holder.id = position;
			return convertView;
		}
	}

	class ViewHolder {
		ImageView imageview;
		CheckBox checkbox;
		int id;
	}

	// Función para validar si la imagen estaba o no seleccionada
	private boolean imagenSeleccionada(String[] datosImagen) {
		int longitud = datos.size();
		for (int i = 0; i < longitud; i++) {
			String[] datosImagenSeleccionada = datos.get(i);
			if(datosImagenSeleccionada[0].equals(datosImagen[0]))
				return true;
		}
		return false;
	}
}
// cambios realizados con exito :D