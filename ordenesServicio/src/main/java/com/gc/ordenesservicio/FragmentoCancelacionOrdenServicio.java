package com.gc.ordenesservicio;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gc.coregestionclientes.librerias.ToolsApp;
 
public class FragmentoCancelacionOrdenServicio extends DialogFragment {
 
	// Parametros
	private Activity actividad;
	private Context contexto;
	private ToolsApp toolsApp = new ToolsApp();
	private ListenerCancelacionOrdenServicio listener;
	
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
		this.actividad = getActivity();
		this.contexto = (Context)getActivity();
		
    	LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_cancelacion_visita_programada, 
        		(ViewGroup) actividad.findViewById(R.id.contenedor_dialogCancelacionOrdenServicio));

	    // Muestra el dialog
		CharSequence titulo = "Cancelar Programación";
		final Dialog dialog = (Dialog) toolsApp.alertDialogView(contexto, titulo, view);
        
		// Setea el mensaje
		String mensaje = getArguments().getString("Mensaje");
		TextView etiqueta_mensaje = (TextView)view.findViewById(R.id.etiqueta_mensaje);
		etiqueta_mensaje.setText(mensaje);
		
		// Setea el max numero de caracteres para la justificación
		final EditText campo_justificacionCancelacion = (EditText)view.findViewById(R.id.campo_justificacionCancelacion);
		campo_justificacionCancelacion.setFilters(new InputFilter[] {new InputFilter.LengthFilter(200)});
		
		// Inicializa los botones
	    final Button boton_confirmarCancelacion = (Button) view.findViewById(R.id.boton_confirmarCancelacion);
	    boton_confirmarCancelacion.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Inhabilita el botón 
				boton_confirmarCancelacion.setEnabled(false);

				// Valida que tenga algun dato
				String justificacion = campo_justificacionCancelacion.getText().toString();
				
				// Ejecuta el listener
			    if (justificacion.length() > 0){
			    	listener.eventoCancelacionOrdenServicio(campo_justificacionCancelacion.getText().toString());
					dialog.dismiss();
			    } else {
					boton_confirmarCancelacion.setEnabled(true);
			    	toolsApp.showLongMessage("Debe justificar la cancelación!", contexto);
			    }
			}
		});
	    
	    final Button boton_cancelarOperacion = (Button) view.findViewById(R.id.boton_cancelarOperacion);
	    boton_cancelarOperacion.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Inhabilita el botón 
				boton_cancelarOperacion.setEnabled(false);
				dialog.dismiss();
			}
		});
	    
	    // Retorno
		return dialog;
    }
	
	public interface ListenerCancelacionOrdenServicio {
        void eventoCancelacionOrdenServicio(String justificacion);
    }
	
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.listener = (ListenerCancelacionOrdenServicio)activity;
        }
        catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " debe implementar ListenerCancelacionVisitaProgramada");
        }
    }
}